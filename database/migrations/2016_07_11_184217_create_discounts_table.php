<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->length(255);
            $table->tinyInteger('category')->length(1); // 0 - cleaners, 1 - workers
            $table->tinyInteger('type')->length(1); // 0 - mount, 1 - percent
            $table->decimal('price', 20);
            $table->tinyInteger('status')->length(1);
            $table->integer('finish');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discounts');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_workers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discount_id');
            $table->integer('points');
            $table->integer('worker_id');
            $table->integer('client_id');
            $table->integer('profession_id');
            $table->text('text');
            $table->text('requirements');
            $table->string('schedule');
            $table->string('wage');
            $table->text('address');
            $table->decimal('total',20,2);
            $table->tinyInteger('payment_type')->length(1);
            $table->tinyInteger('payment_status')->length(1);
            $table->tinyInteger('status')->length(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders_workers');
    }
}

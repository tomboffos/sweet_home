<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCleanersScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cleaners_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cleaner_id');
            $table->tinyInteger('mon')->length(1);
            $table->tinyInteger('tue')->length(1);
            $table->tinyInteger('wed')->length(1);
            $table->tinyInteger('thu')->length(1);
            $table->tinyInteger('fri')->length(1);
            $table->tinyInteger('sat')->length(1);
            $table->tinyInteger('sun')->length(1);
            $table->time('t_from');
            $table->time('t_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cleaners_schedule');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('card_id');
            $table->integer('order_id');
            $table->string('amount');
            $table->string('action_time');
            $table->string('message');
            $table->string('reference');
            $table->string('int_reference');
            $table->string('approval_code');
            $table->string('bank_ses');
            $table->string('result');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cards_payments');
    }
}

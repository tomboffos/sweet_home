<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCleanersFavoriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cleaners_favorite', function (Blueprint $table) {
            $table->integer('client_id')->unsigned();
            $table->integer('cleaner_id')->unsigned();

            $table->foreign('client_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cleaner_id')->references('id')->on('cleaners')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['client_id', 'cleaner_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cleaners_favorite');
    }
}

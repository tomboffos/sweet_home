<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersCleanersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_cleaners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discount_id');
            $table->integer('points');
            $table->integer('cleaner_id');
            $table->integer('client_id');
            $table->integer('additional');
            $table->text('services');
            $table->string('type');
            $table->integer('number_rooms');
            $table->integer('number_bathrooms');
            $table->string('cicle');
            $table->date('cl_date');
            $table->time('cl_time');
            $table->decimal('total',20,2);
            $table->tinyInteger('payment_type')->length(1);
            $table->tinyInteger('payment_status')->length(1);
            $table->tinyInteger('status')->length(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders_cleaners');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category')->default(0);
            $table->char('title')->length(255);
            $table->char('slug')->length(255);
            $table->char('meta_title')->length(255);
            $table->text('meta_description')->length(500);
            $table->text('text');
            $table->integer('sort');
            $table->tinyInteger('status')->length(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('table');
    }
}

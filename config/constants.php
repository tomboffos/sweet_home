<?php

return [

	// sidebar main menu
	'sidebar' => [
		'Главная' => ['fa-dashboard', 'manager',
			'child' => [
				'manager/edit' => 'Редактировать',
			],
		],
		'Заказы' => [
				'fa-shopping-cart',
				'#',
				'child' => [
					'manager/orders/cleaners' => 'Уборка',
					'manager/orders/workers' => 'Подбор персонала',
				],
		],
		'Клинеры' => ['fa-user', 'manager/cleaners'],
		'Персонал' => ['fa-suitcase', '#', 'child' => [
				'manager/workers/cats' => 'Специальности',
				'manager/workers' => 'Сотрудники',
			],
		],
		'Страницы' => ['fa-files-o', '#', 'child' => [
				'manager/pages' => 'Все страницы',
				'manager/pages/new' => 'Добавить новую',
			],
		],
		'Записи' => ['fa-files-o', '#', 'child' => [
				'manager/posts' => 'Все записи',
				'manager/posts/new' => 'Добавить новую',
			],
		],
		'Слайдер' => ['fa-picture-o', '#', 'child' => [
				'manager/slider' => 'Все слайды',
				'manager/slider/new' => 'Добавить слайд',
			],
		],
		'Промокоды' => ['fa-money', 'manager/discounts'],
		'Отзывы' => ['fa-comments', 'manager/testimonals'],
		'Вопросы и ответы' => ['fa-question-circle', '#', 'child' => [
				'manager/faq/cats' => 'Категории',
				'manager/faq' => 'Вопросы',
			],
		],
		'Калькулятор' => ['fa-table', '#', 'child' => [
				'manager/calc' => 'Калькулятор Обычной убоки',
				'manager/calc/general' => 'Калькулятор Генеральной уборки',
			],
		],
		'Пользователи' => ['fa-users', '#', 'child' => [
				'manager/users/clients' => 'Клиенты',
				'manager/users/managers' => 'Менеджеры',
			],
		],
		'Транзакции' => ['fa-usd', 'manager/transactions'],
		'Настройки' => ['fa-wrench', 'manager/settings'],
	],

	// categories for discounts
	'discounts_categories' => [
		'0' => 'Уборка',
		'1' => 'Подбор персонала',
	],

	// types for discounts
	'discounts_types' => [
		'0' => 'Проценты',
		'1' => 'Сумма',
	],

	// services pay methods for calculator
	'services_pay' => [
		'0' => 'Цена',
		'1' => 'Цена за ед.',
		'2' => 'Цена за час',
	],

	// services pay methods for calculator
	'services_icons' => [
		'plate' => 'Посуда',
		'iron' => 'Утюг',
		'window' => 'Окна',
		'fridge' => 'Холодильник',
		'stove' => 'Плита',
		'par' => 'Парогенератор',
		'ozon' => 'Озонатор',
	],

	// pay methods
	'payment_methods' => [
		'0' => 'Оплата наличными',
		'1' => 'Оплата Казкоммерцбанк',
	],

	// order payment statuses
	'payment_status' => [
		'0' => 'Ожидание',
		'1' => 'Оплачен',
	],

	// order statuses
	'order_status' => [
		'0' => 'Ожидание',
		'1' => 'Обрабатывается',
		'2' => 'Выполнен',
		'3' => 'Отменен',
	],

	// additionas service form types
	'form_types' => [
		'0' => 'Няня',
		'1' => 'Сиделка',
		'2' => 'Повар',
		'3' => 'Домработница',
	],
];

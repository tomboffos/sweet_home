<?php 

return [
	
	// Alert! Sorting is important! 0 => cash, 1 => kazkom, ... don't change sorting.
	'paymethods' => ['cash', 'kazkom'],

	'cash' => [
	],
		
	'kazkom' => [
		'controller' => 'KazkomController',
		'action' => 'https://epay.kkb.kz/jsp/process/logon.jsp',
		'action_add_card' => 'https://epay.kkb.kz/jsp/hbpay/logon.jsp',
		'action_control' => 'https://epay.kkb.kz/jsp/hbpay/control.jsp',
		'action_trans' => 'https://epay.kkb.kz/jsp/hbpay/trans.jsp',
		'BackLink' => 'http://homesweet.kz/payment/kazkom/success',
		'PostLink' => 'http://homesweet.kz/payment/kazkom/service',
		'FailureBackLink' => 'http://homesweet.kz/payment/kazkom/error',
		'Language' => 'rus',
		'kazkom_pay_password' => 'WDfUveEf9i3',
		'kazkom_pay_merchant_cert_id' => 'c183dc90',
		'kazkom_pay_merchant_name' => 'Sweet Home',
		'kazkom_pay_currency' => '398',
		'kazkom_pay_merchant_id' => '98130831',
		'kazkom_pay_merchant_id_debit' => '98130832',
	],
];
var gulp = require("gulp"),
    notify = require("gulp-notify"),
    autoprefixer = require("gulp-autoprefixer"),
    sass = require("gulp-sass"),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rigger = require('gulp-rigger'),
    rename = require('gulp-rename');

gulp.task("sass", function() {
    return gulp.src("resources/assets/sass/main.scss")
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 10 versions', 'ie 9'],
            cascade: false
        }))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest("public/assets/front/css"))
        .pipe(notify({
            title: "CSS compiled",
            sound: true
        }));
});

gulp.task('js', function(cb) {
    return gulp.src('resources/assets/js/main.js')
        .pipe(sourcemaps.init())
        .pipe(rigger())
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('public/assets/front/js'))
        .pipe(notify({
            title: "JS compiled",
            sound: true
        }));
});

gulp.task('css_libs', function() {
    return gulp.src([
            'resources/assets/libs/css/bootstrap.min.css',
            'resources/assets/libs/css/owl.carousel.css',
            'resources/assets/libs/css/ion.checkRadio.min.css',
            'resources/assets/libs/css/jquery.datetimepicker.min.css',
        ])
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 10 versions', 'ie 9'],
            cascade: false
        }))
        .pipe(concat('libs.min.css'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('public/assets/front/css'))
        .pipe(notify({
            title: "CSS Libs compiled",
            sound: true
        }));
});

gulp.task('js_libs', function() {
    return gulp.src([
            'resources/assets/libs/js/jquery.min.js',
            'resources/assets/libs/js/bootstrap.min.js',
            'resources/assets/libs/js/owl.carousel.min.js',
            'resources/assets/libs/js/phoneinput.js',
            'resources/assets/libs/js/ion.checkRadio.min.js',
            'resources/assets/libs/js/jquery.datetimepicker.full.min.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(rigger())
        .pipe(uglify())
        .pipe(concat('libs.min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('public/assets/front/js'))
        .pipe(notify({
            title: "JS Libs compiled",
            sound: true
        }));
});

gulp.task('libs', ['js_libs', 'css_libs']);

gulp.task("default", function() {
    gulp.watch("resources/assets/sass/**/*.scss", ['sass']);
    gulp.watch("resources/assets/js/*.js", ['js']);
});
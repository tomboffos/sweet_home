
var SIGN_ALGORITHM = "XLS1-HMAC-SHA256 ";

//var e = document.getElementById("ecommerceBaseURL");
//var mid = document.getElementById("mid").value;
//var mid = document.getElementById("Language").val;
//alert(mid);

var _baseURL = "";
var _baseURI = "";
var _redeemableItemsURL = "";
var _trxSummaryURL = "";

var _frameLoadError=true;
var _LOYALTY_ERROR_TIMEOUT = 30000;  //Willing to wait 30 seconds for Loyalty Frame response
var _frameHeight = 100; //in pixels
initializeValues();



function initializeValues() {
    updateBaseURL();
    document.getElementById("initialTrxId").value = Math.floor((Math.random() * 999999) + 100000);
    var currDate=new Date();
    document.getElementById("trxTime").value = currDate.getFullYear()+leadZeros((currDate.getMonth()+1))+leadZeros(currDate.getDate())+leadZeros(currDate.getHours())+leadZeros(currDate.getMinutes())+leadZeros(currDate.getSeconds()); /* YYYYMMDDHHMISS */
    updateMessage('GET', 'application/html', document.getElementById("trxTime").value);
    updateSignature();

}

function updateBaseURL(){
    var mid = document.getElementById("mid").value;
    var tid = document.getElementById("tid").value;
    var pan = document.getElementById("pan").value;

    var origPA = document.getElementById("origPA").value;

    var e = document.getElementById("ecommerceBaseURL");
    _baseURL = e.options[e.selectedIndex].value;

    var merchantCssUrl = encodeURIComponent(_baseURL + document.getElementById("merchantCssUrl").value);
    var queryString = "?CSSUrl="+merchantCssUrl+"&OriginalPA="+origPA;

    _redeemableItemsURL = _baseURL + "/xls-rest-api/v1/Payment/Merchant/"+mid+"/Device/"+tid+"/RedeemableItems/Customer/";
    _redeemableItemsURL = _redeemableItemsURL + pan;
    _baseURI = _redeemableItemsURL;
    _redeemableItemsURL = _redeemableItemsURL + queryString;
}

function updateMessageAndSignature(){
    updateBaseURL();
    updateMessage('GET', 'application/html', document.getElementById("trxTime").value);
    updateSignature();
}

function updateMessageAndSignature2(){
    var paymentFail = document.getElementById("paymentFail").checked;
    if(paymentFail) {
        var mid = document.getElementById("mid").value;
        var tid = document.getElementById("tid").value;
        _baseURI = _baseURL+"/xls-rest-api/v1/Payment/Merchant/"+mid+"/Device/"+tid+"/Transaction/Sale";
        updateMessage('POST', 'application/json', document.getElementById("trxTime").value);
        updateSignature();
    } else {
        updateMessageAndSignature();
    }
}

function leadZeros(number) {
    return ("0"+number).slice(-2);
}

function doSubmitNext() {
    updateBaseURL();
    doSubmitGetAjax(_redeemableItemsURL);
}

function GetRedeemableItems() {
    initializeValues();
    doSubmitGetAjax(_redeemableItemsURL);
}

/*
 function doSubmit(action){
 var origPA = document.getElementById("origPA").value;
 var lanCode = document.getElementById("languageCode").value;
 var merchantCssUrl = document.getElementById("merchantCssUrl").value;

 var parentForm=document.getElementById("parentForm2");
 document.getElementById("cssurl").value = merchantCssUrl;
 document.getElementById("originalpa").value = origPA;
 document.getElementById("langCode").value = lanCode;
 parentForm.action = action;
 parentForm.submit();
 }
 */

function doSaleInit(){
    var mid = document.getElementById("mid").value;
    var tid = document.getElementById("tid").value;
    var rdItemsStatus = document.getElementById("redeemableItemsStatus").value;
    var payButton = document.getElementById("payButton").value;

    if (_frameLoadError) {
        doPayment('noResponse', null);
    } else {
        if (payButton === 'Pay') {
            if (rdItemsStatus === '0') { //if no error by redeemableItems request
                sendSaleInfoToChild(); //to perform saleTrx, postMessage to loyalty frame, then saleTrx is done by loyalty frame
            } else { //if any error by redeemableItems request, then no need to perform saleTrx, just perform payment
                doPayment('redeemableItemsError', null);
            }
        } else if (payButton === 'Continue') {
            doPayment('saleKO', null);
        }
    }
}

/*function doSaleTrx(message){
 var entityId=message.entityId;
 var postableAmount=message.postableAmount;
 var mid=document.getElementById("mid").value;
 var tid=document.getElementById("tid").value;
 var pan=document.getElementById("pan").value;
 var trxTime=document.getElementById("trxTime").value;
 var origPA=document.getElementById("origPA").value;
 var initialTrxId=document.getElementById("initialTrxId").value;
 var cssUrl = document.getElementById("cssUrl").value;

 var formData = new FormData();
 formData.append("mid",mid);
 formData.append("tid",tid);
 formData.append("pan",pan);
 formData.append("trxTime",trxTime);
 formData.append("origPA",origPA);
 formData.append("initialTrxId",initialTrxId);
 formData.append("entityId",entityId);
 formData.append("postableAmount",postableAmount);

 var xmlhttp = new XMLHttpRequest();

 xmlhttp.open("POST", "http://localhost:8000/xls-rest-api/v1/payment/merchant/"+mid+"/device/"+tid+"/transaction/saleTrx", true);
 xmlhttp.onerror = function() {
 alert("Error occurred while sale transaction [server down or page not found or system error].");
 doPayment(null);
 };

 xmlhttp.onload = function() {
 var message = JSON.parse(xmlhttp.response);

 //When sale is success/fail, call payment transaction of PSP
 var saleSuccess = true;
 var paymentSuccess = true;
 if(message.adjPurAmt){
 alert("Loyalty saleTrx successful! Message received: "+JSON.stringify(message));
 paymentSuccess = doPayment(message.adjPurAmt);
 } else {
 alert("Loyalty saleTrx failed! Message received: "+JSON.stringify(message));
 saleSuccess = false;
 paymentSuccess = doPayment(message.adjPurAmt);
 }

 var loyaltyTxnRef = message.loyaltyTxnRef;

 //1. When payment transaction of PSP is success & sale is success
 //call transaction summary of loyalty
 if (saleSuccess == true && paymentSuccess == true) {
 _trxSummaryURL = _baseURL + "/xls-rest-api/v1/payment/merchant/"+mid+"/device/"+tid+"/transaction/"+loyaltyTxnRef+"/trxSummary";
 _trxSummaryURL = _trxSummaryURL;
 doSubmit(_trxSummaryURL);
 }

 //2. When payment transaction of PSP is success & sale is fail
 //nothing
 if (saleSuccess == false && paymentSuccess == true) {
 alert("Payment is successful!. Sale is failed!.");
 }

 //3. When payment transaction of PSP is fail & sale is success
 //call cancel transaction of loyalty
 if (saleSuccess == true && paymentSuccess == false) {


 doCancelTrx(initialTrxId,loyaltyTxnRef);
 }

 //4. When payment transaction of PSP is fail & sale is fail
 //show error page in child frame
 if (saleSuccess == false && paymentSuccess == false) {
 alert("Payment is failed!. Sale is failed!.");
 }

 };

 xmlhttp.setRequestHeader("Accept", "application/json");
 xmlhttp.send(formData);
 }*/

function doCancelTrx(initialTrxId,loyaltyTxnRef){
    //pass xls trx id or InitTrxId to cancel transaction
    var trxId = initialTrxId;
    if (loyaltyTxnRef != null) {
        trxId = loyaltyTxnRef;
    }
    var mid=document.getElementById("mid").value;
    var tid=document.getElementById("tid").value;
    var trxTime=document.getElementById("trxTime").value;
    var signature=document.getElementById("signature").value;
    var lanCode = document.getElementById("languageCode").value;

    var data = {};
    data.status = 'CANCEL';
    data.initialTrxId = initialTrxId;
    data.xlsTrxId = loyaltyTxnRef;
    data.trxTime = trxTime;
    data.reasonCode = 2;

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.open("POST", _baseURL+"/xls-rest-api/v1/Payment/Merchant/"+mid+"/Device/"+tid+"/Transaction/Sale", true);
    xmlhttp.onerror = function() {
        alert("Error occurred while cancel transaction [server down or page not found or system error].");
    };

    xmlhttp.onload = function() {
        var message = JSON.parse(xmlhttp.response);

        alert("Transaction "+message.xlsTxnId+" was cancelled.  Response is : "+xmlhttp.response+" result flag is "+message.statusCode);
    };

    xmlhttp.setRequestHeader("Accept-Language", lanCode);
    xmlhttp.setRequestHeader("Accept", "application/json");
    xmlhttp.setRequestHeader("content-type", "application/json");
    xmlhttp.setRequestHeader("x-request-date-time", trxTime);
    xmlhttp.setRequestHeader("Authorization", SIGN_ALGORITHM+signature);
    xmlhttp.send(JSON.stringify(data));
}

function doSubmitGetAjax(url){

    var signature=document.getElementById("signature").value;
    var trxTime=document.getElementById("trxTime").value;
    var lanCode = document.getElementById("languageCode").value;

    var xmlhttp = new XMLHttpRequest();


    xmlhttp.open("GET", url, true);

    xmlhttp.onerror = function() {
        alert("Error occurred while redeemableItems or trx summary [server down or page not found or system error].");
    };

    xmlhttp.onload = function() {
        var content=xmlhttp.responseText;

        var iframe = document.getElementById("ifrmChild");

        iframe.srcdoc = xmlhttp.responseText;
        iframe.src = "data:text/html;charset=utf-8," + escape(xmlhttp.responseText);
    };

    xmlhttp.setRequestHeader("Accept-Language", lanCode);
    xmlhttp.setRequestHeader("Accept", "application/html");
    xmlhttp.setRequestHeader("content-type", "application/html");
    xmlhttp.setRequestHeader("x-request-date-time", trxTime);
    xmlhttp.setRequestHeader("Authorization", SIGN_ALGORITHM+signature);
    xmlhttp.send();

}

function doPayment(loyaltyStatus, adjPurAmt) {

    if (loyaltyStatus === 'noResponse') {
        alert("No Response by Loyalty, proceed payment");
    } else if (loyaltyStatus === 'redeemableItemsError') {
        alert("RedeemableItems request is failed with error, proceed payment");
    } else if (loyaltyStatus === 'saleOK') {
        alert("SaleTrx is successful, proceed payment, Adjusted Purchase Amount : "+adjPurAmt);
    } else if (loyaltyStatus === 'saleKO') {
        alert("SaleTrx is failed, proceed payment, Adjusted Purchase Amount : "+adjPurAmt);
    }

    var flag = true;
    var paymentFail = document.getElementById("paymentFail").checked;

    if(paymentFail) {
        flag = false;
    }

    if (flag) alert("Payment is successful");
    else alert("Payment is failed");

    return flag;
}

function generateMessage(method, ctype, datetime){

    //var httpMethod = "GET";
    //var contentType = "application/html";
    var uri = _baseURI;
    //var trxTime = document.getElementById("trxTime").value;
    var myString=method+"#"+uri+"#"+ctype+"#"+datetime;

    return myString;
}

function updateMessage(method, ctype, datetime){
    document.getElementById("message").value=generateMessage(method, ctype, datetime);
}

function getSecretKey(){
    return document.getElementById("secretKey").value;
}

function generateSignature(message){
    var shaObj = new jsSHA(message, "TEXT");
    var secret = getSecretKey();
    var hmac = shaObj.getHMAC(secret, "HEX", "SHA-256", "B64");
    return hmac;
}

function updateSignature() {
    var message = document.getElementById("message").value;
    document.getElementById("signature").value=generateSignature(message);
}

// FF3+, IE8+, Chrome, Safari(5?), Opera10+
function enableButton(){
    document.getElementById("buttonDiv").style.display = "block";
}

function checkFrameError(timeout,origin){
    var timeOutId=setTimeout(function(){
        if(_frameLoadError){
            enableButton();
            alert("Can't load the frame."+origin);
        }
    },timeout);
    return timeOutId;
};

function adjustFrame(frameHeight){
    if(frameHeight!=null){
        _frameHeight=frameHeight;
    } else {
        requestForFrameHeight();
    }
    document.getElementById('ifrmChild').height=_frameHeight+"px";
}

function ReceiveMessage(evt) {
    // This is where response from child frame will come.
    _frameLoadError=false;
    //Add security here
    document.getElementById("childFrameMessage").value=evt.data;
    enableButton();
    try{

        var message = JSON.parse(evt.data);

        if(message.contentHeight) {
            adjustFrame(message.contentHeight);
        }

        if(message.action == "sentSaleTxnResponse") {
            doCancelOrSummary(message);
        }

        if(message.action == "sentSaleDataValidationResponse" && message.statusCode === 2) {
            document.getElementById("payButton").disabled = true;
        }

        if(message.action == "displayedRedeemableItems") {
            document.getElementById("payButton").disabled = false;
            document.getElementById("redeemableItemsStatus").value = message.statusCode;
        }

        if(message.action == "displayedloyaltyTxnSummary") {
            document.getElementById("payButton").value = 'OK';
        }

    } catch (error) {
        alert(error.stack);
    }
} // End Function ReceiveMessage


function doCancelOrSummary(message){

    var xmlhttp = new XMLHttpRequest();

    //When sale is success/fail, call payment transaction of PSP
    var saleSuccess = true;
    var paymentSuccess = true;
    if(message.statusCode === 0){
        alert("Loyalty saleTrx successful! Message received: "+JSON.stringify(message));
        paymentSuccess = doPayment('saleOK', message.adjustedPA);
    } else {
        adjustFrame(message.contentHeight);
        alert("Loyalty saleTrx failed! Message received: "+JSON.stringify(message));
        saleSuccess = false;

        document.getElementById("payButton").value = "Continue";
    }
    var mid = document.getElementById("mid").value;
    var tid = document.getElementById("tid").value;
    var initialTrxId = document.getElementById("initialTrxId").value;
    var xlsTxnId = message.xlsTxnId;

    //When payment transaction of PSP is success & sale is success
    //call transaction summary of loyalty
    if (saleSuccess == true && paymentSuccess == true) {
        var merchantCssUrl = encodeURIComponent(_baseURL + document.getElementById("merchantCssUrl").value);
        var queryString = "?CSSUrl="+merchantCssUrl;

        _trxSummaryURL = _baseURL + "/xls-rest-api/v1/Payment/Merchant/"+mid+"/Device/"+tid+"/Transaction/"+xlsTxnId;
        _baseURI = _trxSummaryURL;
        updateMessage('GET', 'application/html', document.getElementById("trxTime").value);
        updateSignature();
        _trxSummaryURL = _trxSummaryURL + queryString;
        doSubmitGetAjax(_trxSummaryURL);
    }

    //When payment transaction of PSP is fail & sale is success
    //call cancel transaction of loyalty
    if (saleSuccess == true && paymentSuccess == false) {
        doCancelTrx(initialTrxId,xlsTxnId);
    }

}

function requestForFrameHeight() {
    var win = document.getElementById("ifrmChild").contentWindow;

    // Specify origin. Should be a domain or a wildcard "*"
    if (win == null || !window['postMessage'])
        alert("oh crap");
    else
    {
        win.postMessage("{\"sendContentHeight\":true}", "*");
    }
}

function sendSaleInfoToChild() {

    var win = document.getElementById("ifrmChild").contentWindow;
    var mid=document.getElementById("mid").value;
    var tid=document.getElementById("tid").value;
    var pan=document.getElementById("pan").value;
    var trxTime=document.getElementById("trxTime").value;
    var origPA=document.getElementById("origPA").value;
    var initialTrxId=document.getElementById("initialTrxId").value;
    var merchantCssUrl = _baseURL + document.getElementById("merchantCssUrl").value;
    var languageCode = document.getElementById("languageCode").value;
    // Specify origin. Should be a domain or a wildcard "*"
    if (win == null || !window['postMessage'])
        alert("oh crap");
    else {
        //win.postMessage("{\"startSale\":true}", "*");
        win.postMessage(JSON.stringify({"action":"doSaleTxn","languageCode":languageCode,"mid":mid,"tid":tid,"pan":pan,"trxTime":trxTime,"originalPA":origPA,"initialTrxId":initialTrxId,"cssUrl":merchantCssUrl, "baseUrl":_baseURL}), "*");
    }
}

if (!window['postMessage'])
    alert("oh crap");
else {
    if (window.addEventListener) {
        // For standards-compliant web browsers (ie9+)
        window.addEventListener("message", ReceiveMessage, false);
    }
    else {
        window.attachEvent("onmessage", ReceiveMessage);
    }
}

checkFrameError(_LOYALTY_ERROR_TIMEOUT," during initial loading"); //Enable button if no message received from the frame for 5 seconds
//doSubmit(_redeemableItemsURL);
doSubmitGetAjax(_redeemableItemsURL);


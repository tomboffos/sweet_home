//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

    // ckeditor init
    if ($("#text").length) {
        CKEDITOR.replace('text', {
            filebrowserImageBrowseUrl: '/manager/laravel-filemanager?type=Images',
            filebrowserBrowseUrl: '/manager/laravel-filemanager?type=Files'
        });
    }

    // ckeditor basic toolbar init
    if ($("#simple_text").length) {
        CKEDITOR.replace('simple_text', {
            toolbar: 'Basic',
        });
    }

    // ckeditor init
    if ($(".simple_text").length) {
        $("textarea.simple_text").each(function() {
            CKEDITOR.replace($(this).attr("id"), {
                toolbar: 'Basic',
            });
        });
    }

    // ckeditor init
    if ($(".js_ckeditor").length) {
        $(".js_ckeditor").each(function() {
            CKEDITOR.replace($(this).attr('id'), {
                filebrowserImageBrowseUrl: '/manager/laravel-filemanager?type=Images',
                filebrowserBrowseUrl: '/manager/laravel-filemanager?type=Files'
            });
        });
    }

    // sidebar menu init
    $('#side-menu').metisMenu();

    // phone input mask
    $("input[name='phone']").mask("+7 (999) 999-99-99");

    $.datetimepicker.setLocale('ru');

    // choose time when cleaners add or edit
    $('.schedule-date input[type="text"]').datetimepicker({
        datepicker: false,
        format: 'H:i'
    });

    // choose date when discounts add or edit
    $('.discount-date input[type="text"]').datetimepicker({
        format: 'Y-m-d',
        timepicker: false,
        startDate: '+1971/05/01',
        minDate: '-1970/01/01',
    });

    // choose data and time when order edit
    $('#orderCleaner input[name="clean_date"]').datetimepicker({
        timepicker: false,
        startDate: '+1971/05/01',
        minDate: '-1970/01/01',
        format: 'd-m-Y'
    });
    $('#orderCleaner input[name="clean_time"]').datetimepicker({
        datepicker: false,
        format: 'H:i'
    });

    // highlight choosen cleaner in order page
    if ($(".orders-list-cleaner").length && $("input[name='cleaner']").val()) {
        $("#cleaner-id-" + $("input[name='cleaner']").val()).css('background-color', '#BEF3BE');
    }
});

// off/on status btn
$("body").on("click", "#status", function() {
    var current = $(this).attr("class");

    if (current === 'btn btn-default') {
        $(this).attr("class", "btn btn-success");
        $(this).text('Включено');
        $("input[name='status']").prop("value", 1);
    } else {
        $(this).attr("class", "btn btn-default");
        $(this).text('Выключено');
        $("input[name='status']").prop("value", 0);
    }
});

// off/on menu btn
$("body").on("click", "#menu", function() {
    var current = $(this).attr("class");

    if (current === 'btn btn-default') {
        $(this).attr("class", "btn btn-success");
        $(this).text('Показывается в меню');
        $("input[name='menu']").prop("value", 1);
    } else {
        $(this).attr("class", "btn btn-default");
        $(this).text('Не показывается в меню');
        $("input[name='menu']").prop("value", 0);
    }
});

// confirm when delete some item
$("body").on("click", ".delete", function() {
    return confirm('Вы уверены?');
});

// delete thumbnail
$("#delete_thumb").click(function() {
    if (confirm('Вы уверены?')) {
        var current = $("#delete_thumb").attr("class");
        if (current === 'btn btn-default') {
            $(this).attr("class", "btn btn-warning");
            $(this).text('Будет удалено');
            $("input[name='delete_thumb']").prop("value", 1);
        } else {
            $(this).attr("class", "btn btn-default");
            $(this).text('Удалить изображение?');
            $("input[name='delete_thumb']").prop("value", 0);
        }
    }
});

// add field in Clean Type
$("body").on("click", "#addFieldType", function() {

    var html = '<tr class="added"><td><select name="clean_type_label[]" class="form-control"><option value="standart">Калькулятор обычной уборки</option><option value="general">Калькулятор генеральной уборки</option></select></td><td><input type="text" class="form-control" name="clean_type_name[]" placeholder="Название типа уборки"></td><td><input type="text" class="form-control" name="clean_type_k[]" placeholder="Коэфициент"></td><td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td></tr>';

    $(html).appendTo("#cleanType table tbody");
});

// add field in Clean Type General Calculator
$("body").on("click", "#addFieldTypeGeneral", function() {

    var html = '<tr class="added"><td><select name="clean_general_type_label[]" class="form-control"><option value="standart">Калькулятор обычной уборки</option><option value="general">Калькулятор генеральной уборки</option></select></td><td><input type="text" class="form-control" name="clean_general_type_name[]" placeholder="Название типа уборки"></td><td><input type="text" class="form-control" name="clean_general_type_k[]" placeholder="Коэфициент"></td><td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td></tr>';

    $(html).appendTo("#cleanType table tbody");
});

// delete field in Calculator
$("body").on("click", ".delete-clean-row", function() {
    if (confirm('Вы уверены?')) {
        $(this).parents(".added").remove();

        // if it's order page, recalculate all input and select data
        if ($("#orderCleaner").length) {
            calculateTotal();
        }
    }
});

// add field in Clean Service - Calculator
$("body").on("click", "#addFieldService", function() {

    var html = '<tr class="added"><td><input type="text" class="form-control" name="clean_services_name[]" placeholder="Название услуги"></td><td><input type="text" class="form-control" name="clean_services_price[]" placeholder="Цена"></td><td><select name="clean_services_pay[]" class="form-control"><option value="0">Цена</option><option value="1">Цена за ед.</option><option value="2">Цена за час</option></select></td><td><select name="clean_services_icon[]" class="form-control"><option value="plate">Посуда</option><option value="iron">Утюг</option><option value="window">Окна</option><option value="fridge">Холодильник</option><option value="stove">Плита</option></select></td><td><input type="text" class="form-control" name="clean_services_time[]" placeholder="Время" value=""></td><td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td></tr>';

    $(html).appendTo("#cleanServices table tbody");
});

// add field in Clean Service - General Calculator
$("body").on("click", "#addFieldServiceGeneral", function() {

    var html = '<tr class="added"><td><input type="text" class="form-control" name="clean_general_services_name[]" placeholder="Название услуги"></td><td><input type="text" class="form-control" name="clean_general_services_price[]" placeholder="Цена"></td><td><select name="clean_general_services_pay[]" class="form-control"><option value="0">Цена</option><option value="1">Цена за ед.</option><option value="2">Цена за час</option></select></td><td><select name="clean_general_services_icon[]" class="form-control"><option value="plate">Посуда</option><option value="iron">Утюг</option><option value="window">Окна</option><option value="fridge">Холодильник</option><option value="stove">Плита</option></select></td><td><input type="text" class="form-control" name="clean_general_services_time[]" placeholder="Время" value=""></td><td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td></tr>';

    $(html).appendTo("#cleanServices table tbody");
});

// add field in Clean Service
$("body").on("click", "#addFieldCicle", function() {

    var html = '<tr class="added"><td><input type="text" class="form-control" name="clean_cicle_name[]" placeholder="Название"></td><td><input type="text" class="form-control" name="clean_cicle_price[]" placeholder="Коэфициент"></td><td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td></tr>';

    $(html).appendTo("#cleanCicle table tbody");
});

// add field in Clean Service General Calculator
$("body").on("click", "#addFieldCicleGeneral", function() {

    var html = '<tr class="added"><td><input type="text" class="form-control" name="clean_general_cicle_name[]" placeholder="Название"></td><td><input type="text" class="form-control" name="clean_general_cicle_price[]" placeholder="Коэфициент"></td><td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td></tr>';

    $(html).appendTo("#cleanCicle table tbody");
});

// choose cleaner
$("body").on("click", ".choose-cleaner", function() {

    var cleaner_id = +$(this).attr("data-cleaner");
    $(".cleaner-item").removeAttr('style');
    $(this).parents(".cleaner-item").css('background-color', '#BEF3BE');

    $("input[name='cleaner']").val(cleaner_id);
});

// add service in order
$("body").on("click", "#addFieldServiceOrder", function() {

    var html = '<tr class="added"><td><input type="text" class="form-control" name="clean_services_name[]" placeholder="Название услуги"></td><td style="width: 12%;"><input type="text" class="form-control" name="clean_services_price[]" placeholder="Цена"></td><td style="width: 10%;"><input type="text" class="form-control" name="clean_services_parts[]" placeholder="Количество"></td><td style="width: 10%;"><input title="0.5 - 30 минут, 1.5 - 1 час и 30 минут ..." type="text" class="form-control" name="clean_services_time[]" placeholder="Время"></td><td style="width: 12%;"><select name="clean_services_pay[]" class="form-control"><option value="0">Цена</option><option value="1">Цена за ед.</option><option value="2">Цена за час</option></select></td><td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td></tr>';

    $(html).appendTo("#cleanServicesOrder table tbody");
});

// onchange inputs in order
$("body").on("change", "#orderCleaner input, #orderCleaner select", function() {
    calculateTotal();
});

// calculate total
var calculateTotal = function() {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/manager/orders/cleaners/calculate",
        type: "POST",
        data: $("#orderCleaner").serialize(),
        success: function(data) {
            if (data.status === 200) {
                $(".total-order").val(data.data);
            } else {
                toastr.warning(data.data);
            }
        }
    });
};

$("body").on("click", "#copy-order", function() {

    if (!confirm("Будет создана копия данного заказа?")) {
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/manager/orders/cleaners/copyorder",
        type: "POST",
        data: $("#orderCleaner").serialize(),
        success: function(data) {
            if (data.status === 200) {
                toastr.success('Заказ №' + data.id + ' создан. Он будет отображен в списке заказов.');
            }
            if (data.status === 400) {
                toastr.warning('Произошла ошибка при добавлении нового заказа!');
            }
        }
    });
});
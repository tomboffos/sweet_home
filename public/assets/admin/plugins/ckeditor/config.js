/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.skin = 'icy_orange';
	config.toolbar_Basic = [
        {
        	name: 'font',
			items: ['Font','FontSize']
        },
        {
        	name: 'basicstyles',
			items: [
				'Bold', 'Italic', 'Underline','-','JustifyLeft',
				'JustifyCenter','JustifyRight','-','NumberedList',
				'BulletedList','-','Link','Unlink','-','Source'
			]
        },
        {
        	name: 'colors',
			items: ['TextColor', 'BGColor']
        },
  	];
    // config.extraPlugins = 'colorbutton';
    // config.extraPlugins = 'panelbutton';
};

function in_array(e, t, n) {
    var i,
        o = !1,
        n = !!n;
    for (i in t)
        if ((n && t[i] === e) || (!n && t[i] == e)) {
            o = !0;
            break;
        }
    return o;
}
function number_format(e, t, n, i) {
    e = (e + "").replace(/[^0-9+\-Ee.]/g, "");
    var o = isFinite(+e) ? +e : 0,
        r = isFinite(+t) ? Math.abs(t) : 0,
        a = void 0 === i ? "," : i,
        s = void 0 === n ? "." : n,
        l = "";
    return (
        (l = (r
                ? (function (e, t) {
                    var n = Math.pow(10, t);
                    return "" + Math.round(e * n) / n;
                })(o, r)
                : "" + Math.round(o)
        ).split(".")),
        l[0].length > 3 && (l[0] = l[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, a)),
        (l[1] || "").length < r && ((l[1] = l[1] || ""), (l[1] += new Array(r - l[1].length + 1).join("0"))),
            l.join(s)
    );
}
function modalAuth() {
    $.ajax({
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
        url: "/login/ajax",
        type: "POST",
        data: $("#modalAuthForm").serialize(),
        success: function (e) {
            (e && 400 !== e.status && !1 !== e.success) || app.notificate("error", "Неправильно заполнены поля e-mail и/или пароль"), 200 === e.status && !0 === e.success && location.reload();
        },
    });
}
function modalRegister() {
    $.ajax({
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
        url: "/register/ajax",
        type: "POST",
        data: $("#modalRegistrationForm").serialize(),
        success: function (e) {
            if (400 === e.status || !1 === e.success) {
                var t = "";
                if (e.errors.email && e.errors.email.length > 0) for (var n = 0; n < e.errors.email.length; n++) t += e.errors.email[n] + "<br />";
                if (e.errors.password && e.errors.password.length > 0) for (var i = 0; i < e.errors.password.length; i++) t += e.errors.password[i] + "<br />";
                app.notificate("error", t);
            }
            200 === e.status &&
            !0 === e.success &&
            (app.notificate("success", "Вы успешно зарегистрировались!<br />Сейчас мы обновим страницу, чтобы авторизовать вас!"),
                setTimeout(function () {
                    location.reload();
                }, 3e3));
        },
    });
}
function modalResetPassword() {
    $.ajax({
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
        url: "/password/reset/ajax",
        type: "POST",
        data: $("#modalForgotForm").serialize(),
        success: function (e) {
            400 === e.status && !1 === e.success && app.notificate("error", "Неправильно заполнены поля e-mail и/или пароль!"),
            400 === e.status && "none" === e.success && app.notificate("error", e.msg),
            200 === e.status &&
            !0 === e.success &&
            ($("#modalForgotForm").remove(), $("#modalForgotPass .modal-body").html("<p>На указанный email отправлено письмо с инструкциями для восстановлению пароля!</p>"), app.notificate("success", e.msg));
        },
    });
}
function modalCallMeForm() {
    if (!validate_form("modalCallMeForm", ["phone"])) return !1;
    $.ajax({
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
        url: "/form/handle",
        type: "POST",
        data: $("#modalCallMeForm").serialize(),
        success: function (e) {
            400 === e.status && app.notificate("error", e.msg),
            200 === e.status && ($("#modalCallMeForm").remove(), $("#modalCallMe .modal-body").html("<p>Ваша заявка отправлена! Мы свяжемся с вами в ближайшее время!</p>"), app.notificate("success", e.msg));
        },
    });
}
if (
    ((function (e, t) {
        "object" == typeof module && "object" == typeof module.exports
            ? (module.exports = e.document
            ? t(e, !0)
            : function (e) {
                if (!e.document) throw new Error("jQuery requires a window with a document");
                return t(e);
            })
            : t(e);
    })("undefined" != typeof window ? window : this, function (e, t) {
        function n(e) {
            var t = !!e && "length" in e && e.length,
                n = re.type(e);
            return "function" !== n && !re.isWindow(e) && ("array" === n || 0 === t || ("number" == typeof t && t > 0 && t - 1 in e));
        }
        function i(e, t, n) {
            if (re.isFunction(t))
                return re.grep(e, function (e, i) {
                    return !!t.call(e, i, e) !== n;
                });
            if (t.nodeType)
                return re.grep(e, function (e) {
                    return (e === t) !== n;
                });
            if ("string" == typeof t) {
                if (me.test(t)) return re.filter(t, e, n);
                t = re.filter(t, e);
            }
            return re.grep(e, function (e) {
                return Z.call(t, e) > -1 !== n;
            });
        }
        function o(e, t) {
            for (; (e = e[t]) && 1 !== e.nodeType; );
            return e;
        }
        function r(e) {
            var t = {};
            return (
                re.each(e.match(we) || [], function (e, n) {
                    t[n] = !0;
                }),
                    t
            );
        }
        function a() {
            V.removeEventListener("DOMContentLoaded", a), e.removeEventListener("load", a), re.ready();
        }
        function s() {
            this.expando = re.expando + s.uid++;
        }
        function l(e, t, n) {
            var i;
            if (void 0 === n && 1 === e.nodeType)
                if (((i = "data-" + t.replace(De, "-$&").toLowerCase()), "string" == typeof (n = e.getAttribute(i)))) {
                    try {
                        n = "true" === n || ("false" !== n && ("null" === n ? null : +n + "" === n ? +n : Ce.test(n) ? re.parseJSON(n) : n));
                    } catch (e) {}
                    Se.set(e, t, n);
                } else n = void 0;
            return n;
        }
        function u(e, t, n, i) {
            var o,
                r = 1,
                a = 20,
                s = i
                    ? function () {
                        return i.cur();
                    }
                    : function () {
                        return re.css(e, t, "");
                    },
                l = s(),
                u = (n && n[3]) || (re.cssNumber[t] ? "" : "px"),
                c = (re.cssNumber[t] || ("px" !== u && +l)) && Ae.exec(re.css(e, t));
            if (c && c[3] !== u) {
                (u = u || c[3]), (n = n || []), (c = +l || 1);
                do {
                    (r = r || ".5"), (c /= r), re.style(e, t, c + u);
                } while (r !== (r = s() / l) && 1 !== r && --a);
            }
            return n && ((c = +c || +l || 0), (o = n[1] ? c + (n[1] + 1) * n[2] : +n[2]), i && ((i.unit = u), (i.start = c), (i.end = o))), o;
        }
        function c(e, t) {
            var n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [];
            return void 0 === t || (t && re.nodeName(e, t)) ? re.merge([e], n) : n;
        }
        function d(e, t) {
            for (var n = 0, i = e.length; i > n; n++) $e.set(e[n], "globalEval", !t || $e.get(t[n], "globalEval"));
        }
        function p(e, t, n, i, o) {
            for (var r, a, s, l, u, p, h = t.createDocumentFragment(), f = [], m = 0, g = e.length; g > m; m++)
                if ((r = e[m]) || 0 === r)
                    if ("object" === re.type(r)) re.merge(f, r.nodeType ? [r] : r);
                    else if (je.test(r)) {
                        for (a = a || h.appendChild(t.createElement("div")), s = (_e.exec(r) || ["", ""])[1].toLowerCase(), l = Pe[s] || Pe._default, a.innerHTML = l[1] + re.htmlPrefilter(r) + l[2], p = l[0]; p--; ) a = a.lastChild;
                        re.merge(f, a.childNodes), (a = h.firstChild), (a.textContent = "");
                    } else f.push(t.createTextNode(r));
            for (h.textContent = "", m = 0; (r = f[m++]); )
                if (i && re.inArray(r, i) > -1) o && o.push(r);
                else if (((u = re.contains(r.ownerDocument, r)), (a = c(h.appendChild(r), "script")), u && d(a), n)) for (p = 0; (r = a[p++]); ) Ne.test(r.type || "") && n.push(r);
            return h;
        }
        function h() {
            return !0;
        }
        function f() {
            return !1;
        }
        function m() {
            try {
                return V.activeElement;
            } catch (e) {}
        }
        function g(e, t, n, i, o, r) {
            var a, s;
            if ("object" == typeof t) {
                "string" != typeof n && ((i = i || n), (n = void 0));
                for (s in t) g(e, s, n, i, t[s], r);
                return e;
            }
            if ((null == i && null == o ? ((o = n), (i = n = void 0)) : null == o && ("string" == typeof n ? ((o = i), (i = void 0)) : ((o = i), (i = n), (n = void 0))), !1 === o)) o = f;
            else if (!o) return e;
            return (
                1 === r &&
                ((a = o),
                    (o = function (e) {
                        return re().off(e), a.apply(this, arguments);
                    }),
                    (o.guid = a.guid || (a.guid = re.guid++))),
                    e.each(function () {
                        re.event.add(this, t, o, i, n);
                    })
            );
        }
        function v(e, t) {
            return re.nodeName(e, "table") && re.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e;
        }
        function y(e) {
            return (e.type = (null !== e.getAttribute("type")) + "/" + e.type), e;
        }
        function b(e) {
            var t = ze.exec(e.type);
            return t ? (e.type = t[1]) : e.removeAttribute("type"), e;
        }
        function w(e, t) {
            var n, i, o, r, a, s, l, u;
            if (1 === t.nodeType) {
                if ($e.hasData(e) && ((r = $e.access(e)), (a = $e.set(t, r)), (u = r.events))) {
                    delete a.handle, (a.events = {});
                    for (o in u) for (n = 0, i = u[o].length; i > n; n++) re.event.add(t, o, u[o][n]);
                }
                Se.hasData(e) && ((s = Se.access(e)), (l = re.extend({}, s)), Se.set(t, l));
            }
        }
        function x(e, t) {
            var n = t.nodeName.toLowerCase();
            "input" === n && Ee.test(e.type) ? (t.checked = e.checked) : ("input" !== n && "textarea" !== n) || (t.defaultValue = e.defaultValue);
        }
        function T(e, t, n, i) {
            t = K.apply([], t);
            var o,
                r,
                a,
                s,
                l,
                u,
                d = 0,
                h = e.length,
                f = h - 1,
                m = t[0],
                g = re.isFunction(m);
            if (g || (h > 1 && "string" == typeof m && !ie.checkClone && qe.test(m)))
                return e.each(function (o) {
                    var r = e.eq(o);
                    g && (t[0] = m.call(this, o, r.html())), T(r, t, n, i);
                });
            if (h && ((o = p(t, e[0].ownerDocument, !1, e, i)), (r = o.firstChild), 1 === o.childNodes.length && (o = r), r || i)) {
                for (a = re.map(c(o, "script"), y), s = a.length; h > d; d++) (l = o), d !== f && ((l = re.clone(l, !0, !0)), s && re.merge(a, c(l, "script"))), n.call(e[d], l, d);
                if (s)
                    for (u = a[a.length - 1].ownerDocument, re.map(a, b), d = 0; s > d; d++)
                        (l = a[d]), Ne.test(l.type || "") && !$e.access(l, "globalEval") && re.contains(u, l) && (l.src ? re._evalUrl && re._evalUrl(l.src) : re.globalEval(l.textContent.replace(Ye, "")));
            }
            return e;
        }
        function k(e, t, n) {
            for (var i, o = t ? re.filter(t, e) : e, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || re.cleanData(c(i)), i.parentNode && (n && re.contains(i.ownerDocument, i) && d(c(i, "script")), i.parentNode.removeChild(i));
            return e;
        }
        function $(e, t) {
            var n = re(t.createElement(e)).appendTo(t.body),
                i = re.css(n[0], "display");
            return n.detach(), i;
        }
        function S(e) {
            var t = V,
                n = Ue[e];
            return (
                n ||
                ((n = $(e, t)),
                ("none" !== n && n) || ((Be = (Be || re("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement)), (t = Be[0].contentDocument), t.write(), t.close(), (n = $(e, t)), Be.detach()),
                    (Ue[e] = n)),
                    n
            );
        }
        function C(e, t, n) {
            var i,
                o,
                r,
                a,
                s = e.style;
            return (
                (n = n || Ve(e)),
                    (a = n ? n.getPropertyValue(t) || n[t] : void 0),
                ("" !== a && void 0 !== a) || re.contains(e.ownerDocument, e) || (a = re.style(e, t)),
                n && !ie.pixelMarginRight() && Je.test(a) && Xe.test(t) && ((i = s.width), (o = s.minWidth), (r = s.maxWidth), (s.minWidth = s.maxWidth = s.width = a), (a = n.width), (s.width = i), (s.minWidth = o), (s.maxWidth = r)),
                    void 0 !== a ? a + "" : a
            );
        }
        function D(e, t) {
            return {
                get: function () {
                    return e() ? void delete this.get : (this.get = t).apply(this, arguments);
                },
            };
        }
        function O(e) {
            if (e in nt) return e;
            for (var t = e[0].toUpperCase() + e.slice(1), n = tt.length; n--; ) if ((e = tt[n] + t) in nt) return e;
        }
        function A(e, t, n) {
            var i = Ae.exec(t);
            return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t;
        }
        function I(e, t, n, i, o) {
            for (var r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, a = 0; 4 > r; r += 2)
                "margin" === n && (a += re.css(e, n + Ie[r], !0, o)),
                    i
                        ? ("content" === n && (a -= re.css(e, "padding" + Ie[r], !0, o)), "margin" !== n && (a -= re.css(e, "border" + Ie[r] + "Width", !0, o)))
                        : ((a += re.css(e, "padding" + Ie[r], !0, o)), "padding" !== n && (a += re.css(e, "border" + Ie[r] + "Width", !0, o)));
            return a;
        }
        function M(t, n, i) {
            var o = !0,
                r = "width" === n ? t.offsetWidth : t.offsetHeight,
                a = Ve(t),
                s = "border-box" === re.css(t, "boxSizing", !1, a);
            if ((V.msFullscreenElement && e.top !== e && t.getClientRects().length && (r = Math.round(100 * t.getBoundingClientRect()[n])), 0 >= r || null == r)) {
                if (((r = C(t, n, a)), (0 > r || null == r) && (r = t.style[n]), Je.test(r))) return r;
                (o = s && (ie.boxSizingReliable() || r === t.style[n])), (r = parseFloat(r) || 0);
            }
            return r + I(t, n, i || (s ? "border" : "content"), o, a) + "px";
        }
        function E(e, t) {
            for (var n, i, o, r = [], a = 0, s = e.length; s > a; a++)
                (i = e[a]),
                i.style &&
                ((r[a] = $e.get(i, "olddisplay")),
                    (n = i.style.display),
                    t
                        ? (r[a] || "none" !== n || (i.style.display = ""), "" === i.style.display && Me(i) && (r[a] = $e.access(i, "olddisplay", S(i.nodeName))))
                        : ((o = Me(i)), ("none" === n && o) || $e.set(i, "olddisplay", o ? n : re.css(i, "display"))));
            for (a = 0; s > a; a++) (i = e[a]), i.style && ((t && "none" !== i.style.display && "" !== i.style.display) || (i.style.display = t ? r[a] || "" : "none"));
            return e;
        }
        function _(e, t, n, i, o) {
            return new _.prototype.init(e, t, n, i, o);
        }
        function N() {
            return (
                e.setTimeout(function () {
                    it = void 0;
                }),
                    (it = re.now())
            );
        }
        function P(e, t) {
            var n,
                i = 0,
                o = { height: e };
            for (t = t ? 1 : 0; 4 > i; i += 2 - t) (n = Ie[i]), (o["margin" + n] = o["padding" + n] = e);
            return t && (o.opacity = o.width = e), o;
        }
        function j(e, t, n) {
            for (var i, o = (H.tweeners[t] || []).concat(H.tweeners["*"]), r = 0, a = o.length; a > r; r++) if ((i = o[r].call(n, t, e))) return i;
        }
        function W(e, t, n) {
            var i,
                o,
                r,
                a,
                s,
                l,
                u,
                c = this,
                d = {},
                p = e.style,
                h = e.nodeType && Me(e),
                f = $e.get(e, "fxshow");
            n.queue ||
            ((s = re._queueHooks(e, "fx")),
            null == s.unqueued &&
            ((s.unqueued = 0),
                (l = s.empty.fire),
                (s.empty.fire = function () {
                    s.unqueued || l();
                })),
                s.unqueued++,
                c.always(function () {
                    c.always(function () {
                        s.unqueued--, re.queue(e, "fx").length || s.empty.fire();
                    });
                })),
            1 === e.nodeType &&
            ("height" in t || "width" in t) &&
            ((n.overflow = [p.overflow, p.overflowX, p.overflowY]),
                (u = re.css(e, "display")),
            "inline" === ("none" === u ? $e.get(e, "olddisplay") || S(e.nodeName) : u) && "none" === re.css(e, "float") && (p.display = "inline-block")),
            n.overflow &&
            ((p.overflow = "hidden"),
                c.always(function () {
                    (p.overflow = n.overflow[0]), (p.overflowX = n.overflow[1]), (p.overflowY = n.overflow[2]);
                }));
            for (i in t)
                if (((o = t[i]), rt.exec(o))) {
                    if ((delete t[i], (r = r || "toggle" === o), o === (h ? "hide" : "show"))) {
                        if ("show" !== o || !f || void 0 === f[i]) continue;
                        h = !0;
                    }
                    d[i] = (f && f[i]) || re.style(e, i);
                } else u = void 0;
            if (re.isEmptyObject(d)) "inline" === ("none" === u ? S(e.nodeName) : u) && (p.display = u);
            else {
                f ? "hidden" in f && (h = f.hidden) : (f = $e.access(e, "fxshow", {})),
                r && (f.hidden = !h),
                    h
                        ? re(e).show()
                        : c.done(function () {
                            re(e).hide();
                        }),
                    c.done(function () {
                        var t;
                        $e.remove(e, "fxshow");
                        for (t in d) re.style(e, t, d[t]);
                    });
                for (i in d) (a = j(h ? f[i] : 0, i, c)), i in f || ((f[i] = a.start), h && ((a.end = a.start), (a.start = "width" === i || "height" === i ? 1 : 0)));
            }
        }
        function F(e, t) {
            var n, i, o, r, a;
            for (n in e)
                if (((i = re.camelCase(n)), (o = t[i]), (r = e[n]), re.isArray(r) && ((o = r[1]), (r = e[n] = r[0])), n !== i && ((e[i] = r), delete e[n]), (a = re.cssHooks[i]) && "expand" in a)) {
                    (r = a.expand(r)), delete e[i];
                    for (n in r) n in e || ((e[n] = r[n]), (t[n] = o));
                } else t[i] = o;
        }
        function H(e, t, n) {
            var i,
                o,
                r = 0,
                a = H.prefilters.length,
                s = re.Deferred().always(function () {
                    delete l.elem;
                }),
                l = function () {
                    if (o) return !1;
                    for (var t = it || N(), n = Math.max(0, u.startTime + u.duration - t), i = n / u.duration || 0, r = 1 - i, a = 0, l = u.tweens.length; l > a; a++) u.tweens[a].run(r);
                    return s.notifyWith(e, [u, r, n]), 1 > r && l ? n : (s.resolveWith(e, [u]), !1);
                },
                u = s.promise({
                    elem: e,
                    props: re.extend({}, t),
                    opts: re.extend(!0, { specialEasing: {}, easing: re.easing._default }, n),
                    originalProperties: t,
                    originalOptions: n,
                    startTime: it || N(),
                    duration: n.duration,
                    tweens: [],
                    createTween: function (t, n) {
                        var i = re.Tween(e, u.opts, t, n, u.opts.specialEasing[t] || u.opts.easing);
                        return u.tweens.push(i), i;
                    },
                    stop: function (t) {
                        var n = 0,
                            i = t ? u.tweens.length : 0;
                        if (o) return this;
                        for (o = !0; i > n; n++) u.tweens[n].run(1);
                        return t ? (s.notifyWith(e, [u, 1, 0]), s.resolveWith(e, [u, t])) : s.rejectWith(e, [u, t]), this;
                    },
                }),
                c = u.props;
            for (F(c, u.opts.specialEasing); a > r; r++) if ((i = H.prefilters[r].call(u, e, c, u.opts))) return re.isFunction(i.stop) && (re._queueHooks(u.elem, u.opts.queue).stop = re.proxy(i.stop, i)), i;
            return (
                re.map(c, j, u),
                re.isFunction(u.opts.start) && u.opts.start.call(e, u),
                    re.fx.timer(re.extend(l, { elem: e, anim: u, queue: u.opts.queue })),
                    u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always)
            );
        }
        function R(e) {
            return (e.getAttribute && e.getAttribute("class")) || "";
        }
        function L(e) {
            return function (t, n) {
                "string" != typeof t && ((n = t), (t = "*"));
                var i,
                    o = 0,
                    r = t.toLowerCase().match(we) || [];
                if (re.isFunction(n)) for (; (i = r[o++]); ) "+" === i[0] ? ((i = i.slice(1) || "*"), (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n);
            };
        }
        function q(e, t, n, i) {
            function o(s) {
                var l;
                return (
                    (r[s] = !0),
                        re.each(e[s] || [], function (e, s) {
                            var u = s(t, n, i);
                            return "string" != typeof u || a || r[u] ? (a ? !(l = u) : void 0) : (t.dataTypes.unshift(u), o(u), !1);
                        }),
                        l
                );
            }
            var r = {},
                a = e === St;
            return o(t.dataTypes[0]) || (!r["*"] && o("*"));
        }
        function z(e, t) {
            var n,
                i,
                o = re.ajaxSettings.flatOptions || {};
            for (n in t) void 0 !== t[n] && ((o[n] ? e : i || (i = {}))[n] = t[n]);
            return i && re.extend(!0, e, i), e;
        }
        function Y(e, t, n) {
            for (var i, o, r, a, s = e.contents, l = e.dataTypes; "*" === l[0]; ) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
            if (i)
                for (o in s)
                    if (s[o] && s[o].test(i)) {
                        l.unshift(o);
                        break;
                    }
            if (l[0] in n) r = l[0];
            else {
                for (o in n) {
                    if (!l[0] || e.converters[o + " " + l[0]]) {
                        r = o;
                        break;
                    }
                    a || (a = o);
                }
                r = r || a;
            }
            return r ? (r !== l[0] && l.unshift(r), n[r]) : void 0;
        }
        function B(e, t, n, i) {
            var o,
                r,
                a,
                s,
                l,
                u = {},
                c = e.dataTypes.slice();
            if (c[1]) for (a in e.converters) u[a.toLowerCase()] = e.converters[a];
            for (r = c.shift(); r; )
                if ((e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), (l = r), (r = c.shift())))
                    if ("*" === r) r = l;
                    else if ("*" !== l && l !== r) {
                        if (!(a = u[l + " " + r] || u["* " + r]))
                            for (o in u)
                                if (((s = o.split(" ")), s[1] === r && (a = u[l + " " + s[0]] || u["* " + s[0]]))) {
                                    !0 === a ? (a = u[o]) : !0 !== u[o] && ((r = s[0]), c.unshift(s[1]));
                                    break;
                                }
                        if (!0 !== a)
                            if (a && e.throws) t = a(t);
                            else
                                try {
                                    t = a(t);
                                } catch (e) {
                                    return { state: "parsererror", error: a ? e : "No conversion from " + l + " to " + r };
                                }
                    }
            return { state: "success", data: t };
        }
        function U(e, t, n, i) {
            var o;
            if (re.isArray(t))
                re.each(t, function (t, o) {
                    n || At.test(e) ? i(e, o) : U(e + "[" + ("object" == typeof o && null != o ? t : "") + "]", o, n, i);
                });
            else if (n || "object" !== re.type(t)) i(e, t);
            else for (o in t) U(e + "[" + o + "]", t[o], n, i);
        }
        function X(e) {
            return re.isWindow(e) ? e : 9 === e.nodeType && e.defaultView;
        }
        var J = [],
            V = e.document,
            G = J.slice,
            K = J.concat,
            Q = J.push,
            Z = J.indexOf,
            ee = {},
            te = ee.toString,
            ne = ee.hasOwnProperty,
            ie = {},
            oe = "2.2.3",
            re = function (e, t) {
                return new re.fn.init(e, t);
            },
            ae = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            se = /^-ms-/,
            le = /-([\da-z])/gi,
            ue = function (e, t) {
                return t.toUpperCase();
            };
        (re.fn = re.prototype = {
            jquery: oe,
            constructor: re,
            selector: "",
            length: 0,
            toArray: function () {
                return G.call(this);
            },
            get: function (e) {
                return null != e ? (0 > e ? this[e + this.length] : this[e]) : G.call(this);
            },
            pushStack: function (e) {
                var t = re.merge(this.constructor(), e);
                return (t.prevObject = this), (t.context = this.context), t;
            },
            each: function (e) {
                return re.each(this, e);
            },
            map: function (e) {
                return this.pushStack(
                    re.map(this, function (t, n) {
                        return e.call(t, n, t);
                    })
                );
            },
            slice: function () {
                return this.pushStack(G.apply(this, arguments));
            },
            first: function () {
                return this.eq(0);
            },
            last: function () {
                return this.eq(-1);
            },
            eq: function (e) {
                var t = this.length,
                    n = +e + (0 > e ? t : 0);
                return this.pushStack(n >= 0 && t > n ? [this[n]] : []);
            },
            end: function () {
                return this.prevObject || this.constructor();
            },
            push: Q,
            sort: J.sort,
            splice: J.splice,
        }),
            (re.extend = re.fn.extend = function () {
                var e,
                    t,
                    n,
                    i,
                    o,
                    r,
                    a = arguments[0] || {},
                    s = 1,
                    l = arguments.length,
                    u = !1;
                for ("boolean" == typeof a && ((u = a), (a = arguments[s] || {}), s++), "object" == typeof a || re.isFunction(a) || (a = {}), s === l && ((a = this), s--); l > s; s++)
                    if (null != (e = arguments[s]))
                        for (t in e)
                            (n = a[t]),
                                (i = e[t]),
                            a !== i &&
                            (u && i && (re.isPlainObject(i) || (o = re.isArray(i)))
                                ? (o ? ((o = !1), (r = n && re.isArray(n) ? n : [])) : (r = n && re.isPlainObject(n) ? n : {}), (a[t] = re.extend(u, r, i)))
                                : void 0 !== i && (a[t] = i));
                return a;
            }),
            re.extend({
                expando: "jQuery" + (oe + Math.random()).replace(/\D/g, ""),
                isReady: !0,
                error: function (e) {
                    throw new Error(e);
                },
                noop: function () {},
                isFunction: function (e) {
                    return "function" === re.type(e);
                },
                isArray: Array.isArray,
                isWindow: function (e) {
                    return null != e && e === e.window;
                },
                isNumeric: function (e) {
                    var t = e && e.toString();
                    return !re.isArray(e) && t - parseFloat(t) + 1 >= 0;
                },
                isPlainObject: function (e) {
                    var t;
                    if ("object" !== re.type(e) || e.nodeType || re.isWindow(e)) return !1;
                    if (e.constructor && !ne.call(e, "constructor") && !ne.call(e.constructor.prototype || {}, "isPrototypeOf")) return !1;
                    for (t in e);
                    return void 0 === t || ne.call(e, t);
                },
                isEmptyObject: function (e) {
                    var t;
                    for (t in e) return !1;
                    return !0;
                },
                type: function (e) {
                    return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? ee[te.call(e)] || "object" : typeof e;
                },
                globalEval: function (e) {
                    var t,
                        n = eval;
                    (e = re.trim(e)) && (1 === e.indexOf("use strict") ? ((t = V.createElement("script")), (t.text = e), V.head.appendChild(t).parentNode.removeChild(t)) : n(e));
                },
                camelCase: function (e) {
                    return e.replace(se, "ms-").replace(le, ue);
                },
                nodeName: function (e, t) {
                    return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
                },
                each: function (e, t) {
                    var i,
                        o = 0;
                    if (n(e)) for (i = e.length; i > o && !1 !== t.call(e[o], o, e[o]); o++);
                    else for (o in e) if (!1 === t.call(e[o], o, e[o])) break;
                    return e;
                },
                trim: function (e) {
                    return null == e ? "" : (e + "").replace(ae, "");
                },
                makeArray: function (e, t) {
                    var i = t || [];
                    return null != e && (n(Object(e)) ? re.merge(i, "string" == typeof e ? [e] : e) : Q.call(i, e)), i;
                },
                inArray: function (e, t, n) {
                    return null == t ? -1 : Z.call(t, e, n);
                },
                merge: function (e, t) {
                    for (var n = +t.length, i = 0, o = e.length; n > i; i++) e[o++] = t[i];
                    return (e.length = o), e;
                },
                grep: function (e, t, n) {
                    for (var i = [], o = 0, r = e.length, a = !n; r > o; o++) !t(e[o], o) !== a && i.push(e[o]);
                    return i;
                },
                map: function (e, t, i) {
                    var o,
                        r,
                        a = 0,
                        s = [];
                    if (n(e)) for (o = e.length; o > a; a++) null != (r = t(e[a], a, i)) && s.push(r);
                    else for (a in e) null != (r = t(e[a], a, i)) && s.push(r);
                    return K.apply([], s);
                },
                guid: 1,
                proxy: function (e, t) {
                    var n, i, o;
                    return (
                        "string" == typeof t && ((n = e[t]), (t = e), (e = n)),
                            re.isFunction(e)
                                ? ((i = G.call(arguments, 2)),
                                    (o = function () {
                                        return e.apply(t || this, i.concat(G.call(arguments)));
                                    }),
                                    (o.guid = e.guid = e.guid || re.guid++),
                                    o)
                                : void 0
                    );
                },
                now: Date.now,
                support: ie,
            }),
        "function" == typeof Symbol && (re.fn[Symbol.iterator] = J[Symbol.iterator]),
            re.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
                ee["[object " + t + "]"] = t.toLowerCase();
            });
        var ce = (function (e) {
            function t(e, t, n, i) {
                var o,
                    r,
                    a,
                    s,
                    u,
                    d,
                    p,
                    h,
                    f = t && t.ownerDocument,
                    m = t ? t.nodeType : 9;
                if (((n = n || []), "string" != typeof e || !e || (1 !== m && 9 !== m && 11 !== m))) return n;
                if (!i && ((t ? t.ownerDocument || t : F) !== I && A(t), (t = t || I), E)) {
                    if (11 !== m && (d = me.exec(e)))
                        if ((o = d[1])) {
                            if (9 === m) {
                                if (!(a = t.getElementById(o))) return n;
                                if (a.id === o) return n.push(a), n;
                            } else if (f && (a = f.getElementById(o)) && j(t, a) && a.id === o) return n.push(a), n;
                        } else {
                            if (d[2]) return G.apply(n, t.getElementsByTagName(e)), n;
                            if ((o = d[3]) && b.getElementsByClassName && t.getElementsByClassName) return G.apply(n, t.getElementsByClassName(o)), n;
                        }
                    if (b.qsa && !z[e + " "] && (!_ || !_.test(e))) {
                        if (1 !== m) (f = t), (h = e);
                        else if ("object" !== t.nodeName.toLowerCase()) {
                            for ((s = t.getAttribute("id")) ? (s = s.replace(ve, "\\$&")) : t.setAttribute("id", (s = W)), p = k(e), r = p.length, u = ce.test(s) ? "#" + s : "[id='" + s + "']"; r--; ) p[r] = u + " " + c(p[r]);
                            (h = p.join(",")), (f = (ge.test(e) && l(t.parentNode)) || t);
                        }
                        if (h)
                            try {
                                return G.apply(n, f.querySelectorAll(h)), n;
                            } catch (e) {
                            } finally {
                                s === W && t.removeAttribute("id");
                            }
                    }
                }
                return S(e.replace(re, "$1"), t, n, i);
            }
            function n() {
                function e(n, i) {
                    return t.push(n + " ") > w.cacheLength && delete e[t.shift()], (e[n + " "] = i);
                }
                var t = [];
                return e;
            }
            function i(e) {
                return (e[W] = !0), e;
            }
            function o(e) {
                var t = I.createElement("div");
                try {
                    return !!e(t);
                } catch (e) {
                    return !1;
                } finally {
                    t.parentNode && t.parentNode.removeChild(t), (t = null);
                }
            }
            function r(e, t) {
                for (var n = e.split("|"), i = n.length; i--; ) w.attrHandle[n[i]] = t;
            }
            function a(e, t) {
                var n = t && e,
                    i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || B) - (~e.sourceIndex || B);
                if (i) return i;
                if (n) for (; (n = n.nextSibling); ) if (n === t) return -1;
                return e ? 1 : -1;
            }
            function s(e) {
                return i(function (t) {
                    return (
                        (t = +t),
                            i(function (n, i) {
                                for (var o, r = e([], n.length, t), a = r.length; a--; ) n[(o = r[a])] && (n[o] = !(i[o] = n[o]));
                            })
                    );
                });
            }
            function l(e) {
                return e && void 0 !== e.getElementsByTagName && e;
            }
            function u() {}
            function c(e) {
                for (var t = 0, n = e.length, i = ""; n > t; t++) i += e[t].value;
                return i;
            }
            function d(e, t, n) {
                var i = t.dir,
                    o = n && "parentNode" === i,
                    r = R++;
                return t.first
                    ? function (t, n, r) {
                        for (; (t = t[i]); ) if (1 === t.nodeType || o) return e(t, n, r);
                    }
                    : function (t, n, a) {
                        var s,
                            l,
                            u,
                            c = [H, r];
                        if (a) {
                            for (; (t = t[i]); ) if ((1 === t.nodeType || o) && e(t, n, a)) return !0;
                        } else
                            for (; (t = t[i]); )
                                if (1 === t.nodeType || o) {
                                    if (((u = t[W] || (t[W] = {})), (l = u[t.uniqueID] || (u[t.uniqueID] = {})), (s = l[i]) && s[0] === H && s[1] === r)) return (c[2] = s[2]);
                                    if (((l[i] = c), (c[2] = e(t, n, a)))) return !0;
                                }
                    };
            }
            function p(e) {
                return e.length > 1
                    ? function (t, n, i) {
                        for (var o = e.length; o--; ) if (!e[o](t, n, i)) return !1;
                        return !0;
                    }
                    : e[0];
            }
            function h(e, n, i) {
                for (var o = 0, r = n.length; r > o; o++) t(e, n[o], i);
                return i;
            }
            function f(e, t, n, i, o) {
                for (var r, a = [], s = 0, l = e.length, u = null != t; l > s; s++) (r = e[s]) && ((n && !n(r, i, o)) || (a.push(r), u && t.push(s)));
                return a;
            }
            function m(e, t, n, o, r, a) {
                return (
                    o && !o[W] && (o = m(o)),
                    r && !r[W] && (r = m(r, a)),
                        i(function (i, a, s, l) {
                            var u,
                                c,
                                d,
                                p = [],
                                m = [],
                                g = a.length,
                                v = i || h(t || "*", s.nodeType ? [s] : s, []),
                                y = !e || (!i && t) ? v : f(v, p, e, s, l),
                                b = n ? (r || (i ? e : g || o) ? [] : a) : y;
                            if ((n && n(y, b, s, l), o)) for (u = f(b, m), o(u, [], s, l), c = u.length; c--; ) (d = u[c]) && (b[m[c]] = !(y[m[c]] = d));
                            if (i) {
                                if (r || e) {
                                    if (r) {
                                        for (u = [], c = b.length; c--; ) (d = b[c]) && u.push((y[c] = d));
                                        r(null, (b = []), u, l);
                                    }
                                    for (c = b.length; c--; ) (d = b[c]) && (u = r ? Q(i, d) : p[c]) > -1 && (i[u] = !(a[u] = d));
                                }
                            } else (b = f(b === a ? b.splice(g, b.length) : b)), r ? r(null, a, b, l) : G.apply(a, b);
                        })
                );
            }
            function g(e) {
                for (
                    var t,
                        n,
                        i,
                        o = e.length,
                        r = w.relative[e[0].type],
                        a = r || w.relative[" "],
                        s = r ? 1 : 0,
                        l = d(
                            function (e) {
                                return e === t;
                            },
                            a,
                            !0
                        ),
                        u = d(
                            function (e) {
                                return Q(t, e) > -1;
                            },
                            a,
                            !0
                        ),
                        h = [
                            function (e, n, i) {
                                var o = (!r && (i || n !== C)) || ((t = n).nodeType ? l(e, n, i) : u(e, n, i));
                                return (t = null), o;
                            },
                        ];
                    o > s;
                    s++
                )
                    if ((n = w.relative[e[s].type])) h = [d(p(h), n)];
                    else {
                        if (((n = w.filter[e[s].type].apply(null, e[s].matches)), n[W])) {
                            for (i = ++s; o > i && !w.relative[e[i].type]; i++);
                            return m(s > 1 && p(h), s > 1 && c(e.slice(0, s - 1).concat({ value: " " === e[s - 2].type ? "*" : "" })).replace(re, "$1"), n, i > s && g(e.slice(s, i)), o > i && g((e = e.slice(i))), o > i && c(e));
                        }
                        h.push(n);
                    }
                return p(h);
            }
            function v(e, n) {
                var o = n.length > 0,
                    r = e.length > 0,
                    a = function (i, a, s, l, u) {
                        var c,
                            d,
                            p,
                            h = 0,
                            m = "0",
                            g = i && [],
                            v = [],
                            y = C,
                            b = i || (r && w.find.TAG("*", u)),
                            x = (H += null == y ? 1 : Math.random() || 0.1),
                            T = b.length;
                        for (u && (C = a === I || a || u); m !== T && null != (c = b[m]); m++) {
                            if (r && c) {
                                for (d = 0, a || c.ownerDocument === I || (A(c), (s = !E)); (p = e[d++]); )
                                    if (p(c, a || I, s)) {
                                        l.push(c);
                                        break;
                                    }
                                u && (H = x);
                            }
                            o && ((c = !p && c) && h--, i && g.push(c));
                        }
                        if (((h += m), o && m !== h)) {
                            for (d = 0; (p = n[d++]); ) p(g, v, a, s);
                            if (i) {
                                if (h > 0) for (; m--; ) g[m] || v[m] || (v[m] = J.call(l));
                                v = f(v);
                            }
                            G.apply(l, v), u && !i && v.length > 0 && h + n.length > 1 && t.uniqueSort(l);
                        }
                        return u && ((H = x), (C = y)), g;
                    };
                return o ? i(a) : a;
            }
            var y,
                b,
                w,
                x,
                T,
                k,
                $,
                S,
                C,
                D,
                O,
                A,
                I,
                M,
                E,
                _,
                N,
                P,
                j,
                W = "sizzle" + 1 * new Date(),
                F = e.document,
                H = 0,
                R = 0,
                L = n(),
                q = n(),
                z = n(),
                Y = function (e, t) {
                    return e === t && (O = !0), 0;
                },
                B = 1 << 31,
                U = {}.hasOwnProperty,
                X = [],
                J = X.pop,
                V = X.push,
                G = X.push,
                K = X.slice,
                Q = function (e, t) {
                    for (var n = 0, i = e.length; i > n; n++) if (e[n] === t) return n;
                    return -1;
                },
                Z = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                ee = "[\\x20\\t\\r\\n\\f]",
                te = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                ne = "\\[" + ee + "*(" + te + ")(?:" + ee + "*([*^$|!~]?=)" + ee + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + te + "))|)" + ee + "*\\]",
                ie = ":(" + te + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ne + ")*)|.*)\\)|)",
                oe = new RegExp(ee + "+", "g"),
                re = new RegExp("^" + ee + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ee + "+$", "g"),
                ae = new RegExp("^" + ee + "*," + ee + "*"),
                se = new RegExp("^" + ee + "*([>+~]|" + ee + ")" + ee + "*"),
                le = new RegExp("=" + ee + "*([^\\]'\"]*?)" + ee + "*\\]", "g"),
                ue = new RegExp(ie),
                ce = new RegExp("^" + te + "$"),
                de = {
                    ID: new RegExp("^#(" + te + ")"),
                    CLASS: new RegExp("^\\.(" + te + ")"),
                    TAG: new RegExp("^(" + te + "|[*])"),
                    ATTR: new RegExp("^" + ne),
                    PSEUDO: new RegExp("^" + ie),
                    CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ee + "*(even|odd|(([+-]|)(\\d*)n|)" + ee + "*(?:([+-]|)" + ee + "*(\\d+)|))" + ee + "*\\)|)", "i"),
                    bool: new RegExp("^(?:" + Z + ")$", "i"),
                    needsContext: new RegExp("^" + ee + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ee + "*((?:-\\d)?\\d*)" + ee + "*\\)|)(?=[^-]|$)", "i"),
                },
                pe = /^(?:input|select|textarea|button)$/i,
                he = /^h\d$/i,
                fe = /^[^{]+\{\s*\[native \w/,
                me = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                ge = /[+~]/,
                ve = /'|\\/g,
                ye = new RegExp("\\\\([\\da-f]{1,6}" + ee + "?|(" + ee + ")|.)", "ig"),
                be = function (e, t, n) {
                    var i = "0x" + t - 65536;
                    return i !== i || n ? t : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode((i >> 10) | 55296, (1023 & i) | 56320);
                },
                we = function () {
                    A();
                };
            try {
                G.apply((X = K.call(F.childNodes)), F.childNodes), X[F.childNodes.length].nodeType;
            } catch (e) {
                G = {
                    apply: X.length
                        ? function (e, t) {
                            V.apply(e, K.call(t));
                        }
                        : function (e, t) {
                            for (var n = e.length, i = 0; (e[n++] = t[i++]); );
                            e.length = n - 1;
                        },
                };
            }
            (b = t.support = {}),
                (T = t.isXML = function (e) {
                    var t = e && (e.ownerDocument || e).documentElement;
                    return !!t && "HTML" !== t.nodeName;
                }),
                (A = t.setDocument = function (e) {
                    var t,
                        n,
                        i = e ? e.ownerDocument || e : F;
                    return i !== I && 9 === i.nodeType && i.documentElement
                        ? ((I = i),
                            (M = I.documentElement),
                            (E = !T(I)),
                        (n = I.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", we, !1) : n.attachEvent && n.attachEvent("onunload", we)),
                            (b.attributes = o(function (e) {
                                return (e.className = "i"), !e.getAttribute("className");
                            })),
                            (b.getElementsByTagName = o(function (e) {
                                return e.appendChild(I.createComment("")), !e.getElementsByTagName("*").length;
                            })),
                            (b.getElementsByClassName = fe.test(I.getElementsByClassName)),
                            (b.getById = o(function (e) {
                                return (M.appendChild(e).id = W), !I.getElementsByName || !I.getElementsByName(W).length;
                            })),
                            b.getById
                                ? ((w.find.ID = function (e, t) {
                                    if (void 0 !== t.getElementById && E) {
                                        var n = t.getElementById(e);
                                        return n ? [n] : [];
                                    }
                                }),
                                    (w.filter.ID = function (e) {
                                        var t = e.replace(ye, be);
                                        return function (e) {
                                            return e.getAttribute("id") === t;
                                        };
                                    }))
                                : (delete w.find.ID,
                                    (w.filter.ID = function (e) {
                                        var t = e.replace(ye, be);
                                        return function (e) {
                                            var n = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                                            return n && n.value === t;
                                        };
                                    })),
                            (w.find.TAG = b.getElementsByTagName
                                ? function (e, t) {
                                    return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : b.qsa ? t.querySelectorAll(e) : void 0;
                                }
                                : function (e, t) {
                                    var n,
                                        i = [],
                                        o = 0,
                                        r = t.getElementsByTagName(e);
                                    if ("*" === e) {
                                        for (; (n = r[o++]); ) 1 === n.nodeType && i.push(n);
                                        return i;
                                    }
                                    return r;
                                }),
                            (w.find.CLASS =
                                b.getElementsByClassName &&
                                function (e, t) {
                                    return void 0 !== t.getElementsByClassName && E ? t.getElementsByClassName(e) : void 0;
                                }),
                            (N = []),
                            (_ = []),
                        (b.qsa = fe.test(I.querySelectorAll)) &&
                        (o(function (e) {
                            (M.appendChild(e).innerHTML = "<a id='" + W + "'></a><select id='" + W + "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                            e.querySelectorAll("[msallowcapture^='']").length && _.push("[*^$]=" + ee + "*(?:''|\"\")"),
                            e.querySelectorAll("[selected]").length || _.push("\\[" + ee + "*(?:value|" + Z + ")"),
                            e.querySelectorAll("[id~=" + W + "-]").length || _.push("~="),
                            e.querySelectorAll(":checked").length || _.push(":checked"),
                            e.querySelectorAll("a#" + W + "+*").length || _.push(".#.+[+~]");
                        }),
                            o(function (e) {
                                var t = I.createElement("input");
                                t.setAttribute("type", "hidden"),
                                    e.appendChild(t).setAttribute("name", "D"),
                                e.querySelectorAll("[name=d]").length && _.push("name" + ee + "*[*^$|!~]?="),
                                e.querySelectorAll(":enabled").length || _.push(":enabled", ":disabled"),
                                    e.querySelectorAll("*,:x"),
                                    _.push(",.*:");
                            })),
                        (b.matchesSelector = fe.test((P = M.matches || M.webkitMatchesSelector || M.mozMatchesSelector || M.oMatchesSelector || M.msMatchesSelector))) &&
                        o(function (e) {
                            (b.disconnectedMatch = P.call(e, "div")), P.call(e, "[s!='']:x"), N.push("!=", ie);
                        }),
                            (_ = _.length && new RegExp(_.join("|"))),
                            (N = N.length && new RegExp(N.join("|"))),
                            (t = fe.test(M.compareDocumentPosition)),
                            (j =
                                t || fe.test(M.contains)
                                    ? function (e, t) {
                                        var n = 9 === e.nodeType ? e.documentElement : e,
                                            i = t && t.parentNode;
                                        return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)));
                                    }
                                    : function (e, t) {
                                        if (t) for (; (t = t.parentNode); ) if (t === e) return !0;
                                        return !1;
                                    }),
                            (Y = t
                                ? function (e, t) {
                                    if (e === t) return (O = !0), 0;
                                    var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                                    return (
                                        n ||
                                        ((n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1),
                                            1 & n || (!b.sortDetached && t.compareDocumentPosition(e) === n)
                                                ? e === I || (e.ownerDocument === F && j(F, e))
                                                ? -1
                                                : t === I || (t.ownerDocument === F && j(F, t))
                                                    ? 1
                                                    : D
                                                        ? Q(D, e) - Q(D, t)
                                                        : 0
                                                : 4 & n
                                                ? -1
                                                : 1)
                                    );
                                }
                                : function (e, t) {
                                    if (e === t) return (O = !0), 0;
                                    var n,
                                        i = 0,
                                        o = e.parentNode,
                                        r = t.parentNode,
                                        s = [e],
                                        l = [t];
                                    if (!o || !r) return e === I ? -1 : t === I ? 1 : o ? -1 : r ? 1 : D ? Q(D, e) - Q(D, t) : 0;
                                    if (o === r) return a(e, t);
                                    for (n = e; (n = n.parentNode); ) s.unshift(n);
                                    for (n = t; (n = n.parentNode); ) l.unshift(n);
                                    for (; s[i] === l[i]; ) i++;
                                    return i ? a(s[i], l[i]) : s[i] === F ? -1 : l[i] === F ? 1 : 0;
                                }),
                            I)
                        : I;
                }),
                (t.matches = function (e, n) {
                    return t(e, null, null, n);
                }),
                (t.matchesSelector = function (e, n) {
                    if (((e.ownerDocument || e) !== I && A(e), (n = n.replace(le, "='$1']")), b.matchesSelector && E && !z[n + " "] && (!N || !N.test(n)) && (!_ || !_.test(n))))
                        try {
                            var i = P.call(e, n);
                            if (i || b.disconnectedMatch || (e.document && 11 !== e.document.nodeType)) return i;
                        } catch (e) {}
                    return t(n, I, null, [e]).length > 0;
                }),
                (t.contains = function (e, t) {
                    return (e.ownerDocument || e) !== I && A(e), j(e, t);
                }),
                (t.attr = function (e, t) {
                    (e.ownerDocument || e) !== I && A(e);
                    var n = w.attrHandle[t.toLowerCase()],
                        i = n && U.call(w.attrHandle, t.toLowerCase()) ? n(e, t, !E) : void 0;
                    return void 0 !== i ? i : b.attributes || !E ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null;
                }),
                (t.error = function (e) {
                    throw new Error("Syntax error, unrecognized expression: " + e);
                }),
                (t.uniqueSort = function (e) {
                    var t,
                        n = [],
                        i = 0,
                        o = 0;
                    if (((O = !b.detectDuplicates), (D = !b.sortStable && e.slice(0)), e.sort(Y), O)) {
                        for (; (t = e[o++]); ) t === e[o] && (i = n.push(o));
                        for (; i--; ) e.splice(n[i], 1);
                    }
                    return (D = null), e;
                }),
                (x = t.getText = function (e) {
                    var t,
                        n = "",
                        i = 0,
                        o = e.nodeType;
                    if (o) {
                        if (1 === o || 9 === o || 11 === o) {
                            if ("string" == typeof e.textContent) return e.textContent;
                            for (e = e.firstChild; e; e = e.nextSibling) n += x(e);
                        } else if (3 === o || 4 === o) return e.nodeValue;
                    } else for (; (t = e[i++]); ) n += x(t);
                    return n;
                }),
                (w = t.selectors = {
                    cacheLength: 50,
                    createPseudo: i,
                    match: de,
                    attrHandle: {},
                    find: {},
                    relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } },
                    preFilter: {
                        ATTR: function (e) {
                            return (e[1] = e[1].replace(ye, be)), (e[3] = (e[3] || e[4] || e[5] || "").replace(ye, be)), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4);
                        },
                        CHILD: function (e) {
                            return (
                                (e[1] = e[1].toLowerCase()),
                                    "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), (e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3]))), (e[5] = +(e[7] + e[8] || "odd" === e[3]))) : e[3] && t.error(e[0]),
                                    e
                            );
                        },
                        PSEUDO: function (e) {
                            var t,
                                n = !e[6] && e[2];
                            return de.CHILD.test(e[0])
                                ? null
                                : (e[3] ? (e[2] = e[4] || e[5] || "") : n && ue.test(n) && (t = k(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && ((e[0] = e[0].slice(0, t)), (e[2] = n.slice(0, t))), e.slice(0, 3));
                        },
                    },
                    filter: {
                        TAG: function (e) {
                            var t = e.replace(ye, be).toLowerCase();
                            return "*" === e
                                ? function () {
                                    return !0;
                                }
                                : function (e) {
                                    return e.nodeName && e.nodeName.toLowerCase() === t;
                                };
                        },
                        CLASS: function (e) {
                            var t = L[e + " "];
                            return (
                                t ||
                                ((t = new RegExp("(^|" + ee + ")" + e + "(" + ee + "|$)")) &&
                                    L(e, function (e) {
                                        return t.test(("string" == typeof e.className && e.className) || (void 0 !== e.getAttribute && e.getAttribute("class")) || "");
                                    }))
                            );
                        },
                        ATTR: function (e, n, i) {
                            return function (o) {
                                var r = t.attr(o, e);
                                return null == r
                                    ? "!=" === n
                                    : !n ||
                                    ((r += ""),
                                        "=" === n
                                            ? r === i
                                            : "!=" === n
                                            ? r !== i
                                            : "^=" === n
                                                ? i && 0 === r.indexOf(i)
                                                : "*=" === n
                                                    ? i && r.indexOf(i) > -1
                                                    : "$=" === n
                                                        ? i && r.slice(-i.length) === i
                                                        : "~=" === n
                                                            ? (" " + r.replace(oe, " ") + " ").indexOf(i) > -1
                                                            : "|=" === n && (r === i || r.slice(0, i.length + 1) === i + "-"));
                            };
                        },
                        CHILD: function (e, t, n, i, o) {
                            var r = "nth" !== e.slice(0, 3),
                                a = "last" !== e.slice(-4),
                                s = "of-type" === t;
                            return 1 === i && 0 === o
                                ? function (e) {
                                    return !!e.parentNode;
                                }
                                : function (t, n, l) {
                                    var u,
                                        c,
                                        d,
                                        p,
                                        h,
                                        f,
                                        m = r !== a ? "nextSibling" : "previousSibling",
                                        g = t.parentNode,
                                        v = s && t.nodeName.toLowerCase(),
                                        y = !l && !s,
                                        b = !1;
                                    if (g) {
                                        if (r) {
                                            for (; m; ) {
                                                for (p = t; (p = p[m]); ) if (s ? p.nodeName.toLowerCase() === v : 1 === p.nodeType) return !1;
                                                f = m = "only" === e && !f && "nextSibling";
                                            }
                                            return !0;
                                        }
                                        if (((f = [a ? g.firstChild : g.lastChild]), a && y)) {
                                            for (
                                                p = g, d = p[W] || (p[W] = {}), c = d[p.uniqueID] || (d[p.uniqueID] = {}), u = c[e] || [], h = u[0] === H && u[1], b = h && u[2], p = h && g.childNodes[h];
                                                (p = (++h && p && p[m]) || (b = h = 0) || f.pop());

                                            )
                                                if (1 === p.nodeType && ++b && p === t) {
                                                    c[e] = [H, h, b];
                                                    break;
                                                }
                                        } else if ((y && ((p = t), (d = p[W] || (p[W] = {})), (c = d[p.uniqueID] || (d[p.uniqueID] = {})), (u = c[e] || []), (h = u[0] === H && u[1]), (b = h)), !1 === b))
                                            for (
                                                ;
                                                (p = (++h && p && p[m]) || (b = h = 0) || f.pop()) &&
                                                ((s ? p.nodeName.toLowerCase() !== v : 1 !== p.nodeType) || !++b || (y && ((d = p[W] || (p[W] = {})), (c = d[p.uniqueID] || (d[p.uniqueID] = {})), (c[e] = [H, b])), p !== t));

                                            );
                                        return (b -= o) === i || (b % i == 0 && b / i >= 0);
                                    }
                                };
                        },
                        PSEUDO: function (e, n) {
                            var o,
                                r = w.pseudos[e] || w.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                            return r[W]
                                ? r(n)
                                : r.length > 1
                                    ? ((o = [e, e, "", n]),
                                        w.setFilters.hasOwnProperty(e.toLowerCase())
                                            ? i(function (e, t) {
                                                for (var i, o = r(e, n), a = o.length; a--; ) (i = Q(e, o[a])), (e[i] = !(t[i] = o[a]));
                                            })
                                            : function (e) {
                                                return r(e, 0, o);
                                            })
                                    : r;
                        },
                    },
                    pseudos: {
                        not: i(function (e) {
                            var t = [],
                                n = [],
                                o = $(e.replace(re, "$1"));
                            return o[W]
                                ? i(function (e, t, n, i) {
                                    for (var r, a = o(e, null, i, []), s = e.length; s--; ) (r = a[s]) && (e[s] = !(t[s] = r));
                                })
                                : function (e, i, r) {
                                    return (t[0] = e), o(t, null, r, n), (t[0] = null), !n.pop();
                                };
                        }),
                        has: i(function (e) {
                            return function (n) {
                                return t(e, n).length > 0;
                            };
                        }),
                        contains: i(function (e) {
                            return (
                                (e = e.replace(ye, be)),
                                    function (t) {
                                        return (t.textContent || t.innerText || x(t)).indexOf(e) > -1;
                                    }
                            );
                        }),
                        lang: i(function (e) {
                            return (
                                ce.test(e || "") || t.error("unsupported lang: " + e),
                                    (e = e.replace(ye, be).toLowerCase()),
                                    function (t) {
                                        var n;
                                        do {
                                            if ((n = E ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang"))) return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-");
                                        } while ((t = t.parentNode) && 1 === t.nodeType);
                                        return !1;
                                    }
                            );
                        }),
                        target: function (t) {
                            var n = e.location && e.location.hash;
                            return n && n.slice(1) === t.id;
                        },
                        root: function (e) {
                            return e === M;
                        },
                        focus: function (e) {
                            return e === I.activeElement && (!I.hasFocus || I.hasFocus()) && !!(e.type || e.href || ~e.tabIndex);
                        },
                        enabled: function (e) {
                            return !1 === e.disabled;
                        },
                        disabled: function (e) {
                            return !0 === e.disabled;
                        },
                        checked: function (e) {
                            var t = e.nodeName.toLowerCase();
                            return ("input" === t && !!e.checked) || ("option" === t && !!e.selected);
                        },
                        selected: function (e) {
                            return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected;
                        },
                        empty: function (e) {
                            for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
                            return !0;
                        },
                        parent: function (e) {
                            return !w.pseudos.empty(e);
                        },
                        header: function (e) {
                            return he.test(e.nodeName);
                        },
                        input: function (e) {
                            return pe.test(e.nodeName);
                        },
                        button: function (e) {
                            var t = e.nodeName.toLowerCase();
                            return ("input" === t && "button" === e.type) || "button" === t;
                        },
                        text: function (e) {
                            var t;
                            return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase());
                        },
                        first: s(function () {
                            return [0];
                        }),
                        last: s(function (e, t) {
                            return [t - 1];
                        }),
                        eq: s(function (e, t, n) {
                            return [0 > n ? n + t : n];
                        }),
                        even: s(function (e, t) {
                            for (var n = 0; t > n; n += 2) e.push(n);
                            return e;
                        }),
                        odd: s(function (e, t) {
                            for (var n = 1; t > n; n += 2) e.push(n);
                            return e;
                        }),
                        lt: s(function (e, t, n) {
                            for (var i = 0 > n ? n + t : n; --i >= 0; ) e.push(i);
                            return e;
                        }),
                        gt: s(function (e, t, n) {
                            for (var i = 0 > n ? n + t : n; ++i < t; ) e.push(i);
                            return e;
                        }),
                    },
                }),
                (w.pseudos.nth = w.pseudos.eq);
            for (y in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 })
                w.pseudos[y] = (function (e) {
                    return function (t) {
                        return "input" === t.nodeName.toLowerCase() && t.type === e;
                    };
                })(y);
            for (y in { submit: !0, reset: !0 })
                w.pseudos[y] = (function (e) {
                    return function (t) {
                        var n = t.nodeName.toLowerCase();
                        return ("input" === n || "button" === n) && t.type === e;
                    };
                })(y);
            return (
                (u.prototype = w.filters = w.pseudos),
                    (w.setFilters = new u()),
                    (k = t.tokenize = function (e, n) {
                        var i,
                            o,
                            r,
                            a,
                            s,
                            l,
                            u,
                            c = q[e + " "];
                        if (c) return n ? 0 : c.slice(0);
                        for (s = e, l = [], u = w.preFilter; s; ) {
                            (i && !(o = ae.exec(s))) || (o && (s = s.slice(o[0].length) || s), l.push((r = []))), (i = !1), (o = se.exec(s)) && ((i = o.shift()), r.push({ value: i, type: o[0].replace(re, " ") }), (s = s.slice(i.length)));
                            for (a in w.filter) !(o = de[a].exec(s)) || (u[a] && !(o = u[a](o))) || ((i = o.shift()), r.push({ value: i, type: a, matches: o }), (s = s.slice(i.length)));
                            if (!i) break;
                        }
                        return n ? s.length : s ? t.error(e) : q(e, l).slice(0);
                    }),
                    ($ = t.compile = function (e, t) {
                        var n,
                            i = [],
                            o = [],
                            r = z[e + " "];
                        if (!r) {
                            for (t || (t = k(e)), n = t.length; n--; ) (r = g(t[n])), r[W] ? i.push(r) : o.push(r);
                            (r = z(e, v(o, i))), (r.selector = e);
                        }
                        return r;
                    }),
                    (S = t.select = function (e, t, n, i) {
                        var o,
                            r,
                            a,
                            s,
                            u,
                            d = "function" == typeof e && e,
                            p = !i && k((e = d.selector || e));
                        if (((n = n || []), 1 === p.length)) {
                            if (((r = p[0] = p[0].slice(0)), r.length > 2 && "ID" === (a = r[0]).type && b.getById && 9 === t.nodeType && E && w.relative[r[1].type])) {
                                if (!(t = (w.find.ID(a.matches[0].replace(ye, be), t) || [])[0])) return n;
                                d && (t = t.parentNode), (e = e.slice(r.shift().value.length));
                            }
                            for (o = de.needsContext.test(e) ? 0 : r.length; o-- && ((a = r[o]), !w.relative[(s = a.type)]); )
                                if ((u = w.find[s]) && (i = u(a.matches[0].replace(ye, be), (ge.test(r[0].type) && l(t.parentNode)) || t))) {
                                    if ((r.splice(o, 1), !(e = i.length && c(r)))) return G.apply(n, i), n;
                                    break;
                                }
                        }
                        return (d || $(e, p))(i, t, !E, n, !t || (ge.test(e) && l(t.parentNode)) || t), n;
                    }),
                    (b.sortStable = W.split("").sort(Y).join("") === W),
                    (b.detectDuplicates = !!O),
                    A(),
                    (b.sortDetached = o(function (e) {
                        return 1 & e.compareDocumentPosition(I.createElement("div"));
                    })),
                o(function (e) {
                    return (e.innerHTML = "<a href='#'></a>"), "#" === e.firstChild.getAttribute("href");
                }) ||
                r("type|href|height|width", function (e, t, n) {
                    return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2);
                }),
                (b.attributes &&
                    o(function (e) {
                        return (e.innerHTML = "<input/>"), e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value");
                    })) ||
                r("value", function (e, t, n) {
                    return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue;
                }),
                o(function (e) {
                    return null == e.getAttribute("disabled");
                }) ||
                r(Z, function (e, t, n) {
                    var i;
                    return n ? void 0 : !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null;
                }),
                    t
            );
        })(e);
        (re.find = ce), (re.expr = ce.selectors), (re.expr[":"] = re.expr.pseudos), (re.uniqueSort = re.unique = ce.uniqueSort), (re.text = ce.getText), (re.isXMLDoc = ce.isXML), (re.contains = ce.contains);
        var de = function (e, t, n) {
                for (var i = [], o = void 0 !== n; (e = e[t]) && 9 !== e.nodeType; )
                    if (1 === e.nodeType) {
                        if (o && re(e).is(n)) break;
                        i.push(e);
                    }
                return i;
            },
            pe = function (e, t) {
                for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
                return n;
            },
            he = re.expr.match.needsContext,
            fe = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
            me = /^.[^:#\[\.,]*$/;
        (re.filter = function (e, t, n) {
            var i = t[0];
            return (
                n && (e = ":not(" + e + ")"),
                    1 === t.length && 1 === i.nodeType
                        ? re.find.matchesSelector(i, e)
                        ? [i]
                        : []
                        : re.find.matches(
                        e,
                        re.grep(t, function (e) {
                            return 1 === e.nodeType;
                        })
                        )
            );
        }),
            re.fn.extend({
                find: function (e) {
                    var t,
                        n = this.length,
                        i = [],
                        o = this;
                    if ("string" != typeof e)
                        return this.pushStack(
                            re(e).filter(function () {
                                for (t = 0; n > t; t++) if (re.contains(o[t], this)) return !0;
                            })
                        );
                    for (t = 0; n > t; t++) re.find(e, o[t], i);
                    return (i = this.pushStack(n > 1 ? re.unique(i) : i)), (i.selector = this.selector ? this.selector + " " + e : e), i;
                },
                filter: function (e) {
                    return this.pushStack(i(this, e || [], !1));
                },
                not: function (e) {
                    return this.pushStack(i(this, e || [], !0));
                },
                is: function (e) {
                    return !!i(this, "string" == typeof e && he.test(e) ? re(e) : e || [], !1).length;
                },
            });
        var ge,
            ve = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
        ((re.fn.init = function (e, t, n) {
            var i, o;
            if (!e) return this;
            if (((n = n || ge), "string" == typeof e)) {
                if (!(i = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : ve.exec(e)) || (!i[1] && t)) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
                if (i[1]) {
                    if (((t = t instanceof re ? t[0] : t), re.merge(this, re.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : V, !0)), fe.test(i[1]) && re.isPlainObject(t)))
                        for (i in t) re.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
                    return this;
                }
                return (o = V.getElementById(i[2])), o && o.parentNode && ((this.length = 1), (this[0] = o)), (this.context = V), (this.selector = e), this;
            }
            return e.nodeType
                ? ((this.context = this[0] = e), (this.length = 1), this)
                : re.isFunction(e)
                    ? void 0 !== n.ready
                        ? n.ready(e)
                        : e(re)
                    : (void 0 !== e.selector && ((this.selector = e.selector), (this.context = e.context)), re.makeArray(e, this));
        }).prototype = re.fn),
            (ge = re(V));
        var ye = /^(?:parents|prev(?:Until|All))/,
            be = { children: !0, contents: !0, next: !0, prev: !0 };
        re.fn.extend({
            has: function (e) {
                var t = re(e, this),
                    n = t.length;
                return this.filter(function () {
                    for (var e = 0; n > e; e++) if (re.contains(this, t[e])) return !0;
                });
            },
            closest: function (e, t) {
                for (var n, i = 0, o = this.length, r = [], a = he.test(e) || "string" != typeof e ? re(e, t || this.context) : 0; o > i; i++)
                    for (n = this[i]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && re.find.matchesSelector(n, e))) {
                            r.push(n);
                            break;
                        }
                return this.pushStack(r.length > 1 ? re.uniqueSort(r) : r);
            },
            index: function (e) {
                return e ? ("string" == typeof e ? Z.call(re(e), this[0]) : Z.call(this, e.jquery ? e[0] : e)) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
            },
            add: function (e, t) {
                return this.pushStack(re.uniqueSort(re.merge(this.get(), re(e, t))));
            },
            addBack: function (e) {
                return this.add(null == e ? this.prevObject : this.prevObject.filter(e));
            },
        }),
            re.each(
                {
                    parent: function (e) {
                        var t = e.parentNode;
                        return t && 11 !== t.nodeType ? t : null;
                    },
                    parents: function (e) {
                        return de(e, "parentNode");
                    },
                    parentsUntil: function (e, t, n) {
                        return de(e, "parentNode", n);
                    },
                    next: function (e) {
                        return o(e, "nextSibling");
                    },
                    prev: function (e) {
                        return o(e, "previousSibling");
                    },
                    nextAll: function (e) {
                        return de(e, "nextSibling");
                    },
                    prevAll: function (e) {
                        return de(e, "previousSibling");
                    },
                    nextUntil: function (e, t, n) {
                        return de(e, "nextSibling", n);
                    },
                    prevUntil: function (e, t, n) {
                        return de(e, "previousSibling", n);
                    },
                    siblings: function (e) {
                        return pe((e.parentNode || {}).firstChild, e);
                    },
                    children: function (e) {
                        return pe(e.firstChild);
                    },
                    contents: function (e) {
                        return e.contentDocument || re.merge([], e.childNodes);
                    },
                },
                function (e, t) {
                    re.fn[e] = function (n, i) {
                        var o = re.map(this, t, n);
                        return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (o = re.filter(i, o)), this.length > 1 && (be[e] || re.uniqueSort(o), ye.test(e) && o.reverse()), this.pushStack(o);
                    };
                }
            );
        var we = /\S+/g;
        (re.Callbacks = function (e) {
            e = "string" == typeof e ? r(e) : re.extend({}, e);
            var t,
                n,
                i,
                o,
                a = [],
                s = [],
                l = -1,
                u = function () {
                    for (o = e.once, i = t = !0; s.length; l = -1) for (n = s.shift(); ++l < a.length; ) !1 === a[l].apply(n[0], n[1]) && e.stopOnFalse && ((l = a.length), (n = !1));
                    e.memory || (n = !1), (t = !1), o && (a = n ? [] : "");
                },
                c = {
                    add: function () {
                        return (
                            a &&
                            (n && !t && ((l = a.length - 1), s.push(n)),
                                (function t(n) {
                                    re.each(n, function (n, i) {
                                        re.isFunction(i) ? (e.unique && c.has(i)) || a.push(i) : i && i.length && "string" !== re.type(i) && t(i);
                                    });
                                })(arguments),
                            n && !t && u()),
                                this
                        );
                    },
                    remove: function () {
                        return (
                            re.each(arguments, function (e, t) {
                                for (var n; (n = re.inArray(t, a, n)) > -1; ) a.splice(n, 1), l >= n && l--;
                            }),
                                this
                        );
                    },
                    has: function (e) {
                        return e ? re.inArray(e, a) > -1 : a.length > 0;
                    },
                    empty: function () {
                        return a && (a = []), this;
                    },
                    disable: function () {
                        return (o = s = []), (a = n = ""), this;
                    },
                    disabled: function () {
                        return !a;
                    },
                    lock: function () {
                        return (o = s = []), n || (a = n = ""), this;
                    },
                    locked: function () {
                        return !!o;
                    },
                    fireWith: function (e, n) {
                        return o || ((n = n || []), (n = [e, n.slice ? n.slice() : n]), s.push(n), t || u()), this;
                    },
                    fire: function () {
                        return c.fireWith(this, arguments), this;
                    },
                    fired: function () {
                        return !!i;
                    },
                };
            return c;
        }),
            re.extend({
                Deferred: function (e) {
                    var t = [
                            ["resolve", "done", re.Callbacks("once memory"), "resolved"],
                            ["reject", "fail", re.Callbacks("once memory"), "rejected"],
                            ["notify", "progress", re.Callbacks("memory")],
                        ],
                        n = "pending",
                        i = {
                            state: function () {
                                return n;
                            },
                            always: function () {
                                return o.done(arguments).fail(arguments), this;
                            },
                            then: function () {
                                var e = arguments;
                                return re
                                    .Deferred(function (n) {
                                        re.each(t, function (t, r) {
                                            var a = re.isFunction(e[t]) && e[t];
                                            o[r[1]](function () {
                                                var e = a && a.apply(this, arguments);
                                                e && re.isFunction(e.promise) ? e.promise().progress(n.notify).done(n.resolve).fail(n.reject) : n[r[0] + "With"](this === i ? n.promise() : this, a ? [e] : arguments);
                                            });
                                        }),
                                            (e = null);
                                    })
                                    .promise();
                            },
                            promise: function (e) {
                                return null != e ? re.extend(e, i) : i;
                            },
                        },
                        o = {};
                    return (
                        (i.pipe = i.then),
                            re.each(t, function (e, r) {
                                var a = r[2],
                                    s = r[3];
                                (i[r[1]] = a.add),
                                s &&
                                a.add(
                                    function () {
                                        n = s;
                                    },
                                    t[1 ^ e][2].disable,
                                    t[2][2].lock
                                ),
                                    (o[r[0]] = function () {
                                        return o[r[0] + "With"](this === o ? i : this, arguments), this;
                                    }),
                                    (o[r[0] + "With"] = a.fireWith);
                            }),
                            i.promise(o),
                        e && e.call(o, o),
                            o
                    );
                },
                when: function (e) {
                    var t,
                        n,
                        i,
                        o = 0,
                        r = G.call(arguments),
                        a = r.length,
                        s = 1 !== a || (e && re.isFunction(e.promise)) ? a : 0,
                        l = 1 === s ? e : re.Deferred(),
                        u = function (e, n, i) {
                            return function (o) {
                                (n[e] = this), (i[e] = arguments.length > 1 ? G.call(arguments) : o), i === t ? l.notifyWith(n, i) : --s || l.resolveWith(n, i);
                            };
                        };
                    if (a > 1) for (t = new Array(a), n = new Array(a), i = new Array(a); a > o; o++) r[o] && re.isFunction(r[o].promise) ? r[o].promise().progress(u(o, n, t)).done(u(o, i, r)).fail(l.reject) : --s;
                    return s || l.resolveWith(i, r), l.promise();
                },
            });
        var xe;
        (re.fn.ready = function (e) {
            return re.ready.promise().done(e), this;
        }),
            re.extend({
                isReady: !1,
                readyWait: 1,
                holdReady: function (e) {
                    e ? re.readyWait++ : re.ready(!0);
                },
                ready: function (e) {
                    (!0 === e ? --re.readyWait : re.isReady) || ((re.isReady = !0), (!0 !== e && --re.readyWait > 0) || (xe.resolveWith(V, [re]), re.fn.triggerHandler && (re(V).triggerHandler("ready"), re(V).off("ready"))));
                },
            }),
            (re.ready.promise = function (t) {
                return (
                    xe ||
                    ((xe = re.Deferred()),
                        "complete" === V.readyState || ("loading" !== V.readyState && !V.documentElement.doScroll) ? e.setTimeout(re.ready) : (V.addEventListener("DOMContentLoaded", a), e.addEventListener("load", a))),
                        xe.promise(t)
                );
            }),
            re.ready.promise();
        var Te = function (e, t, n, i, o, r, a) {
                var s = 0,
                    l = e.length,
                    u = null == n;
                if ("object" === re.type(n)) {
                    o = !0;
                    for (s in n) Te(e, t, s, n[s], !0, r, a);
                } else if (
                    void 0 !== i &&
                    ((o = !0),
                    re.isFunction(i) || (a = !0),
                    u &&
                    (a
                        ? (t.call(e, i), (t = null))
                        : ((u = t),
                            (t = function (e, t, n) {
                                return u.call(re(e), n);
                            }))),
                        t)
                )
                    for (; l > s; s++) t(e[s], n, a ? i : i.call(e[s], s, t(e[s], n)));
                return o ? e : u ? t.call(e) : l ? t(e[0], n) : r;
            },
            ke = function (e) {
                return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType;
            };
        (s.uid = 1),
            (s.prototype = {
                register: function (e, t) {
                    var n = t || {};
                    return e.nodeType ? (e[this.expando] = n) : Object.defineProperty(e, this.expando, { value: n, writable: !0, configurable: !0 }), e[this.expando];
                },
                cache: function (e) {
                    if (!ke(e)) return {};
                    var t = e[this.expando];
                    return t || ((t = {}), ke(e) && (e.nodeType ? (e[this.expando] = t) : Object.defineProperty(e, this.expando, { value: t, configurable: !0 }))), t;
                },
                set: function (e, t, n) {
                    var i,
                        o = this.cache(e);
                    if ("string" == typeof t) o[t] = n;
                    else for (i in t) o[i] = t[i];
                    return o;
                },
                get: function (e, t) {
                    return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][t];
                },
                access: function (e, t, n) {
                    var i;
                    return void 0 === t || (t && "string" == typeof t && void 0 === n) ? ((i = this.get(e, t)), void 0 !== i ? i : this.get(e, re.camelCase(t))) : (this.set(e, t, n), void 0 !== n ? n : t);
                },
                remove: function (e, t) {
                    var n,
                        i,
                        o,
                        r = e[this.expando];
                    if (void 0 !== r) {
                        if (void 0 === t) this.register(e);
                        else {
                            re.isArray(t) ? (i = t.concat(t.map(re.camelCase))) : ((o = re.camelCase(t)), t in r ? (i = [t, o]) : ((i = o), (i = i in r ? [i] : i.match(we) || []))), (n = i.length);
                            for (; n--; ) delete r[i[n]];
                        }
                        (void 0 === t || re.isEmptyObject(r)) && (e.nodeType ? (e[this.expando] = void 0) : delete e[this.expando]);
                    }
                },
                hasData: function (e) {
                    var t = e[this.expando];
                    return void 0 !== t && !re.isEmptyObject(t);
                },
            });
        var $e = new s(),
            Se = new s(),
            Ce = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
            De = /[A-Z]/g;
        re.extend({
            hasData: function (e) {
                return Se.hasData(e) || $e.hasData(e);
            },
            data: function (e, t, n) {
                return Se.access(e, t, n);
            },
            removeData: function (e, t) {
                Se.remove(e, t);
            },
            _data: function (e, t, n) {
                return $e.access(e, t, n);
            },
            _removeData: function (e, t) {
                $e.remove(e, t);
            },
        }),
            re.fn.extend({
                data: function (e, t) {
                    var n,
                        i,
                        o,
                        r = this[0],
                        a = r && r.attributes;
                    if (void 0 === e) {
                        if (this.length && ((o = Se.get(r)), 1 === r.nodeType && !$e.get(r, "hasDataAttrs"))) {
                            for (n = a.length; n--; ) a[n] && ((i = a[n].name), 0 === i.indexOf("data-") && ((i = re.camelCase(i.slice(5))), l(r, i, o[i])));
                            $e.set(r, "hasDataAttrs", !0);
                        }
                        return o;
                    }
                    return "object" == typeof e
                        ? this.each(function () {
                            Se.set(this, e);
                        })
                        : Te(
                            this,
                            function (t) {
                                var n, i;
                                if (r && void 0 === t) {
                                    if (void 0 !== (n = Se.get(r, e) || Se.get(r, e.replace(De, "-$&").toLowerCase()))) return n;
                                    if (((i = re.camelCase(e)), void 0 !== (n = Se.get(r, i)))) return n;
                                    if (void 0 !== (n = l(r, i, void 0))) return n;
                                } else
                                    (i = re.camelCase(e)),
                                        this.each(function () {
                                            var n = Se.get(this, i);
                                            Se.set(this, i, t), e.indexOf("-") > -1 && void 0 !== n && Se.set(this, e, t);
                                        });
                            },
                            null,
                            t,
                            arguments.length > 1,
                            null,
                            !0
                        );
                },
                removeData: function (e) {
                    return this.each(function () {
                        Se.remove(this, e);
                    });
                },
            }),
            re.extend({
                queue: function (e, t, n) {
                    var i;
                    return e ? ((t = (t || "fx") + "queue"), (i = $e.get(e, t)), n && (!i || re.isArray(n) ? (i = $e.access(e, t, re.makeArray(n))) : i.push(n)), i || []) : void 0;
                },
                dequeue: function (e, t) {
                    t = t || "fx";
                    var n = re.queue(e, t),
                        i = n.length,
                        o = n.shift(),
                        r = re._queueHooks(e, t),
                        a = function () {
                            re.dequeue(e, t);
                        };
                    "inprogress" === o && ((o = n.shift()), i--), o && ("fx" === t && n.unshift("inprogress"), delete r.stop, o.call(e, a, r)), !i && r && r.empty.fire();
                },
                _queueHooks: function (e, t) {
                    var n = t + "queueHooks";
                    return (
                        $e.get(e, n) ||
                        $e.access(e, n, {
                            empty: re.Callbacks("once memory").add(function () {
                                $e.remove(e, [t + "queue", n]);
                            }),
                        })
                    );
                },
            }),
            re.fn.extend({
                queue: function (e, t) {
                    var n = 2;
                    return (
                        "string" != typeof e && ((t = e), (e = "fx"), n--),
                            arguments.length < n
                                ? re.queue(this[0], e)
                                : void 0 === t
                                ? this
                                : this.each(function () {
                                    var n = re.queue(this, e, t);
                                    re._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && re.dequeue(this, e);
                                })
                    );
                },
                dequeue: function (e) {
                    return this.each(function () {
                        re.dequeue(this, e);
                    });
                },
                clearQueue: function (e) {
                    return this.queue(e || "fx", []);
                },
                promise: function (e, t) {
                    var n,
                        i = 1,
                        o = re.Deferred(),
                        r = this,
                        a = this.length,
                        s = function () {
                            --i || o.resolveWith(r, [r]);
                        };
                    for ("string" != typeof e && ((t = e), (e = void 0)), e = e || "fx"; a--; ) (n = $e.get(r[a], e + "queueHooks")) && n.empty && (i++, n.empty.add(s));
                    return s(), o.promise(t);
                },
            });
        var Oe = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            Ae = new RegExp("^(?:([+-])=|)(" + Oe + ")([a-z%]*)$", "i"),
            Ie = ["Top", "Right", "Bottom", "Left"],
            Me = function (e, t) {
                return (e = t || e), "none" === re.css(e, "display") || !re.contains(e.ownerDocument, e);
            },
            Ee = /^(?:checkbox|radio)$/i,
            _e = /<([\w:-]+)/,
            Ne = /^$|\/(?:java|ecma)script/i,
            Pe = {
                option: [1, "<select multiple='multiple'>", "</select>"],
                thead: [1, "<table>", "</table>"],
                col: [2, "<table><colgroup>", "</colgroup></table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: [0, "", ""],
            };
        (Pe.optgroup = Pe.option), (Pe.tbody = Pe.tfoot = Pe.colgroup = Pe.caption = Pe.thead), (Pe.th = Pe.td);
        var je = /<|&#?\w+;/;
        !(function () {
            var e = V.createDocumentFragment(),
                t = e.appendChild(V.createElement("div")),
                n = V.createElement("input");
            n.setAttribute("type", "radio"),
                n.setAttribute("checked", "checked"),
                n.setAttribute("name", "t"),
                t.appendChild(n),
                (ie.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked),
                (t.innerHTML = "<textarea>x</textarea>"),
                (ie.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue);
        })();
        var We = /^key/,
            Fe = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
            He = /^([^.]*)(?:\.(.+)|)/;
        (re.event = {
            global: {},
            add: function (e, t, n, i, o) {
                var r,
                    a,
                    s,
                    l,
                    u,
                    c,
                    d,
                    p,
                    h,
                    f,
                    m,
                    g = $e.get(e);
                if (g)
                    for (
                        n.handler && ((r = n), (n = r.handler), (o = r.selector)),
                        n.guid || (n.guid = re.guid++),
                        (l = g.events) || (l = g.events = {}),
                        (a = g.handle) ||
                        (a = g.handle = function (t) {
                            return void 0 !== re && re.event.triggered !== t.type ? re.event.dispatch.apply(e, arguments) : void 0;
                        }),
                            t = (t || "").match(we) || [""],
                            u = t.length;
                        u--;

                    )
                        (s = He.exec(t[u]) || []),
                            (h = m = s[1]),
                            (f = (s[2] || "").split(".").sort()),
                        h &&
                        ((d = re.event.special[h] || {}),
                            (h = (o ? d.delegateType : d.bindType) || h),
                            (d = re.event.special[h] || {}),
                            (c = re.extend({ type: h, origType: m, data: i, handler: n, guid: n.guid, selector: o, needsContext: o && re.expr.match.needsContext.test(o), namespace: f.join(".") }, r)),
                        (p = l[h]) || ((p = l[h] = []), (p.delegateCount = 0), (d.setup && !1 !== d.setup.call(e, i, f, a)) || (e.addEventListener && e.addEventListener(h, a))),
                        d.add && (d.add.call(e, c), c.handler.guid || (c.handler.guid = n.guid)),
                            o ? p.splice(p.delegateCount++, 0, c) : p.push(c),
                            (re.event.global[h] = !0));
            },
            remove: function (e, t, n, i, o) {
                var r,
                    a,
                    s,
                    l,
                    u,
                    c,
                    d,
                    p,
                    h,
                    f,
                    m,
                    g = $e.hasData(e) && $e.get(e);
                if (g && (l = g.events)) {
                    for (t = (t || "").match(we) || [""], u = t.length; u--; )
                        if (((s = He.exec(t[u]) || []), (h = m = s[1]), (f = (s[2] || "").split(".").sort()), h)) {
                            for (d = re.event.special[h] || {}, h = (i ? d.delegateType : d.bindType) || h, p = l[h] || [], s = s[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = r = p.length; r--; )
                                (c = p[r]),
                                (!o && m !== c.origType) ||
                                (n && n.guid !== c.guid) ||
                                (s && !s.test(c.namespace)) ||
                                (i && i !== c.selector && ("**" !== i || !c.selector)) ||
                                (p.splice(r, 1), c.selector && p.delegateCount--, d.remove && d.remove.call(e, c));
                            a && !p.length && ((d.teardown && !1 !== d.teardown.call(e, f, g.handle)) || re.removeEvent(e, h, g.handle), delete l[h]);
                        } else for (h in l) re.event.remove(e, h + t[u], n, i, !0);
                    re.isEmptyObject(l) && $e.remove(e, "handle events");
                }
            },
            dispatch: function (e) {
                e = re.event.fix(e);
                var t,
                    n,
                    i,
                    o,
                    r,
                    a = [],
                    s = G.call(arguments),
                    l = ($e.get(this, "events") || {})[e.type] || [],
                    u = re.event.special[e.type] || {};
                if (((s[0] = e), (e.delegateTarget = this), !u.preDispatch || !1 !== u.preDispatch.call(this, e))) {
                    for (a = re.event.handlers.call(this, e, l), t = 0; (o = a[t++]) && !e.isPropagationStopped(); )
                        for (e.currentTarget = o.elem, n = 0; (r = o.handlers[n++]) && !e.isImmediatePropagationStopped(); )
                            (e.rnamespace && !e.rnamespace.test(r.namespace)) ||
                            ((e.handleObj = r), (e.data = r.data), void 0 !== (i = ((re.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, s)) && !1 === (e.result = i) && (e.preventDefault(), e.stopPropagation()));
                    return u.postDispatch && u.postDispatch.call(this, e), e.result;
                }
            },
            handlers: function (e, t) {
                var n,
                    i,
                    o,
                    r,
                    a = [],
                    s = t.delegateCount,
                    l = e.target;
                if (s && l.nodeType && ("click" !== e.type || isNaN(e.button) || e.button < 1))
                    for (; l !== this; l = l.parentNode || this)
                        if (1 === l.nodeType && (!0 !== l.disabled || "click" !== e.type)) {
                            for (i = [], n = 0; s > n; n++) (r = t[n]), (o = r.selector + " "), void 0 === i[o] && (i[o] = r.needsContext ? re(o, this).index(l) > -1 : re.find(o, this, null, [l]).length), i[o] && i.push(r);
                            i.length && a.push({ elem: l, handlers: i });
                        }
                return s < t.length && a.push({ elem: this, handlers: t.slice(s) }), a;
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function (e, t) {
                    return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e;
                },
            },
            mouseHooks: {
                props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function (e, t) {
                    var n,
                        i,
                        o,
                        r = t.button;
                    return (
                        null == e.pageX &&
                        null != t.clientX &&
                        ((n = e.target.ownerDocument || V),
                            (i = n.documentElement),
                            (o = n.body),
                            (e.pageX = t.clientX + ((i && i.scrollLeft) || (o && o.scrollLeft) || 0) - ((i && i.clientLeft) || (o && o.clientLeft) || 0)),
                            (e.pageY = t.clientY + ((i && i.scrollTop) || (o && o.scrollTop) || 0) - ((i && i.clientTop) || (o && o.clientTop) || 0))),
                        e.which || void 0 === r || (e.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0),
                            e
                    );
                },
            },
            fix: function (e) {
                if (e[re.expando]) return e;
                var t,
                    n,
                    i,
                    o = e.type,
                    r = e,
                    a = this.fixHooks[o];
                for (a || (this.fixHooks[o] = a = Fe.test(o) ? this.mouseHooks : We.test(o) ? this.keyHooks : {}), i = a.props ? this.props.concat(a.props) : this.props, e = new re.Event(r), t = i.length; t--; ) (n = i[t]), (e[n] = r[n]);
                return e.target || (e.target = V), 3 === e.target.nodeType && (e.target = e.target.parentNode), a.filter ? a.filter(e, r) : e;
            },
            special: {
                load: { noBubble: !0 },
                focus: {
                    trigger: function () {
                        return this !== m() && this.focus ? (this.focus(), !1) : void 0;
                    },
                    delegateType: "focusin",
                },
                blur: {
                    trigger: function () {
                        return this === m() && this.blur ? (this.blur(), !1) : void 0;
                    },
                    delegateType: "focusout",
                },
                click: {
                    trigger: function () {
                        return "checkbox" === this.type && this.click && re.nodeName(this, "input") ? (this.click(), !1) : void 0;
                    },
                    _default: function (e) {
                        return re.nodeName(e.target, "a");
                    },
                },
                beforeunload: {
                    postDispatch: function (e) {
                        void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result);
                    },
                },
            },
        }),
            (re.removeEvent = function (e, t, n) {
                e.removeEventListener && e.removeEventListener(t, n);
            }),
            (re.Event = function (e, t) {
                return this instanceof re.Event
                    ? (e && e.type ? ((this.originalEvent = e), (this.type = e.type), (this.isDefaultPrevented = e.defaultPrevented || (void 0 === e.defaultPrevented && !1 === e.returnValue) ? h : f)) : (this.type = e),
                    t && re.extend(this, t),
                        (this.timeStamp = (e && e.timeStamp) || re.now()),
                        void (this[re.expando] = !0))
                    : new re.Event(e, t);
            }),
            (re.Event.prototype = {
                constructor: re.Event,
                isDefaultPrevented: f,
                isPropagationStopped: f,
                isImmediatePropagationStopped: f,
                preventDefault: function () {
                    var e = this.originalEvent;
                    (this.isDefaultPrevented = h), e && e.preventDefault();
                },
                stopPropagation: function () {
                    var e = this.originalEvent;
                    (this.isPropagationStopped = h), e && e.stopPropagation();
                },
                stopImmediatePropagation: function () {
                    var e = this.originalEvent;
                    (this.isImmediatePropagationStopped = h), e && e.stopImmediatePropagation(), this.stopPropagation();
                },
            }),
            re.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function (e, t) {
                re.event.special[e] = {
                    delegateType: t,
                    bindType: t,
                    handle: function (e) {
                        var n,
                            i = this,
                            o = e.relatedTarget,
                            r = e.handleObj;
                        return (o && (o === i || re.contains(i, o))) || ((e.type = r.origType), (n = r.handler.apply(this, arguments)), (e.type = t)), n;
                    },
                };
            }),
            re.fn.extend({
                on: function (e, t, n, i) {
                    return g(this, e, t, n, i);
                },
                one: function (e, t, n, i) {
                    return g(this, e, t, n, i, 1);
                },
                off: function (e, t, n) {
                    var i, o;
                    if (e && e.preventDefault && e.handleObj) return (i = e.handleObj), re(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
                    if ("object" == typeof e) {
                        for (o in e) this.off(o, t, e[o]);
                        return this;
                    }
                    return (
                        (!1 !== t && "function" != typeof t) || ((n = t), (t = void 0)),
                        !1 === n && (n = f),
                            this.each(function () {
                                re.event.remove(this, e, n, t);
                            })
                    );
                },
            });
        var Re = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
            Le = /<script|<style|<link/i,
            qe = /checked\s*(?:[^=]|=\s*.checked.)/i,
            ze = /^true\/(.*)/,
            Ye = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
        re.extend({
            htmlPrefilter: function (e) {
                return e.replace(Re, "<$1></$2>");
            },
            clone: function (e, t, n) {
                var i,
                    o,
                    r,
                    a,
                    s = e.cloneNode(!0),
                    l = re.contains(e.ownerDocument, e);
                if (!(ie.noCloneChecked || (1 !== e.nodeType && 11 !== e.nodeType) || re.isXMLDoc(e))) for (a = c(s), r = c(e), i = 0, o = r.length; o > i; i++) x(r[i], a[i]);
                if (t)
                    if (n) for (r = r || c(e), a = a || c(s), i = 0, o = r.length; o > i; i++) w(r[i], a[i]);
                    else w(e, s);
                return (a = c(s, "script")), a.length > 0 && d(a, !l && c(e, "script")), s;
            },
            cleanData: function (e) {
                for (var t, n, i, o = re.event.special, r = 0; void 0 !== (n = e[r]); r++)
                    if (ke(n)) {
                        if ((t = n[$e.expando])) {
                            if (t.events) for (i in t.events) o[i] ? re.event.remove(n, i) : re.removeEvent(n, i, t.handle);
                            n[$e.expando] = void 0;
                        }
                        n[Se.expando] && (n[Se.expando] = void 0);
                    }
            },
        }),
            re.fn.extend({
                domManip: T,
                detach: function (e) {
                    return k(this, e, !0);
                },
                remove: function (e) {
                    return k(this, e);
                },
                text: function (e) {
                    return Te(
                        this,
                        function (e) {
                            return void 0 === e
                                ? re.text(this)
                                : this.empty().each(function () {
                                    (1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || (this.textContent = e);
                                });
                        },
                        null,
                        e,
                        arguments.length
                    );
                },
                append: function () {
                    return T(this, arguments, function (e) {
                        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                            v(this, e).appendChild(e);
                        }
                    });
                },
                prepend: function () {
                    return T(this, arguments, function (e) {
                        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                            var t = v(this, e);
                            t.insertBefore(e, t.firstChild);
                        }
                    });
                },
                before: function () {
                    return T(this, arguments, function (e) {
                        this.parentNode && this.parentNode.insertBefore(e, this);
                    });
                },
                after: function () {
                    return T(this, arguments, function (e) {
                        this.parentNode && this.parentNode.insertBefore(e, this.nextSibling);
                    });
                },
                empty: function () {
                    for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (re.cleanData(c(e, !1)), (e.textContent = ""));
                    return this;
                },
                clone: function (e, t) {
                    return (
                        (e = null != e && e),
                            (t = null == t ? e : t),
                            this.map(function () {
                                return re.clone(this, e, t);
                            })
                    );
                },
                html: function (e) {
                    return Te(
                        this,
                        function (e) {
                            var t = this[0] || {},
                                n = 0,
                                i = this.length;
                            if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                            if ("string" == typeof e && !Le.test(e) && !Pe[(_e.exec(e) || ["", ""])[1].toLowerCase()]) {
                                e = re.htmlPrefilter(e);
                                try {
                                    for (; i > n; n++) (t = this[n] || {}), 1 === t.nodeType && (re.cleanData(c(t, !1)), (t.innerHTML = e));
                                    t = 0;
                                } catch (e) {}
                            }
                            t && this.empty().append(e);
                        },
                        null,
                        e,
                        arguments.length
                    );
                },
                replaceWith: function () {
                    var e = [];
                    return T(
                        this,
                        arguments,
                        function (t) {
                            var n = this.parentNode;
                            re.inArray(this, e) < 0 && (re.cleanData(c(this)), n && n.replaceChild(t, this));
                        },
                        e
                    );
                },
            }),
            re.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (e, t) {
                re.fn[e] = function (e) {
                    for (var n, i = [], o = re(e), r = o.length - 1, a = 0; r >= a; a++) (n = a === r ? this : this.clone(!0)), re(o[a])[t](n), Q.apply(i, n.get());
                    return this.pushStack(i);
                };
            });
        var Be,
            Ue = { HTML: "block", BODY: "block" },
            Xe = /^margin/,
            Je = new RegExp("^(" + Oe + ")(?!px)[a-z%]+$", "i"),
            Ve = function (t) {
                var n = t.ownerDocument.defaultView;
                return (n && n.opener) || (n = e), n.getComputedStyle(t);
            },
            Ge = function (e, t, n, i) {
                var o,
                    r,
                    a = {};
                for (r in t) (a[r] = e.style[r]), (e.style[r] = t[r]);
                o = n.apply(e, i || []);
                for (r in t) e.style[r] = a[r];
                return o;
            },
            Ke = V.documentElement;
        !(function () {
            function t() {
                (s.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%"), (s.innerHTML = ""), Ke.appendChild(a);
                var t = e.getComputedStyle(s);
                (n = "1%" !== t.top), (r = "2px" === t.marginLeft), (i = "4px" === t.width), (s.style.marginRight = "50%"), (o = "4px" === t.marginRight), Ke.removeChild(a);
            }
            var n,
                i,
                o,
                r,
                a = V.createElement("div"),
                s = V.createElement("div");
            s.style &&
            ((s.style.backgroundClip = "content-box"),
                (s.cloneNode(!0).style.backgroundClip = ""),
                (ie.clearCloneStyle = "content-box" === s.style.backgroundClip),
                (a.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute"),
                a.appendChild(s),
                re.extend(ie, {
                    pixelPosition: function () {
                        return t(), n;
                    },
                    boxSizingReliable: function () {
                        return null == i && t(), i;
                    },
                    pixelMarginRight: function () {
                        return null == i && t(), o;
                    },
                    reliableMarginLeft: function () {
                        return null == i && t(), r;
                    },
                    reliableMarginRight: function () {
                        var t,
                            n = s.appendChild(V.createElement("div"));
                        return (
                            (n.style.cssText = s.style.cssText = "-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0"),
                                (n.style.marginRight = n.style.width = "0"),
                                (s.style.width = "1px"),
                                Ke.appendChild(a),
                                (t = !parseFloat(e.getComputedStyle(n).marginRight)),
                                Ke.removeChild(a),
                                s.removeChild(n),
                                t
                        );
                    },
                }));
        })();
        var Qe = /^(none|table(?!-c[ea]).+)/,
            Ze = { position: "absolute", visibility: "hidden", display: "block" },
            et = { letterSpacing: "0", fontWeight: "400" },
            tt = ["Webkit", "O", "Moz", "ms"],
            nt = V.createElement("div").style;
        re.extend({
            cssHooks: {
                opacity: {
                    get: function (e, t) {
                        if (t) {
                            var n = C(e, "opacity");
                            return "" === n ? "1" : n;
                        }
                    },
                },
            },
            cssNumber: { animationIterationCount: !0, columnCount: !0, fillOpacity: !0, flexGrow: !0, flexShrink: !0, fontWeight: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 },
            cssProps: { float: "cssFloat" },
            style: function (e, t, n, i) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var o,
                        r,
                        a,
                        s = re.camelCase(t),
                        l = e.style;
                    return (
                        (t = re.cssProps[s] || (re.cssProps[s] = O(s) || s)),
                            (a = re.cssHooks[t] || re.cssHooks[s]),
                            void 0 === n
                                ? a && "get" in a && void 0 !== (o = a.get(e, !1, i))
                                ? o
                                : l[t]
                                : ((r = typeof n),
                                "string" === r && (o = Ae.exec(n)) && o[1] && ((n = u(e, t, o)), (r = "number")),
                                    void (
                                        null != n &&
                                        n === n &&
                                        ("number" === r && (n += (o && o[3]) || (re.cssNumber[s] ? "" : "px")),
                                        ie.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"),
                                        (a && "set" in a && void 0 === (n = a.set(e, n, i))) || (l[t] = n))
                                    ))
                    );
                }
            },
            css: function (e, t, n, i) {
                var o,
                    r,
                    a,
                    s = re.camelCase(t);
                return (
                    (t = re.cssProps[s] || (re.cssProps[s] = O(s) || s)),
                        (a = re.cssHooks[t] || re.cssHooks[s]),
                    a && "get" in a && (o = a.get(e, !0, n)),
                    void 0 === o && (o = C(e, t, i)),
                    "normal" === o && t in et && (o = et[t]),
                        "" === n || n ? ((r = parseFloat(o)), !0 === n || isFinite(r) ? r || 0 : o) : o
                );
            },
        }),
            re.each(["height", "width"], function (e, t) {
                re.cssHooks[t] = {
                    get: function (e, n, i) {
                        return n
                            ? Qe.test(re.css(e, "display")) && 0 === e.offsetWidth
                                ? Ge(e, Ze, function () {
                                    return M(e, t, i);
                                })
                                : M(e, t, i)
                            : void 0;
                    },
                    set: function (e, n, i) {
                        var o,
                            r = i && Ve(e),
                            a = i && I(e, t, i, "border-box" === re.css(e, "boxSizing", !1, r), r);
                        return a && (o = Ae.exec(n)) && "px" !== (o[3] || "px") && ((e.style[t] = n), (n = re.css(e, t))), A(e, n, a);
                    },
                };
            }),
            (re.cssHooks.marginLeft = D(ie.reliableMarginLeft, function (e, t) {
                return t
                    ? (parseFloat(C(e, "marginLeft")) ||
                    e.getBoundingClientRect().left -
                    Ge(e, { marginLeft: 0 }, function () {
                        return e.getBoundingClientRect().left;
                    })) + "px"
                    : void 0;
            })),
            (re.cssHooks.marginRight = D(ie.reliableMarginRight, function (e, t) {
                return t ? Ge(e, { display: "inline-block" }, C, [e, "marginRight"]) : void 0;
            })),
            re.each({ margin: "", padding: "", border: "Width" }, function (e, t) {
                (re.cssHooks[e + t] = {
                    expand: function (n) {
                        for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++) o[e + Ie[i] + t] = r[i] || r[i - 2] || r[0];
                        return o;
                    },
                }),
                Xe.test(e) || (re.cssHooks[e + t].set = A);
            }),
            re.fn.extend({
                css: function (e, t) {
                    return Te(
                        this,
                        function (e, t, n) {
                            var i,
                                o,
                                r = {},
                                a = 0;
                            if (re.isArray(t)) {
                                for (i = Ve(e), o = t.length; o > a; a++) r[t[a]] = re.css(e, t[a], !1, i);
                                return r;
                            }
                            return void 0 !== n ? re.style(e, t, n) : re.css(e, t);
                        },
                        e,
                        t,
                        arguments.length > 1
                    );
                },
                show: function () {
                    return E(this, !0);
                },
                hide: function () {
                    return E(this);
                },
                toggle: function (e) {
                    return "boolean" == typeof e
                        ? e
                            ? this.show()
                            : this.hide()
                        : this.each(function () {
                            Me(this) ? re(this).show() : re(this).hide();
                        });
                },
            }),
            (re.Tween = _),
            (_.prototype = {
                constructor: _,
                init: function (e, t, n, i, o, r) {
                    (this.elem = e), (this.prop = n), (this.easing = o || re.easing._default), (this.options = t), (this.start = this.now = this.cur()), (this.end = i), (this.unit = r || (re.cssNumber[n] ? "" : "px"));
                },
                cur: function () {
                    var e = _.propHooks[this.prop];
                    return e && e.get ? e.get(this) : _.propHooks._default.get(this);
                },
                run: function (e) {
                    var t,
                        n = _.propHooks[this.prop];
                    return (
                        this.options.duration ? (this.pos = t = re.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration)) : (this.pos = t = e),
                            (this.now = (this.end - this.start) * t + this.start),
                        this.options.step && this.options.step.call(this.elem, this.now, this),
                            n && n.set ? n.set(this) : _.propHooks._default.set(this),
                            this
                    );
                },
            }),
            (_.prototype.init.prototype = _.prototype),
            (_.propHooks = {
                _default: {
                    get: function (e) {
                        var t;
                        return 1 !== e.elem.nodeType || (null != e.elem[e.prop] && null == e.elem.style[e.prop]) ? e.elem[e.prop] : ((t = re.css(e.elem, e.prop, "")), t && "auto" !== t ? t : 0);
                    },
                    set: function (e) {
                        re.fx.step[e.prop] ? re.fx.step[e.prop](e) : 1 !== e.elem.nodeType || (null == e.elem.style[re.cssProps[e.prop]] && !re.cssHooks[e.prop]) ? (e.elem[e.prop] = e.now) : re.style(e.elem, e.prop, e.now + e.unit);
                    },
                },
            }),
            (_.propHooks.scrollTop = _.propHooks.scrollLeft = {
                set: function (e) {
                    e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
                },
            }),
            (re.easing = {
                linear: function (e) {
                    return e;
                },
                swing: function (e) {
                    return 0.5 - Math.cos(e * Math.PI) / 2;
                },
                _default: "swing",
            }),
            (re.fx = _.prototype.init),
            (re.fx.step = {});
        var it,
            ot,
            rt = /^(?:toggle|show|hide)$/,
            at = /queueHooks$/;
        (re.Animation = re.extend(H, {
            tweeners: {
                "*": [
                    function (e, t) {
                        var n = this.createTween(e, t);
                        return u(n.elem, e, Ae.exec(t), n), n;
                    },
                ],
            },
            tweener: function (e, t) {
                re.isFunction(e) ? ((t = e), (e = ["*"])) : (e = e.match(we));
                for (var n, i = 0, o = e.length; o > i; i++) (n = e[i]), (H.tweeners[n] = H.tweeners[n] || []), H.tweeners[n].unshift(t);
            },
            prefilters: [W],
            prefilter: function (e, t) {
                t ? H.prefilters.unshift(e) : H.prefilters.push(e);
            },
        })),
            (re.speed = function (e, t, n) {
                var i = e && "object" == typeof e ? re.extend({}, e) : { complete: n || (!n && t) || (re.isFunction(e) && e), duration: e, easing: (n && t) || (t && !re.isFunction(t) && t) };
                return (
                    (i.duration = re.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in re.fx.speeds ? re.fx.speeds[i.duration] : re.fx.speeds._default),
                    (null != i.queue && !0 !== i.queue) || (i.queue = "fx"),
                        (i.old = i.complete),
                        (i.complete = function () {
                            re.isFunction(i.old) && i.old.call(this), i.queue && re.dequeue(this, i.queue);
                        }),
                        i
                );
            }),
            re.fn.extend({
                fadeTo: function (e, t, n, i) {
                    return this.filter(Me).css("opacity", 0).show().end().animate({ opacity: t }, e, n, i);
                },
                animate: function (e, t, n, i) {
                    var o = re.isEmptyObject(e),
                        r = re.speed(t, n, i),
                        a = function () {
                            var t = H(this, re.extend({}, e), r);
                            (o || $e.get(this, "finish")) && t.stop(!0);
                        };
                    return (a.finish = a), o || !1 === r.queue ? this.each(a) : this.queue(r.queue, a);
                },
                stop: function (e, t, n) {
                    var i = function (e) {
                        var t = e.stop;
                        delete e.stop, t(n);
                    };
                    return (
                        "string" != typeof e && ((n = t), (t = e), (e = void 0)),
                        t && !1 !== e && this.queue(e || "fx", []),
                            this.each(function () {
                                var t = !0,
                                    o = null != e && e + "queueHooks",
                                    r = re.timers,
                                    a = $e.get(this);
                                if (o) a[o] && a[o].stop && i(a[o]);
                                else for (o in a) a[o] && a[o].stop && at.test(o) && i(a[o]);
                                for (o = r.length; o--; ) r[o].elem !== this || (null != e && r[o].queue !== e) || (r[o].anim.stop(n), (t = !1), r.splice(o, 1));
                                (!t && n) || re.dequeue(this, e);
                            })
                    );
                },
                finish: function (e) {
                    return (
                        !1 !== e && (e = e || "fx"),
                            this.each(function () {
                                var t,
                                    n = $e.get(this),
                                    i = n[e + "queue"],
                                    o = n[e + "queueHooks"],
                                    r = re.timers,
                                    a = i ? i.length : 0;
                                for (n.finish = !0, re.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = r.length; t--; ) r[t].elem === this && r[t].queue === e && (r[t].anim.stop(!0), r.splice(t, 1));
                                for (t = 0; a > t; t++) i[t] && i[t].finish && i[t].finish.call(this);
                                delete n.finish;
                            })
                    );
                },
            }),
            re.each(["toggle", "show", "hide"], function (e, t) {
                var n = re.fn[t];
                re.fn[t] = function (e, i, o) {
                    return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(P(t, !0), e, i, o);
                };
            }),
            re.each({ slideDown: P("show"), slideUp: P("hide"), slideToggle: P("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function (e, t) {
                re.fn[e] = function (e, n, i) {
                    return this.animate(t, e, n, i);
                };
            }),
            (re.timers = []),
            (re.fx.tick = function () {
                var e,
                    t = 0,
                    n = re.timers;
                for (it = re.now(); t < n.length; t++) (e = n[t])() || n[t] !== e || n.splice(t--, 1);
                n.length || re.fx.stop(), (it = void 0);
            }),
            (re.fx.timer = function (e) {
                re.timers.push(e), e() ? re.fx.start() : re.timers.pop();
            }),
            (re.fx.interval = 13),
            (re.fx.start = function () {
                ot || (ot = e.setInterval(re.fx.tick, re.fx.interval));
            }),
            (re.fx.stop = function () {
                e.clearInterval(ot), (ot = null);
            }),
            (re.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
            (re.fn.delay = function (t, n) {
                return (
                    (t = re.fx ? re.fx.speeds[t] || t : t),
                        (n = n || "fx"),
                        this.queue(n, function (n, i) {
                            var o = e.setTimeout(n, t);
                            i.stop = function () {
                                e.clearTimeout(o);
                            };
                        })
                );
            }),
            (function () {
                var e = V.createElement("input"),
                    t = V.createElement("select"),
                    n = t.appendChild(V.createElement("option"));
                (e.type = "checkbox"),
                    (ie.checkOn = "" !== e.value),
                    (ie.optSelected = n.selected),
                    (t.disabled = !0),
                    (ie.optDisabled = !n.disabled),
                    (e = V.createElement("input")),
                    (e.value = "t"),
                    (e.type = "radio"),
                    (ie.radioValue = "t" === e.value);
            })();
        var st,
            lt = re.expr.attrHandle;
        re.fn.extend({
            attr: function (e, t) {
                return Te(this, re.attr, e, t, arguments.length > 1);
            },
            removeAttr: function (e) {
                return this.each(function () {
                    re.removeAttr(this, e);
                });
            },
        }),
            re.extend({
                attr: function (e, t, n) {
                    var i,
                        o,
                        r = e.nodeType;
                    if (3 !== r && 8 !== r && 2 !== r)
                        return void 0 === e.getAttribute
                            ? re.prop(e, t, n)
                            : ((1 === r && re.isXMLDoc(e)) || ((t = t.toLowerCase()), (o = re.attrHooks[t] || (re.expr.match.bool.test(t) ? st : void 0))),
                                void 0 !== n
                                    ? null === n
                                    ? void re.removeAttr(e, t)
                                    : o && "set" in o && void 0 !== (i = o.set(e, n, t))
                                        ? i
                                        : (e.setAttribute(t, n + ""), n)
                                    : o && "get" in o && null !== (i = o.get(e, t))
                                    ? i
                                    : ((i = re.find.attr(e, t)), null == i ? void 0 : i));
                },
                attrHooks: {
                    type: {
                        set: function (e, t) {
                            if (!ie.radioValue && "radio" === t && re.nodeName(e, "input")) {
                                var n = e.value;
                                return e.setAttribute("type", t), n && (e.value = n), t;
                            }
                        },
                    },
                },
                removeAttr: function (e, t) {
                    var n,
                        i,
                        o = 0,
                        r = t && t.match(we);
                    if (r && 1 === e.nodeType) for (; (n = r[o++]); ) (i = re.propFix[n] || n), re.expr.match.bool.test(n) && (e[i] = !1), e.removeAttribute(n);
                },
            }),
            (st = {
                set: function (e, t, n) {
                    return !1 === t ? re.removeAttr(e, n) : e.setAttribute(n, n), n;
                },
            }),
            re.each(re.expr.match.bool.source.match(/\w+/g), function (e, t) {
                var n = lt[t] || re.find.attr;
                lt[t] = function (e, t, i) {
                    var o, r;
                    return i || ((r = lt[t]), (lt[t] = o), (o = null != n(e, t, i) ? t.toLowerCase() : null), (lt[t] = r)), o;
                };
            });
        var ut = /^(?:input|select|textarea|button)$/i,
            ct = /^(?:a|area)$/i;
        re.fn.extend({
            prop: function (e, t) {
                return Te(this, re.prop, e, t, arguments.length > 1);
            },
            removeProp: function (e) {
                return this.each(function () {
                    delete this[re.propFix[e] || e];
                });
            },
        }),
            re.extend({
                prop: function (e, t, n) {
                    var i,
                        o,
                        r = e.nodeType;
                    if (3 !== r && 8 !== r && 2 !== r)
                        return (
                            (1 === r && re.isXMLDoc(e)) || ((t = re.propFix[t] || t), (o = re.propHooks[t])),
                                void 0 !== n ? (o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : (e[t] = n)) : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]
                        );
                },
                propHooks: {
                    tabIndex: {
                        get: function (e) {
                            var t = re.find.attr(e, "tabindex");
                            return t ? parseInt(t, 10) : ut.test(e.nodeName) || (ct.test(e.nodeName) && e.href) ? 0 : -1;
                        },
                    },
                },
                propFix: { for: "htmlFor", class: "className" },
            }),
        ie.optSelected ||
        (re.propHooks.selected = {
            get: function (e) {
                var t = e.parentNode;
                return t && t.parentNode && t.parentNode.selectedIndex, null;
            },
            set: function (e) {
                var t = e.parentNode;
                t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex);
            },
        }),
            re.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
                re.propFix[this.toLowerCase()] = this;
            });
        var dt = /[\t\r\n\f]/g;
        re.fn.extend({
            addClass: function (e) {
                var t,
                    n,
                    i,
                    o,
                    r,
                    a,
                    s,
                    l = 0;
                if (re.isFunction(e))
                    return this.each(function (t) {
                        re(this).addClass(e.call(this, t, R(this)));
                    });
                if ("string" == typeof e && e)
                    for (t = e.match(we) || []; (n = this[l++]); )
                        if (((o = R(n)), (i = 1 === n.nodeType && (" " + o + " ").replace(dt, " ")))) {
                            for (a = 0; (r = t[a++]); ) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                            (s = re.trim(i)), o !== s && n.setAttribute("class", s);
                        }
                return this;
            },
            removeClass: function (e) {
                var t,
                    n,
                    i,
                    o,
                    r,
                    a,
                    s,
                    l = 0;
                if (re.isFunction(e))
                    return this.each(function (t) {
                        re(this).removeClass(e.call(this, t, R(this)));
                    });
                if (!arguments.length) return this.attr("class", "");
                if ("string" == typeof e && e)
                    for (t = e.match(we) || []; (n = this[l++]); )
                        if (((o = R(n)), (i = 1 === n.nodeType && (" " + o + " ").replace(dt, " ")))) {
                            for (a = 0; (r = t[a++]); ) for (; i.indexOf(" " + r + " ") > -1; ) i = i.replace(" " + r + " ", " ");
                            (s = re.trim(i)), o !== s && n.setAttribute("class", s);
                        }
                return this;
            },
            toggleClass: function (e, t) {
                var n = typeof e;
                return "boolean" == typeof t && "string" === n
                    ? t
                        ? this.addClass(e)
                        : this.removeClass(e)
                    : re.isFunction(e)
                        ? this.each(function (n) {
                            re(this).toggleClass(e.call(this, n, R(this), t), t);
                        })
                        : this.each(function () {
                            var t, i, o, r;
                            if ("string" === n) for (i = 0, o = re(this), r = e.match(we) || []; (t = r[i++]); ) o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
                            else (void 0 !== e && "boolean" !== n) || ((t = R(this)), t && $e.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : $e.get(this, "__className__") || ""));
                        });
            },
            hasClass: function (e) {
                var t,
                    n,
                    i = 0;
                for (t = " " + e + " "; (n = this[i++]); ) if (1 === n.nodeType && (" " + R(n) + " ").replace(dt, " ").indexOf(t) > -1) return !0;
                return !1;
            },
        });
        var pt = /\r/g,
            ht = /[\x20\t\r\n\f]+/g;
        re.fn.extend({
            val: function (e) {
                var t,
                    n,
                    i,
                    o = this[0];
                return arguments.length
                    ? ((i = re.isFunction(e)),
                        this.each(function (n) {
                            var o;
                            1 === this.nodeType &&
                            ((o = i ? e.call(this, n, re(this).val()) : e),
                                null == o
                                    ? (o = "")
                                    : "number" == typeof o
                                    ? (o += "")
                                    : re.isArray(o) &&
                                    (o = re.map(o, function (e) {
                                        return null == e ? "" : e + "";
                                    })),
                            ((t = re.valHooks[this.type] || re.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, o, "value")) || (this.value = o));
                        }))
                    : o
                        ? ((t = re.valHooks[o.type] || re.valHooks[o.nodeName.toLowerCase()]), t && "get" in t && void 0 !== (n = t.get(o, "value")) ? n : ((n = o.value), "string" == typeof n ? n.replace(pt, "") : null == n ? "" : n))
                        : void 0;
            },
        }),
            re.extend({
                valHooks: {
                    option: {
                        get: function (e) {
                            var t = re.find.attr(e, "value");
                            return null != t ? t : re.trim(re.text(e)).replace(ht, " ");
                        },
                    },
                    select: {
                        get: function (e) {
                            for (var t, n, i = e.options, o = e.selectedIndex, r = "select-one" === e.type || 0 > o, a = r ? null : [], s = r ? o + 1 : i.length, l = 0 > o ? s : r ? o : 0; s > l; l++)
                                if (((n = i[l]), (n.selected || l === o) && (ie.optDisabled ? !n.disabled : null === n.getAttribute("disabled")) && (!n.parentNode.disabled || !re.nodeName(n.parentNode, "optgroup")))) {
                                    if (((t = re(n).val()), r)) return t;
                                    a.push(t);
                                }
                            return a;
                        },
                        set: function (e, t) {
                            for (var n, i, o = e.options, r = re.makeArray(t), a = o.length; a--; ) (i = o[a]), (i.selected = re.inArray(re.valHooks.option.get(i), r) > -1) && (n = !0);
                            return n || (e.selectedIndex = -1), r;
                        },
                    },
                },
            }),
            re.each(["radio", "checkbox"], function () {
                (re.valHooks[this] = {
                    set: function (e, t) {
                        return re.isArray(t) ? (e.checked = re.inArray(re(e).val(), t) > -1) : void 0;
                    },
                }),
                ie.checkOn ||
                (re.valHooks[this].get = function (e) {
                    return null === e.getAttribute("value") ? "on" : e.value;
                });
            });
        var ft = /^(?:focusinfocus|focusoutblur)$/;
        re.extend(re.event, {
            trigger: function (t, n, i, o) {
                var r,
                    a,
                    s,
                    l,
                    u,
                    c,
                    d,
                    p = [i || V],
                    h = ne.call(t, "type") ? t.type : t,
                    f = ne.call(t, "namespace") ? t.namespace.split(".") : [];
                if (
                    ((a = s = i = i || V),
                    3 !== i.nodeType &&
                    8 !== i.nodeType &&
                    !ft.test(h + re.event.triggered) &&
                    (h.indexOf(".") > -1 && ((f = h.split(".")), (h = f.shift()), f.sort()),
                        (u = h.indexOf(":") < 0 && "on" + h),
                        (t = t[re.expando] ? t : new re.Event(h, "object" == typeof t && t)),
                        (t.isTrigger = o ? 2 : 3),
                        (t.namespace = f.join(".")),
                        (t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null),
                        (t.result = void 0),
                    t.target || (t.target = i),
                        (n = null == n ? [t] : re.makeArray(n, [t])),
                        (d = re.event.special[h] || {}),
                    o || !d.trigger || !1 !== d.trigger.apply(i, n)))
                ) {
                    if (!o && !d.noBubble && !re.isWindow(i)) {
                        for (l = d.delegateType || h, ft.test(l + h) || (a = a.parentNode); a; a = a.parentNode) p.push(a), (s = a);
                        s === (i.ownerDocument || V) && p.push(s.defaultView || s.parentWindow || e);
                    }
                    for (r = 0; (a = p[r++]) && !t.isPropagationStopped(); )
                        (t.type = r > 1 ? l : d.bindType || h),
                            (c = ($e.get(a, "events") || {})[t.type] && $e.get(a, "handle")),
                        c && c.apply(a, n),
                        (c = u && a[u]) && c.apply && ke(a) && ((t.result = c.apply(a, n)), !1 === t.result && t.preventDefault());
                    return (
                        (t.type = h),
                        o ||
                        t.isDefaultPrevented() ||
                        (d._default && !1 !== d._default.apply(p.pop(), n)) ||
                        !ke(i) ||
                        (u && re.isFunction(i[h]) && !re.isWindow(i) && ((s = i[u]), s && (i[u] = null), (re.event.triggered = h), i[h](), (re.event.triggered = void 0), s && (i[u] = s))),
                            t.result
                    );
                }
            },
            simulate: function (e, t, n) {
                var i = re.extend(new re.Event(), n, { type: e, isSimulated: !0 });
                re.event.trigger(i, null, t), i.isDefaultPrevented() && n.preventDefault();
            },
        }),
            re.fn.extend({
                trigger: function (e, t) {
                    return this.each(function () {
                        re.event.trigger(e, t, this);
                    });
                },
                triggerHandler: function (e, t) {
                    var n = this[0];
                    return n ? re.event.trigger(e, t, n, !0) : void 0;
                },
            }),
            re.each(
                "blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),
                function (e, t) {
                    re.fn[t] = function (e, n) {
                        return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t);
                    };
                }
            ),
            re.fn.extend({
                hover: function (e, t) {
                    return this.mouseenter(e).mouseleave(t || e);
                },
            }),
            (ie.focusin = "onfocusin" in e),
        ie.focusin ||
        re.each({ focus: "focusin", blur: "focusout" }, function (e, t) {
            var n = function (e) {
                re.event.simulate(t, e.target, re.event.fix(e));
            };
            re.event.special[t] = {
                setup: function () {
                    var i = this.ownerDocument || this,
                        o = $e.access(i, t);
                    o || i.addEventListener(e, n, !0), $e.access(i, t, (o || 0) + 1);
                },
                teardown: function () {
                    var i = this.ownerDocument || this,
                        o = $e.access(i, t) - 1;
                    o ? $e.access(i, t, o) : (i.removeEventListener(e, n, !0), $e.remove(i, t));
                },
            };
        });
        var mt = e.location,
            gt = re.now(),
            vt = /\?/;
        (re.parseJSON = function (e) {
            return JSON.parse(e + "");
        }),
            (re.parseXML = function (t) {
                var n;
                if (!t || "string" != typeof t) return null;
                try {
                    n = new e.DOMParser().parseFromString(t, "text/xml");
                } catch (e) {
                    n = void 0;
                }
                return (n && !n.getElementsByTagName("parsererror").length) || re.error("Invalid XML: " + t), n;
            });
        var yt = /#.*$/,
            bt = /([?&])_=[^&]*/,
            wt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
            xt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
            Tt = /^(?:GET|HEAD)$/,
            kt = /^\/\//,
            $t = {},
            St = {},
            Ct = "*/".concat("*"),
            Dt = V.createElement("a");
        (Dt.href = mt.href),
            re.extend({
                active: 0,
                lastModified: {},
                etag: {},
                ajaxSettings: {
                    url: mt.href,
                    type: "GET",
                    isLocal: xt.test(mt.protocol),
                    global: !0,
                    processData: !0,
                    async: !0,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    accepts: { "*": Ct, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" },
                    contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
                    responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" },
                    converters: { "* text": String, "text html": !0, "text json": re.parseJSON, "text xml": re.parseXML },
                    flatOptions: { url: !0, context: !0 },
                },
                ajaxSetup: function (e, t) {
                    return t ? z(z(e, re.ajaxSettings), t) : z(re.ajaxSettings, e);
                },
                ajaxPrefilter: L($t),
                ajaxTransport: L(St),
                ajax: function (t, n) {
                    function i(t, n, i, s) {
                        var u,
                            d,
                            y,
                            b,
                            x,
                            k = n;
                        2 !== w &&
                        ((w = 2),
                        l && e.clearTimeout(l),
                            (o = void 0),
                            (a = s || ""),
                            (T.readyState = t > 0 ? 4 : 0),
                            (u = (t >= 200 && 300 > t) || 304 === t),
                        i && (b = Y(p, T, i)),
                            (b = B(p, b, T, u)),
                            u
                                ? (p.ifModified && ((x = T.getResponseHeader("Last-Modified")), x && (re.lastModified[r] = x), (x = T.getResponseHeader("etag")) && (re.etag[r] = x)),
                                    204 === t || "HEAD" === p.type ? (k = "nocontent") : 304 === t ? (k = "notmodified") : ((k = b.state), (d = b.data), (y = b.error), (u = !y)))
                                : ((y = k), (!t && k) || ((k = "error"), 0 > t && (t = 0))),
                            (T.status = t),
                            (T.statusText = (n || k) + ""),
                            u ? m.resolveWith(h, [d, k, T]) : m.rejectWith(h, [T, k, y]),
                            T.statusCode(v),
                            (v = void 0),
                        c && f.trigger(u ? "ajaxSuccess" : "ajaxError", [T, p, u ? d : y]),
                            g.fireWith(h, [T, k]),
                        c && (f.trigger("ajaxComplete", [T, p]), --re.active || re.event.trigger("ajaxStop")));
                    }
                    "object" == typeof t && ((n = t), (t = void 0)), (n = n || {});
                    var o,
                        r,
                        a,
                        s,
                        l,
                        u,
                        c,
                        d,
                        p = re.ajaxSetup({}, n),
                        h = p.context || p,
                        f = p.context && (h.nodeType || h.jquery) ? re(h) : re.event,
                        m = re.Deferred(),
                        g = re.Callbacks("once memory"),
                        v = p.statusCode || {},
                        y = {},
                        b = {},
                        w = 0,
                        x = "canceled",
                        T = {
                            readyState: 0,
                            getResponseHeader: function (e) {
                                var t;
                                if (2 === w) {
                                    if (!s) for (s = {}; (t = wt.exec(a)); ) s[t[1].toLowerCase()] = t[2];
                                    t = s[e.toLowerCase()];
                                }
                                return null == t ? null : t;
                            },
                            getAllResponseHeaders: function () {
                                return 2 === w ? a : null;
                            },
                            setRequestHeader: function (e, t) {
                                var n = e.toLowerCase();
                                return w || ((e = b[n] = b[n] || e), (y[e] = t)), this;
                            },
                            overrideMimeType: function (e) {
                                return w || (p.mimeType = e), this;
                            },
                            statusCode: function (e) {
                                var t;
                                if (e)
                                    if (2 > w) for (t in e) v[t] = [v[t], e[t]];
                                    else T.always(e[T.status]);
                                return this;
                            },
                            abort: function (e) {
                                var t = e || x;
                                return o && o.abort(t), i(0, t), this;
                            },
                        };
                    if (
                        ((m.promise(T).complete = g.add),
                            (T.success = T.done),
                            (T.error = T.fail),
                            (p.url = ((t || p.url || mt.href) + "").replace(yt, "").replace(kt, mt.protocol + "//")),
                            (p.type = n.method || n.type || p.method || p.type),
                            (p.dataTypes = re
                                .trim(p.dataType || "*")
                                .toLowerCase()
                                .match(we) || [""]),
                        null == p.crossDomain)
                    ) {
                        u = V.createElement("a");
                        try {
                            (u.href = p.url), (u.href = u.href), (p.crossDomain = Dt.protocol + "//" + Dt.host != u.protocol + "//" + u.host);
                        } catch (e) {
                            p.crossDomain = !0;
                        }
                    }
                    if ((p.data && p.processData && "string" != typeof p.data && (p.data = re.param(p.data, p.traditional)), q($t, p, n, T), 2 === w)) return T;
                    (c = re.event && p.global),
                    c && 0 == re.active++ && re.event.trigger("ajaxStart"),
                        (p.type = p.type.toUpperCase()),
                        (p.hasContent = !Tt.test(p.type)),
                        (r = p.url),
                    p.hasContent || (p.data && ((r = p.url += (vt.test(r) ? "&" : "?") + p.data), delete p.data), !1 === p.cache && (p.url = bt.test(r) ? r.replace(bt, "$1_=" + gt++) : r + (vt.test(r) ? "&" : "?") + "_=" + gt++)),
                    p.ifModified && (re.lastModified[r] && T.setRequestHeader("If-Modified-Since", re.lastModified[r]), re.etag[r] && T.setRequestHeader("If-None-Match", re.etag[r])),
                    ((p.data && p.hasContent && !1 !== p.contentType) || n.contentType) && T.setRequestHeader("Content-Type", p.contentType),
                        T.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + Ct + "; q=0.01" : "") : p.accepts["*"]);
                    for (d in p.headers) T.setRequestHeader(d, p.headers[d]);
                    if (p.beforeSend && (!1 === p.beforeSend.call(h, T, p) || 2 === w)) return T.abort();
                    x = "abort";
                    for (d in { success: 1, error: 1, complete: 1 }) T[d](p[d]);
                    if ((o = q(St, p, n, T))) {
                        if (((T.readyState = 1), c && f.trigger("ajaxSend", [T, p]), 2 === w)) return T;
                        p.async &&
                        p.timeout > 0 &&
                        (l = e.setTimeout(function () {
                            T.abort("timeout");
                        }, p.timeout));
                        try {
                            (w = 1), o.send(y, i);
                        } catch (e) {
                            if (!(2 > w)) throw e;
                            i(-1, e);
                        }
                    } else i(-1, "No Transport");
                    return T;
                },
                getJSON: function (e, t, n) {
                    return re.get(e, t, n, "json");
                },
                getScript: function (e, t) {
                    return re.get(e, void 0, t, "script");
                },
            }),
            re.each(["get", "post"], function (e, t) {
                re[t] = function (e, n, i, o) {
                    return re.isFunction(n) && ((o = o || i), (i = n), (n = void 0)), re.ajax(re.extend({ url: e, type: t, dataType: o, data: n, success: i }, re.isPlainObject(e) && e));
                };
            }),
            (re._evalUrl = function (e) {
                return re.ajax({ url: e, type: "GET", dataType: "script", async: !1, global: !1, throws: !0 });
            }),
            re.fn.extend({
                wrapAll: function (e) {
                    var t;
                    return re.isFunction(e)
                        ? this.each(function (t) {
                            re(this).wrapAll(e.call(this, t));
                        })
                        : (this[0] &&
                        ((t = re(e, this[0].ownerDocument).eq(0).clone(!0)),
                        this[0].parentNode && t.insertBefore(this[0]),
                            t
                                .map(function () {
                                    for (var e = this; e.firstElementChild; ) e = e.firstElementChild;
                                    return e;
                                })
                                .append(this)),
                            this);
                },
                wrapInner: function (e) {
                    return re.isFunction(e)
                        ? this.each(function (t) {
                            re(this).wrapInner(e.call(this, t));
                        })
                        : this.each(function () {
                            var t = re(this),
                                n = t.contents();
                            n.length ? n.wrapAll(e) : t.append(e);
                        });
                },
                wrap: function (e) {
                    var t = re.isFunction(e);
                    return this.each(function (n) {
                        re(this).wrapAll(t ? e.call(this, n) : e);
                    });
                },
                unwrap: function () {
                    return this.parent()
                        .each(function () {
                            re.nodeName(this, "body") || re(this).replaceWith(this.childNodes);
                        })
                        .end();
                },
            }),
            (re.expr.filters.hidden = function (e) {
                return !re.expr.filters.visible(e);
            }),
            (re.expr.filters.visible = function (e) {
                return e.offsetWidth > 0 || e.offsetHeight > 0 || e.getClientRects().length > 0;
            });
        var Ot = /%20/g,
            At = /\[\]$/,
            It = /\r?\n/g,
            Mt = /^(?:submit|button|image|reset|file)$/i,
            Et = /^(?:input|select|textarea|keygen)/i;
        (re.param = function (e, t) {
            var n,
                i = [],
                o = function (e, t) {
                    (t = re.isFunction(t) ? t() : null == t ? "" : t), (i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t));
                };
            if ((void 0 === t && (t = re.ajaxSettings && re.ajaxSettings.traditional), re.isArray(e) || (e.jquery && !re.isPlainObject(e))))
                re.each(e, function () {
                    o(this.name, this.value);
                });
            else for (n in e) U(n, e[n], t, o);
            return i.join("&").replace(Ot, "+");
        }),
            re.fn.extend({
                serialize: function () {
                    return re.param(this.serializeArray());
                },
                serializeArray: function () {
                    return this.map(function () {
                        var e = re.prop(this, "elements");
                        return e ? re.makeArray(e) : this;
                    })
                        .filter(function () {
                            var e = this.type;
                            return this.name && !re(this).is(":disabled") && Et.test(this.nodeName) && !Mt.test(e) && (this.checked || !Ee.test(e));
                        })
                        .map(function (e, t) {
                            var n = re(this).val();
                            return null == n
                                ? null
                                : re.isArray(n)
                                    ? re.map(n, function (e) {
                                        return { name: t.name, value: e.replace(It, "\r\n") };
                                    })
                                    : { name: t.name, value: n.replace(It, "\r\n") };
                        })
                        .get();
                },
            }),
            (re.ajaxSettings.xhr = function () {
                try {
                    return new e.XMLHttpRequest();
                } catch (e) {}
            });
        var _t = { 0: 200, 1223: 204 },
            Nt = re.ajaxSettings.xhr();
        (ie.cors = !!Nt && "withCredentials" in Nt),
            (ie.ajax = Nt = !!Nt),
            re.ajaxTransport(function (t) {
                var n, i;
                return ie.cors || (Nt && !t.crossDomain)
                    ? {
                        send: function (o, r) {
                            var a,
                                s = t.xhr();
                            if ((s.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)) for (a in t.xhrFields) s[a] = t.xhrFields[a];
                            t.mimeType && s.overrideMimeType && s.overrideMimeType(t.mimeType), t.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
                            for (a in o) s.setRequestHeader(a, o[a]);
                            (n = function (e) {
                                return function () {
                                    n &&
                                    ((n = i = s.onload = s.onerror = s.onabort = s.onreadystatechange = null),
                                        "abort" === e
                                            ? s.abort()
                                            : "error" === e
                                            ? "number" != typeof s.status
                                                ? r(0, "error")
                                                : r(s.status, s.statusText)
                                            : r(
                                                _t[s.status] || s.status,
                                                s.statusText,
                                                "text" !== (s.responseType || "text") || "string" != typeof s.responseText ? { binary: s.response } : { text: s.responseText },
                                                s.getAllResponseHeaders()
                                            ));
                                };
                            }),
                                (s.onload = n()),
                                (i = s.onerror = n("error")),
                                void 0 !== s.onabort
                                    ? (s.onabort = i)
                                    : (s.onreadystatechange = function () {
                                        4 === s.readyState &&
                                        e.setTimeout(function () {
                                            n && i();
                                        });
                                    }),
                                (n = n("abort"));
                            try {
                                s.send((t.hasContent && t.data) || null);
                            } catch (e) {
                                if (n) throw e;
                            }
                        },
                        abort: function () {
                            n && n();
                        },
                    }
                    : void 0;
            }),
            re.ajaxSetup({
                accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" },
                contents: { script: /\b(?:java|ecma)script\b/ },
                converters: {
                    "text script": function (e) {
                        return re.globalEval(e), e;
                    },
                },
            }),
            re.ajaxPrefilter("script", function (e) {
                void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET");
            }),
            re.ajaxTransport("script", function (e) {
                if (e.crossDomain) {
                    var t, n;
                    return {
                        send: function (i, o) {
                            (t = re("<script>")
                                .prop({ charset: e.scriptCharset, src: e.url })
                                .on(
                                    "load error",
                                    (n = function (e) {
                                        t.remove(), (n = null), e && o("error" === e.type ? 404 : 200, e.type);
                                    })
                                )),
                                V.head.appendChild(t[0]);
                        },
                        abort: function () {
                            n && n();
                        },
                    };
                }
            });
        var Pt = [],
            jt = /(=)\?(?=&|$)|\?\?/;
        re.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function () {
                var e = Pt.pop() || re.expando + "_" + gt++;
                return (this[e] = !0), e;
            },
        }),
            re.ajaxPrefilter("json jsonp", function (t, n, i) {
                var o,
                    r,
                    a,
                    s = !1 !== t.jsonp && (jt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && jt.test(t.data) && "data");
                return s || "jsonp" === t.dataTypes[0]
                    ? ((o = t.jsonpCallback = re.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback),
                        s ? (t[s] = t[s].replace(jt, "$1" + o)) : !1 !== t.jsonp && (t.url += (vt.test(t.url) ? "&" : "?") + t.jsonp + "=" + o),
                        (t.converters["script json"] = function () {
                            return a || re.error(o + " was not called"), a[0];
                        }),
                        (t.dataTypes[0] = "json"),
                        (r = e[o]),
                        (e[o] = function () {
                            a = arguments;
                        }),
                        i.always(function () {
                            void 0 === r ? re(e).removeProp(o) : (e[o] = r), t[o] && ((t.jsonpCallback = n.jsonpCallback), Pt.push(o)), a && re.isFunction(r) && r(a[0]), (a = r = void 0);
                        }),
                        "script")
                    : void 0;
            }),
            (re.parseHTML = function (e, t, n) {
                if (!e || "string" != typeof e) return null;
                "boolean" == typeof t && ((n = t), (t = !1)), (t = t || V);
                var i = fe.exec(e),
                    o = !n && [];
                return i ? [t.createElement(i[1])] : ((i = p([e], t, o)), o && o.length && re(o).remove(), re.merge([], i.childNodes));
            });
        var Wt = re.fn.load;
        (re.fn.load = function (e, t, n) {
            if ("string" != typeof e && Wt) return Wt.apply(this, arguments);
            var i,
                o,
                r,
                a = this,
                s = e.indexOf(" ");
            return (
                s > -1 && ((i = re.trim(e.slice(s))), (e = e.slice(0, s))),
                    re.isFunction(t) ? ((n = t), (t = void 0)) : t && "object" == typeof t && (o = "POST"),
                a.length > 0 &&
                re
                    .ajax({ url: e, type: o || "GET", dataType: "html", data: t })
                    .done(function (e) {
                        (r = arguments), a.html(i ? re("<div>").append(re.parseHTML(e)).find(i) : e);
                    })
                    .always(
                        n &&
                        function (e, t) {
                            a.each(function () {
                                n.apply(this, r || [e.responseText, t, e]);
                            });
                        }
                    ),
                    this
            );
        }),
            re.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
                re.fn[t] = function (e) {
                    return this.on(t, e);
                };
            }),
            (re.expr.filters.animated = function (e) {
                return re.grep(re.timers, function (t) {
                    return e === t.elem;
                }).length;
            }),
            (re.offset = {
                setOffset: function (e, t, n) {
                    var i,
                        o,
                        r,
                        a,
                        s,
                        l,
                        u,
                        c = re.css(e, "position"),
                        d = re(e),
                        p = {};
                    "static" === c && (e.style.position = "relative"),
                        (s = d.offset()),
                        (r = re.css(e, "top")),
                        (l = re.css(e, "left")),
                        (u = ("absolute" === c || "fixed" === c) && (r + l).indexOf("auto") > -1),
                        u ? ((i = d.position()), (a = i.top), (o = i.left)) : ((a = parseFloat(r) || 0), (o = parseFloat(l) || 0)),
                    re.isFunction(t) && (t = t.call(e, n, re.extend({}, s))),
                    null != t.top && (p.top = t.top - s.top + a),
                    null != t.left && (p.left = t.left - s.left + o),
                        "using" in t ? t.using.call(e, p) : d.css(p);
                },
            }),
            re.fn.extend({
                offset: function (e) {
                    if (arguments.length)
                        return void 0 === e
                            ? this
                            : this.each(function (t) {
                                re.offset.setOffset(this, e, t);
                            });
                    var t,
                        n,
                        i = this[0],
                        o = { top: 0, left: 0 },
                        r = i && i.ownerDocument;
                    return r ? ((t = r.documentElement), re.contains(t, i) ? ((o = i.getBoundingClientRect()), (n = X(r)), { top: o.top + n.pageYOffset - t.clientTop, left: o.left + n.pageXOffset - t.clientLeft }) : o) : void 0;
                },
                position: function () {
                    if (this[0]) {
                        var e,
                            t,
                            n = this[0],
                            i = { top: 0, left: 0 };
                        return (
                            "fixed" === re.css(n, "position")
                                ? (t = n.getBoundingClientRect())
                                : ((e = this.offsetParent()), (t = this.offset()), re.nodeName(e[0], "html") || (i = e.offset()), (i.top += re.css(e[0], "borderTopWidth", !0)), (i.left += re.css(e[0], "borderLeftWidth", !0))),
                                { top: t.top - i.top - re.css(n, "marginTop", !0), left: t.left - i.left - re.css(n, "marginLeft", !0) }
                        );
                    }
                },
                offsetParent: function () {
                    return this.map(function () {
                        for (var e = this.offsetParent; e && "static" === re.css(e, "position"); ) e = e.offsetParent;
                        return e || Ke;
                    });
                },
            }),
            re.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (e, t) {
                var n = "pageYOffset" === t;
                re.fn[e] = function (i) {
                    return Te(
                        this,
                        function (e, i, o) {
                            var r = X(e);
                            return void 0 === o ? (r ? r[t] : e[i]) : void (r ? r.scrollTo(n ? r.pageXOffset : o, n ? o : r.pageYOffset) : (e[i] = o));
                        },
                        e,
                        i,
                        arguments.length
                    );
                };
            }),
            re.each(["top", "left"], function (e, t) {
                re.cssHooks[t] = D(ie.pixelPosition, function (e, n) {
                    return n ? ((n = C(e, t)), Je.test(n) ? re(e).position()[t] + "px" : n) : void 0;
                });
            }),
            re.each({ Height: "height", Width: "width" }, function (e, t) {
                re.each({ padding: "inner" + e, content: t, "": "outer" + e }, function (n, i) {
                    re.fn[i] = function (i, o) {
                        var r = arguments.length && (n || "boolean" != typeof i),
                            a = n || (!0 === i || !0 === o ? "margin" : "border");
                        return Te(
                            this,
                            function (t, n, i) {
                                var o;
                                return re.isWindow(t)
                                    ? t.document.documentElement["client" + e]
                                    : 9 === t.nodeType
                                        ? ((o = t.documentElement), Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e]))
                                        : void 0 === i
                                            ? re.css(t, n, a)
                                            : re.style(t, n, i, a);
                            },
                            t,
                            r ? i : void 0,
                            r,
                            null
                        );
                    };
                });
            }),
            re.fn.extend({
                bind: function (e, t, n) {
                    return this.on(e, null, t, n);
                },
                unbind: function (e, t) {
                    return this.off(e, null, t);
                },
                delegate: function (e, t, n, i) {
                    return this.on(t, e, n, i);
                },
                undelegate: function (e, t, n) {
                    return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n);
                },
                size: function () {
                    return this.length;
                },
            }),
            (re.fn.andSelf = re.fn.addBack),
        "function" == typeof define &&
        define.amd &&
        define("jquery", [], function () {
            return re;
        });
        var Ft = e.jQuery,
            Ht = e.$;
        return (
            (re.noConflict = function (t) {
                return e.$ === re && (e.$ = Ht), t && e.jQuery === re && (e.jQuery = Ft), re;
            }),
            t || (e.jQuery = e.$ = re),
                re
        );
    }),
    "undefined" == typeof jQuery)
)
    throw new Error("Bootstrap's JavaScript requires jQuery");
+(function (e) {
    "use strict";
    var t = e.fn.jquery.split(" ")[0].split(".");
    if ((t[0] < 2 && t[1] < 9) || (1 == t[0] && 9 == t[1] && t[2] < 1) || t[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4");
})(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            return this.each(function () {
                var n = e(this),
                    o = n.data("bs.alert");
                o || n.data("bs.alert", (o = new i(this))), "string" == typeof t && o[t].call(n);
            });
        }
        var n = '[data-dismiss="alert"]',
            i = function (t) {
                e(t).on("click", n, this.close);
            };
        (i.VERSION = "3.3.7"),
            (i.TRANSITION_DURATION = 150),
            (i.prototype.close = function (t) {
                function n() {
                    a.detach().trigger("closed.bs.alert").remove();
                }
                var o = e(this),
                    r = o.attr("data-target");
                r || ((r = o.attr("href")), (r = r && r.replace(/.*(?=#[^\s]*$)/, "")));
                var a = e("#" === r ? [] : r);
                t && t.preventDefault(),
                a.length || (a = o.closest(".alert")),
                    a.trigger((t = e.Event("close.bs.alert"))),
                t.isDefaultPrevented() || (a.removeClass("in"), e.support.transition && a.hasClass("fade") ? a.one("bsTransitionEnd", n).emulateTransitionEnd(i.TRANSITION_DURATION) : n());
            });
        var o = e.fn.alert;
        (e.fn.alert = t),
            (e.fn.alert.Constructor = i),
            (e.fn.alert.noConflict = function () {
                return (e.fn.alert = o), this;
            }),
            e(document).on("click.bs.alert.data-api", n, i.prototype.close);
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            return this.each(function () {
                var i = e(this),
                    o = i.data("bs.button"),
                    r = "object" == typeof t && t;
                o || i.data("bs.button", (o = new n(this, r))), "toggle" == t ? o.toggle() : t && o.setState(t);
            });
        }
        var n = function (t, i) {
            (this.$element = e(t)), (this.options = e.extend({}, n.DEFAULTS, i)), (this.isLoading = !1);
        };
        (n.VERSION = "3.3.7"),
            (n.DEFAULTS = { loadingText: "loading..." }),
            (n.prototype.setState = function (t) {
                var n = "disabled",
                    i = this.$element,
                    o = i.is("input") ? "val" : "html",
                    r = i.data();
                (t += "Text"),
                null == r.resetText && i.data("resetText", i[o]()),
                    setTimeout(
                        e.proxy(function () {
                            i[o](null == r[t] ? this.options[t] : r[t]),
                                "loadingText" == t ? ((this.isLoading = !0), i.addClass(n).attr(n, n).prop(n, !0)) : this.isLoading && ((this.isLoading = !1), i.removeClass(n).removeAttr(n).prop(n, !1));
                        }, this),
                        0
                    );
            }),
            (n.prototype.toggle = function () {
                var e = !0,
                    t = this.$element.closest('[data-toggle="buttons"]');
                if (t.length) {
                    var n = this.$element.find("input");
                    "radio" == n.prop("type")
                        ? (n.prop("checked") && (e = !1), t.find(".active").removeClass("active"), this.$element.addClass("active"))
                        : "checkbox" == n.prop("type") && (n.prop("checked") !== this.$element.hasClass("active") && (e = !1), this.$element.toggleClass("active")),
                        n.prop("checked", this.$element.hasClass("active")),
                    e && n.trigger("change");
                } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active");
            });
        var i = e.fn.button;
        (e.fn.button = t),
            (e.fn.button.Constructor = n),
            (e.fn.button.noConflict = function () {
                return (e.fn.button = i), this;
            }),
            e(document)
                .on("click.bs.button.data-api", '[data-toggle^="button"]', function (n) {
                    var i = e(n.target).closest(".btn");
                    t.call(i, "toggle"), e(n.target).is('input[type="radio"], input[type="checkbox"]') || (n.preventDefault(), i.is("input,button") ? i.trigger("focus") : i.find("input:visible,button:visible").first().trigger("focus"));
                })
                .on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function (t) {
                    e(t.target)
                        .closest(".btn")
                        .toggleClass("focus", /^focus(in)?$/.test(t.type));
                });
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            return this.each(function () {
                var i = e(this),
                    o = i.data("bs.carousel"),
                    r = e.extend({}, n.DEFAULTS, i.data(), "object" == typeof t && t),
                    a = "string" == typeof t ? t : r.slide;
                o || i.data("bs.carousel", (o = new n(this, r))), "number" == typeof t ? o.to(t) : a ? o[a]() : r.interval && o.pause().cycle();
            });
        }
        var n = function (t, n) {
            (this.$element = e(t)),
                (this.$indicators = this.$element.find(".carousel-indicators")),
                (this.options = n),
                (this.paused = null),
                (this.sliding = null),
                (this.interval = null),
                (this.$active = null),
                (this.$items = null),
            this.options.keyboard && this.$element.on("keydown.bs.carousel", e.proxy(this.keydown, this)),
            "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", e.proxy(this.pause, this)).on("mouseleave.bs.carousel", e.proxy(this.cycle, this));
        };
        (n.VERSION = "3.3.7"),
            (n.TRANSITION_DURATION = 600),
            (n.DEFAULTS = { interval: 5e3, pause: "hover", wrap: !0, keyboard: !0 }),
            (n.prototype.keydown = function (e) {
                if (!/input|textarea/i.test(e.target.tagName)) {
                    switch (e.which) {
                        case 37:
                            this.prev();
                            break;
                        case 39:
                            this.next();
                            break;
                        default:
                            return;
                    }
                    e.preventDefault();
                }
            }),
            (n.prototype.cycle = function (t) {
                return t || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(e.proxy(this.next, this), this.options.interval)), this;
            }),
            (n.prototype.getItemIndex = function (e) {
                return (this.$items = e.parent().children(".item")), this.$items.index(e || this.$active);
            }),
            (n.prototype.getItemForDirection = function (e, t) {
                var n = this.getItemIndex(t);
                if ((("prev" == e && 0 === n) || ("next" == e && n == this.$items.length - 1)) && !this.options.wrap) return t;
                var i = "prev" == e ? -1 : 1,
                    o = (n + i) % this.$items.length;
                return this.$items.eq(o);
            }),
            (n.prototype.to = function (e) {
                var t = this,
                    n = this.getItemIndex((this.$active = this.$element.find(".item.active")));
                return e > this.$items.length - 1 || 0 > e
                    ? void 0
                    : this.sliding
                        ? this.$element.one("slid.bs.carousel", function () {
                            t.to(e);
                        })
                        : n == e
                            ? this.pause().cycle()
                            : this.slide(e > n ? "next" : "prev", this.$items.eq(e));
            }),
            (n.prototype.pause = function (t) {
                return t || (this.paused = !0), this.$element.find(".next, .prev").length && e.support.transition && (this.$element.trigger(e.support.transition.end), this.cycle(!0)), (this.interval = clearInterval(this.interval)), this;
            }),
            (n.prototype.next = function () {
                return this.sliding ? void 0 : this.slide("next");
            }),
            (n.prototype.prev = function () {
                return this.sliding ? void 0 : this.slide("prev");
            }),
            (n.prototype.slide = function (t, i) {
                var o = this.$element.find(".item.active"),
                    r = i || this.getItemForDirection(t, o),
                    a = this.interval,
                    s = "next" == t ? "left" : "right",
                    l = this;
                if (r.hasClass("active")) return (this.sliding = !1);
                var u = r[0],
                    c = e.Event("slide.bs.carousel", { relatedTarget: u, direction: s });
                if ((this.$element.trigger(c), !c.isDefaultPrevented())) {
                    if (((this.sliding = !0), a && this.pause(), this.$indicators.length)) {
                        this.$indicators.find(".active").removeClass("active");
                        var d = e(this.$indicators.children()[this.getItemIndex(r)]);
                        d && d.addClass("active");
                    }
                    var p = e.Event("slid.bs.carousel", { relatedTarget: u, direction: s });
                    return (
                        e.support.transition && this.$element.hasClass("slide")
                            ? (r.addClass(t),
                                r[0].offsetWidth,
                                o.addClass(s),
                                r.addClass(s),
                                o
                                    .one("bsTransitionEnd", function () {
                                        r.removeClass([t, s].join(" ")).addClass("active"),
                                            o.removeClass(["active", s].join(" ")),
                                            (l.sliding = !1),
                                            setTimeout(function () {
                                                l.$element.trigger(p);
                                            }, 0);
                                    })
                                    .emulateTransitionEnd(n.TRANSITION_DURATION))
                            : (o.removeClass("active"), r.addClass("active"), (this.sliding = !1), this.$element.trigger(p)),
                        a && this.cycle(),
                            this
                    );
                }
            });
        var i = e.fn.carousel;
        (e.fn.carousel = t),
            (e.fn.carousel.Constructor = n),
            (e.fn.carousel.noConflict = function () {
                return (e.fn.carousel = i), this;
            });
        var o = function (n) {
            var i,
                o = e(this),
                r = e(o.attr("data-target") || ((i = o.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, "")));
            if (r.hasClass("carousel")) {
                var a = e.extend({}, r.data(), o.data()),
                    s = o.attr("data-slide-to");
                s && (a.interval = !1), t.call(r, a), s && r.data("bs.carousel").to(s), n.preventDefault();
            }
        };
        e(document).on("click.bs.carousel.data-api", "[data-slide]", o).on("click.bs.carousel.data-api", "[data-slide-to]", o),
            e(window).on("load", function () {
                e('[data-ride="carousel"]').each(function () {
                    var n = e(this);
                    t.call(n, n.data());
                });
            });
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            var n = t.attr("data-target");
            n || ((n = t.attr("href")), (n = n && /#[A-Za-z]/.test(n) && n.replace(/.*(?=#[^\s]*$)/, "")));
            var i = n && e(n);
            return i && i.length ? i : t.parent();
        }
        function n(n) {
            (n && 3 === n.which) ||
            (e(o).remove(),

                e(r).each(function () {
                    var i = e(this),
                        o = t(i),
                        r = { relatedTarget: this };
                    o.hasClass("open") &&
                    ((n && "click" == n.type && /input|textarea/i.test(n.target.tagName) && e.contains(o[0], n.target)) ||
                        (o.trigger((n = e.Event("hide.bs.dropdown", r))), n.isDefaultPrevented() || (i.attr("aria-expanded", "false"), o.removeClass("open").trigger(e.Event("hidden.bs.dropdown", r)))));
                }));
        }
        function i(t) {
            return this.each(function () {
                var n = e(this),
                    i = n.data("bs.dropdown");
                i || n.data("bs.dropdown", (i = new a(this))), "string" == typeof t && i[t].call(n);
            });
        }
        var o = ".dropdown-backdrop",
            r = '[data-toggle="dropdown"]',
            a = function (t) {
                e(t).on("click.bs.dropdown", this.toggle);
            };
        (a.VERSION = "3.3.7"),
            (a.prototype.toggle = function (i) {
                var o = e(this);
                if (!o.is(".disabled, :disabled")) {
                    var r = t(o),
                        a = r.hasClass("open");
                    if ((n(), !a)) {
                        "ontouchstart" in document.documentElement && !r.closest(".navbar-nav").length && e(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(e(this)).on("click", n);
                        var s = { relatedTarget: this };
                        if ((r.trigger((i = e.Event("show.bs.dropdown", s))), i.isDefaultPrevented())) return;
                        o.trigger("focus").attr("aria-expanded", "true"), r.toggleClass("open").trigger(e.Event("shown.bs.dropdown", s));
                    }
                    return !1;
                }
            }),
            (a.prototype.keydown = function (n) {
                if (/(38|40|27|32)/.test(n.which) && !/input|textarea/i.test(n.target.tagName)) {
                    var i = e(this);
                    if ((n.preventDefault(), n.stopPropagation(), !i.is(".disabled, :disabled"))) {
                        var o = t(i),
                            a = o.hasClass("open");
                        if ((!a && 27 != n.which) || (a && 27 == n.which)) return 27 == n.which && o.find(r).trigger("focus"), i.trigger("click");
                        var s = o.find(".dropdown-menu li:not(.disabled):visible a");
                        if (s.length) {
                            var l = s.index(n.target);
                            38 == n.which && l > 0 && l--, 40 == n.which && l < s.length - 1 && l++, ~l || (l = 0), s.eq(l).trigger("focus");
                        }
                    }
                }
            });
        var s = e.fn.dropdown;
        (e.fn.dropdown = i),
            (e.fn.dropdown.Constructor = a),
            (e.fn.dropdown.noConflict = function () {
                return (e.fn.dropdown = s), this;
            }),
            e(document)
                .on("click.bs.dropdown.data-api", n)
                .on("click.bs.dropdown.data-api", ".dropdown form", function (e) {
                    e.stopPropagation();
                })
                .on("click.bs.dropdown.data-api", r, a.prototype.toggle)
                .on("keydown.bs.dropdown.data-api", r, a.prototype.keydown)
                .on("keydown.bs.dropdown.data-api", ".dropdown-menu", a.prototype.keydown);
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t, i) {
            return this.each(function () {
                var o = e(this),
                    r = o.data("bs.modal"),
                    a = e.extend({}, n.DEFAULTS, o.data(), "object" == typeof t && t);
                r || o.data("bs.modal", (r = new n(this, a))), "string" == typeof t ? r[t](i) : a.show && r.show(i);
            });
        }
        var n = function (t, n) {
            (this.options = n),
                (this.$body = e(document.body)),
                (this.$element = e(t)),
                (this.$dialog = this.$element.find(".modal-dialog")),
                (this.$backdrop = null),
                (this.isShown = null),
                (this.originalBodyPad = null),
                (this.scrollbarWidth = 0),
                (this.ignoreBackdropClick = !1),
            this.options.remote &&
            this.$element.find(".modal-content").load(
                this.options.remote,
                e.proxy(function () {
                    this.$element.trigger("loaded.bs.modal");
                }, this)
            );
        };
        (n.VERSION = "3.3.7"),
            (n.TRANSITION_DURATION = 300),
            (n.BACKDROP_TRANSITION_DURATION = 150),
            (n.DEFAULTS = { backdrop: !0, keyboard: !0, show: !0 }),
            (n.prototype.toggle = function (e) {
                return this.isShown ? this.hide() : this.show(e);
            }),
            (n.prototype.show = function (t) {
                var i = this,
                    o = e.Event("show.bs.modal", { relatedTarget: t });
                this.$element.trigger(o),
                this.isShown ||
                o.isDefaultPrevented() ||
                ((this.isShown = !0),
                    this.checkScrollbar(),
                    this.setScrollbar(),
                    this.$body.addClass("modal-open"),
                    this.escape(),
                    this.resize(),
                    this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', e.proxy(this.hide, this)),
                    this.$dialog.on("mousedown.dismiss.bs.modal", function () {
                        i.$element.one("mouseup.dismiss.bs.modal", function (t) {
                            e(t.target).is(i.$element) && (i.ignoreBackdropClick = !0);
                        });
                    }),
                    this.backdrop(function () {
                        var o = e.support.transition && i.$element.hasClass("fade");
                        i.$element.parent().length || i.$element.appendTo(i.$body), i.$element.show().scrollTop(0), i.adjustDialog(), o && i.$element[0].offsetWidth, i.$element.addClass("in"), i.enforceFocus();
                        var r = e.Event("shown.bs.modal", { relatedTarget: t });
                        o
                            ? i.$dialog
                                .one("bsTransitionEnd", function () {
                                    i.$element.trigger("focus").trigger(r);
                                })
                                .emulateTransitionEnd(n.TRANSITION_DURATION)
                            : i.$element.trigger("focus").trigger(r);
                    }));
            }),
            (n.prototype.hide = function (t) {
                t && t.preventDefault(),
                    (t = e.Event("hide.bs.modal")),
                    this.$element.trigger(t),
                this.isShown &&
                !t.isDefaultPrevented() &&
                ((this.isShown = !1),
                    this.escape(),
                    this.resize(),
                    e(document).off("focusin.bs.modal"),
                    this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),
                    this.$dialog.off("mousedown.dismiss.bs.modal"),
                    e.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", e.proxy(this.hideModal, this)).emulateTransitionEnd(n.TRANSITION_DURATION) : this.hideModal());
            }),
            (n.prototype.enforceFocus = function () {
                e(document)
                    .off("focusin.bs.modal")
                    .on(
                        "focusin.bs.modal",
                        e.proxy(function (e) {
                            document === e.target || this.$element[0] === e.target || this.$element.has(e.target).length || this.$element.trigger("focus");
                        }, this)
                    );
            }),
            (n.prototype.escape = function () {
                this.isShown && this.options.keyboard
                    ? this.$element.on(
                    "keydown.dismiss.bs.modal",
                    e.proxy(function (e) {
                        27 == e.which && this.hide();
                    }, this)
                    )
                    : this.isShown || this.$element.off("keydown.dismiss.bs.modal");
            }),
            (n.prototype.resize = function () {
                this.isShown ? e(window).on("resize.bs.modal", e.proxy(this.handleUpdate, this)) : e(window).off("resize.bs.modal");
            }),
            (n.prototype.hideModal = function () {
                var e = this;
                this.$element.hide(),
                    this.backdrop(function () {
                        e.$body.removeClass("modal-open"), e.resetAdjustments(), e.resetScrollbar(), e.$element.trigger("hidden.bs.modal");
                    });
            }),
            (n.prototype.removeBackdrop = function () {
                this.$backdrop && this.$backdrop.remove(), (this.$backdrop = null);
            }),
            (n.prototype.backdrop = function (t) {
                var i = this,
                    o = this.$element.hasClass("fade") ? "fade" : "";
                if (this.isShown && this.options.backdrop) {
                    var r = e.support.transition && o;
                    if (
                        ((this.$backdrop = e(document.createElement("div"))
                            .addClass("modal-backdrop " + o)
                            .appendTo(this.$body)),
                            this.$element.on(
                                "click.dismiss.bs.modal",
                                e.proxy(function (e) {
                                    return this.ignoreBackdropClick ? void (this.ignoreBackdropClick = !1) : void (e.target === e.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()));
                                }, this)
                            ),
                        r && this.$backdrop[0].offsetWidth,
                            this.$backdrop.addClass("in"),
                            !t)
                    )
                        return;
                    r ? this.$backdrop.one("bsTransitionEnd", t).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION) : t();
                } else if (!this.isShown && this.$backdrop) {
                    this.$backdrop.removeClass("in");
                    var a = function () {
                        i.removeBackdrop(), t && t();
                    };
                    e.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", a).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION) : a();
                } else t && t();
            }),
            (n.prototype.handleUpdate = function () {
                this.adjustDialog();
            }),
            (n.prototype.adjustDialog = function () {
                var e = this.$element[0].scrollHeight > document.documentElement.clientHeight;
                this.$element.css({ paddingLeft: !this.bodyIsOverflowing && e ? this.scrollbarWidth : "", paddingRight: this.bodyIsOverflowing && !e ? this.scrollbarWidth : "" });
            }),
            (n.prototype.resetAdjustments = function () {
                this.$element.css({ paddingLeft: "", paddingRight: "" });
            }),
            (n.prototype.checkScrollbar = function () {
                var e = window.innerWidth;
                if (!e) {
                    var t = document.documentElement.getBoundingClientRect();
                    e = t.right - Math.abs(t.left);
                }
                (this.bodyIsOverflowing = document.body.clientWidth < e), (this.scrollbarWidth = this.measureScrollbar());
            }),
            (n.prototype.setScrollbar = function () {
                var e = parseInt(this.$body.css("padding-right") || 0, 10);
                (this.originalBodyPad = document.body.style.paddingRight || ""), this.bodyIsOverflowing && this.$body.css("padding-right", e + this.scrollbarWidth);
            }),
            (n.prototype.resetScrollbar = function () {
                this.$body.css("padding-right", this.originalBodyPad);
            }),
            (n.prototype.measureScrollbar = function () {
                var e = document.createElement("div");
                (e.className = "modal-scrollbar-measure"), this.$body.append(e);
                var t = e.offsetWidth - e.clientWidth;
                return this.$body[0].removeChild(e), t;
            });
        var i = e.fn.modal;
        (e.fn.modal = t),
            (e.fn.modal.Constructor = n),
            (e.fn.modal.noConflict = function () {
                return (e.fn.modal = i), this;
            }),
            e(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (n) {
                var i = e(this),
                    o = i.attr("href"),
                    r = e(i.attr("data-target") || (o && o.replace(/.*(?=#[^\s]+$)/, ""))),
                    a = r.data("bs.modal") ? "toggle" : e.extend({ remote: !/#/.test(o) && o }, r.data(), i.data());
                i.is("a") && n.preventDefault(),
                    r.one("show.bs.modal", function (e) {
                        e.isDefaultPrevented() ||
                        r.one("hidden.bs.modal", function () {
                            i.is(":visible") && i.trigger("focus");
                        });
                    }),
                    t.call(r, a, this);
            });
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            return this.each(function () {
                var i = e(this),
                    o = i.data("bs.tooltip"),
                    r = "object" == typeof t && t;
                (!o && /destroy|hide/.test(t)) || (o || i.data("bs.tooltip", (o = new n(this, r))), "string" == typeof t && o[t]());
            });
        }
        var n = function (e, t) {
            (this.type = null), (this.options = null), (this.enabled = null), (this.timeout = null), (this.hoverState = null), (this.$element = null), (this.inState = null), this.init("tooltip", e, t);
        };
        (n.VERSION = "3.3.7"),
            (n.TRANSITION_DURATION = 150),
            (n.DEFAULTS = {
                animation: !0,
                placement: "top",
                selector: !1,
                template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
                trigger: "hover focus",
                title: "",
                delay: 0,
                html: !1,
                container: !1,
                viewport: { selector: "body", padding: 0 },
            }),
            (n.prototype.init = function (t, n, i) {
                if (
                    ((this.enabled = !0),
                        (this.type = t),
                        (this.$element = e(n)),
                        (this.options = this.getOptions(i)),
                        (this.$viewport = this.options.viewport && e(e.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport)),
                        (this.inState = { click: !1, hover: !1, focus: !1 }),
                    this.$element[0] instanceof document.constructor && !this.options.selector)
                )
                    throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
                for (var o = this.options.trigger.split(" "), r = o.length; r--; ) {
                    var a = o[r];
                    if ("click" == a) this.$element.on("click." + this.type, this.options.selector, e.proxy(this.toggle, this));
                    else if ("manual" != a) {
                        var s = "hover" == a ? "mouseenter" : "focusin",
                            l = "hover" == a ? "mouseleave" : "focusout";
                        this.$element.on(s + "." + this.type, this.options.selector, e.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, e.proxy(this.leave, this));
                    }
                }
                this.options.selector ? (this._options = e.extend({}, this.options, { trigger: "manual", selector: "" })) : this.fixTitle();
            }),
            (n.prototype.getDefaults = function () {
                return n.DEFAULTS;
            }),
            (n.prototype.getOptions = function (t) {
                return (t = e.extend({}, this.getDefaults(), this.$element.data(), t)), t.delay && "number" == typeof t.delay && (t.delay = { show: t.delay, hide: t.delay }), t;
            }),
            (n.prototype.getDelegateOptions = function () {
                var t = {},
                    n = this.getDefaults();
                return (
                    this._options &&
                    e.each(this._options, function (e, i) {
                        n[e] != i && (t[e] = i);
                    }),
                        t
                );
            }),
            (n.prototype.enter = function (t) {
                var n = t instanceof this.constructor ? t : e(t.currentTarget).data("bs." + this.type);
                return (
                    n || ((n = new this.constructor(t.currentTarget, this.getDelegateOptions())), e(t.currentTarget).data("bs." + this.type, n)),
                    t instanceof e.Event && (n.inState["focusin" == t.type ? "focus" : "hover"] = !0),
                        n.tip().hasClass("in") || "in" == n.hoverState
                            ? void (n.hoverState = "in")
                            : (clearTimeout(n.timeout),
                                (n.hoverState = "in"),
                                n.options.delay && n.options.delay.show
                                    ? void (n.timeout = setTimeout(function () {
                                        "in" == n.hoverState && n.show();
                                    }, n.options.delay.show))
                                    : n.show())
                );
            }),
            (n.prototype.isInStateTrue = function () {
                for (var e in this.inState) if (this.inState[e]) return !0;
                return !1;
            }),
            (n.prototype.leave = function (t) {
                var n = t instanceof this.constructor ? t : e(t.currentTarget).data("bs." + this.type);
                return (
                    n || ((n = new this.constructor(t.currentTarget, this.getDelegateOptions())), e(t.currentTarget).data("bs." + this.type, n)),
                    t instanceof e.Event && (n.inState["focusout" == t.type ? "focus" : "hover"] = !1),
                        n.isInStateTrue()
                            ? void 0
                            : (clearTimeout(n.timeout),
                                (n.hoverState = "out"),
                                n.options.delay && n.options.delay.hide
                                    ? void (n.timeout = setTimeout(function () {
                                        "out" == n.hoverState && n.hide();
                                    }, n.options.delay.hide))
                                    : n.hide())
                );
            }),
            (n.prototype.show = function () {
                var t = e.Event("show.bs." + this.type);
                if (this.hasContent() && this.enabled) {
                    this.$element.trigger(t);
                    var i = e.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
                    if (t.isDefaultPrevented() || !i) return;
                    var o = this,
                        r = this.tip(),
                        a = this.getUID(this.type);
                    this.setContent(), r.attr("id", a), this.$element.attr("aria-describedby", a), this.options.animation && r.addClass("fade");
                    var s = "function" == typeof this.options.placement ? this.options.placement.call(this, r[0], this.$element[0]) : this.options.placement,
                        l = /\s?auto?\s?/i,
                        u = l.test(s);
                    u && (s = s.replace(l, "") || "top"),
                        r
                            .detach()
                            .css({ top: 0, left: 0, display: "block" })
                            .addClass(s)
                            .data("bs." + this.type, this),
                        this.options.container ? r.appendTo(this.options.container) : r.insertAfter(this.$element),
                        this.$element.trigger("inserted.bs." + this.type);
                    var c = this.getPosition(),
                        d = r[0].offsetWidth,
                        p = r[0].offsetHeight;
                    if (u) {
                        var h = s,
                            f = this.getPosition(this.$viewport);
                        (s = "bottom" == s && c.bottom + p > f.bottom ? "top" : "top" == s && c.top - p < f.top ? "bottom" : "right" == s && c.right + d > f.width ? "left" : "left" == s && c.left - d < f.left ? "right" : s),
                            r.removeClass(h).addClass(s);
                    }
                    var m = this.getCalculatedOffset(s, c, d, p);
                    this.applyPlacement(m, s);
                    var g = function () {
                        var e = o.hoverState;
                        o.$element.trigger("shown.bs." + o.type), (o.hoverState = null), "out" == e && o.leave(o);
                    };
                    e.support.transition && this.$tip.hasClass("fade") ? r.one("bsTransitionEnd", g).emulateTransitionEnd(n.TRANSITION_DURATION) : g();
                }
            }),
            (n.prototype.applyPlacement = function (t, n) {
                var i = this.tip(),
                    o = i[0].offsetWidth,
                    r = i[0].offsetHeight,
                    a = parseInt(i.css("margin-top"), 10),
                    s = parseInt(i.css("margin-left"), 10);
                isNaN(a) && (a = 0),
                isNaN(s) && (s = 0),
                    (t.top += a),
                    (t.left += s),
                    e.offset.setOffset(
                        i[0],
                        e.extend(
                            {
                                using: function (e) {
                                    i.css({ top: Math.round(e.top), left: Math.round(e.left) });
                                },
                            },
                            t
                        ),
                        0
                    ),
                    i.addClass("in");
                var l = i[0].offsetWidth,
                    u = i[0].offsetHeight;
                "top" == n && u != r && (t.top = t.top + r - u);
                var c = this.getViewportAdjustedDelta(n, t, l, u);
                c.left ? (t.left += c.left) : (t.top += c.top);
                var d = /top|bottom/.test(n),
                    p = d ? 2 * c.left - o + l : 2 * c.top - r + u,
                    h = d ? "offsetWidth" : "offsetHeight";
                i.offset(t), this.replaceArrow(p, i[0][h], d);
            }),
            (n.prototype.replaceArrow = function (e, t, n) {
                this.arrow()
                    .css(n ? "left" : "top", 50 * (1 - e / t) + "%")
                    .css(n ? "top" : "left", "");
            }),
            (n.prototype.setContent = function () {
                var e = this.tip(),
                    t = this.getTitle();
                e.find(".tooltip-inner")[this.options.html ? "html" : "text"](t), e.removeClass("fade in top bottom left right");
            }),
            (n.prototype.hide = function (t) {
                function i() {
                    "in" != o.hoverState && r.detach(), o.$element && o.$element.removeAttr("aria-describedby").trigger("hidden.bs." + o.type), t && t();
                }
                var o = this,
                    r = e(this.$tip),
                    a = e.Event("hide.bs." + this.type);
                return (
                    this.$element.trigger(a),
                        a.isDefaultPrevented() ? void 0 : (r.removeClass("in"), e.support.transition && r.hasClass("fade") ? r.one("bsTransitionEnd", i).emulateTransitionEnd(n.TRANSITION_DURATION) : i(), (this.hoverState = null), this)
                );
            }),
            (n.prototype.fixTitle = function () {
                var e = this.$element;
                (e.attr("title") || "string" != typeof e.attr("data-original-title")) && e.attr("data-original-title", e.attr("title") || "").attr("title", "");
            }),
            (n.prototype.hasContent = function () {
                return this.getTitle();
            }),
            (n.prototype.getPosition = function (t) {
                t = t || this.$element;
                var n = t[0],
                    i = "BODY" == n.tagName,
                    o = n.getBoundingClientRect();
                null == o.width && (o = e.extend({}, o, { width: o.right - o.left, height: o.bottom - o.top }));
                var r = window.SVGElement && n instanceof window.SVGElement,
                    a = i ? { top: 0, left: 0 } : r ? null : t.offset(),
                    s = { scroll: i ? document.documentElement.scrollTop || document.body.scrollTop : t.scrollTop() },
                    l = i ? { width: e(window).width(), height: e(window).height() } : null;
                return e.extend({}, o, s, l, a);
            }),
            (n.prototype.getCalculatedOffset = function (e, t, n, i) {
                return "bottom" == e
                    ? { top: t.top + t.height, left: t.left + t.width / 2 - n / 2 }
                    : "top" == e
                        ? { top: t.top - i, left: t.left + t.width / 2 - n / 2 }
                        : "left" == e
                            ? { top: t.top + t.height / 2 - i / 2, left: t.left - n }
                            : { top: t.top + t.height / 2 - i / 2, left: t.left + t.width };
            }),
            (n.prototype.getViewportAdjustedDelta = function (e, t, n, i) {
                var o = { top: 0, left: 0 };
                if (!this.$viewport) return o;
                var r = (this.options.viewport && this.options.viewport.padding) || 0,
                    a = this.getPosition(this.$viewport);
                if (/right|left/.test(e)) {
                    var s = t.top - r - a.scroll,
                        l = t.top + r - a.scroll + i;
                    s < a.top ? (o.top = a.top - s) : l > a.top + a.height && (o.top = a.top + a.height - l);
                } else {
                    var u = t.left - r,
                        c = t.left + r + n;
                    u < a.left ? (o.left = a.left - u) : c > a.right && (o.left = a.left + a.width - c);
                }
                return o;
            }),
            (n.prototype.getTitle = function () {
                var e = this.$element,
                    t = this.options;
                return e.attr("data-original-title") || ("function" == typeof t.title ? t.title.call(e[0]) : t.title);
            }),
            (n.prototype.getUID = function (e) {
                do {
                    e += ~~(1e6 * Math.random());
                } while (document.getElementById(e));
                return e;
            }),
            (n.prototype.tip = function () {
                if (!this.$tip && ((this.$tip = e(this.options.template)), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
                return this.$tip;
            }),
            (n.prototype.arrow = function () {
                return (this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow"));
            }),
            (n.prototype.enable = function () {
                this.enabled = !0;
            }),
            (n.prototype.disable = function () {
                this.enabled = !1;
            }),
            (n.prototype.toggleEnabled = function () {
                this.enabled = !this.enabled;
            }),
            (n.prototype.toggle = function (t) {
                var n = this;
                t && ((n = e(t.currentTarget).data("bs." + this.type)) || ((n = new this.constructor(t.currentTarget, this.getDelegateOptions())), e(t.currentTarget).data("bs." + this.type, n))),
                    t ? ((n.inState.click = !n.inState.click), n.isInStateTrue() ? n.enter(n) : n.leave(n)) : n.tip().hasClass("in") ? n.leave(n) : n.enter(n);
            }),
            (n.prototype.destroy = function () {
                var e = this;
                clearTimeout(this.timeout),
                    this.hide(function () {
                        e.$element.off("." + e.type).removeData("bs." + e.type), e.$tip && e.$tip.detach(), (e.$tip = null), (e.$arrow = null), (e.$viewport = null), (e.$element = null);
                    });
            });
        var i = e.fn.tooltip;
        (e.fn.tooltip = t),
            (e.fn.tooltip.Constructor = n),
            (e.fn.tooltip.noConflict = function () {
                return (e.fn.tooltip = i), this;
            });
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            return this.each(function () {
                var i = e(this),
                    o = i.data("bs.popover"),
                    r = "object" == typeof t && t;
                (!o && /destroy|hide/.test(t)) || (o || i.data("bs.popover", (o = new n(this, r))), "string" == typeof t && o[t]());
            });
        }
        var n = function (e, t) {
            this.init("popover", e, t);
        };
        if (!e.fn.tooltip) throw new Error("Popover requires tooltip.js");
        (n.VERSION = "3.3.7"),
            (n.DEFAULTS = e.extend({}, e.fn.tooltip.Constructor.DEFAULTS, {
                placement: "right",
                trigger: "click",
                content: "",
                template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
            })),
            (n.prototype = e.extend({}, e.fn.tooltip.Constructor.prototype)),
            (n.prototype.constructor = n),
            (n.prototype.getDefaults = function () {
                return n.DEFAULTS;
            }),
            (n.prototype.setContent = function () {
                var e = this.tip(),
                    t = this.getTitle(),
                    n = this.getContent();
                e.find(".popover-title")[this.options.html ? "html" : "text"](t),
                    e.find(".popover-content").children().detach().end()[this.options.html ? ("string" == typeof n ? "html" : "append") : "text"](n),
                    e.removeClass("fade top bottom left right in"),
                e.find(".popover-title").html() || e.find(".popover-title").hide();
            }),
            (n.prototype.hasContent = function () {
                return this.getTitle() || this.getContent();
            }),
            (n.prototype.getContent = function () {
                var e = this.$element,
                    t = this.options;
                return e.attr("data-content") || ("function" == typeof t.content ? t.content.call(e[0]) : t.content);
            }),
            (n.prototype.arrow = function () {
                return (this.$arrow = this.$arrow || this.tip().find(".arrow"));
            });
        var i = e.fn.popover;
        (e.fn.popover = t),
            (e.fn.popover.Constructor = n),
            (e.fn.popover.noConflict = function () {
                return (e.fn.popover = i), this;
            });
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            return this.each(function () {
                var i = e(this),
                    o = i.data("bs.tab");
                o || i.data("bs.tab", (o = new n(this))), "string" == typeof t && o[t]();
            });
        }
        var n = function (t) {
            this.element = e(t);
        };
        (n.VERSION = "3.3.7"),
            (n.TRANSITION_DURATION = 150),
            (n.prototype.show = function () {
                var t = this.element,
                    n = t.closest("ul:not(.dropdown-menu)"),
                    i = t.data("target");
                if ((i || ((i = t.attr("href")), (i = i && i.replace(/.*(?=#[^\s]*$)/, ""))), !t.parent("li").hasClass("active"))) {
                    var o = n.find(".active:last a"),
                        r = e.Event("hide.bs.tab", { relatedTarget: t[0] }),
                        a = e.Event("show.bs.tab", { relatedTarget: o[0] });
                    if ((o.trigger(r), t.trigger(a), !a.isDefaultPrevented() && !r.isDefaultPrevented())) {
                        var s = e(i);
                        this.activate(t.closest("li"), n),
                            this.activate(s, s.parent(), function () {
                                o.trigger({ type: "hidden.bs.tab", relatedTarget: t[0] }), t.trigger({ type: "shown.bs.tab", relatedTarget: o[0] });
                            });
                    }
                }
            }),
            (n.prototype.activate = function (t, i, o) {
                function r() {
                    a.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1),
                        t.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0),
                        s ? (t[0].offsetWidth, t.addClass("in")) : t.removeClass("fade"),
                    t.parent(".dropdown-menu").length && t.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0),
                    o && o();
                }
                var a = i.find("> .active"),
                    s = o && e.support.transition && ((a.length && a.hasClass("fade")) || !!i.find("> .fade").length);
                a.length && s ? a.one("bsTransitionEnd", r).emulateTransitionEnd(n.TRANSITION_DURATION) : r(), a.removeClass("in");
            });
        var i = e.fn.tab;
        (e.fn.tab = t),
            (e.fn.tab.Constructor = n),
            (e.fn.tab.noConflict = function () {
                return (e.fn.tab = i), this;
            });
        var o = function (n) {
            n.preventDefault(), t.call(e(this), "show");
        };
        e(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', o).on("click.bs.tab.data-api", '[data-toggle="pill"]', o);
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            return this.each(function () {
                var i = e(this),
                    o = i.data("bs.affix"),
                    r = "object" == typeof t && t;
                o || i.data("bs.affix", (o = new n(this, r))), "string" == typeof t && o[t]();
            });
        }
        var n = function (t, i) {
            (this.options = e.extend({}, n.DEFAULTS, i)),
                (this.$target = e(this.options.target).on("scroll.bs.affix.data-api", e.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", e.proxy(this.checkPositionWithEventLoop, this))),
                (this.$element = e(t)),
                (this.affixed = null),
                (this.unpin = null),
                (this.pinnedOffset = null),
                this.checkPosition();
        };
        (n.VERSION = "3.3.7"),
            (n.RESET = "affix affix-top affix-bottom"),
            (n.DEFAULTS = { offset: 0, target: window }),
            (n.prototype.getState = function (e, t, n, i) {
                var o = this.$target.scrollTop(),
                    r = this.$element.offset(),
                    a = this.$target.height();
                if (null != n && "top" == this.affixed) return n > o && "top";
                if ("bottom" == this.affixed) return null != n ? !(o + this.unpin <= r.top) && "bottom" : !(e - i >= o + a) && "bottom";
                var s = null == this.affixed,
                    l = s ? o : r.top,
                    u = s ? a : t;
                return null != n && n >= o ? "top" : null != i && l + u >= e - i && "bottom";
            }),
            (n.prototype.getPinnedOffset = function () {
                if (this.pinnedOffset) return this.pinnedOffset;
                this.$element.removeClass(n.RESET).addClass("affix");
                var e = this.$target.scrollTop(),
                    t = this.$element.offset();
                return (this.pinnedOffset = t.top - e);
            }),
            (n.prototype.checkPositionWithEventLoop = function () {
                setTimeout(e.proxy(this.checkPosition, this), 1);
            }),
            (n.prototype.checkPosition = function () {
                if (this.$element.is(":visible")) {
                    var t = this.$element.height(),
                        i = this.options.offset,
                        o = i.top,
                        r = i.bottom,
                        a = Math.max(e(document).height(), e(document.body).height());
                    "object" != typeof i && (r = o = i), "function" == typeof o && (o = i.top(this.$element)), "function" == typeof r && (r = i.bottom(this.$element));
                    var s = this.getState(a, t, o, r);
                    if (this.affixed != s) {
                        null != this.unpin && this.$element.css("top", "");
                        var l = "affix" + (s ? "-" + s : ""),
                            u = e.Event(l + ".bs.affix");
                        if ((this.$element.trigger(u), u.isDefaultPrevented())) return;
                        (this.affixed = s),
                            (this.unpin = "bottom" == s ? this.getPinnedOffset() : null),
                            this.$element
                                .removeClass(n.RESET)
                                .addClass(l)
                                .trigger(l.replace("affix", "affixed") + ".bs.affix");
                    }
                    "bottom" == s && this.$element.offset({ top: a - t - r });
                }
            });
        var i = e.fn.affix;
        (e.fn.affix = t),
            (e.fn.affix.Constructor = n),
            (e.fn.affix.noConflict = function () {
                return (e.fn.affix = i), this;
            }),
            e(window).on("load", function () {
                e('[data-spy="affix"]').each(function () {
                    var n = e(this),
                        i = n.data();
                    (i.offset = i.offset || {}), null != i.offsetBottom && (i.offset.bottom = i.offsetBottom), null != i.offsetTop && (i.offset.top = i.offsetTop), t.call(n, i);
                });
            });
    })(jQuery),
    (function (e) {
        "use strict";
        function t(t) {
            var n,
                i = t.attr("data-target") || ((n = t.attr("href")) && n.replace(/.*(?=#[^\s]+$)/, ""));
            return e(i);
        }
        function n(t) {
            return this.each(function () {
                var n = e(this),
                    o = n.data("bs.collapse"),
                    r = e.extend({}, i.DEFAULTS, n.data(), "object" == typeof t && t);
                !o && r.toggle && /show|hide/.test(t) && (r.toggle = !1), o || n.data("bs.collapse", (o = new i(this, r))), "string" == typeof t && o[t]();
            });
        }
        var i = function (t, n) {
            (this.$element = e(t)),
                (this.options = e.extend({}, i.DEFAULTS, n)),
                (this.$trigger = e('[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]')),
                (this.transitioning = null),
                this.options.parent ? (this.$parent = this.getParent()) : this.addAriaAndCollapsedClass(this.$element, this.$trigger),
            this.options.toggle && this.toggle();
        };
        (i.VERSION = "3.3.7"),
            (i.TRANSITION_DURATION = 350),
            (i.DEFAULTS = { toggle: !0 }),
            (i.prototype.dimension = function () {
                return this.$element.hasClass("width") ? "width" : "height";
            }),
            (i.prototype.show = function () {
                if (!this.transitioning && !this.$element.hasClass("in")) {
                    var t,
                        o = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
                    if (!(o && o.length && (t = o.data("bs.collapse")) && t.transitioning)) {
                        var r = e.Event("show.bs.collapse");
                        if ((this.$element.trigger(r), !r.isDefaultPrevented())) {
                            o && o.length && (n.call(o, "hide"), t || o.data("bs.collapse", null));
                            var a = this.dimension();
                            this.$element.removeClass("collapse").addClass("collapsing")[a](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), (this.transitioning = 1);
                            var s = function () {
                                this.$element.removeClass("collapsing").addClass("collapse in")[a](""), (this.transitioning = 0), this.$element.trigger("shown.bs.collapse");
                            };
                            if (!e.support.transition) return s.call(this);
                            var l = e.camelCase(["scroll", a].join("-"));
                            this.$element.one("bsTransitionEnd", e.proxy(s, this)).emulateTransitionEnd(i.TRANSITION_DURATION)[a](this.$element[0][l]);
                        }
                    }
                }
            }),
            (i.prototype.hide = function () {
                if (!this.transitioning && this.$element.hasClass("in")) {
                    var t = e.Event("hide.bs.collapse");
                    if ((this.$element.trigger(t), !t.isDefaultPrevented())) {
                        var n = this.dimension();
                        this.$element[n](this.$element[n]())[0].offsetHeight,
                            this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1),
                            this.$trigger.addClass("collapsed").attr("aria-expanded", !1),
                            (this.transitioning = 1);
                        var o = function () {
                            (this.transitioning = 0), this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse");
                        };
                        return e.support.transition ? void this.$element[n](0).one("bsTransitionEnd", e.proxy(o, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : o.call(this);
                    }
                }
            }),
            (i.prototype.toggle = function () {
                this[this.$element.hasClass("in") ? "hide" : "show"]();
            }),
            (i.prototype.getParent = function () {
                return e(this.options.parent)
                    .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
                    .each(
                        e.proxy(function (n, i) {
                            var o = e(i);
                            this.addAriaAndCollapsedClass(t(o), o);
                        }, this)
                    )
                    .end();
            }),
            (i.prototype.addAriaAndCollapsedClass = function (e, t) {
                var n = e.hasClass("in");
                e.attr("aria-expanded", n), t.toggleClass("collapsed", !n).attr("aria-expanded", n);
            });
        var o = e.fn.collapse;
        (e.fn.collapse = n),
            (e.fn.collapse.Constructor = i),
            (e.fn.collapse.noConflict = function () {
                return (e.fn.collapse = o), this;
            }),
            e(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function (i) {
                var o = e(this);
                o.attr("data-target") || i.preventDefault();
                var r = t(o),
                    a = r.data("bs.collapse"),
                    s = a ? "toggle" : o.data();
                n.call(r, s);
            });
    })(jQuery),
    (function (e) {
        "use strict";
        function t(n, i) {
            (this.$body = e(document.body)),
                (this.$scrollElement = e(e(n).is(document.body) ? window : n)),
                (this.options = e.extend({}, t.DEFAULTS, i)),
                (this.selector = (this.options.target || "") + " .nav li > a"),
                (this.offsets = []),
                (this.targets = []),
                (this.activeTarget = null),
                (this.scrollHeight = 0),
                this.$scrollElement.on("scroll.bs.scrollspy", e.proxy(this.process, this)),
                this.refresh(),
                this.process();
        }
        function n(n) {
            return this.each(function () {
                var i = e(this),
                    o = i.data("bs.scrollspy"),
                    r = "object" == typeof n && n;
                o || i.data("bs.scrollspy", (o = new t(this, r))), "string" == typeof n && o[n]();
            });
        }
        (t.VERSION = "3.3.7"),
            (t.DEFAULTS = { offset: 10 }),
            (t.prototype.getScrollHeight = function () {
                return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight);
            }),
            (t.prototype.refresh = function () {
                var t = this,
                    n = "offset",
                    i = 0;
                (this.offsets = []),
                    (this.targets = []),
                    (this.scrollHeight = this.getScrollHeight()),
                e.isWindow(this.$scrollElement[0]) || ((n = "position"), (i = this.$scrollElement.scrollTop())),
                    this.$body
                        .find(this.selector)
                        .map(function () {
                            var t = e(this),
                                o = t.data("target") || t.attr("href"),
                                r = /^#./.test(o) && e(o);
                            return (r && r.length && r.is(":visible") && [[r[n]().top + i, o]]) || null;
                        })
                        .sort(function (e, t) {
                            return e[0] - t[0];
                        })
                        .each(function () {
                            t.offsets.push(this[0]), t.targets.push(this[1]);
                        });
            }),
            (t.prototype.process = function () {
                var e,
                    t = this.$scrollElement.scrollTop() + this.options.offset,
                    n = this.getScrollHeight(),
                    i = this.options.offset + n - this.$scrollElement.height(),
                    o = this.offsets,
                    r = this.targets,
                    a = this.activeTarget;
                if ((this.scrollHeight != n && this.refresh(), t >= i)) return a != (e = r[r.length - 1]) && this.activate(e);
                if (a && t < o[0]) return (this.activeTarget = null), this.clear();
                for (e = o.length; e--; ) a != r[e] && t >= o[e] && (void 0 === o[e + 1] || t < o[e + 1]) && this.activate(r[e]);
            }),
            (t.prototype.activate = function (t) {
                (this.activeTarget = t), this.clear();
                var n = this.selector + '[data-target="' + t + '"],' + this.selector + '[href="' + t + '"]',
                    i = e(n).parents("li").addClass("active");
                i.parent(".dropdown-menu").length && (i = i.closest("li.dropdown").addClass("active")), i.trigger("activate.bs.scrollspy");
            }),
            (t.prototype.clear = function () {
                e(this.selector).parentsUntil(this.options.target, ".active").removeClass("active");
            });
        var i = e.fn.scrollspy;
        (e.fn.scrollspy = n),
            (e.fn.scrollspy.Constructor = t),
            (e.fn.scrollspy.noConflict = function () {
                return (e.fn.scrollspy = i), this;
            }),
            e(window).on("load.bs.scrollspy.data-api", function () {
                e('[data-spy="scroll"]').each(function () {
                    var t = e(this);
                    n.call(t, t.data());
                });
            });
    })(jQuery),
    (function (e) {
        "use strict";
        function t() {
            var e = document.createElement("bootstrap"),
                t = { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd otransitionend", transition: "transitionend" };
            for (var n in t) if (void 0 !== e.style[n]) return { end: t[n] };
            return !1;
        }
        (e.fn.emulateTransitionEnd = function (t) {
            var n = !1,
                i = this;
            e(this).one("bsTransitionEnd", function () {
                n = !0;
            });
            var o = function () {
                n || e(i).trigger(e.support.transition.end);
            };
            return setTimeout(o, t), this;
        }),
            e(function () {
                (e.support.transition = t()),
                e.support.transition &&
                (e.event.special.bsTransitionEnd = {
                    bindType: e.support.transition.end,
                    delegateType: e.support.transition.end,
                    handle: function (t) {
                        return e(t.target).is(this) ? t.handleObj.handler.apply(this, arguments) : void 0;
                    },
                });
            });
    })(jQuery),
"function" != typeof Object.create &&
(Object.create = function (e) {
    function t() {}
    return (t.prototype = e), new t();
}),
    (function (e, t, n) {
        var i = {
            init: function (t, n) {
                (this.$elem = e(n)), (this.options = e.extend({}, e.fn.owlCarousel.options, this.$elem.data(), t)), (this.userOptions = t), this.loadContent();
            },
            loadContent: function () {
                function t(e) {
                    var t,
                        n = "";
                    if ("function" == typeof i.options.jsonSuccess) i.options.jsonSuccess.apply(this, [e]);
                    else {
                        for (t in e.owl) e.owl.hasOwnProperty(t) && (n += e.owl[t].item);
                        i.$elem.html(n);
                    }
                    i.logIn();
                }
                var n,
                    i = this;
                "function" == typeof i.options.beforeInit && i.options.beforeInit.apply(this, [i.$elem]), "string" == typeof i.options.jsonPath ? ((n = i.options.jsonPath), e.getJSON(n, t)) : i.logIn();
            },
            logIn: function () {
                this.$elem.data("owl-originalStyles", this.$elem.attr("style")),
                    this.$elem.data("owl-originalClasses", this.$elem.attr("class")),
                    this.$elem.css({ opacity: 0 }),
                    (this.orignalItems = this.options.items),
                    this.checkBrowser(),
                    (this.wrapperWidth = 0),
                    (this.checkVisible = null),
                    this.setVars();
            },
            setVars: function () {
                if (0 === this.$elem.children().length) return !1;
                this.baseClass(),
                    this.eventTypes(),
                    (this.$userItems = this.$elem.children()),
                    (this.itemsAmount = this.$userItems.length),
                    this.wrapItems(),
                    (this.$owlItems = this.$elem.find(".owl-item")),
                    (this.$owlWrapper = this.$elem.find(".owl-wrapper")),
                    (this.playDirection = "next"),
                    (this.prevItem = 0),
                    (this.prevArr = [0]),
                    (this.currentItem = 0),
                    this.customEvents(),
                    this.onStartup();
            },
            onStartup: function () {
                this.updateItems(),
                    this.calculateAll(),
                    this.buildControls(),
                    this.updateControls(),
                    this.response(),
                    this.moveEvents(),
                    this.stopOnHover(),
                    this.owlStatus(),
                !1 !== this.options.transitionStyle && this.transitionTypes(this.options.transitionStyle),
                !0 === this.options.autoPlay && (this.options.autoPlay = 5e3),
                    this.play(),
                    this.$elem.find(".owl-wrapper").css("display", "block"),
                    this.$elem.is(":visible") ? this.$elem.css("opacity", 1) : this.watchVisibility(),
                    (this.onstartup = !1),
                    this.eachMoveUpdate(),
                "function" == typeof this.options.afterInit && this.options.afterInit.apply(this, [this.$elem]);
            },
            eachMoveUpdate: function () {
                !0 === this.options.lazyLoad && this.lazyLoad(),
                !0 === this.options.autoHeight && this.autoHeight(),
                    this.onVisibleItems(),
                "function" == typeof this.options.afterAction && this.options.afterAction.apply(this, [this.$elem]);
            },
            updateVars: function () {
                "function" == typeof this.options.beforeUpdate && this.options.beforeUpdate.apply(this, [this.$elem]),
                    this.watchVisibility(),
                    this.updateItems(),
                    this.calculateAll(),
                    this.updatePosition(),
                    this.updateControls(),
                    this.eachMoveUpdate(),
                "function" == typeof this.options.afterUpdate && this.options.afterUpdate.apply(this, [this.$elem]);
            },
            reload: function () {
                var e = this;
                t.setTimeout(function () {
                    e.updateVars();
                }, 0);
            },
            watchVisibility: function () {
                var e = this;
                if (!1 !== e.$elem.is(":visible")) return !1;
                e.$elem.css({ opacity: 0 }),
                    t.clearInterval(e.autoPlayInterval),
                    t.clearInterval(e.checkVisible),
                    (e.checkVisible = t.setInterval(function () {
                        e.$elem.is(":visible") && (e.reload(), e.$elem.animate({ opacity: 1 }, 200), t.clearInterval(e.checkVisible));
                    }, 500));
            },
            wrapItems: function () {
                this.$userItems.wrapAll('<div class="owl-wrapper">').wrap('<div class="owl-item"></div>'),
                    this.$elem.find(".owl-wrapper").wrap('<div class="owl-wrapper-outer">'),
                    (this.wrapperOuter = this.$elem.find(".owl-wrapper-outer")),
                    this.$elem.css("display", "block");
            },
            baseClass: function () {
                var e = this.$elem.hasClass(this.options.baseClass),
                    t = this.$elem.hasClass(this.options.theme);
                e || this.$elem.addClass(this.options.baseClass), t || this.$elem.addClass(this.options.theme);
            },
            updateItems: function () {
                var t, n;
                if (!1 === this.options.responsive) return !1;
                if (!0 === this.options.singleItem)
                    return (
                        (this.options.items = this.orignalItems = 1),
                            (this.options.itemsCustom = !1),
                            (this.options.itemsDesktop = !1),
                            (this.options.itemsDesktopSmall = !1),
                            (this.options.itemsTablet = !1),
                            (this.options.itemsTabletSmall = !1),
                            (this.options.itemsMobile = !1)
                    );
                if (((t = e(this.options.responsiveBaseWidth).width()), t > (this.options.itemsDesktop[0] || this.orignalItems) && (this.options.items = this.orignalItems), !1 !== this.options.itemsCustom))
                    for (
                        this.options.itemsCustom.sort(function (e, t) {
                            return e[0] - t[0];
                        }),
                            n = 0;
                        n < this.options.itemsCustom.length;
                        n += 1
                    )
                        this.options.itemsCustom[n][0] <= t && (this.options.items = this.options.itemsCustom[n][1]);
                else
                    t <= this.options.itemsDesktop[0] && !1 !== this.options.itemsDesktop && (this.options.items = this.options.itemsDesktop[1]),
                    t <= this.options.itemsDesktopSmall[0] && !1 !== this.options.itemsDesktopSmall && (this.options.items = this.options.itemsDesktopSmall[1]),
                    t <= this.options.itemsTablet[0] && !1 !== this.options.itemsTablet && (this.options.items = this.options.itemsTablet[1]),
                    t <= this.options.itemsTabletSmall[0] && !1 !== this.options.itemsTabletSmall && (this.options.items = this.options.itemsTabletSmall[1]),
                    t <= this.options.itemsMobile[0] && !1 !== this.options.itemsMobile && (this.options.items = this.options.itemsMobile[1]);
                this.options.items > this.itemsAmount && !0 === this.options.itemsScaleUp && (this.options.items = this.itemsAmount);
            },
            response: function () {
                var n,
                    i,
                    o = this;
                if (!0 !== o.options.responsive) return !1;
                (i = e(t).width()),
                    (o.resizer = function () {
                        e(t).width() !== i &&
                        (!1 !== o.options.autoPlay && t.clearInterval(o.autoPlayInterval),
                            t.clearTimeout(n),
                            (n = t.setTimeout(function () {
                                (i = e(t).width()), o.updateVars();
                            }, o.options.responsiveRefreshRate)));
                    }),
                    e(t).resize(o.resizer);
            },
            updatePosition: function () {
                this.jumpTo(this.currentItem), !1 !== this.options.autoPlay && this.checkAp();
            },
            appendItemsSizes: function () {
                var t = this,
                    n = 0,
                    i = t.itemsAmount - t.options.items;
                t.$owlItems.each(function (o) {
                    var r = e(this);
                    r.css({ width: t.itemWidth }).data("owl-item", Number(o)), (0 != o % t.options.items && o !== i) || o > i || (n += 1), r.data("owl-roundPages", n);
                });
            },
            appendWrapperSizes: function () {
                this.$owlWrapper.css({ width: this.$owlItems.length * this.itemWidth * 2, left: 0 }), this.appendItemsSizes();
            },
            calculateAll: function () {
                this.calculateWidth(), this.appendWrapperSizes(), this.loops(), this.max();
            },
            calculateWidth: function () {
                this.itemWidth = Math.round(this.$elem.width() / this.options.items);
            },
            max: function () {
                var e = -1 * (this.itemsAmount * this.itemWidth - this.options.items * this.itemWidth);
                return this.options.items > this.itemsAmount ? (this.maximumPixels = e = this.maximumItem = 0) : ((this.maximumItem = this.itemsAmount - this.options.items), (this.maximumPixels = e)), e;
            },
            min: function () {
                return 0;
            },
            loops: function () {
                var t,
                    n,
                    i = 0,
                    o = 0;
                for (this.positionsInArray = [0], this.pagesInArray = [], t = 0; t < this.itemsAmount; t += 1)
                    (o += this.itemWidth),
                        this.positionsInArray.push(-o),
                    !0 === this.options.scrollPerPage && ((n = e(this.$owlItems[t])), (n = n.data("owl-roundPages")) !== i && ((this.pagesInArray[i] = this.positionsInArray[t]), (i = n)));
            },
            buildControls: function () {
                (!0 !== this.options.navigation && !0 !== this.options.pagination) || (this.owlControls = e('<div class="owl-controls"/>').toggleClass("clickable", !this.browser.isTouch).appendTo(this.$elem)),
                !0 === this.options.pagination && this.buildPagination(),
                !0 === this.options.navigation && this.buildButtons();
            },
            buildButtons: function () {
                var t = this,
                    n = e('<div class="owl-buttons"/>');
                t.owlControls.append(n),
                    (t.buttonPrev = e("<div/>", { class: "owl-prev", html: t.options.navigationText[0] || "" })),
                    (t.buttonNext = e("<div/>", { class: "owl-next", html: t.options.navigationText[1] || "" })),
                    n.append(t.buttonPrev).append(t.buttonNext),
                    n.on("touchstart.owlControls mousedown.owlControls", 'div[class^="owl"]', function (e) {
                        e.preventDefault();
                    }),
                    n.on("touchend.owlControls mouseup.owlControls", 'div[class^="owl"]', function (n) {
                        n.preventDefault(), e(this).hasClass("owl-next") ? t.next() : t.prev();
                    });
            },
            buildPagination: function () {
                var t = this;
                (t.paginationWrapper = e('<div class="owl-pagination"/>')),
                    t.owlControls.append(t.paginationWrapper),
                    t.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function (n) {
                        n.preventDefault(), Number(e(this).data("owl-page")) !== t.currentItem && t.goTo(Number(e(this).data("owl-page")), !0);
                    });
            },
            updatePagination: function () {
                var t, n, i, o, r, a;
                if (!1 === this.options.pagination) return !1;
                for (this.paginationWrapper.html(""), t = 0, n = this.itemsAmount - (this.itemsAmount % this.options.items), o = 0; o < this.itemsAmount; o += 1)
                    0 == o % this.options.items &&
                    ((t += 1),
                    n === o && (i = this.itemsAmount - this.options.items),
                        (r = e("<div/>", { class: "owl-page" })),
                        (a = e("<span></span>", { text: !0 === this.options.paginationNumbers ? t : "", class: !0 === this.options.paginationNumbers ? "owl-numbers" : "" })),
                        r.append(a),
                        r.data("owl-page", n === o ? i : o),
                        r.data("owl-roundPages", t),
                        this.paginationWrapper.append(r));
                this.checkPagination();
            },
            checkPagination: function () {
                var t = this;
                if (!1 === t.options.pagination) return !1;
                t.paginationWrapper.find(".owl-page").each(function () {
                    e(this).data("owl-roundPages") === e(t.$owlItems[t.currentItem]).data("owl-roundPages") && (t.paginationWrapper.find(".owl-page").removeClass("active"), e(this).addClass("active"));
                });
            },
            checkNavigation: function () {
                if (!1 === this.options.navigation) return !1;
                !1 === this.options.rewindNav &&
                (0 === this.currentItem && 0 === this.maximumItem
                    ? (this.buttonPrev.addClass("disabled"), this.buttonNext.addClass("disabled"))
                    : 0 === this.currentItem && 0 !== this.maximumItem
                        ? (this.buttonPrev.addClass("disabled"), this.buttonNext.removeClass("disabled"))
                        : this.currentItem === this.maximumItem
                            ? (this.buttonPrev.removeClass("disabled"), this.buttonNext.addClass("disabled"))
                            : 0 !== this.currentItem && this.currentItem !== this.maximumItem && (this.buttonPrev.removeClass("disabled"), this.buttonNext.removeClass("disabled")));
            },
            updateControls: function () {
                this.updatePagination(), this.checkNavigation(), this.owlControls && (this.options.items >= this.itemsAmount ? this.owlControls.hide() : this.owlControls.show());
            },
            destroyControls: function () {
                this.owlControls && this.owlControls.remove();
            },
            next: function (e) {
                if (this.isTransition) return !1;
                if (((this.currentItem += !0 === this.options.scrollPerPage ? this.options.items : 1), this.currentItem > this.maximumItem + (!0 === this.options.scrollPerPage ? this.options.items - 1 : 0))) {
                    if (!0 !== this.options.rewindNav) return (this.currentItem = this.maximumItem), !1;
                    (this.currentItem = 0), (e = "rewind");
                }
                this.goTo(this.currentItem, e);
            },
            prev: function (e) {
                if (this.isTransition) return !1;
                if (
                    ((this.currentItem = !0 === this.options.scrollPerPage && 0 < this.currentItem && this.currentItem < this.options.items ? 0 : this.currentItem - (!0 === this.options.scrollPerPage ? this.options.items : 1)),
                    0 > this.currentItem)
                ) {
                    if (!0 !== this.options.rewindNav) return (this.currentItem = 0), !1;
                    (this.currentItem = this.maximumItem), (e = "rewind");
                }
                this.goTo(this.currentItem, e);
            },
            goTo: function (e, n, i) {
                var o = this;
                return (
                    !o.isTransition &&
                    ("function" == typeof o.options.beforeMove && o.options.beforeMove.apply(this, [o.$elem]),
                        e >= o.maximumItem ? (e = o.maximumItem) : 0 >= e && (e = 0),
                        (o.currentItem = o.owl.currentItem = e),
                        !1 !== o.options.transitionStyle && "drag" !== i && 1 === o.options.items && !0 === o.browser.support3d
                            ? (o.swapSpeed(0), !0 === o.browser.support3d ? o.transition3d(o.positionsInArray[e]) : o.css2slide(o.positionsInArray[e], 1), o.afterGo(), o.singleItemTransition(), !1)
                            : ((e = o.positionsInArray[e]),
                                !0 === o.browser.support3d
                                    ? ((o.isCss3Finish = !1),
                                        !0 === n
                                            ? (o.swapSpeed("paginationSpeed"),
                                                t.setTimeout(function () {
                                                    o.isCss3Finish = !0;
                                                }, o.options.paginationSpeed))
                                            : "rewind" === n
                                            ? (o.swapSpeed(o.options.rewindSpeed),
                                                t.setTimeout(function () {
                                                    o.isCss3Finish = !0;
                                                }, o.options.rewindSpeed))
                                            : (o.swapSpeed("slideSpeed"),
                                                t.setTimeout(function () {
                                                    o.isCss3Finish = !0;
                                                }, o.options.slideSpeed)),
                                        o.transition3d(e))
                                    : !0 === n
                                    ? o.css2slide(e, o.options.paginationSpeed)
                                    : "rewind" === n
                                        ? o.css2slide(e, o.options.rewindSpeed)
                                        : o.css2slide(e, o.options.slideSpeed),
                                void o.afterGo()))
                );
            },
            jumpTo: function (e) {
                "function" == typeof this.options.beforeMove && this.options.beforeMove.apply(this, [this.$elem]),
                    e >= this.maximumItem || -1 === e ? (e = this.maximumItem) : 0 >= e && (e = 0),
                    this.swapSpeed(0),
                    !0 === this.browser.support3d ? this.transition3d(this.positionsInArray[e]) : this.css2slide(this.positionsInArray[e], 1),
                    (this.currentItem = this.owl.currentItem = e),
                    this.afterGo();
            },
            afterGo: function () {
                this.prevArr.push(this.currentItem),
                    (this.prevItem = this.owl.prevItem = this.prevArr[this.prevArr.length - 2]),
                    this.prevArr.shift(0),
                this.prevItem !== this.currentItem && (this.checkPagination(), this.checkNavigation(), this.eachMoveUpdate(), !1 !== this.options.autoPlay && this.checkAp()),
                "function" == typeof this.options.afterMove && this.prevItem !== this.currentItem && this.options.afterMove.apply(this, [this.$elem]);
            },
            stop: function () {
                (this.apStatus = "stop"), t.clearInterval(this.autoPlayInterval);
            },
            checkAp: function () {
                "stop" !== this.apStatus && this.play();
            },
            play: function () {
                var e = this;
                if (((e.apStatus = "play"), !1 === e.options.autoPlay)) return !1;
                t.clearInterval(e.autoPlayInterval),
                    (e.autoPlayInterval = t.setInterval(function () {
                        e.next(!0);
                    }, e.options.autoPlay));
            },
            swapSpeed: function (e) {
                "slideSpeed" === e
                    ? this.$owlWrapper.css(this.addCssSpeed(this.options.slideSpeed))
                    : "paginationSpeed" === e
                    ? this.$owlWrapper.css(this.addCssSpeed(this.options.paginationSpeed))
                    : "string" != typeof e && this.$owlWrapper.css(this.addCssSpeed(e));
            },
            addCssSpeed: function (e) {
                return { "-webkit-transition": "all " + e + "ms ease", "-moz-transition": "all " + e + "ms ease", "-o-transition": "all " + e + "ms ease", transition: "all " + e + "ms ease" };
            },
            removeTransition: function () {
                return { "-webkit-transition": "", "-moz-transition": "", "-o-transition": "", transition: "" };
            },
            doTranslate: function (e) {
                return {
                    "-webkit-transform": "translate3d(" + e + "px, 0px, 0px)",
                    "-moz-transform": "translate3d(" + e + "px, 0px, 0px)",
                    "-o-transform": "translate3d(" + e + "px, 0px, 0px)",
                    "-ms-transform": "translate3d(" + e + "px, 0px, 0px)",
                    transform: "translate3d(" + e + "px, 0px,0px)",
                };
            },
            transition3d: function (e) {
                this.$owlWrapper.css(this.doTranslate(e));
            },
            css2move: function (e) {
                this.$owlWrapper.css({ left: e });
            },
            css2slide: function (e, t) {
                var n = this;
                (n.isCssFinish = !1),
                    n.$owlWrapper.stop(!0, !0).animate(
                        { left: e },
                        {
                            duration: t || n.options.slideSpeed,
                            complete: function () {
                                n.isCssFinish = !0;
                            },
                        }
                    );
            },
            checkBrowser: function () {
                var e = n.createElement("div");
                (e.style.cssText =
                    "  -moz-transform:translate3d(0px, 0px, 0px); -ms-transform:translate3d(0px, 0px, 0px); -o-transform:translate3d(0px, 0px, 0px); -webkit-transform:translate3d(0px, 0px, 0px); transform:translate3d(0px, 0px, 0px)"),
                    (e = e.style.cssText.match(/translate3d\(0px, 0px, 0px\)/g)),
                    (this.browser = { support3d: null !== e && 1 === e.length, isTouch: "ontouchstart" in t || t.navigator.msMaxTouchPoints });
            },
            moveEvents: function () {
                (!1 === this.options.mouseDrag && !1 === this.options.touchDrag) || (this.gestures(), this.disabledEvents());
            },
            eventTypes: function () {
                var e = ["s", "e", "x"];
                (this.ev_types = {}),
                    !0 === this.options.mouseDrag && !0 === this.options.touchDrag
                        ? (e = ["touchstart.owl mousedown.owl", "touchmove.owl mousemove.owl", "touchend.owl touchcancel.owl mouseup.owl"])
                        : !1 === this.options.mouseDrag && !0 === this.options.touchDrag
                        ? (e = ["touchstart.owl", "touchmove.owl", "touchend.owl touchcancel.owl"])
                        : !0 === this.options.mouseDrag && !1 === this.options.touchDrag && (e = ["mousedown.owl", "mousemove.owl", "mouseup.owl"]),
                    (this.ev_types.start = e[0]),
                    (this.ev_types.move = e[1]),
                    (this.ev_types.end = e[2]);
            },
            disabledEvents: function () {
                this.$elem.on("dragstart.owl", function (e) {
                    e.preventDefault();
                }),
                    this.$elem.on("mousedown.disableTextSelect", function (t) {
                        return e(t.target).is("input, textarea, select, option");
                    });
            },
            gestures: function () {
                function i(e) {
                    if (void 0 !== e.touches) return { x: e.touches[0].pageX, y: e.touches[0].pageY };
                    if (void 0 === e.touches) {
                        if (void 0 !== e.pageX) return { x: e.pageX, y: e.pageY };
                        if (void 0 === e.pageX) return { x: e.clientX, y: e.clientY };
                    }
                }
                function o(t) {
                    "on" === t ? (e(n).on(s.ev_types.move, r), e(n).on(s.ev_types.end, a)) : "off" === t && (e(n).off(s.ev_types.move), e(n).off(s.ev_types.end));
                }
                function r(o) {
                    (o = o.originalEvent || o || t.event),
                        (s.newPosX = i(o).x - l.offsetX),
                        (s.newPosY = i(o).y - l.offsetY),
                        (s.newRelativeX = s.newPosX - l.relativePos),
                    "function" == typeof s.options.startDragging && !0 !== l.dragging && 0 !== s.newRelativeX && ((l.dragging = !0), s.options.startDragging.apply(s, [s.$elem])),
                    (8 < s.newRelativeX || -8 > s.newRelativeX) && !0 === s.browser.isTouch && (void 0 !== o.preventDefault ? o.preventDefault() : (o.returnValue = !1), (l.sliding = !0)),
                    (10 < s.newPosY || -10 > s.newPosY) && !1 === l.sliding && e(n).off("touchmove.owl"),
                        (s.newPosX = Math.max(Math.min(s.newPosX, s.newRelativeX / 5), s.maximumPixels + s.newRelativeX / 5)),
                        !0 === s.browser.support3d ? s.transition3d(s.newPosX) : s.css2move(s.newPosX);
                }
                function a(n) {
                    n = n.originalEvent || n || t.event;
                    var i;
                    (n.target = n.target || n.srcElement),
                        (l.dragging = !1),
                    !0 !== s.browser.isTouch && s.$owlWrapper.removeClass("grabbing"),
                        (s.dragDirection = 0 > s.newRelativeX ? (s.owl.dragDirection = "left") : (s.owl.dragDirection = "right")),
                    0 !== s.newRelativeX &&
                    ((i = s.getNewPosition()),
                        s.goTo(i, !1, "drag"),
                    l.targetElement === n.target &&
                    !0 !== s.browser.isTouch &&
                    (e(n.target).on("click.disable", function (t) {
                        t.stopImmediatePropagation(), t.stopPropagation(), t.preventDefault(), e(t.target).off("click.disable");
                    }),
                        (n = e._data(n.target, "events").click),
                        (i = n.pop()),
                        n.splice(0, 0, i))),
                        o("off");
                }
                var s = this,
                    l = { offsetX: 0, offsetY: 0, baseElWidth: 0, relativePos: 0, position: null, minSwipe: null, maxSwipe: null, sliding: null, dargging: null, targetElement: null };
                (s.isCssFinish = !0),
                    s.$elem.on(s.ev_types.start, ".owl-wrapper", function (n) {
                        n = n.originalEvent || n || t.event;
                        var r;
                        if (3 === n.which) return !1;
                        if (!(s.itemsAmount <= s.options.items)) {
                            if ((!1 === s.isCssFinish && !s.options.dragBeforeAnimFinish) || (!1 === s.isCss3Finish && !s.options.dragBeforeAnimFinish)) return !1;
                            !1 !== s.options.autoPlay && t.clearInterval(s.autoPlayInterval),
                            !0 === s.browser.isTouch || s.$owlWrapper.hasClass("grabbing") || s.$owlWrapper.addClass("grabbing"),
                                (s.newPosX = 0),
                                (s.newRelativeX = 0),
                                e(this).css(s.removeTransition()),
                                (r = e(this).position()),
                                (l.relativePos = r.left),
                                (l.offsetX = i(n).x - r.left),
                                (l.offsetY = i(n).y - r.top),
                                o("on"),
                                (l.sliding = !1),
                                (l.targetElement = n.target || n.srcElement);
                        }
                    });
            },
            getNewPosition: function () {
                var e = this.closestItem();
                return e > this.maximumItem ? (e = this.currentItem = this.maximumItem) : 0 <= this.newPosX && (this.currentItem = e = 0), e;
            },
            closestItem: function () {
                var t = this,
                    n = !0 === t.options.scrollPerPage ? t.pagesInArray : t.positionsInArray,
                    i = t.newPosX,
                    o = null;
                return (
                    e.each(n, function (r, a) {
                        i - t.itemWidth / 20 > n[r + 1] && i - t.itemWidth / 20 < a && "left" === t.moveDirection()
                            ? ((o = a), (t.currentItem = !0 === t.options.scrollPerPage ? e.inArray(o, t.positionsInArray) : r))
                            : i + t.itemWidth / 20 < a &&
                            i + t.itemWidth / 20 > (n[r + 1] || n[r] - t.itemWidth) &&
                            "right" === t.moveDirection() &&
                            (!0 === t.options.scrollPerPage ? ((o = n[r + 1] || n[n.length - 1]), (t.currentItem = e.inArray(o, t.positionsInArray))) : ((o = n[r + 1]), (t.currentItem = r + 1)));
                    }),
                        t.currentItem
                );
            },
            moveDirection: function () {
                var e;
                return 0 > this.newRelativeX ? ((e = "right"), (this.playDirection = "next")) : ((e = "left"), (this.playDirection = "prev")), e;
            },
            customEvents: function () {
                var e = this;
                e.$elem.on("owl.next", function () {
                    e.next();
                }),
                    e.$elem.on("owl.prev", function () {
                        e.prev();
                    }),
                    e.$elem.on("owl.play", function (t, n) {
                        (e.options.autoPlay = n), e.play(), (e.hoverStatus = "play");
                    }),
                    e.$elem.on("owl.stop", function () {
                        e.stop(), (e.hoverStatus = "stop");
                    }),
                    e.$elem.on("owl.goTo", function (t, n) {
                        e.goTo(n);
                    }),
                    e.$elem.on("owl.jumpTo", function (t, n) {
                        e.jumpTo(n);
                    });
            },
            stopOnHover: function () {
                var e = this;
                !0 === e.options.stopOnHover &&
                !0 !== e.browser.isTouch &&
                !1 !== e.options.autoPlay &&
                (e.$elem.on("mouseover", function () {
                    e.stop();
                }),
                    e.$elem.on("mouseout", function () {
                        "stop" !== e.hoverStatus && e.play();
                    }));
            },
            lazyLoad: function () {
                var t, n, i, o;
                if (!1 === this.options.lazyLoad) return !1;
                for (t = 0; t < this.itemsAmount; t += 1)
                    (n = e(this.$owlItems[t])),
                    "loaded" !== n.data("owl-loaded") &&
                    ((i = n.data("owl-item")),
                        (o = n.find(".lazyOwl")),
                        "string" != typeof o.data("src")
                            ? n.data("owl-loaded", "loaded")
                            : (void 0 === n.data("owl-loaded") && (o.hide(), n.addClass("loading").data("owl-loaded", "checked")),
                            (!0 !== this.options.lazyFollow || i >= this.currentItem) && i < this.currentItem + this.options.items && o.length && this.lazyPreload(n, o)));
            },
            lazyPreload: function (e, n) {
                function i() {
                    e.data("owl-loaded", "loaded").removeClass("loading"),
                        n.removeAttr("data-src"),
                        "fade" === a.options.lazyEffect ? n.fadeIn(400) : n.show(),
                    "function" == typeof a.options.afterLazyLoad && a.options.afterLazyLoad.apply(this, [a.$elem]);
                }
                function o() {
                    (s += 1), a.completeImg(n.get(0)) || !0 === r ? i() : 100 >= s ? t.setTimeout(o, 100) : i();
                }
                var r,
                    a = this,
                    s = 0;
                "DIV" === n.prop("tagName") ? (n.css("background-image", "url(" + n.data("src") + ")"), (r = !0)) : (n[0].src = n.data("src")), o();
            },
            autoHeight: function () {
                function n() {
                    var n = e(r.$owlItems[r.currentItem]).height();
                    r.wrapperOuter.css("height", n + "px"),
                    r.wrapperOuter.hasClass("autoHeight") ||
                    t.setTimeout(function () {
                        r.wrapperOuter.addClass("autoHeight");
                    }, 0);
                }
                function i() {
                    (o += 1), r.completeImg(a.get(0)) ? n() : 100 >= o ? t.setTimeout(i, 100) : r.wrapperOuter.css("height", "");
                }
                var o,
                    r = this,
                    a = e(r.$owlItems[r.currentItem]).find("img");
                void 0 !== a.get(0) ? ((o = 0), i()) : n();
            },
            completeImg: function (e) {
                return !(!e.complete || (void 0 !== e.naturalWidth && 0 === e.naturalWidth));
            },
            onVisibleItems: function () {
                var t;
                for (!0 === this.options.addClassActive && this.$owlItems.removeClass("active"), this.visibleItems = [], t = this.currentItem; t < this.currentItem + this.options.items; t += 1)
                    this.visibleItems.push(t), !0 === this.options.addClassActive && e(this.$owlItems[t]).addClass("active");
                this.owl.visibleItems = this.visibleItems;
            },
            transitionTypes: function (e) {
                (this.outClass = "owl-" + e + "-out"), (this.inClass = "owl-" + e + "-in");
            },
            singleItemTransition: function () {
                var e = this,
                    t = e.outClass,
                    n = e.inClass,
                    i = e.$owlItems.eq(e.currentItem),
                    o = e.$owlItems.eq(e.prevItem),
                    r = Math.abs(e.positionsInArray[e.currentItem]) + e.positionsInArray[e.prevItem],
                    a = Math.abs(e.positionsInArray[e.currentItem]) + e.itemWidth / 2;
                (e.isTransition = !0),
                    e.$owlWrapper.addClass("owl-origin").css({ "-webkit-transform-origin": a + "px", "-moz-perspective-origin": a + "px", "perspective-origin": a + "px" }),
                    o
                        .css({ position: "relative", left: r + "px" })
                        .addClass(t)
                        .on("webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend", function () {
                            (e.endPrev = !0), o.off("webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend"), e.clearTransStyle(o, t);
                        }),
                    i.addClass(n).on("webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend", function () {
                        (e.endCurrent = !0), i.off("webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend"), e.clearTransStyle(i, n);
                    });
            },
            clearTransStyle: function (e, t) {
                e.css({ position: "", left: "" }).removeClass(t), this.endPrev && this.endCurrent && (this.$owlWrapper.removeClass("owl-origin"), (this.isTransition = this.endCurrent = this.endPrev = !1));
            },
            owlStatus: function () {
                this.owl = {
                    userOptions: this.userOptions,
                    baseElement: this.$elem,
                    userItems: this.$userItems,
                    owlItems: this.$owlItems,
                    currentItem: this.currentItem,
                    prevItem: this.prevItem,
                    visibleItems: this.visibleItems,
                    isTouch: this.browser.isTouch,
                    browser: this.browser,
                    dragDirection: this.dragDirection,
                };
            },
            clearEvents: function () {
                this.$elem.off(".owl owl mousedown.disableTextSelect"), e(n).off(".owl owl"), e(t).off("resize", this.resizer);
            },
            unWrap: function () {
                0 !== this.$elem.children().length && (this.$owlWrapper.unwrap(), this.$userItems.unwrap().unwrap(), this.owlControls && this.owlControls.remove()),
                    this.clearEvents(),
                    this.$elem.attr("style", this.$elem.data("owl-originalStyles") || "").attr("class", this.$elem.data("owl-originalClasses"));
            },
            destroy: function () {
                this.stop(), t.clearInterval(this.checkVisible), this.unWrap(), this.$elem.removeData();
            },
            reinit: function (t) {
                (t = e.extend({}, this.userOptions, t)), this.unWrap(), this.init(t, this.$elem);
            },
            addItem: function (e, t) {
                var n;
                return (
                    !!e &&
                    (0 === this.$elem.children().length
                        ? (this.$elem.append(e), this.setVars(), !1)
                        : (this.unWrap(), (n = void 0 === t || -1 === t ? -1 : t), n >= this.$userItems.length || -1 === n ? this.$userItems.eq(-1).after(e) : this.$userItems.eq(n).before(e), void this.setVars()))
                );
            },
            removeItem: function (e) {
                if (0 === this.$elem.children().length) return !1;
                (e = void 0 === e || -1 === e ? -1 : e), this.unWrap(), this.$userItems.eq(e).remove(), this.setVars();
            },
        };
        (e.fn.owlCarousel = function (t) {
            return this.each(function () {
                if (!0 === e(this).data("owl-init")) return !1;
                e(this).data("owl-init", !0);
                var n = Object.create(i);
                n.init(t, this), e.data(this, "owlCarousel", n);
            });
        }),
            (e.fn.owlCarousel.options = {
                items: 5,
                itemsCustom: !1,
                itemsDesktop: [1199, 4],
                itemsDesktopSmall: [979, 3],
                itemsTablet: [768, 2],
                itemsTabletSmall: !1,
                itemsMobile: [479, 1],
                singleItem: !1,
                itemsScaleUp: !1,
                slideSpeed: 200,
                paginationSpeed: 800,
                rewindSpeed: 1e3,
                autoPlay: !1,
                stopOnHover: !1,
                navigation: !1,
                navigationText: ["prev", "next"],
                rewindNav: !0,
                scrollPerPage: !1,
                pagination: !0,
                paginationNumbers: !1,
                responsive: !0,
                responsiveRefreshRate: 200,
                responsiveBaseWidth: t,
                baseClass: "owl-carousel",
                theme: "owl-theme",
                lazyLoad: !1,
                lazyFollow: !0,
                lazyEffect: "fade",
                autoHeight: !1,
                jsonPath: !1,
                jsonSuccess: !1,
                dragBeforeAnimFinish: !0,
                mouseDrag: !0,
                touchDrag: !0,
                addClassActive: !1,
                transitionStyle: !1,
                beforeUpdate: !1,
                afterUpdate: !1,
                beforeInit: !1,
                afterInit: !1,
                beforeMove: !1,
                afterMove: !1,
                afterAction: !1,
                startDragging: !1,
                afterLazyLoad: !1,
            });
    })(jQuery, window, document),
    (function (e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : jQuery);
    })(function (e) {
        var t,
            n = navigator.userAgent,
            i = /iphone/i.test(n),
            o = /chrome/i.test(n),
            r = /android/i.test(n);
        (e.mask = { definitions: { 9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]" }, autoclear: !0, dataName: "rawMaskFn", placeholder: "_" }),
            e.fn.extend({
                caret: function (e, t) {
                    var n;
                    if (0 !== this.length && !this.is(":hidden"))
                        return "number" == typeof e
                            ? ((t = "number" == typeof t ? t : e),
                                this.each(function () {
                                    this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && ((n = this.createTextRange()), n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select());
                                }))
                            : (this[0].setSelectionRange
                                ? ((e = this[0].selectionStart), (t = this[0].selectionEnd))
                                : document.selection && document.selection.createRange && ((n = document.selection.createRange()), (e = 0 - n.duplicate().moveStart("character", -1e5)), (t = e + n.text.length)),
                                { begin: e, end: t });
                },
                unmask: function () {
                    return this.trigger("unmask");
                },
                mask: function (n, a) {
                    var s, l, u, c, d, p, h, f;
                    if (!n && this.length > 0) {
                        s = e(this[0]);
                        var m = s.data(e.mask.dataName);
                        return m ? m() : void 0;
                    }
                    return (
                        (a = e.extend({ autoclear: e.mask.autoclear, placeholder: e.mask.placeholder, completed: null }, a)),
                            (l = e.mask.definitions),
                            (u = []),
                            (c = h = n.length),
                            (d = null),
                            e.each(n.split(""), function (e, t) {
                                "?" == t ? (h--, (c = e)) : l[t] ? (u.push(new RegExp(l[t])), null === d && (d = u.length - 1), c > e && (p = u.length - 1)) : u.push(null);
                            }),
                            this.trigger("unmask").each(function () {
                                function s() {
                                    if (a.completed) {
                                        for (var e = d; p >= e; e++) if (u[e] && O[e] === m(e)) return;
                                        a.completed.call(D);
                                    }
                                }
                                function m(e) {
                                    return a.placeholder.charAt(e < a.placeholder.length ? e : 0);
                                }
                                function g(e) {
                                    for (; ++e < h && !u[e]; );
                                    return e;
                                }
                                function v(e) {
                                    for (; --e >= 0 && !u[e]; );
                                    return e;
                                }
                                function y(e, t) {
                                    var n, i;
                                    if (!(0 > e)) {
                                        for (n = e, i = g(t); h > n; n++)
                                            if (u[n]) {
                                                if (!(h > i && u[n].test(O[i]))) break;
                                                (O[n] = O[i]), (O[i] = m(i)), (i = g(i));
                                            }
                                        S(), D.caret(Math.max(d, e));
                                    }
                                }
                                function b(e) {
                                    var t, n, i, o;
                                    for (t = e, n = m(e); h > t; t++)
                                        if (u[t]) {
                                            if (((i = g(t)), (o = O[t]), (O[t] = n), !(h > i && u[i].test(o)))) break;
                                            n = o;
                                        }
                                }
                                function w() {
                                    var e = D.val(),
                                        t = D.caret();
                                    if (f && f.length && f.length > e.length) {
                                        for (C(!0); t.begin > 0 && !u[t.begin - 1]; ) t.begin--;
                                        if (0 === t.begin) for (; t.begin < d && !u[t.begin]; ) t.begin++;
                                        D.caret(t.begin, t.begin);
                                    } else {
                                        for (C(!0); t.begin < h && !u[t.begin]; ) t.begin++;
                                        D.caret(t.begin, t.begin);
                                    }
                                    s();
                                }
                                function x() {
                                    C(), D.val() != I && D.change();
                                }
                                function T(e) {
                                    if (!D.prop("readonly")) {
                                        var t,
                                            n,
                                            o,
                                            r = e.which || e.keyCode;
                                        (f = D.val()),
                                            8 === r || 46 === r || (i && 127 === r)
                                                ? ((t = D.caret()), (n = t.begin), (o = t.end), o - n == 0 && ((n = 46 !== r ? v(n) : (o = g(n - 1))), (o = 46 === r ? g(o) : o)), $(n, o), y(n, o - 1), e.preventDefault())
                                                : 13 === r
                                                ? x.call(this, e)
                                                : 27 === r && (D.val(I), D.caret(0, C()), e.preventDefault());
                                    }
                                }
                                function k(t) {
                                    if (!D.prop("readonly")) {
                                        var n,
                                            i,
                                            o,
                                            a = t.which || t.keyCode,
                                            l = D.caret();
                                        if (!(t.ctrlKey || t.altKey || t.metaKey || 32 > a) && a && 13 !== a) {
                                            if ((l.end - l.begin != 0 && ($(l.begin, l.end), y(l.begin, l.end - 1)), (n = g(l.begin - 1)), h > n && ((i = String.fromCharCode(a)), u[n].test(i)))) {
                                                if ((b(n), (O[n] = i), S(), (o = g(n)), r)) {
                                                    var c = function () {
                                                        e.proxy(e.fn.caret, D, o)();
                                                    };
                                                    setTimeout(c, 0);
                                                } else D.caret(o);
                                                l.begin <= p && s();
                                            }
                                            t.preventDefault();
                                        }
                                    }
                                }
                                function $(e, t) {
                                    var n;
                                    for (n = e; t > n && h > n; n++) u[n] && (O[n] = m(n));
                                }
                                function S() {
                                    D.val(O.join(""));
                                }
                                function C(e) {
                                    var t,
                                        n,
                                        i,
                                        o = D.val(),
                                        r = -1;
                                    for (t = 0, i = 0; h > t; t++)
                                        if (u[t]) {
                                            for (O[t] = m(t); i++ < o.length; )
                                                if (((n = o.charAt(i - 1)), u[t].test(n))) {
                                                    (O[t] = n), (r = t);
                                                    break;
                                                }
                                            if (i > o.length) {
                                                $(t + 1, h);
                                                break;
                                            }
                                        } else O[t] === o.charAt(i) && i++, c > t && (r = t);
                                    return e ? S() : c > r + 1 ? (a.autoclear || O.join("") === A ? (D.val() && D.val(""), $(0, h)) : S()) : (S(), D.val(D.val().substring(0, r + 1))), c ? t : d;
                                }
                                var D = e(this),
                                    O = e.map(n.split(""), function (e, t) {
                                        return "?" != e ? (l[e] ? m(t) : e) : void 0;
                                    }),
                                    A = O.join(""),
                                    I = D.val();
                                D.data(e.mask.dataName, function () {
                                    return e
                                        .map(O, function (e, t) {
                                            return u[t] && e != m(t) ? e : null;
                                        })
                                        .join("");
                                }),
                                    D.one("unmask", function () {
                                        D.off(".mask").removeData(e.mask.dataName);
                                    })
                                        .on("focus.mask", function () {
                                            if (!D.prop("readonly")) {
                                                clearTimeout(t);
                                                var e;
                                                (I = D.val()),
                                                    (e = C()),
                                                    (t = setTimeout(function () {
                                                        D.get(0) === document.activeElement && (S(), e == n.replace("?", "").length ? D.caret(0, e) : D.caret(e));
                                                    }, 10));
                                            }
                                        })
                                        .on("blur.mask", x)
                                        .on("keydown.mask", T)
                                        .on("keypress.mask", k)
                                        .on("input.mask paste.mask", function () {
                                            D.prop("readonly") ||
                                            setTimeout(function () {
                                                var e = C(!0);
                                                D.caret(e), s();
                                            }, 0);
                                        }),
                                o && r && D.off("input.mask").on("input.mask", w),
                                    C();
                            })
                    );
                },
            });
    }),
    (function (e, t) {
        if (!e.fn.ionCheckRadio) {
            String.prototype.trim ||
            (function () {
                var e = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(e, "");
                };
            })();
            var n = {},
                i = {},
                o = function (t) {
                    (this.group = t.content), (this.type = t.type), (this.$group = e(this.group)), (this.observer = this.old = null), this.init();
                };
            (o.prototype = {
                init: function () {
                    this.$group.eq(0).hasClass("icr-input") ? this.prepare() : this.createHTML();
                },
                prepare: function () {
                    var n,
                        i,
                        o = this;
                    for (i = 0; i < this.group.length; i++) (n = e(this.group[i])), (n = n.parent().parent()), e.data(this.group[i], "icr-parent", n), this.presetChecked(this.group[i]), this.presetDisabled(this.group[i]);
                    this.$group.on("change", function () {
                        o.change(this);
                    }),
                        this.$group.on("focus", function () {
                            o.focus(this);
                        }),
                        this.$group.on("blur", function () {
                            o.blur(this);
                        }),
                    t.MutationObserver && this.setUpObserver();
                },
                setUpObserver: function () {
                    var e,
                        t,
                        n = this;
                    for (
                        this.observer = new MutationObserver(function (t) {
                            t.forEach(function (t) {
                                (e = t.target), "disabled" === t.attributeName && n.toggle(n.getParent(e), e.disabled, "disabled");
                            });
                        }),
                            t = 0;
                        t < this.group.length;
                        t++
                    )
                        this.observer.observe(this.group[t], { attributes: !0 });
                },
                destroy: function () {
                    this.$group.off(), this.observer && (this.observer.disconnect(), (this.observer = null));
                },
                presetChecked: function (e) {
                    e.checked && (this.toggle(this.getParent(e), !0, "checked"), "radio" === this.type && (this.old = e));
                },
                presetDisabled: function (e) {
                    e.disabled && this.toggle(this.getParent(e), !0, "disabled");
                },
                change: function (e) {
                    this.toggle(this.getParent(e), e.checked, "checked"), "radio" === this.type && this.old && this.old !== e && this.toggle(this.getParent(this.old), this.old.checked, "checked"), (this.old = e);
                },
                focus: function (e) {
                    this.toggle(this.getParent(e), !0, "focused");
                },
                blur: function (e) {
                    this.toggle(this.getParent(e), !1, "focused");
                },
                toggle: function (e, t, n) {
                    t ? e.addClass(n) : e.removeClass(n);
                },
                getParent: function (t) {
                    return e.data(t, "icr-parent");
                },
                createHTML: function () {
                    var t = [],
                        i = [],
                        o = [],
                        r = [],
                        a = [],
                        s = [],
                        l = [],
                        u = [],
                        c = this,
                        d = function (e) {
                            var t = [];
                            e = e[0].childNodes;
                            var n, i;
                            for (i = 0; i < e.length; i++) t.push(e[i]);
                            for (; t.length; ) {
                                if (((n = t[0]), 3 === n.nodeType)) {
                                    if ((e = n.nodeValue.trim())) break;
                                } else if (1 === n.nodeType) for (e = n.childNodes, i = 0; i < e.length; i++) t.push(e[i]);
                                Array.prototype.splice.call(t, 0, 1);
                            }
                            return (t = n.parentNode.innerHTML), 0 <= t.indexOf("<input") && ((n = t.indexOf("<input")), (t = t.slice(n)), (n = t.indexOf(">")), (t = t.slice(n + 1).trim())), t;
                        };
                    this.$group.each(function (n) {
                        var c,
                            p = e(this);
                        c = p.prop("className");
                        var h = p.prop("type"),
                            f = p.prop("name"),
                            m = p.prop("value"),
                            g = p.prop("checked"),
                            v = p.prop("disabled"),
                            y = p.prop("id");
                        t.push(c),
                            i.push(h),
                            o.push(f),
                            r.push(m),
                            s.push(g),
                            l.push(v),
                            (c = y ? e("label[for='" + y + "']") : p.closest("label")),
                            a.push(d(c)),
                            (h = '<label class="icr-label">   <span class="icr-item type_{type}"></span>   <span class="icr-hidden"><input class="icr-input {class_list}" type="{type}" name="{name}" value="{value}" {disabled} {checked} /></span>   <span class="icr-text">{text}</span></label>'.replace(
                                /\{class_list\}/,
                                t[n]
                            )),
                            (h = h.replace(/\{type\}/g, i[n])),
                            (h = h.replace(/\{name\}/, o[n])),
                            (h = h.replace(/\{value\}/, r[n])),
                            (h = h.replace(/\{text\}/, a[n])),
                            (h = l[n] ? h.replace(/\{disabled\}/, "disabled") : h.replace(/\{disabled\}/, "")),
                            (h = s[n] ? h.replace(/\{checked\}/, "checked") : h.replace(/\{checked\}/, "")),
                            c.after(h),
                            (n = c.next()),
                            u.push(n[0]),
                            p.remove(),
                            c.remove();
                    }),
                        (this.$group = e(u).find("input")),
                        this.$group.each(function (e) {
                            (c.group[e] = this), (n[o[0]][e] = this);
                        }),
                        this.prepare();
                },
            }),
                (e.fn.ionCheckRadio = function () {
                    var e,
                        r,
                        a,
                        s = [];
                    for (e = 0; e < this.length; e++)
                        (r = this[e]),
                            (a = r.name),
                            ("radio" !== r.type && "checkbox" !== r.type) || !a
                                ? t.console && t.console.warn && t.console.warn("Ion.CheckRadio: Some inputs have wrong type or absent name attribute!")
                                : ((n[a] = { type: r.type, content: [] }), s.push(r));
                    for (e = 0; e < s.length; e++) (r = s[e]), (a = r.name), n[a].content.push(r);
                    for (e in n) i[e] && (i[e].destroy(), (i[e] = null)), (i[e] = new o(n[e]));
                });
        }
    })(jQuery, window);
var DateFormatter;
!(function () {
    "use strict";
    var e, t, n, i, o, r;
    (o = 864e5),
        (r = 3600),
        (e = function (e, t) {
            return "string" == typeof e && "string" == typeof t && e.toLowerCase() === t.toLowerCase();
        }),
        (t = function (e, n, i) {
            var o = i || "0",
                r = e.toString();
            return r.length < n ? t(o + r, n) : r;
        }),
        (n = function (e) {
            var t, i;
            for (e = e || {}, t = 1; t < arguments.length; t++) if ((i = arguments[t])) for (var o in i) i.hasOwnProperty(o) && ("object" == typeof i[o] ? n(e[o], i[o]) : (e[o] = i[o]));
            return e;
        }),
        (i = {
            dateSettings: {
                days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                meridiem: ["AM", "PM"],
                ordinal: function (e) {
                    var t = e % 10,
                        n = { 1: "st", 2: "nd", 3: "rd" };
                    return 1 !== Math.floor((e % 100) / 10) && n[t] ? n[t] : "th";
                },
            },
            separators: /[ \-+\/\.T:@]/g,
            validParts: /[dDjlNSwzWFmMntLoYyaABgGhHisueTIOPZcrU]/g,
            intParts: /[djwNzmnyYhHgGis]/g,
            tzParts: /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
            tzClip: /[^-+\dA-Z]/g,
        }),
        (DateFormatter = function (e) {
            var t = this,
                o = n(i, e);
            (t.dateSettings = o.dateSettings), (t.separators = o.separators), (t.validParts = o.validParts), (t.intParts = o.intParts), (t.tzParts = o.tzParts), (t.tzClip = o.tzClip);
        }),
        (DateFormatter.prototype = {
            constructor: DateFormatter,
            parseDate: function (t, n) {
                var i,
                    o,
                    r,
                    a,
                    s,
                    l,
                    u,
                    c,
                    d,
                    p,
                    h = this,
                    f = !1,
                    m = !1,
                    g = h.dateSettings,
                    v = { date: null, year: null, month: null, day: null, hour: 0, min: 0, sec: 0 };
                if (t) {
                    if (t instanceof Date) return t;
                    if ("number" == typeof t) return new Date(t);
                    if ("U" === n) return (r = parseInt(t)), r ? new Date(1e3 * r) : t;
                    if ("string" != typeof t) return "";
                    if (!(i = n.match(h.validParts)) || 0 === i.length) throw new Error("Invalid date format definition.");
                    for (o = t.replace(h.separators, "\0").split("\0"), r = 0; r < o.length; r++)
                        switch (((a = o[r]), (s = parseInt(a)), i[r])) {
                            case "y":
                            case "Y":
                                (d = a.length), 2 === d ? (v.year = parseInt((70 > s ? "20" : "19") + a)) : 4 === d && (v.year = s), (f = !0);
                                break;
                            case "m":
                            case "n":
                            case "M":
                            case "F":
                                isNaN(a) ? ((l = g.monthsShort.indexOf(a)), l > -1 && (v.month = l + 1), (l = g.months.indexOf(a)) > -1 && (v.month = l + 1)) : s >= 1 && 12 >= s && (v.month = s), (f = !0);
                                break;
                            case "d":
                            case "j":
                                s >= 1 && 31 >= s && (v.day = s), (f = !0);
                                break;
                            case "g":
                            case "h":
                                (u = i.indexOf("a") > -1 ? i.indexOf("a") : i.indexOf("A") > -1 ? i.indexOf("A") : -1),
                                    (p = o[u]),
                                    u > -1 ? ((c = e(p, g.meridiem[0]) ? 0 : e(p, g.meridiem[1]) ? 12 : -1), s >= 1 && 12 >= s && c > -1 ? (v.hour = s + c) : s >= 0 && 23 >= s && (v.hour = s)) : s >= 0 && 23 >= s && (v.hour = s),
                                    (m = !0);
                                break;
                            case "G":
                            case "H":
                                s >= 0 && 23 >= s && (v.hour = s), (m = !0);
                                break;
                            case "i":
                                s >= 0 && 59 >= s && (v.min = s), (m = !0);
                                break;
                            case "s":
                                s >= 0 && 59 >= s && (v.sec = s), (m = !0);
                        }
                    if (!0 === f && v.year && v.month && v.day) v.date = new Date(v.year, v.month - 1, v.day, v.hour, v.min, v.sec, 0);
                    else {
                        if (!0 !== m) return !1;
                        v.date = new Date(0, 0, 0, v.hour, v.min, v.sec, 0);
                    }
                    return v.date;
                }
            },
            guessDate: function (e, t) {
                if ("string" != typeof e) return e;
                var n,
                    i,
                    o,
                    r,
                    a = this,
                    s = e.replace(a.separators, "\0").split("\0"),
                    l = /^[djmn]/g,
                    u = t.match(a.validParts),
                    c = new Date(),
                    d = 0;
                if (!l.test(u[0])) return e;
                for (i = 0; i < s.length; i++) {
                    switch (((d = 2), (o = s[i]), (r = parseInt(o.substr(0, 2))), i)) {
                        case 0:
                            "m" === u[0] || "n" === u[0] ? c.setMonth(r - 1) : c.setDate(r);
                            break;
                        case 1:
                            "m" === u[0] || "n" === u[0] ? c.setDate(r) : c.setMonth(r - 1);
                            break;
                        case 2:
                            (n = c.getFullYear()), o.length < 4 ? (c.setFullYear(parseInt(n.toString().substr(0, 4 - o.length) + o)), (d = o.length)) : ((c.setFullYear = parseInt(o.substr(0, 4))), (d = 4));
                            break;
                        case 3:
                            c.setHours(r);
                            break;
                        case 4:
                            c.setMinutes(r);
                            break;
                        case 5:
                            c.setSeconds(r);
                    }
                    o.substr(d).length > 0 && s.splice(i + 1, 0, o.substr(d));
                }
                return c;
            },
            parseFormat: function (e, n) {
                var i,
                    a = this,
                    s = a.dateSettings,
                    l = /\\?(.?)/gi,
                    u = function (e, t) {
                        return i[e] ? i[e]() : t;
                    };
                return (
                    (i = {
                        d: function () {
                            return t(i.j(), 2);
                        },
                        D: function () {
                            return s.daysShort[i.w()];
                        },
                        j: function () {
                            return n.getDate();
                        },
                        l: function () {
                            return s.days[i.w()];
                        },
                        N: function () {
                            return i.w() || 7;
                        },
                        w: function () {
                            return n.getDay();
                        },
                        z: function () {
                            var e = new Date(i.Y(), i.n() - 1, i.j()),
                                t = new Date(i.Y(), 0, 1);
                            return Math.round((e - t) / o);
                        },
                        W: function () {
                            var e = new Date(i.Y(), i.n() - 1, i.j() - i.N() + 3),
                                n = new Date(e.getFullYear(), 0, 4);
                            return t(1 + Math.round((e - n) / o / 7), 2);
                        },
                        F: function () {
                            return s.months[n.getMonth()];
                        },
                        m: function () {
                            return t(i.n(), 2);
                        },
                        M: function () {
                            return s.monthsShort[n.getMonth()];
                        },
                        n: function () {
                            return n.getMonth() + 1;
                        },
                        t: function () {
                            return new Date(i.Y(), i.n(), 0).getDate();
                        },
                        L: function () {
                            var e = i.Y();
                            return (e % 4 == 0 && e % 100 != 0) || e % 400 == 0 ? 1 : 0;
                        },
                        o: function () {
                            var e = i.n(),
                                t = i.W();
                            return i.Y() + (12 === e && 9 > t ? 1 : 1 === e && t > 9 ? -1 : 0);
                        },
                        Y: function () {
                            return n.getFullYear();
                        },
                        y: function () {
                            return i.Y().toString().slice(-2);
                        },
                        a: function () {
                            return i.A().toLowerCase();
                        },
                        A: function () {
                            var e = i.G() < 12 ? 0 : 1;
                            return s.meridiem[e];
                        },
                        B: function () {
                            var e = n.getUTCHours() * r,
                                i = 60 * n.getUTCMinutes(),
                                o = n.getUTCSeconds();
                            return t(Math.floor((e + i + o + r) / 86.4) % 1e3, 3);
                        },
                        g: function () {
                            return i.G() % 12 || 12;
                        },
                        G: function () {
                            return n.getHours();
                        },
                        h: function () {
                            return t(i.g(), 2);
                        },
                        H: function () {
                            return t(i.G(), 2);
                        },
                        i: function () {
                            return t(n.getMinutes(), 2);
                        },
                        s: function () {
                            return t(n.getSeconds(), 2);
                        },
                        u: function () {
                            return t(1e3 * n.getMilliseconds(), 6);
                        },
                        e: function () {
                            return /\((.*)\)/.exec(String(n))[1] || "Coordinated Universal Time";
                        },
                        T: function () {
                            return (String(n).match(a.tzParts) || [""]).pop().replace(a.tzClip, "") || "UTC";
                        },
                        I: function () {
                            return new Date(i.Y(), 0) - Date.UTC(i.Y(), 0) != new Date(i.Y(), 6) - Date.UTC(i.Y(), 6) ? 1 : 0;
                        },
                        O: function () {
                            var e = n.getTimezoneOffset(),
                                i = Math.abs(e);
                            return (e > 0 ? "-" : "+") + t(100 * Math.floor(i / 60) + (i % 60), 4);
                        },
                        P: function () {
                            var e = i.O();
                            return e.substr(0, 3) + ":" + e.substr(3, 2);
                        },
                        Z: function () {
                            return 60 * -n.getTimezoneOffset();
                        },
                        c: function () {
                            return "Y-m-d\\TH:i:sP".replace(l, u);
                        },
                        r: function () {
                            return "D, d M Y H:i:s O".replace(l, u);
                        },
                        U: function () {
                            return n.getTime() / 1e3 || 0;
                        },
                    }),
                        u(e, e)
                );
            },
            formatDate: function (e, t) {
                var n,
                    i,
                    o,
                    r,
                    a,
                    s = this,
                    l = "";
                if ("string" == typeof e && !1 === (e = s.parseDate(e, t))) return !1;
                if (e instanceof Date) {
                    for (o = t.length, n = 0; o > n; n++)
                        "S" !== (a = t.charAt(n)) && ((r = s.parseFormat(a, e)), n !== o - 1 && s.intParts.test(a) && "S" === t.charAt(n + 1) && ((i = parseInt(r)), (r += s.dateSettings.ordinal(i))), (l += r));
                    return l;
                }
                return "";
            },
        });
})(),
    (function (e) {
        "function" == typeof define && define.amd ? define(["jquery", "jquery-mousewheel"], e) : "object" == typeof exports ? (module.exports = e) : e(jQuery);
    })(function (e) {
        "use strict";
        function t(e, t, n) {
            (this.date = e), (this.desc = t), (this.style = n);
        }
        var n = {
                i18n: {
                    ar: {
                        months: ["كانون الثاني", "شباط", "آذار", "نيسان", "مايو", "حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني", "كانون الأول"],
                        dayOfWeekShort: ["ن", "ث", "ع", "خ", "ج", "س", "ح"],
                        dayOfWeek: ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت", "الأحد"],
                    },
                    ro: {
                        months: ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
                        dayOfWeekShort: ["Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sâ"],
                        dayOfWeek: ["Duminică", "Luni", "Marţi", "Miercuri", "Joi", "Vineri", "Sâmbătă"],
                    },
                    id: {
                        months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        dayOfWeekShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
                        dayOfWeek: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
                    },
                    is: {
                        months: ["Janúar", "Febrúar", "Mars", "Apríl", "Maí", "Júní", "Júlí", "Ágúst", "September", "Október", "Nóvember", "Desember"],
                        dayOfWeekShort: ["Sun", "Mán", "Þrið", "Mið", "Fim", "Fös", "Lau"],
                        dayOfWeek: ["Sunnudagur", "Mánudagur", "Þriðjudagur", "Miðvikudagur", "Fimmtudagur", "Föstudagur", "Laugardagur"],
                    },
                    bg: {
                        months: ["Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"],
                        dayOfWeekShort: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                        dayOfWeek: ["Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота"],
                    },
                    fa: {
                        months: ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"],
                        dayOfWeekShort: ["یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"],
                        dayOfWeek: ["یک‌شنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنج‌شنبه", "جمعه", "شنبه", "یک‌شنبه"],
                    },
                    ru: {
                        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                        dayOfWeekShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                        dayOfWeek: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
                    },
                    uk: {
                        months: ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"],
                        dayOfWeekShort: ["Ндл", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Сбт"],
                        dayOfWeek: ["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота"],
                    },
                    en: {
                        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        dayOfWeekShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                        dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    },
                    el: {
                        months: ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"],
                        dayOfWeekShort: ["Κυρ", "Δευ", "Τρι", "Τετ", "Πεμ", "Παρ", "Σαβ"],
                        dayOfWeek: ["Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη", "Πέμπτη", "Παρασκευή", "Σάββατο"],
                    },
                    de: {
                        months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
                        dayOfWeekShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                        dayOfWeek: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                    },
                    nl: {
                        months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
                        dayOfWeekShort: ["zo", "ma", "di", "wo", "do", "vr", "za"],
                        dayOfWeek: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"],
                    },
                    tr: {
                        months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
                        dayOfWeekShort: ["Paz", "Pts", "Sal", "Çar", "Per", "Cum", "Cts"],
                        dayOfWeek: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
                    },
                    fr: {
                        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                        dayOfWeekShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
                        dayOfWeek: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                    },
                    es: {
                        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
                        dayOfWeek: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                    },
                    th: {
                        months: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
                        dayOfWeekShort: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
                        dayOfWeek: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์", "อาทิตย์"],
                    },
                    pl: {
                        months: ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"],
                        dayOfWeekShort: ["nd", "pn", "wt", "śr", "cz", "pt", "sb"],
                        dayOfWeek: ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"],
                    },
                    pt: {
                        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                        dayOfWeekShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                        dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
                    },
                    ch: { months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"], dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"] },
                    se: { months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"], dayOfWeekShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"] },
                    kr: {
                        months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                        dayOfWeekShort: ["일", "월", "화", "수", "목", "금", "토"],
                        dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
                    },
                    it: {
                        months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
                        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
                        dayOfWeek: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"],
                    },
                    da: {
                        months: ["January", "Februar", "Marts", "April", "Maj", "Juni", "July", "August", "September", "Oktober", "November", "December"],
                        dayOfWeekShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                        dayOfWeek: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"],
                    },
                    no: {
                        months: ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
                        dayOfWeekShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                        dayOfWeek: ["Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag"],
                    },
                    ja: {
                        months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                        dayOfWeekShort: ["日", "月", "火", "水", "木", "金", "土"],
                        dayOfWeek: ["日曜", "月曜", "火曜", "水曜", "木曜", "金曜", "土曜"],
                    },
                    vi: {
                        months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
                        dayOfWeekShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                        dayOfWeek: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
                    },
                    sl: {
                        months: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
                        dayOfWeekShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"],
                        dayOfWeek: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "Petek", "Sobota"],
                    },
                    cs: { months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"], dayOfWeekShort: ["Ne", "Po", "Út", "St", "Čt", "Pá", "So"] },
                    hu: {
                        months: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
                        dayOfWeekShort: ["Va", "Hé", "Ke", "Sze", "Cs", "Pé", "Szo"],
                        dayOfWeek: ["vasárnap", "hétfő", "kedd", "szerda", "csütörtök", "péntek", "szombat"],
                    },
                    az: {
                        months: ["Yanvar", "Fevral", "Mart", "Aprel", "May", "Iyun", "Iyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
                        dayOfWeekShort: ["B", "Be", "Ça", "Ç", "Ca", "C", "Ş"],
                        dayOfWeek: ["Bazar", "Bazar ertəsi", "Çərşənbə axşamı", "Çərşənbə", "Cümə axşamı", "Cümə", "Şənbə"],
                    },
                    bs: {
                        months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
                        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"],
                        dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"],
                    },
                    ca: {
                        months: ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"],
                        dayOfWeekShort: ["Dg", "Dl", "Dt", "Dc", "Dj", "Dv", "Ds"],
                        dayOfWeek: ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"],
                    },
                    "en-GB": {
                        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        dayOfWeekShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                        dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    },
                    et: {
                        months: ["Jaanuar", "Veebruar", "Märts", "Aprill", "Mai", "Juuni", "Juuli", "August", "September", "Oktoober", "November", "Detsember"],
                        dayOfWeekShort: ["P", "E", "T", "K", "N", "R", "L"],
                        dayOfWeek: ["Pühapäev", "Esmaspäev", "Teisipäev", "Kolmapäev", "Neljapäev", "Reede", "Laupäev"],
                    },
                    eu: {
                        months: ["Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"],
                        dayOfWeekShort: ["Ig.", "Al.", "Ar.", "Az.", "Og.", "Or.", "La."],
                        dayOfWeek: ["Igandea", "Astelehena", "Asteartea", "Asteazkena", "Osteguna", "Ostirala", "Larunbata"],
                    },
                    fi: {
                        months: ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kesäkuu", "Heinäkuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"],
                        dayOfWeekShort: ["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"],
                        dayOfWeek: ["sunnuntai", "maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai"],
                    },
                    gl: {
                        months: ["Xan", "Feb", "Maz", "Abr", "Mai", "Xun", "Xul", "Ago", "Set", "Out", "Nov", "Dec"],
                        dayOfWeekShort: ["Dom", "Lun", "Mar", "Mer", "Xov", "Ven", "Sab"],
                        dayOfWeek: ["Domingo", "Luns", "Martes", "Mércores", "Xoves", "Venres", "Sábado"],
                    },
                    hr: {
                        months: ["Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"],
                        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"],
                        dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"],
                    },
                    ko: {
                        months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                        dayOfWeekShort: ["일", "월", "화", "수", "목", "금", "토"],
                        dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
                    },
                    lt: {
                        months: ["Sausio", "Vasario", "Kovo", "Balandžio", "Gegužės", "Birželio", "Liepos", "Rugpjūčio", "Rugsėjo", "Spalio", "Lapkričio", "Gruodžio"],
                        dayOfWeekShort: ["Sek", "Pir", "Ant", "Tre", "Ket", "Pen", "Šeš"],
                        dayOfWeek: ["Sekmadienis", "Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis"],
                    },
                    lv: {
                        months: ["Janvāris", "Februāris", "Marts", "Aprīlis ", "Maijs", "Jūnijs", "Jūlijs", "Augusts", "Septembris", "Oktobris", "Novembris", "Decembris"],
                        dayOfWeekShort: ["Sv", "Pr", "Ot", "Tr", "Ct", "Pk", "St"],
                        dayOfWeek: ["Svētdiena", "Pirmdiena", "Otrdiena", "Trešdiena", "Ceturtdiena", "Piektdiena", "Sestdiena"],
                    },
                    mk: {
                        months: ["јануари", "февруари", "март", "април", "мај", "јуни", "јули", "август", "септември", "октомври", "ноември", "декември"],
                        dayOfWeekShort: ["нед", "пон", "вто", "сре", "чет", "пет", "саб"],
                        dayOfWeek: ["Недела", "Понеделник", "Вторник", "Среда", "Четврток", "Петок", "Сабота"],
                    },
                    mn: {
                        months: ["1-р сар", "2-р сар", "3-р сар", "4-р сар", "5-р сар", "6-р сар", "7-р сар", "8-р сар", "9-р сар", "10-р сар", "11-р сар", "12-р сар"],
                        dayOfWeekShort: ["Дав", "Мяг", "Лха", "Пүр", "Бсн", "Бям", "Ням"],
                        dayOfWeek: ["Даваа", "Мягмар", "Лхагва", "Пүрэв", "Баасан", "Бямба", "Ням"],
                    },
                    "pt-BR": {
                        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                        dayOfWeekShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
                        dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
                    },
                    sk: {
                        months: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
                        dayOfWeekShort: ["Ne", "Po", "Ut", "St", "Št", "Pi", "So"],
                        dayOfWeek: ["Nedeľa", "Pondelok", "Utorok", "Streda", "Štvrtok", "Piatok", "Sobota"],
                    },
                    sq: {
                        months: ["Janar", "Shkurt", "Mars", "Prill", "Maj", "Qershor", "Korrik", "Gusht", "Shtator", "Tetor", "Nëntor", "Dhjetor"],
                        dayOfWeekShort: ["Die", "Hën", "Mar", "Mër", "Enj", "Pre", "Shtu"],
                        dayOfWeek: ["E Diel", "E Hënë", "E Martē", "E Mërkurë", "E Enjte", "E Premte", "E Shtunë"],
                    },
                    "sr-YU": {
                        months: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
                        dayOfWeekShort: ["Ned", "Pon", "Uto", "Sre", "čet", "Pet", "Sub"],
                        dayOfWeek: ["Nedelja", "Ponedeljak", "Utorak", "Sreda", "Četvrtak", "Petak", "Subota"],
                    },
                    sr: {
                        months: ["јануар", "фебруар", "март", "април", "мај", "јун", "јул", "август", "септембар", "октобар", "новембар", "децембар"],
                        dayOfWeekShort: ["нед", "пон", "уто", "сре", "чет", "пет", "суб"],
                        dayOfWeek: ["Недеља", "Понедељак", "Уторак", "Среда", "Четвртак", "Петак", "Субота"],
                    },
                    sv: {
                        months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
                        dayOfWeekShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"],
                        dayOfWeek: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag"],
                    },
                    "zh-TW": {
                        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                        dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"],
                        dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                    },
                    zh: {
                        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                        dayOfWeekShort: ["日", "一", "二", "三", "四", "五", "六"],
                        dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                    },
                    he: {
                        months: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
                        dayOfWeekShort: ["א'", "ב'", "ג'", "ד'", "ה'", "ו'", "שבת"],
                        dayOfWeek: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת", "ראשון"],
                    },
                    hy: {
                        months: ["Հունվար", "Փետրվար", "Մարտ", "Ապրիլ", "Մայիս", "Հունիս", "Հուլիս", "Օգոստոս", "Սեպտեմբեր", "Հոկտեմբեր", "Նոյեմբեր", "Դեկտեմբեր"],
                        dayOfWeekShort: ["Կի", "Երկ", "Երք", "Չոր", "Հնգ", "Ուրբ", "Շբթ"],
                        dayOfWeek: ["Կիրակի", "Երկուշաբթի", "Երեքշաբթի", "Չորեքշաբթի", "Հինգշաբթի", "Ուրբաթ", "Շաբաթ"],
                    },
                    kg: {
                        months: ["Үчтүн айы", "Бирдин айы", "Жалган Куран", "Чын Куран", "Бугу", "Кулжа", "Теке", "Баш Оона", "Аяк Оона", "Тогуздун айы", "Жетинин айы", "Бештин айы"],
                        dayOfWeekShort: ["Жек", "Дүй", "Шей", "Шар", "Бей", "Жум", "Ише"],
                        dayOfWeek: ["Жекшемб", "Дүйшөмб", "Шейшемб", "Шаршемб", "Бейшемби", "Жума", "Ишенб"],
                    },
                    rm: {
                        months: ["Schaner", "Favrer", "Mars", "Avrigl", "Matg", "Zercladur", "Fanadur", "Avust", "Settember", "October", "November", "December"],
                        dayOfWeekShort: ["Du", "Gli", "Ma", "Me", "Gie", "Ve", "So"],
                        dayOfWeek: ["Dumengia", "Glindesdi", "Mardi", "Mesemna", "Gievgia", "Venderdi", "Sonda"],
                    },
                    ka: {
                        months: ["იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი", "ოქტომბერი", "ნოემბერი", "დეკემბერი"],
                        dayOfWeekShort: ["კვ", "ორშ", "სამშ", "ოთხ", "ხუთ", "პარ", "შაბ"],
                        dayOfWeek: ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი"],
                    },
                },
                value: "",
                rtl: !1,
                format: "Y/m/d H:i",
                formatTime: "H:i",
                formatDate: "Y/m/d",
                startDate: !1,
                step: 60,
                monthChangeSpinner: !0,
                closeOnDateSelect: !1,
                closeOnTimeSelect: !0,
                closeOnWithoutClick: !0,
                closeOnInputClick: !0,
                timepicker: !0,
                datepicker: !0,
                weeks: !1,
                defaultTime: !1,
                defaultDate: !1,
                minDate: !1,
                maxDate: !1,
                minTime: !1,
                maxTime: !1,
                disabledMinTime: !1,
                disabledMaxTime: !1,
                allowTimes: [],
                opened: !1,
                initTime: !0,
                inline: !1,
                theme: "",
                onSelectDate: function () {},
                onSelectTime: function () {},
                onChangeMonth: function () {},
                onGetWeekOfYear: function () {},
                onChangeYear: function () {},
                onChangeDateTime: function () {},
                onShow: function () {},
                onClose: function () {},
                onGenerate: function () {},
                withoutCopyright: !0,
                inverseButton: !1,
                hours12: !1,
                next: "xdsoft_next",
                prev: "xdsoft_prev",
                dayOfWeekStart: 0,
                parentID: "body",
                timeHeightInTimePicker: 25,
                timepickerScrollbar: !0,
                todayButton: !0,
                prevButton: !0,
                nextButton: !0,
                defaultSelect: !0,
                scrollMonth: !0,
                scrollTime: !0,
                scrollInput: !0,
                lazyInit: !1,
                mask: !1,
                validateOnBlur: !0,
                allowBlank: !0,
                yearStart: 1950,
                yearEnd: 2050,
                monthStart: 0,
                monthEnd: 11,
                style: "",
                id: "",
                fixed: !1,
                roundTime: "round",
                className: "",
                weekends: [],
                highlightedDates: [],
                highlightedPeriods: [],
                allowDates: [],
                allowDateRe: null,
                disabledDates: [],
                disabledWeekDays: [],
                yearOffset: 0,
                beforeShowDay: null,
                enterLikeTab: !0,
                showApplyButton: !1,
            },
            i = null,
            o = "en",
            r = { meridiem: ["AM", "PM"] },
            a = function () {
                var t = n.i18n[o],
                    a = {
                        days: t.dayOfWeek,
                        daysShort: t.dayOfWeekShort,
                        months: t.months,
                        monthsShort: e.map(t.months, function (e) {
                            return e.substring(0, 3);
                        }),
                    };
                i = new DateFormatter({ dateSettings: e.extend({}, r, a) });
            };
        (e.datetimepicker = {
            setLocale: function (e) {
                var t = n.i18n[e] ? e : "en";
                o != t && ((o = t), a());
            },
            setDateFormatter: function (e) {
                i = e;
            },
            RFC_2822: "D, d M Y H:i:s O",
            ATOM: "Y-m-dTH:i:sP",
            ISO_8601: "Y-m-dTH:i:sO",
            RFC_822: "D, d M y H:i:s O",
            RFC_850: "l, d-M-y H:i:s T",
            RFC_1036: "D, d M y H:i:s O",
            RFC_1123: "D, d M Y H:i:s O",
            RSS: "D, d M Y H:i:s O",
            W3C: "Y-m-dTH:i:sP",
        }),
            a(),
        window.getComputedStyle ||
        (window.getComputedStyle = function (e) {
            return (
                (this.el = e),
                    (this.getPropertyValue = function (t) {
                        var n = /(\-([a-z]){1})/g;
                        return (
                            "float" === t && (t = "styleFloat"),
                            n.test(t) &&
                            (t = t.replace(n, function (e, t, n) {
                                return n.toUpperCase();
                            })),
                            e.currentStyle[t] || null
                        );
                    }),
                    this
            );
        }),
        Array.prototype.indexOf ||
        (Array.prototype.indexOf = function (e, t) {
            var n, i;
            for (n = t || 0, i = this.length; i > n; n += 1) if (this[n] === e) return n;
            return -1;
        }),
            (Date.prototype.countDaysInMonth = function () {
                return new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate();
            }),
            (e.fn.xdsoftScroller = function (t) {
                return this.each(function () {
                    var n,
                        i,
                        o,
                        r,
                        a,
                        s = e(this),
                        l = function (e) {
                            var t,
                                n = { x: 0, y: 0 };
                            return (
                                "touchstart" === e.type || "touchmove" === e.type || "touchend" === e.type || "touchcancel" === e.type
                                    ? ((t = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]), (n.x = t.clientX), (n.y = t.clientY))
                                    : ("mousedown" === e.type || "mouseup" === e.type || "mousemove" === e.type || "mouseover" === e.type || "mouseout" === e.type || "mouseenter" === e.type || "mouseleave" === e.type) &&
                                    ((n.x = e.clientX), (n.y = e.clientY)),
                                    n
                            );
                        },
                        u = 100,
                        c = !1,
                        d = 0,
                        p = 0,
                        h = 0,
                        f = !1,
                        m = 0,
                        g = function () {};
                    return "hide" === t
                        ? void s.find(".xdsoft_scrollbar").hide()
                        : (e(this).hasClass("xdsoft_scroller_box") ||
                        ((n = s.children().eq(0)),
                            (i = s[0].clientHeight),
                            (o = n[0].offsetHeight),
                            (r = e('<div class="xdsoft_scrollbar"></div>')),
                            (a = e('<div class="xdsoft_scroller"></div>')),
                            r.append(a),
                            s.addClass("xdsoft_scroller_box").append(r),
                            (g = function (e) {
                                var t = l(e).y - d + m;
                                0 > t && (t = 0), t + a[0].offsetHeight > h && (t = h - a[0].offsetHeight), s.trigger("scroll_element.xdsoft_scroller", [u ? t / u : 0]);
                            }),
                            a
                                .on("touchstart.xdsoft_scroller mousedown.xdsoft_scroller", function (n) {
                                    i || s.trigger("resize_scroll.xdsoft_scroller", [t]),
                                        (d = l(n).y),
                                        (m = parseInt(a.css("margin-top"), 10)),
                                        (h = r[0].offsetHeight),
                                        "mousedown" === n.type || "touchstart" === n.type
                                            ? (document && e(document.body).addClass("xdsoft_noselect"),
                                                e([document.body, window]).on("touchend mouseup.xdsoft_scroller", function t() {
                                                    e([document.body, window]).off("touchend mouseup.xdsoft_scroller", t).off("mousemove.xdsoft_scroller", g).removeClass("xdsoft_noselect");
                                                }),
                                                e(document.body).on("mousemove.xdsoft_scroller", g))
                                            : ((f = !0), n.stopPropagation(), n.preventDefault());
                                })
                                .on("touchmove", function (e) {
                                    f && (e.preventDefault(), g(e));
                                })
                                .on("touchend touchcancel", function () {
                                    (f = !1), (m = 0);
                                }),
                            s
                                .on("scroll_element.xdsoft_scroller", function (e, t) {
                                    i || s.trigger("resize_scroll.xdsoft_scroller", [t, !0]),
                                        (t = t > 1 ? 1 : 0 > t || isNaN(t) ? 0 : t),
                                        a.css("margin-top", u * t),
                                        setTimeout(function () {
                                            n.css("marginTop", -parseInt((n[0].offsetHeight - i) * t, 10));
                                        }, 10);
                                })
                                .on("resize_scroll.xdsoft_scroller", function (e, t, l) {
                                    var c, d;
                                    (i = s[0].clientHeight),
                                        (o = n[0].offsetHeight),
                                        (c = i / o),
                                        (d = c * r[0].offsetHeight),
                                        c > 1
                                            ? a.hide()
                                            : (a.show(),
                                                a.css("height", parseInt(d > 10 ? d : 10, 10)),
                                                (u = r[0].offsetHeight - a[0].offsetHeight),
                                            !0 !== l && s.trigger("scroll_element.xdsoft_scroller", [t || Math.abs(parseInt(n.css("marginTop"), 10)) / (o - i)]));
                                }),
                            s.on("mousewheel", function (e) {
                                var t = Math.abs(parseInt(n.css("marginTop"), 10));
                                return (t -= 20 * e.deltaY), 0 > t && (t = 0), s.trigger("scroll_element.xdsoft_scroller", [t / (o - i)]), e.stopPropagation(), !1;
                            }),
                            s.on("touchstart", function (e) {
                                (c = l(e)), (p = Math.abs(parseInt(n.css("marginTop"), 10)));
                            }),
                            s.on("touchmove", function (e) {
                                if (c) {
                                    e.preventDefault();
                                    var t = l(e);
                                    s.trigger("scroll_element.xdsoft_scroller", [(p - (t.y - c.y)) / (o - i)]);
                                }
                            }),
                            s.on("touchend touchcancel", function () {
                                (c = !1), (p = 0);
                            })),
                            void s.trigger("resize_scroll.xdsoft_scroller", [t]));
                });
            }),
            (e.fn.datetimepicker = function (r, a) {
                var s,
                    l,
                    u = this,
                    c = 48,
                    d = 96,
                    p = 105,
                    h = 17,
                    f = 46,
                    m = 13,
                    g = 8,
                    v = 9,
                    y = !1,
                    b = e.isPlainObject(r) || !r ? e.extend(!0, {}, n, r) : e.extend(!0, {}, n),
                    w = 0,
                    x = function (e) {
                        e.on("open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart", function t() {
                            e.is(":disabled") ||
                            e.data("xdsoft_datetimepicker") ||
                            (clearTimeout(w),
                                (w = setTimeout(function () {
                                    e.data("xdsoft_datetimepicker") || s(e), e.off("open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart", t).trigger("open.xdsoft");
                                }, 100)));
                        });
                    };
                return (
                    (s = function (n) {
                        function a() {
                            var e,
                                t = !1;
                            return (
                                b.startDate
                                    ? (t = k.strToDate(b.startDate))
                                    : ((t = b.value || (n && n.val && n.val() ? n.val() : "")),
                                        t ? (t = k.strToDateTime(t)) : b.defaultDate && ((t = k.strToDateTime(b.defaultDate)), b.defaultTime && ((e = k.strtotime(b.defaultTime)), t.setHours(e.getHours()), t.setMinutes(e.getMinutes())))),
                                    t && k.isValidDate(t) ? S.data("changed", !0) : (t = ""),
                                t || 0
                            );
                        }
                        function s(t) {
                            var i = function (e, t) {
                                    var n = e
                                        .replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g, "\\$1")
                                        .replace(/_/g, "{digit+}")
                                        .replace(/([0-9]{1})/g, "{digit$1}")
                                        .replace(/\{digit([0-9]{1})\}/g, "[0-$1_]{1}")
                                        .replace(/\{digit[\+]\}/g, "[0-9_]{1}");
                                    return new RegExp(n).test(t);
                                },
                                o = function (e) {
                                    try {
                                        if (document.selection && document.selection.createRange) return document.selection.createRange().getBookmark().charCodeAt(2) - 2;
                                        if (e.setSelectionRange) return e.selectionStart;
                                    } catch (e) {
                                        return 0;
                                    }
                                },
                                r = function (e, t) {
                                    if (!(e = "string" == typeof e || e instanceof String ? document.getElementById(e) : e)) return !1;
                                    if (e.createTextRange) {
                                        var n = e.createTextRange();
                                        return n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", t), n.select(), !0;
                                    }
                                    return !!e.setSelectionRange && (e.setSelectionRange(t, t), !0);
                                };
                            t.mask && n.off("keydown.xdsoft"),
                            !0 === t.mask &&
                            (t.mask =
                                "undefined" != typeof moment
                                    ? t.format.replace(/Y{4}/g, "9999").replace(/Y{2}/g, "99").replace(/M{2}/g, "19").replace(/D{2}/g, "39").replace(/H{2}/g, "29").replace(/m{2}/g, "59").replace(/s{2}/g, "59")
                                    : t.format.replace(/Y/g, "9999").replace(/F/g, "9999").replace(/m/g, "19").replace(/d/g, "39").replace(/H/g, "29").replace(/i/g, "59").replace(/s/g, "59")),
                            "string" === e.type(t.mask) &&
                            (i(t.mask, n.val()) || (n.val(t.mask.replace(/[0-9]/g, "_")), r(n[0], 0)),
                                n.on("keydown.xdsoft", function (a) {
                                    var s,
                                        l,
                                        u = this.value,
                                        b = a.which;
                                    if ((b >= c && 57 >= b) || (b >= d && p >= b) || b === g || b === f) {
                                        for (
                                            s = o(this), l = b !== g && b !== f ? String.fromCharCode(b >= d && p >= b ? b - c : b) : "_", (b !== g && b !== f) || !s || ((s -= 1), (l = "_"));
                                            /[^0-9_]/.test(t.mask.substr(s, 1)) && s < t.mask.length && s > 0;

                                        )
                                            s += b === g || b === f ? -1 : 1;
                                        if (((u = u.substr(0, s) + l + u.substr(s + 1)), "" === e.trim(u))) u = t.mask.replace(/[0-9]/g, "_");
                                        else if (s === t.mask.length) return a.preventDefault(), !1;
                                        for (s += b === g || b === f ? 0 : 1; /[^0-9_]/.test(t.mask.substr(s, 1)) && s < t.mask.length && s > 0; ) s += b === g || b === f ? -1 : 1;
                                        i(t.mask, u) ? ((this.value = u), r(this, s)) : "" === e.trim(u) ? (this.value = t.mask.replace(/[0-9]/g, "_")) : n.trigger("error_input.xdsoft");
                                    } else if ((-1 !== [65, 67, 86, 90, 89].indexOf(b) && y) || -1 !== [27, 38, 40, 37, 39, 116, h, v, m].indexOf(b)) return !0;
                                    return a.preventDefault(), !1;
                                }));
                        }
                        var l,
                            u,
                            w,
                            x,
                            T,
                            k,
                            $,
                            S = e('<div class="xdsoft_datetimepicker xdsoft_noselect"></div>'),
                            C = e('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),
                            D = e('<div class="xdsoft_datepicker active"></div>'),
                            O = e(
                                '<div class="xdsoft_monthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button"></button><div class="xdsoft_label xdsoft_month"><span></span><i></i></div><div class="xdsoft_label xdsoft_year"><span></span><i></i></div><button type="button" class="xdsoft_next"></button></div>'
                            ),
                            A = e('<div class="xdsoft_calendar"></div>'),
                            I = e('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button></div>'),
                            M = I.find(".xdsoft_time_box").eq(0),
                            E = e('<div class="xdsoft_time_variant"></div>'),
                            _ = e('<button type="button" class="xdsoft_save_selected blue-gradient-button">Save Selected</button>'),
                            N = e('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>'),
                            P = e('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>'),
                            j = !1,
                            W = 0;
                        b.id && S.attr("id", b.id),
                        b.style && S.attr("style", b.style),
                        b.weeks && S.addClass("xdsoft_showweeks"),
                        b.rtl && S.addClass("xdsoft_rtl"),
                            S.addClass("xdsoft_" + b.theme),
                            S.addClass(b.className),
                            O.find(".xdsoft_month span").after(N),
                            O.find(".xdsoft_year span").after(P),
                            O.find(".xdsoft_month,.xdsoft_year").on("touchstart mousedown.xdsoft", function (t) {
                                var n,
                                    i,
                                    o = e(this).find(".xdsoft_select").eq(0),
                                    r = 0,
                                    a = 0,
                                    s = o.is(":visible");
                                for (
                                    O.find(".xdsoft_select").hide(), k.currentTime && (r = k.currentTime[e(this).hasClass("xdsoft_month") ? "getMonth" : "getFullYear"]()), o[s ? "hide" : "show"](), n = o.find("div.xdsoft_option"), i = 0;
                                    i < n.length && n.eq(i).data("value") !== r;
                                    i += 1
                                )
                                    a += n[0].offsetHeight;
                                return o.xdsoftScroller(a / (o.children()[0].offsetHeight - o[0].clientHeight)), t.stopPropagation(), !1;
                            }),
                            O.find(".xdsoft_select")
                                .xdsoftScroller()
                                .on("touchstart mousedown.xdsoft", function (e) {
                                    e.stopPropagation(), e.preventDefault();
                                })
                                .on("touchstart mousedown.xdsoft", ".xdsoft_option", function () {
                                    (void 0 === k.currentTime || null === k.currentTime) && (k.currentTime = k.now());
                                    var t = k.currentTime.getFullYear();
                                    k && k.currentTime && k.currentTime[e(this).parent().parent().hasClass("xdsoft_monthselect") ? "setMonth" : "setFullYear"](e(this).data("value")),
                                        e(this).parent().parent().hide(),
                                        S.trigger("xchange.xdsoft"),
                                    b.onChangeMonth && e.isFunction(b.onChangeMonth) && b.onChangeMonth.call(S, k.currentTime, S.data("input")),
                                    t !== k.currentTime.getFullYear() && e.isFunction(b.onChangeYear) && b.onChangeYear.call(S, k.currentTime, S.data("input"));
                                }),
                            (S.getValue = function () {
                                return k.getCurrentTime();
                            }),
                            (S.setOptions = function (o) {
                                var r = {};
                                (b = e.extend(!0, {}, b, o)),
                                o.allowTimes && e.isArray(o.allowTimes) && o.allowTimes.length && (b.allowTimes = e.extend(!0, [], o.allowTimes)),
                                o.weekends && e.isArray(o.weekends) && o.weekends.length && (b.weekends = e.extend(!0, [], o.weekends)),
                                o.allowDates && e.isArray(o.allowDates) && o.allowDates.length && (b.allowDates = e.extend(!0, [], o.allowDates)),
                                o.allowDateRe && "[object String]" === Object.prototype.toString.call(o.allowDateRe) && (b.allowDateRe = new RegExp(o.allowDateRe)),
                                o.highlightedDates &&
                                e.isArray(o.highlightedDates) &&
                                o.highlightedDates.length &&
                                (e.each(o.highlightedDates, function (n, o) {
                                    var a,
                                        s = e.map(o.split(","), e.trim),
                                        l = new t(i.parseDate(s[0], b.formatDate), s[1], s[2]),
                                        u = i.formatDate(l.date, b.formatDate);
                                    void 0 !== r[u] ? (a = r[u].desc) && a.length && l.desc && l.desc.length && (r[u].desc = a + "\n" + l.desc) : (r[u] = l);
                                }),
                                    (b.highlightedDates = e.extend(!0, [], r))),
                                o.highlightedPeriods &&
                                e.isArray(o.highlightedPeriods) &&
                                o.highlightedPeriods.length &&
                                ((r = e.extend(!0, [], b.highlightedDates)),
                                    e.each(o.highlightedPeriods, function (n, o) {
                                        var a, s, l, u, c, d, p;
                                        if (e.isArray(o)) (a = o[0]), (s = o[1]), (l = o[2]), (p = o[3]);
                                        else {
                                            var h = e.map(o.split(","), e.trim);
                                            (a = i.parseDate(h[0], b.formatDate)), (s = i.parseDate(h[1], b.formatDate)), (l = h[2]), (p = h[3]);
                                        }
                                        for (; s >= a; )
                                            (u = new t(a, l, p)),
                                                (c = i.formatDate(a, b.formatDate)),
                                                a.setDate(a.getDate() + 1),
                                                void 0 !== r[c] ? (d = r[c].desc) && d.length && u.desc && u.desc.length && (r[c].desc = d + "\n" + u.desc) : (r[c] = u);
                                    }),
                                    (b.highlightedDates = e.extend(!0, [], r))),
                                o.disabledDates && e.isArray(o.disabledDates) && o.disabledDates.length && (b.disabledDates = e.extend(!0, [], o.disabledDates)),
                                o.disabledWeekDays && e.isArray(o.disabledWeekDays) && o.disabledWeekDays.length && (b.disabledWeekDays = e.extend(!0, [], o.disabledWeekDays)),
                                (!b.open && !b.opened) || b.inline || n.trigger("open.xdsoft"),
                                b.inline && ((j = !0), S.addClass("xdsoft_inline"), n.after(S).hide()),
                                b.inverseButton && ((b.next = "xdsoft_prev"), (b.prev = "xdsoft_next")),
                                    b.datepicker ? D.addClass("active") : D.removeClass("active"),
                                    b.timepicker ? I.addClass("active") : I.removeClass("active"),
                                b.value && (k.setCurrentTime(b.value), n && n.val && n.val(k.str)),
                                    (b.dayOfWeekStart = isNaN(b.dayOfWeekStart) ? 0 : parseInt(b.dayOfWeekStart, 10) % 7),
                                b.timepickerScrollbar || M.xdsoftScroller("hide"),
                                b.minDate && /^[\+\-](.*)$/.test(b.minDate) && (b.minDate = i.formatDate(k.strToDateTime(b.minDate), b.formatDate)),
                                b.maxDate && /^[\+\-](.*)$/.test(b.maxDate) && (b.maxDate = i.formatDate(k.strToDateTime(b.maxDate), b.formatDate)),
                                    _.toggle(b.showApplyButton),
                                    O.find(".xdsoft_today_button").css("visibility", b.todayButton ? "visible" : "hidden"),
                                    O.find("." + b.prev).css("visibility", b.prevButton ? "visible" : "hidden"),
                                    O.find("." + b.next).css("visibility", b.nextButton ? "visible" : "hidden"),
                                    s(b),
                                b.validateOnBlur &&
                                n.off("blur.xdsoft").on("blur.xdsoft", function () {
                                    if (b.allowBlank && (!e.trim(e(this).val()).length || ("string" == typeof b.mask && e.trim(e(this).val()) === b.mask.replace(/[0-9]/g, "_")))) e(this).val(null), S.data("xdsoft_datetime").empty();
                                    else {
                                        var t = i.parseDate(e(this).val(), b.format);
                                        if (t) e(this).val(i.formatDate(t, b.format));
                                        else {
                                            var n = +[e(this).val()[0], e(this).val()[1]].join(""),
                                                o = +[e(this).val()[2], e(this).val()[3]].join("");
                                            e(this).val(
                                                !b.datepicker && b.timepicker && n >= 0 && 24 > n && o >= 0 && 60 > o
                                                    ? [n, o]
                                                        .map(function (e) {
                                                            return e > 9 ? e : "0" + e;
                                                        })
                                                        .join(":")
                                                    : i.formatDate(k.now(), b.format)
                                            );
                                        }
                                        S.data("xdsoft_datetime").setCurrentTime(e(this).val());
                                    }
                                    S.trigger("changedatetime.xdsoft"), S.trigger("close.xdsoft");
                                }),
                                    (b.dayOfWeekStartPrev = 0 === b.dayOfWeekStart ? 6 : b.dayOfWeekStart - 1),
                                    S.trigger("xchange.xdsoft").trigger("afterOpen.xdsoft");
                            }),
                            S.data("options", b).on("touchstart mousedown.xdsoft", function (e) {
                                return e.stopPropagation(), e.preventDefault(), P.hide(), N.hide(), !1;
                            }),
                            M.append(E),
                            M.xdsoftScroller(),
                            S.on("afterOpen.xdsoft", function () {
                                M.xdsoftScroller();
                            }),
                            S.append(D).append(I),
                        !0 !== b.withoutCopyright && S.append(C),
                            D.append(O).append(A).append(_),
                            e(b.parentID).append(S),
                            (l = function () {
                                var t = this;
                                (t.now = function (e) {
                                    var n,
                                        i,
                                        o = new Date();
                                    return (
                                        !e && b.defaultDate && ((n = t.strToDateTime(b.defaultDate)), o.setFullYear(n.getFullYear()), o.setMonth(n.getMonth()), o.setDate(n.getDate())),
                                        b.yearOffset && o.setFullYear(o.getFullYear() + b.yearOffset),
                                        !e && b.defaultTime && ((i = t.strtotime(b.defaultTime)), o.setHours(i.getHours()), o.setMinutes(i.getMinutes())),
                                            o
                                    );
                                }),
                                    (t.isValidDate = function (e) {
                                        return "[object Date]" === Object.prototype.toString.call(e) && !isNaN(e.getTime());
                                    }),
                                    (t.setCurrentTime = function (e, n) {
                                        (t.currentTime = "string" == typeof e ? t.strToDateTime(e) : t.isValidDate(e) ? e : e || n || !b.allowBlank ? t.now() : null), S.trigger("xchange.xdsoft");
                                    }),
                                    (t.empty = function () {
                                        t.currentTime = null;
                                    }),
                                    (t.getCurrentTime = function () {
                                        return t.currentTime;
                                    }),
                                    (t.nextMonth = function () {
                                        (void 0 === t.currentTime || null === t.currentTime) && (t.currentTime = t.now());
                                        var n,
                                            i = t.currentTime.getMonth() + 1;
                                        return (
                                            12 === i && (t.currentTime.setFullYear(t.currentTime.getFullYear() + 1), (i = 0)),
                                                (n = t.currentTime.getFullYear()),
                                                t.currentTime.setDate(Math.min(new Date(t.currentTime.getFullYear(), i + 1, 0).getDate(), t.currentTime.getDate())),
                                                t.currentTime.setMonth(i),
                                            b.onChangeMonth && e.isFunction(b.onChangeMonth) && b.onChangeMonth.call(S, k.currentTime, S.data("input")),
                                            n !== t.currentTime.getFullYear() && e.isFunction(b.onChangeYear) && b.onChangeYear.call(S, k.currentTime, S.data("input")),
                                                S.trigger("xchange.xdsoft"),
                                                i
                                        );
                                    }),
                                    (t.prevMonth = function () {
                                        (void 0 === t.currentTime || null === t.currentTime) && (t.currentTime = t.now());
                                        var n = t.currentTime.getMonth() - 1;
                                        return (
                                            -1 === n && (t.currentTime.setFullYear(t.currentTime.getFullYear() - 1), (n = 11)),
                                                t.currentTime.setDate(Math.min(new Date(t.currentTime.getFullYear(), n + 1, 0).getDate(), t.currentTime.getDate())),
                                                t.currentTime.setMonth(n),
                                            b.onChangeMonth && e.isFunction(b.onChangeMonth) && b.onChangeMonth.call(S, k.currentTime, S.data("input")),
                                                S.trigger("xchange.xdsoft"),
                                                n
                                        );
                                    }),
                                    (t.getWeekOfYear = function (t) {
                                        if (b.onGetWeekOfYear && e.isFunction(b.onGetWeekOfYear)) {
                                            var n = b.onGetWeekOfYear.call(S, t);
                                            if (void 0 !== n) return n;
                                        }
                                        var i = new Date(t.getFullYear(), 0, 1);
                                        return 4 != i.getDay() && i.setMonth(0, 1 + ((4 - i.getDay() + 7) % 7)), Math.ceil(((t - i) / 864e5 + i.getDay() + 1) / 7);
                                    }),
                                    (t.strToDateTime = function (e) {
                                        var n,
                                            o,
                                            r = [];
                                        return e && e instanceof Date && t.isValidDate(e)
                                            ? e
                                            : ((r = /^(\+|\-)(.*)$/.exec(e)),
                                            r && (r[2] = i.parseDate(r[2], b.formatDate)),
                                                r && r[2] ? ((n = r[2].getTime() - 6e4 * r[2].getTimezoneOffset()), (o = new Date(t.now(!0).getTime() + parseInt(r[1] + "1", 10) * n))) : (o = e ? i.parseDate(e, b.format) : t.now()),
                                            t.isValidDate(o) || (o = t.now()),
                                                o);
                                    }),
                                    (t.strToDate = function (e) {
                                        if (e && e instanceof Date && t.isValidDate(e)) return e;
                                        var n = e ? i.parseDate(e, b.formatDate) : t.now(!0);
                                        return t.isValidDate(n) || (n = t.now(!0)), n;
                                    }),
                                    (t.strtotime = function (e) {
                                        if (e && e instanceof Date && t.isValidDate(e)) return e;
                                        var n = e ? i.parseDate(e, b.formatTime) : t.now(!0);
                                        return t.isValidDate(n) || (n = t.now(!0)), n;
                                    }),
                                    (t.str = function () {
                                        return i.formatDate(t.currentTime, b.format);
                                    }),
                                    (t.currentTime = this.now());
                            }),
                            (k = new l()),
                            _.on("touchend click", function (e) {
                                e.preventDefault(), S.data("changed", !0), k.setCurrentTime(a()), n.val(k.str()), S.trigger("close.xdsoft");
                            }),
                            O.find(".xdsoft_today_button")
                                .on("touchend mousedown.xdsoft", function () {
                                    S.data("changed", !0), k.setCurrentTime(0, !0), S.trigger("afterOpen.xdsoft");
                                })
                                .on("dblclick.xdsoft", function () {
                                    var e,
                                        t,
                                        i = k.getCurrentTime();
                                    (i = new Date(i.getFullYear(), i.getMonth(), i.getDate())),
                                        (e = k.strToDate(b.minDate)),
                                    (e = new Date(e.getFullYear(), e.getMonth(), e.getDate())) > i ||
                                    ((t = k.strToDate(b.maxDate)), (t = new Date(t.getFullYear(), t.getMonth(), t.getDate())), i > t || (n.val(k.str()), n.trigger("change"), S.trigger("close.xdsoft")));
                                }),
                            O.find(".xdsoft_prev,.xdsoft_next").on("touchend mousedown.xdsoft", function () {
                                var t = e(this),
                                    n = 0,
                                    i = !1;
                                !(function e(o) {
                                    t.hasClass(b.next) ? k.nextMonth() : t.hasClass(b.prev) && k.prevMonth(), b.monthChangeSpinner && (i || (n = setTimeout(e, o || 100)));
                                })(500),
                                    e([document.body, window]).on("touchend mouseup.xdsoft", function t() {
                                        clearTimeout(n), (i = !0), e([document.body, window]).off("touchend mouseup.xdsoft", t);
                                    });
                            }),
                            I.find(".xdsoft_prev,.xdsoft_next").on("touchend mousedown.xdsoft", function () {
                                var t = e(this),
                                    n = 0,
                                    i = !1,
                                    o = 110;
                                !(function e(r) {
                                    var a = M[0].clientHeight,
                                        s = E[0].offsetHeight,
                                        l = Math.abs(parseInt(E.css("marginTop"), 10));
                                    t.hasClass(b.next) && s - a - b.timeHeightInTimePicker >= l
                                        ? E.css("marginTop", "-" + (l + b.timeHeightInTimePicker) + "px")
                                        : t.hasClass(b.prev) && l - b.timeHeightInTimePicker >= 0 && E.css("marginTop", "-" + (l - b.timeHeightInTimePicker) + "px"),
                                        M.trigger("scroll_element.xdsoft_scroller", [Math.abs(parseInt(E[0].style.marginTop, 10) / (s - a))]),
                                        (o = o > 10 ? 10 : o - 10),
                                    i || (n = setTimeout(e, r || o));
                                })(500),
                                    e([document.body, window]).on("touchend mouseup.xdsoft", function t() {
                                        clearTimeout(n), (i = !0), e([document.body, window]).off("touchend mouseup.xdsoft", t);
                                    });
                            }),
                            (u = 0),
                            S.on("xchange.xdsoft", function (t) {
                                clearTimeout(u),
                                    (u = setTimeout(function () {
                                        if (void 0 === k.currentTime || null === k.currentTime) {
                                            if (b.allowBlank) return;
                                            k.currentTime = k.now();
                                        }
                                        for (
                                            var t, a, s, l, u, c, d, p, h, f, m = "", g = new Date(k.currentTime.getFullYear(), k.currentTime.getMonth(), 1, 12, 0, 0), v = 0, y = k.now(), w = !1, x = !1, T = [], $ = !0, C = "", D = "";
                                            g.getDay() !== b.dayOfWeekStart;

                                        )
                                            g.setDate(g.getDate() - 1);
                                        for (m += "<table><thead><tr>", b.weeks && (m += "<th></th>"), t = 0; 7 > t; t += 1) m += "<th>" + b.i18n[o].dayOfWeekShort[(t + b.dayOfWeekStart) % 7] + "</th>";
                                        for (
                                            m += "</tr></thead>",
                                                m += "<tbody>",
                                            !1 !== b.maxDate && ((w = k.strToDate(b.maxDate)), (w = new Date(w.getFullYear(), w.getMonth(), w.getDate(), 23, 59, 59, 999))),
                                            !1 !== b.minDate && ((x = k.strToDate(b.minDate)), (x = new Date(x.getFullYear(), x.getMonth(), x.getDate())));
                                            v < k.currentTime.countDaysInMonth() || g.getDay() !== b.dayOfWeekStart || k.currentTime.getMonth() === g.getMonth();

                                        )
                                            (T = []),
                                                (v += 1),
                                                (s = g.getDay()),
                                                (l = g.getDate()),
                                                (u = g.getFullYear()),
                                                (c = g.getMonth()),
                                                (d = k.getWeekOfYear(g)),
                                                (f = ""),
                                                T.push("xdsoft_date"),
                                                (p = b.beforeShowDay && e.isFunction(b.beforeShowDay.call) ? b.beforeShowDay.call(S, g) : null),
                                                b.allowDateRe && "[object RegExp]" === Object.prototype.toString.call(b.allowDateRe)
                                                    ? b.allowDateRe.test(i.formatDate(g, b.formatDate)) || T.push("xdsoft_disabled")
                                                    : b.allowDates && b.allowDates.length > 0
                                                    ? -1 === b.allowDates.indexOf(i.formatDate(g, b.formatDate)) && T.push("xdsoft_disabled")
                                                    : (!1 !== w && g > w) || (!1 !== x && x > g) || (p && !1 === p[0])
                                                        ? T.push("xdsoft_disabled")
                                                        : -1 !== b.disabledDates.indexOf(i.formatDate(g, b.formatDate))
                                                            ? T.push("xdsoft_disabled")
                                                            : -1 !== b.disabledWeekDays.indexOf(s)
                                                                ? T.push("xdsoft_disabled")
                                                                : n.is("[readonly]") && T.push("xdsoft_disabled"),
                                            p && "" !== p[1] && T.push(p[1]),
                                            k.currentTime.getMonth() !== c && T.push("xdsoft_other_month"),
                                            (b.defaultSelect || S.data("changed")) && i.formatDate(k.currentTime, b.formatDate) === i.formatDate(g, b.formatDate) && T.push("xdsoft_current"),
                                            i.formatDate(y, b.formatDate) === i.formatDate(g, b.formatDate) && T.push("xdsoft_today"),
                                            (0 === g.getDay() || 6 === g.getDay() || -1 !== b.weekends.indexOf(i.formatDate(g, b.formatDate))) && T.push("xdsoft_weekend"),
                                            void 0 !== b.highlightedDates[i.formatDate(g, b.formatDate)] &&
                                            ((a = b.highlightedDates[i.formatDate(g, b.formatDate)]), T.push(void 0 === a.style ? "xdsoft_highlighted_default" : a.style), (f = void 0 === a.desc ? "" : a.desc)),
                                            b.beforeShowDay && e.isFunction(b.beforeShowDay) && T.push(b.beforeShowDay(g)),
                                            $ && ((m += "<tr>"), ($ = !1), b.weeks && (m += "<th>" + d + "</th>")),
                                                (m +=
                                                    '<td data-date="' +
                                                    l +
                                                    '" data-month="' +
                                                    c +
                                                    '" data-year="' +
                                                    u +
                                                    '" class="xdsoft_date xdsoft_day_of_week' +
                                                    g.getDay() +
                                                    " " +
                                                    T.join(" ") +
                                                    '" title="' +
                                                    f +
                                                    '"><div>' +
                                                    l +
                                                    "</div></td>"),
                                            g.getDay() === b.dayOfWeekStartPrev && ((m += "</tr>"), ($ = !0)),
                                                g.setDate(l + 1);
                                        if (
                                            ((m += "</tbody></table>"),
                                                A.html(m),
                                                O.find(".xdsoft_label span").eq(0).text(b.i18n[o].months[k.currentTime.getMonth()]),
                                                O.find(".xdsoft_label span").eq(1).text(k.currentTime.getFullYear()),
                                                (C = ""),
                                                (D = ""),
                                                (c = ""),
                                                (h = function (t, o) {
                                                    var r,
                                                        a,
                                                        s = k.now(),
                                                        l = b.allowTimes && e.isArray(b.allowTimes) && b.allowTimes.length;
                                                    s.setHours(t),
                                                        (t = parseInt(s.getHours(), 10)),
                                                        s.setMinutes(o),
                                                        (o = parseInt(s.getMinutes(), 10)),
                                                        (r = new Date(k.currentTime)),
                                                        r.setHours(t),
                                                        r.setMinutes(o),
                                                        (T = []),
                                                        (!1 !== b.minDateTime && b.minDateTime > r) || (!1 !== b.maxTime && k.strtotime(b.maxTime).getTime() < s.getTime()) || (!1 !== b.minTime && k.strtotime(b.minTime).getTime() > s.getTime())
                                                            ? T.push("xdsoft_disabled")
                                                            : (!1 !== b.minDateTime && b.minDateTime > r) ||
                                                            (!1 !== b.disabledMinTime && s.getTime() > k.strtotime(b.disabledMinTime).getTime() && !1 !== b.disabledMaxTime && s.getTime() < k.strtotime(b.disabledMaxTime).getTime())
                                                            ? T.push("xdsoft_disabled")
                                                            : n.is("[readonly]") && T.push("xdsoft_disabled"),
                                                        (a = new Date(k.currentTime)),
                                                        a.setHours(parseInt(k.currentTime.getHours(), 10)),
                                                    l || a.setMinutes(Math[b.roundTime](k.currentTime.getMinutes() / b.step) * b.step),
                                                    (b.initTime || b.defaultSelect || S.data("changed")) &&
                                                    a.getHours() === parseInt(t, 10) &&
                                                    ((!l && b.step > 59) || a.getMinutes() === parseInt(o, 10)) &&
                                                    (b.defaultSelect || S.data("changed") ? T.push("xdsoft_current") : b.initTime && T.push("xdsoft_init_time")),
                                                    parseInt(y.getHours(), 10) === parseInt(t, 10) && parseInt(y.getMinutes(), 10) === parseInt(o, 10) && T.push("xdsoft_today"),
                                                        (C += '<div class="xdsoft_time ' + T.join(" ") + '" data-hour="' + t + '" data-minute="' + o + '">' + i.formatDate(s, b.formatTime) + "</div>");
                                                }),
                                            b.allowTimes && e.isArray(b.allowTimes) && b.allowTimes.length)
                                        )
                                            for (v = 0; v < b.allowTimes.length; v += 1) (D = k.strtotime(b.allowTimes[v]).getHours()), (c = k.strtotime(b.allowTimes[v]).getMinutes()), h(D, c);
                                        else for (v = 0, t = 0; v < (b.hours12 ? 12 : 24); v += 1) for (t = 0; 60 > t; t += b.step) (D = (10 > v ? "0" : "") + v), (c = (10 > t ? "0" : "") + t), h(D, c);
                                        for (E.html(C), r = "", v = 0, v = parseInt(b.yearStart, 10) + b.yearOffset; v <= parseInt(b.yearEnd, 10) + b.yearOffset; v += 1)
                                            r += '<div class="xdsoft_option ' + (k.currentTime.getFullYear() === v ? "xdsoft_current" : "") + '" data-value="' + v + '">' + v + "</div>";
                                        for (P.children().eq(0).html(r), v = parseInt(b.monthStart, 10), r = ""; v <= parseInt(b.monthEnd, 10); v += 1)
                                            r += '<div class="xdsoft_option ' + (k.currentTime.getMonth() === v ? "xdsoft_current" : "") + '" data-value="' + v + '">' + b.i18n[o].months[v] + "</div>";
                                        N.children().eq(0).html(r), e(S).trigger("generate.xdsoft");
                                    }, 10)),
                                    t.stopPropagation();
                            }).on("afterOpen.xdsoft", function () {
                                if (b.timepicker) {
                                    var e, t, n, i;
                                    E.find(".xdsoft_current").length ? (e = ".xdsoft_current") : E.find(".xdsoft_init_time").length && (e = ".xdsoft_init_time"),
                                        e
                                            ? ((t = M[0].clientHeight),
                                                (n = E[0].offsetHeight),
                                                (i = E.find(e).index() * b.timeHeightInTimePicker + 1),
                                            i > n - t && (i = n - t),
                                                M.trigger("scroll_element.xdsoft_scroller", [parseInt(i, 10) / (n - t)]))
                                            : M.trigger("scroll_element.xdsoft_scroller", [0]);
                                }
                            }),
                            (w = 0),
                            A.on("touchend click.xdsoft", "td", function (t) {
                                t.stopPropagation(), (w += 1);
                                var i = e(this),
                                    o = k.currentTime;
                                return (
                                    (void 0 === o || null === o) && ((k.currentTime = k.now()), (o = k.currentTime)),
                                    !i.hasClass("xdsoft_disabled") &&
                                    (o.setDate(1),
                                        o.setFullYear(i.data("year")),
                                        o.setMonth(i.data("month")),
                                        o.setDate(i.data("date")),
                                        S.trigger("select.xdsoft", [o]),
                                        n.val(k.str()),
                                    b.onSelectDate && e.isFunction(b.onSelectDate) && b.onSelectDate.call(S, k.currentTime, S.data("input"), t),
                                        S.data("changed", !0),
                                        S.trigger("xchange.xdsoft"),
                                        S.trigger("changedatetime.xdsoft"),
                                    (w > 1 || !0 === b.closeOnDateSelect || (!1 === b.closeOnDateSelect && !b.timepicker)) && !b.inline && S.trigger("close.xdsoft"),
                                        void setTimeout(function () {
                                            w = 0;
                                        }, 200))
                                );
                            }),
                            E.on("touchend click.xdsoft", "div", function (t) {
                                t.stopPropagation();
                                var n = e(this),
                                    i = k.currentTime;
                                return (
                                    (void 0 === i || null === i) && ((k.currentTime = k.now()), (i = k.currentTime)),
                                    !n.hasClass("xdsoft_disabled") &&
                                    (i.setHours(n.data("hour")),
                                        i.setMinutes(n.data("minute")),
                                        S.trigger("select.xdsoft", [i]),
                                        S.data("input").val(k.str()),
                                    b.onSelectTime && e.isFunction(b.onSelectTime) && b.onSelectTime.call(S, k.currentTime, S.data("input"), t),
                                        S.data("changed", !0),
                                        S.trigger("xchange.xdsoft"),
                                        S.trigger("changedatetime.xdsoft"),
                                        void (!0 !== b.inline && !0 === b.closeOnTimeSelect && S.trigger("close.xdsoft")))
                                );
                            }),
                            D.on("mousewheel.xdsoft", function (e) {
                                return !b.scrollMonth || (e.deltaY < 0 ? k.nextMonth() : k.prevMonth(), !1);
                            }),
                            n.on("mousewheel.xdsoft", function (e) {
                                return (
                                    !b.scrollInput ||
                                    (!b.datepicker && b.timepicker
                                        ? ((x = E.find(".xdsoft_current").length ? E.find(".xdsoft_current").eq(0).index() : 0),
                                        x + e.deltaY >= 0 && x + e.deltaY < E.children().length && (x += e.deltaY),
                                        E.children().eq(x).length && E.children().eq(x).trigger("mousedown"),
                                            !1)
                                        : b.datepicker && !b.timepicker
                                            ? (D.trigger(e, [e.deltaY, e.deltaX, e.deltaY]), n.val && n.val(k.str()), S.trigger("changedatetime.xdsoft"), !1)
                                            : void 0)
                                );
                            }),
                            S.on("changedatetime.xdsoft", function (t) {
                                if (b.onChangeDateTime && e.isFunction(b.onChangeDateTime)) {
                                    var n = S.data("input");
                                    b.onChangeDateTime.call(S, k.currentTime, n, t), delete b.value, n.trigger("change");
                                }
                            })
                                .on("generate.xdsoft", function () {
                                    b.onGenerate && e.isFunction(b.onGenerate) && b.onGenerate.call(S, k.currentTime, S.data("input")), j && (S.trigger("afterOpen.xdsoft"), (j = !1));
                                })
                                .on("click.xdsoft", function (e) {
                                    e.stopPropagation();
                                }),
                            (x = 0),
                            ($ = function (e, t) {
                                do {
                                    if (((e = e.parentNode), !1 === t(e))) break;
                                } while ("HTML" !== e.nodeName);
                            }),
                            (T = function () {
                                var t, n, i, o, r, a, s, l, u, c, d, p, h;
                                if (
                                    ((l = S.data("input")),
                                        (t = l.offset()),
                                        (n = l[0]),
                                        (c = "top"),
                                        (i = t.top + n.offsetHeight - 1),
                                        (o = t.left),
                                        (r = "absolute"),
                                        (u = e(window).width()),
                                        (p = e(window).height()),
                                        (h = e(window).scrollTop()),
                                    document.documentElement.clientWidth - t.left < D.parent().outerWidth(!0))
                                ) {
                                    var f = D.parent().outerWidth(!0) - n.offsetWidth;
                                    o -= f;
                                }
                                "rtl" === l.parent().css("direction") && (o -= S.outerWidth() - l.outerWidth()),
                                    b.fixed
                                        ? ((i -= h), (o -= e(window).scrollLeft()), (r = "fixed"))
                                        : ((s = !1),
                                            $(n, function (e) {
                                                return "fixed" === window.getComputedStyle(e).getPropertyValue("position") ? ((s = !0), !1) : void 0;
                                            }),
                                            s ? ((r = "fixed"), i + S.outerHeight() > p + h ? ((c = "bottom"), (i = p + h - t.top)) : (i -= h)) : i + n.offsetHeight > p + h && (i = t.top - n.offsetHeight + 1),
                                        0 > i && (i = 0),
                                        o + n.offsetWidth > u && (o = u - n.offsetWidth)),
                                    (a = S[0]),
                                    $(a, function (e) {
                                        var t;
                                        return (t = window.getComputedStyle(e).getPropertyValue("position")), "relative" === t && u >= e.offsetWidth ? ((o -= (u - e.offsetWidth) / 2), !1) : void 0;
                                    }),
                                    (d = { position: r, left: o, top: "", bottom: "" }),
                                    (d[c] = i),
                                    S.css(d);
                            }),
                            S.on("open.xdsoft", function (t) {
                                var n = !0;
                                b.onShow && e.isFunction(b.onShow) && (n = b.onShow.call(S, k.currentTime, S.data("input"), t)),
                                !1 !== n &&
                                (S.show(),
                                    T(),
                                    e(window).off("resize.xdsoft", T).on("resize.xdsoft", T),
                                b.closeOnWithoutClick &&
                                e([document.body, window]).on("touchstart mousedown.xdsoft", function t() {
                                    S.trigger("close.xdsoft"), e([document.body, window]).off("touchstart mousedown.xdsoft", t);
                                }));
                            })
                                .on("close.xdsoft", function (t) {
                                    var n = !0;
                                    O.find(".xdsoft_month,.xdsoft_year").find(".xdsoft_select").hide(),
                                    b.onClose && e.isFunction(b.onClose) && (n = b.onClose.call(S, k.currentTime, S.data("input"), t)),
                                    !1 === n || b.opened || b.inline || S.hide(),
                                        t.stopPropagation();
                                })
                                .on("toggle.xdsoft", function () {
                                    S.trigger(S.is(":visible") ? "close.xdsoft" : "open.xdsoft");
                                })
                                .data("input", n),
                            (W = 0),
                            S.data("xdsoft_datetime", k),
                            S.setOptions(b),
                            k.setCurrentTime(a()),
                            n
                                .data("xdsoft_datetimepicker", S)
                                .on("open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart", function () {
                                    n.is(":disabled") ||
                                    (n.data("xdsoft_datetimepicker").is(":visible") && b.closeOnInputClick) ||
                                    (clearTimeout(W),
                                        (W = setTimeout(function () {
                                            n.is(":disabled") || ((j = !0), k.setCurrentTime(a(), !0), b.mask && s(b), S.trigger("open.xdsoft"));
                                        }, 100)));
                                })
                                .on("keydown.xdsoft", function (t) {
                                    var n,
                                        i = t.which;
                                    return -1 !== [m].indexOf(i) && b.enterLikeTab
                                        ? ((n = e("input:visible,textarea:visible,button:visible,a:visible")), S.trigger("close.xdsoft"), n.eq(n.index(this) + 1).focus(), !1)
                                        : -1 !== [v].indexOf(i)
                                            ? (S.trigger("close.xdsoft"), !0)
                                            : void 0;
                                })
                                .on("blur.xdsoft", function () {
                                    S.trigger("close.xdsoft");
                                });
                    }),
                        (l = function (t) {
                            var n = t.data("xdsoft_datetimepicker");
                            n &&
                            (n.data("xdsoft_datetime", null),
                                n.remove(),
                                t.data("xdsoft_datetimepicker", null).off(".xdsoft"),
                                e(window).off("resize.xdsoft"),
                                e([window, document.body]).off("mousedown.xdsoft touchstart"),
                            t.unmousewheel && t.unmousewheel());
                        }),
                        e(document)
                            .off("keydown.xdsoftctrl keyup.xdsoftctrl")
                            .on("keydown.xdsoftctrl", function (e) {
                                e.keyCode === h && (y = !0);
                            })
                            .on("keyup.xdsoftctrl", function (e) {
                                e.keyCode === h && (y = !1);
                            }),
                        this.each(function () {
                            var t,
                                n = e(this).data("xdsoft_datetimepicker");
                            if (n) {
                                if ("string" === e.type(r))
                                    switch (r) {
                                        case "show":
                                            e(this).select().focus(), n.trigger("open.xdsoft");
                                            break;
                                        case "hide":
                                            n.trigger("close.xdsoft");
                                            break;
                                        case "toggle":
                                            n.trigger("toggle.xdsoft");
                                            break;
                                        case "destroy":
                                            l(e(this));
                                            break;
                                        case "reset":
                                            (this.value = this.defaultValue),
                                            (this.value && n.data("xdsoft_datetime").isValidDate(i.parseDate(this.value, b.format))) || n.data("changed", !1),
                                                n.data("xdsoft_datetime").setCurrentTime(this.value);
                                            break;
                                        case "validate":
                                            (t = n.data("input")), t.trigger("blur.xdsoft");
                                            break;
                                        default:
                                            n[r] && e.isFunction(n[r]) && (u = n[r](a));
                                    }
                                else n.setOptions(r);
                                return 0;
                            }
                            "string" !== e.type(r) && (!b.lazyInit || b.open || b.inline ? s(e(this)) : x(e(this)));
                        }),
                        u
                );
            }),
            (e.fn.datetimepicker.defaults = n);
    }),
    (function (e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? (module.exports = e) : e(jQuery);
    })(function (e) {
        function t(t) {
            var a = t || window.event,
                s = l.call(arguments, 1),
                u = 0,
                d = 0,
                p = 0,
                h = 0,
                f = 0,
                m = 0;
            if (
                ((t = e.event.fix(a)),
                    (t.type = "mousewheel"),
                "detail" in a && (p = -1 * a.detail),
                "wheelDelta" in a && (p = a.wheelDelta),
                "wheelDeltaY" in a && (p = a.wheelDeltaY),
                "wheelDeltaX" in a && (d = -1 * a.wheelDeltaX),
                "axis" in a && a.axis === a.HORIZONTAL_AXIS && ((d = -1 * p), (p = 0)),
                    (u = 0 === p ? d : p),
                "deltaY" in a && ((p = -1 * a.deltaY), (u = p)),
                "deltaX" in a && ((d = a.deltaX), 0 === p && (u = -1 * d)),
                0 !== p || 0 !== d)
            ) {
                if (1 === a.deltaMode) {
                    var g = e.data(this, "mousewheel-line-height");
                    (u *= g), (p *= g), (d *= g);
                } else if (2 === a.deltaMode) {
                    var v = e.data(this, "mousewheel-page-height");
                    (u *= v), (p *= v), (d *= v);
                }
                if (
                    ((h = Math.max(Math.abs(p), Math.abs(d))),
                    (!r || r > h) && ((r = h), i(a, h) && (r /= 40)),
                    i(a, h) && ((u /= 40), (d /= 40), (p /= 40)),
                        (u = Math[u >= 1 ? "floor" : "ceil"](u / r)),
                        (d = Math[d >= 1 ? "floor" : "ceil"](d / r)),
                        (p = Math[p >= 1 ? "floor" : "ceil"](p / r)),
                    c.settings.normalizeOffset && this.getBoundingClientRect)
                ) {
                    var y = this.getBoundingClientRect();
                    (f = t.clientX - y.left), (m = t.clientY - y.top);
                }
                return (
                    (t.deltaX = d),
                        (t.deltaY = p),
                        (t.deltaFactor = r),
                        (t.offsetX = f),
                        (t.offsetY = m),
                        (t.deltaMode = 0),
                        s.unshift(t, u, d, p),
                    o && clearTimeout(o),
                        (o = setTimeout(n, 200)),
                        (e.event.dispatch || e.event.handle).apply(this, s)
                );
            }
        }
        function n() {
            r = null;
        }
        function i(e, t) {
            return c.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 == 0;
        }
        var o,
            r,
            a = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            s = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
            l = Array.prototype.slice;
        if (e.event.fixHooks) for (var u = a.length; u; ) e.event.fixHooks[a[--u]] = e.event.mouseHooks;
        var c = (e.event.special.mousewheel = {
            version: "3.1.12",
            setup: function () {
                if (this.addEventListener) for (var n = s.length; n; ) this.addEventListener(s[--n], t, !1);
                else this.onmousewheel = t;
                e.data(this, "mousewheel-line-height", c.getLineHeight(this)), e.data(this, "mousewheel-page-height", c.getPageHeight(this));
            },
            teardown: function () {
                if (this.removeEventListener) for (var n = s.length; n; ) this.removeEventListener(s[--n], t, !1);
                else this.onmousewheel = null;
                e.removeData(this, "mousewheel-line-height"), e.removeData(this, "mousewheel-page-height");
            },
            getLineHeight: function (t) {
                var n = e(t),
                    i = n["offsetParent" in e.fn ? "offsetParent" : "parent"]();
                return i.length || (i = e("body")), parseInt(i.css("fontSize"), 10) || parseInt(n.css("fontSize"), 10) || 16;
            },
            getPageHeight: function (t) {
                return e(t).height();
            },
            settings: { adjustOldDeltas: !0, normalizeOffset: !0 },
        });
        e.fn.extend({
            mousewheel: function (e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel");
            },
            unmousewheel: function (e) {
                return this.unbind("mousewheel", e);
            },
        });
    });
var app = {
        animationSpeed: 100,
        notificate: function (e, t) {
            return {
                view: function (e, t) {
                    $(".notificate-add-" + e).html("<p>" + t + "</p>"),
                        $(".notificate-add-" + e).fadeIn(200),
                        setTimeout(function () {
                            $(".notificate-add-" + e).fadeOut(200);
                        }, 5e3);
                },
                info: function (e) {
                    return this.view(e, "info");
                },
                warning: function (e) {
                    return this.view(e, "warning");
                },
                success: function (e) {
                    return this.view(e, "success");
                },
                error: function (e) {
                    return this.view(e, "error");
                },
            }.view(e, t);
        },
        month: "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря",
    },
    dinamicBlock = function () {
        if ($(document).width() < 992 || !$(".fixed-block").length) return !1;
        if ($(".order-total").length && !$(".fix-btn").hasClass("active")) return !1;
        var e = +$(".order-total").outerHeight() + 100,
            t = $(".sidebar").offset().top,
            n = $(document).scrollTop(),
            i = $(".skills").offset().top,
            o = $(".fixed-block");
        n > t && n < i - e
            ? (o.addClass("fix").removeAttr("style"), o.find(".fix-btn").removeAttr("style"))
            : n > i - e
            ? (o.removeClass("fix").css({ position: "absolute", bottom: "0", "margin-bottom": "0" }), o.find(".fix-btn").css({ "margin-right": "0", right: "0" }))
            : o.removeClass("fix"),
            n > i - (+$(".order-total").outerHeight() + $(".faq-sidebar").outerHeight() + 120) ? $(".faq-sidebar").attr("style", "position: absolute; display: none;") : $(".faq-sidebar").removeAttr("style");
    },
    skillsOnHomeSlider = function () {
        if ($("#skills").length) {
            var e = { items: 3, itemsDesktop: [1920, 3], itemsTablet: [991, 1], pagination: !0 };
            $("#skills").owlCarousel(e);
            var t = $("#skills").data("owlCarousel");
            $(document).width() < 991 ? t.reinit(e) : t.destroy();
        }
    };
$(document).ready(function () {
    if (
        ($(document).width() < 768 && $("#toggle-primary-menu").addClass("not-active"),
        $("input[type='radio']").length && $(".radio").find("label>input[type='radio']").ionCheckRadio(),
            $("input[name='phone']").mask("+7 (999) 999-99-99"),
        $(".choose-block").length && $(".cleaning-order-page").length)
    ) {
        $.datetimepicker.setLocale("ru"),
            $('.choose-block input[name="date"]').datetimepicker({ timepicker: !1, startDate: "+1971/05/01", minDate: Schedule.enableOrder > 0 ? "-1969/12/31" : "-1969/12/30", format: "d-m-Y", disabledWeekDays: Schedule.disabledWeekDays });
        for (var e = +Schedule.minTime.split(":")[0], t = +Schedule.maxTime.split(":")[0], n = [], i = e; i <= t; i++) n.push(i + ":00");
        $('.choose-block input[name="time"]').datetimepicker({ datepicker: !1, allowTimes: n, format: "H:i" });
    }
    $(".what-is-this").length && $(".what-is-this").popover({ container: "body" }),
    $(".raiting").length && $(".raiting").popover({ container: "body" }),
    $("#subscribe").length && $("input[type='checkbox']").ionCheckRadio(),
    $("#add-testimonal").length && $("input[type='checkbox']").ionCheckRadio(),
        expandBtnOnHome(),
        skillsOnHomeSlider(),
    $("#testimonal").length && location.hash && "#testimonal" === location.hash && $("html, body").animate({ scrollTop: $("#testimonal").offset().top }, 700),
    $("#section-slider").length &&
    $("#section-slider").owlCarousel({
        items: 1,
        itemsDesktop: [1920, 1],
        itemsTablet: [991, 1],
        pagination: !0,
        navigation: !0,
        autoPlay: !0,
        navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    }),
        $('[data-toggle="tooltip"]').tooltip();
}),
    $(window).resize(function () {
        $(document).width() < 768 &&
        ($("#toggle-primary-menu").addClass("not-active"), $(".primary-menu .has-childs.active").length && ($(".primary-menu .has-childs.active").removeClass("active"), $(".primary-menu .has-childs ul").slideUp(app.animationSpeed))),
            dinamicBlock(),
            skillsOnHomeSlider();
    }),
    $(window).scroll(function () {
        dinamicBlock();
    }),
    $(".fix-btn").on("click", function () {
        $(this).hasClass("active") ? ($(".fixed-block").removeClass("fix"), $(".fixed-block").removeAttr("style"), $(this).removeClass("active")) : $(this).addClass("active");
    }),
    $("#toggle-menu").on("click", function () {
        $(".mobile-menu").is(":hidden") ? $(".mobile-menu").slideDown(app.animationSpeed) : $(".mobile-menu").slideUp(app.animationSpeed);
    }),
    $("#toggle-primary-menu").on("click", function () {
        $(".primary-menu>ul").is(":hidden")
            ? ($(this).removeClass("not-active"), $(".primary-menu>ul").slideDown(app.animationSpeed), $(".primary-menu .title").fadeOut(app.animationSpeed))
            : ($(this).addClass("not-active"),
                $(".primary-menu>ul").slideUp(app.animationSpeed),
                $(".primary-menu .title").fadeIn(app.animationSpeed),
            $(".primary-menu .has-childs.active").length && ($(".primary-menu .has-childs.active").removeClass("active"), $(".primary-menu .has-childs ul").slideUp(app.animationSpeed)));
    }),
    $(".has-childs").on("click", function () {
        $(this).find("ul").is(":hidden") ? ($(this).addClass("active"), $(this).find("ul").slideDown(app.animationSpeed)) : ($(this).removeClass("active"), $(this).find("ul").slideUp(app.animationSpeed));
    }),
    $(".open-answer").on("click", function () {
        $(this).parents(".question-block").find(".answer-block").is(":hidden") && $(".question-block").find(".answer-block").slideUp(app.animationSpeed),
            $(this).parents(".question-block").find(".answer-block").slideDown(app.animationSpeed);
    }),
    $(".close-this-modal").click(function () {
        var e = $(this).parents(".modal-auth").attr("id");
        $("#" + e).modal("hide");
    });
var validate_form = function (e, t) {
        var n = [];
        return (
            $("#" + e + " input").removeAttr("style"),
                $("#" + e + " textarea").removeAttr("style"),
                $("#" + e + " input").each(function (e) {
                    $(this).val() || (!in_array($(this).attr("name"), t) && !$(this).attr("required")) || ($(this).css("border", "1px solid red"), n.push(e));
                }),
                $("#" + e + " textarea").each(function (e) {
                    $(this).val() || (!in_array($(this).attr("name"), t) && !$(this).attr("required")) || ($(this).css("border", "1px solid red"), n.push(e));
                }),
                !n.length
        );
    },
    onlyOneSaleTypeCanUse = function () {
        return !(!$("input[name='discount']").val() || !$("input[name='points']").val());
    };
$("body").on("click", ".vote-block i", function () {
    var e = $(this).attr("data-vote"),
        t = $(this).parents(".vote-block");
    t.find("i").removeClass("active"),
        t.find("input").val(e),
        t.find("i").each(function (t) {
            ++t <= e && $(this).addClass("active");
        });
}),
    $(document).on("click", ".add-testimonal-btn", function (e) {
        e.preventDefault();
        var t = [];
        if (
            ($(".vote-block")
                .find("input")
                .each(function (e) {
                    ($(this).val() && "" !== $(this).val()) || t.push(e);
                }),
                t.length)
        )
            return (
                app.notificate("warning", "Пожалуйста, оцените заказ."),
                    $("#add-testimonal").submit(function () {
                        return !1;
                    }),
                    !1
            );
        $("#add-testimonal")[0].submit();
    });
var deleteCleaner = function (e, t) {
        return (
            !(!t || "" === t || !e || "" === e) &&
            !!confirm("Вы уверены?") &&
            void $.ajax({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                url: "/user/" + t,
                type: "POST",
                data: { cleaner: e },
                success: function (t) {
                    400 === t.status && app.notificate("error", t.msg), 200 === t.status && ($("#cleaner-" + e).remove(), app.notificate("success", t.msg));
                },
            })
        );
    },
    scrollTopAfterOrder = function () {
        $(".fixed-block").length && $(".fixed-block").removeAttr("style");
        var e = $("#scroll-here");
        return 1 == e.length && $("html, body").animate({ scrollTop: e.offset().top }, 700), !1;
    },
    expandBtnOnHome = function () {
        $(".why-we").length &&
        $(".why-we .item").each(function (e) {
            $(this).find(".short").height() > 150 && ($(this).find(".info-block").addClass("expanding-block"), $(this).find(".expand-block").fadeIn(0));
        });
    };
$("body").on("click", ".expand-block", function () {
    $(this).hasClass("active")
        ? ($(this).removeClass("active"),
            $(this).text(
                {
                    ru: "Показать полностью",
                    en: "Show all",
                    kz: "Толық көрсету",
                }[locale]
            ),
            $(this).parents(".why-we .item").find(".info-block").animate({ maxHeight: "150" }, 0))
        : ($(this).addClass("active"),
            $(this).text(
                {
                    ru: "Скрыть подробности",
                    en: "Hide details",
                    kz: "Мәліметтерді жасыру",
                }[locale]
            ),
            $(this).parents(".why-we .item").find(".info-block").animate({ maxHeight: "100%" }, 0));
}),
    $("body").on("click", ".scroll-to-workers", function () {
        return $(".other-services").length && $("html, body").animate({ scrollTop: $(".other-services").offset().top }, 700), !1;
    }),
    $("body").on("click", ".cleaner-action", function () {
        var e = $(this).attr("data-action"),
            t = +$(this).attr("data-cleaner");
        $.ajax({
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            url: "/cleaners/action",
            type: "POST",
            data: { action: e, cleaner: t },
            success: function (e) {
                400 === e.status &&
                !1 === e.success &&
                app.notificate(
                    "error",
                    {
                        ru: "Ошибка! Пожалуйста, сообщите администрации!",
                        en: "Error! Please inform the administration!",
                        kz: "Қате! Әкімшілікке хабарлаңыз!",
                    }[locale]
                ),
                200 === e.status && !0 === e.success && location.reload();
            },
        });
    }),
    $(".dropdown-toggle").dropdown();
var dateCache = { lastDate: "" },
    declension = function (e, t) {
        var n,
            i = e % 100;
        return i >= 5 && i <= 20 ? (n = t[2]) : ((i %= 10), (n = 1 == i ? t[0] : i >= 2 && i <= 4 ? t[1] : t[2])), n;
    },
    recalculate = function (e, t, n, i, o, r, a, s, l, x=1) {
        if (!(e && t && n && i && o)) return !1;
        var u = Calculator.main.base_price;
        if (
            (e > Calculator.main.number_room_elevated_price
                ? (u += Calculator.main.one_room_price * Calculator.main.number_room_elevated_price + (e - Calculator.main.number_room_elevated_price) * Calculator.main.one_room_elevated_price)
                : (u += Calculator.main.one_room_price * e),
                (u += Calculator.main.one_bathroom_price * t),
            void 0 !== l && "" !== l && (u += Calculator.main.one_window_price * l),
                o.length)
        ) {
            for (var c = 0; c < o.length; c++)
                for (var d in Calculator.services)
                    Calculator.services[d].id === o[c].id &&
                    (0 === Calculator.services[d].pay
                        ? (u += Calculator.services[d].price)
                        : 1 === Calculator.services[d].pay
                            ? (u += Calculator.services[d].price * o[c].unit)
                            : 2 === Calculator.services[d].pay && (u += Calculator.services[d].price * o[c].unit));
            for (var c = 0; c < o.length; c++) "additional_cleaner" === o[c].id && (u += Calculator.additional_cleaner);
        }
        for (var p in Calculator.type) Calculator.type[p].id === n && (u = number_format(u * Calculator.type[p].ratio, 0, "", ""));
        for (var h in Calculator.cicle) Calculator.cicle[h].id === i && (u = number_format(u * Calculator.cicle[h].ratio, 0, "", ""));
        if ((r ? (0 === r.type && (u -= u * r.price), 1 === r.type && (u -= r.price)) : a && (u -= a), u >= Calculator.additional_cleaner_border)) {
            if ($(".js_additional-cleaner").hasClass("active")) {
                if (u - Calculator.additional_cleaner >= Calculator.additional_cleaner_border && !$(".js_additional-free-cleaner").hasClass("active")) return  $(".js_additional-free-cleaner").click() ;
            } else if (!$(".js_additional-free-cleaner").hasClass("active")) return $(".js_additional-free-cleaner").click();
        } else if (u < Calculator.additional_cleaner_border && $(".js_additional-free-cleaner").hasClass("active")) return   $(".js_additional-free-cleaner").click() ;
        if (void 0 === s || s) return u;
        if(x === 1){
            $(".order-total .total-summary b").text(
                number_format(u, 0, "", " ") +
                {
                    ru: " тг.",
                    en: " tg.",
                    kz: " тг.",
                }[locale]
            );
        }
        return u
    },
    recalculates = function (e, t, n, i, o, r, a, s, l, x=1) {
        if (!(e && t && n && i && o)) return !1;
        var u = Calculator.main.base_price;
        if (
            (e > Calculator.main.number_room_elevated_price
                ? (u += Calculator.main.one_room_price * Calculator.main.number_room_elevated_price + (e - Calculator.main.number_room_elevated_price) * Calculator.main.one_room_elevated_price)
                : (u += Calculator.main.one_room_price * e),
                (u += Calculator.main.one_bathroom_price * t),
            void 0 !== l && "" !== l && (u += Calculator.main.one_window_price * l),
                o.length)
        ) {
            for (var c = 0; c < o.length; c++)
                for (var d in Calculator.services)
                    Calculator.services[d].id === o[c].id &&
                    (0 === Calculator.services[d].pay
                        ? (u += Calculator.services[d].price)
                        : 1 === Calculator.services[d].pay
                            ? (u += Calculator.services[d].price * o[c].unit)
                            : 2 === Calculator.services[d].pay && (u += Calculator.services[d].price * o[c].unit));
            for (var c = 0; c < o.length; c++) "additional_cleaner" === o[c].id && (u += Calculator.additional_cleaner);
        }
        for (var p in Calculator.type) Calculator.type[p].id === n && (u = number_format(u * Calculator.type[p].ratio, 0, "", ""));
        for (var h in Calculator.cicle) Calculator.cicle[h].id === i && (u = number_format(u * Calculator.cicle[h].ratio, 0, "", ""));
        if ((r ? (0 === r.type && (u -= u * r.price), 1 === r.type && (u -= r.price)) : a && (u -= a), u >= Calculator.additional_cleaner_border)) {
            if ($(".js_additional-cleaner").hasClass("active")) {
                if (u - Calculator.additional_cleaner >= Calculator.additional_cleaner_border && !$(".js_additional-free-cleaner").hasClass("active")) return   u;
            } else if (!$(".js_additional-free-cleaner").hasClass("active")) return   u;
        } else if (u < Calculator.additional_cleaner_border && $(".js_additional-free-cleaner").hasClass("active")) return u;
        if (void 0 === s || s) return u;
        if(x === 1){
            $(".order-total .total-summary b").text(
                number_format(u, 0, "", " ") +
                {
                    ru: " тг.",
                    en: " tg.",
                    kz: " тг.",
                }[locale]
            );
        }
        return u
    },
    summTime = function (e, t, n, i) {
        e.setHours(e.getHours() + +t, e.getMinutes() + +n, e.getSeconds() + +i);
    },
    getSumTime = function (e) {
        return e.getHours() + ":" + e.getMinutes();
    },
    timeCalculator = function (e, t, n, i) {
        var o = new Date();
        o.setFullYear(0), o.setMonth(0), o.setDate(0), o.setHours(0, 0, 0), summTime(o, Calculator.main.base_time.split(":")[0], Calculator.main.base_time.split(":")[1], 0);
        for (var r = 0; r < e; r++) summTime(o, Calculator.main.room_time.split(":")[0], Calculator.main.room_time.split(":")[1], 0);
        for (var r = 0; r < t; r++) summTime(o, Calculator.main.bathroom_time.split(":")[0], Calculator.main.bathroom_time.split(":")[1], 0);
        if (void 0 !== i && "" !== i) for (var r = 0; r < i; r++) summTime(o, Calculator.main.window_time.split(":")[0], Calculator.main.window_time.split(":")[1], 0);
        var a = 1;
        if (n.length) {
            for (var r = 0; r < n.length; r++) if ("additional_cleaner" !== n[r].id && "additional_free_cleaner" !== n[r].id) for (var s = 0; s < +n[r].unit; s++) summTime(o, n[r].time.split(":")[0], n[r].time.split(":")[1], 0);
            for (var r = 0; r < n.length; r++) "additional_cleaner" === n[r].id && (a += 1);
            for (var r = 0; r < n.length; r++) "additional_free_cleaner" === n[r].id && (a += 1);
        }
        if (a > 1) {
            var l = +o.getMinutes() + 60 * +o.getHours();
            l > 0 && (l = Math.floor(l / a)), o.setHours(Math.floor(l / 60), Math.floor(l % 60), 0);
        }
        return o;
    },
    calculateTime = function (e, t, n, i) {
        cleaningTime = timeCalculator(e, t, n, i);
        var o = cleaningTime.getHours() + " ";
        (o += declension(
            +cleaningTime.getHours(),
            {
                ru: ["час", "часа", "часов"],
                en: ["hour", "hours", "hours"],
                kz: ["сағат", "сағат", "сағат"],
            }[locale]
        )),
            (o +=
                +cleaningTime.getMinutes() > 0
                    ? " и " +
                    cleaningTime.getMinutes() +
                    declension(
                        +cleaningTime.getMinutes(),
                        {
                            ru: [" минута", " минуты", " минут"],
                            en: [" minute", " minutes", " minutes"],
                            kz: [" минутына", " минутына", " минутына"],
                        }[locale]
                    )
                    : ""),
            $(".order-total .item.time").find(".panel").text(o);
    },
    calculateElapsedTime = function () {
        var e = +$("input[name='number_rooms']").val(),
            t = +$("input[name='number_bath_rooms']").val();
        if ($("input[name='number_window']").length) var n = +$("input[name='number_window']").val();
        else var n = "";
        var i = [];
        return (
            $(".service-item.active .inputs").each(function () {
                i.push({ id: $(this).find(".service-name").val(), time: $(this).find(".service-name").attr("data-time"), unit: $(this).find(".service-value").val() });
            }),
                (cleaningTime = timeCalculator(e, t, i, n)),
                (time = cleaningTime.getHours() + ":" + (+cleaningTime.getMinutes() > 0 ? cleaningTime.getMinutes() : "00")),
                time
        );
    },
    collectFormData = function () {
        if ("undefined" == typeof Calculator) return !1;
        setTimeout(function () {
            var e = +$("input[name='number_rooms']").val(),
                t = +$("input[name='number_bath_rooms']").val(),
                n = "";
            $("input[name='number_window']").length && (n = +$("input[name='number_window']").val());
            var i = $(".type input[name='type']:checked").val(),
                o = $(".cicle input[name='cicle']:checked").val(),
                r = [];
            $(".service-item.active .inputs").each(function () {
                r.push({ id: $(this).find(".service-name").val(), time: $(this).find(".service-name").attr("data-time"), unit: $(this).find(".service-value").val() });
            });

            var a = (points = !1);

            return (
                $(".discount-params").attr("data-type") && $(".discount-params").attr("data-price")
                    ? (a = { price: $(".discount-params").attr("data-price"), type: +$(".discount-params").attr("data-type") })
                    : $(".points-params").attr("data-price") && (points = +$(".points-params").attr("data-price")),
                    calculateTime(e, t, r, n),
                    recalculate(e, t, i, o, r, a, points, !1, n)

            );

        }, 300);
    },
    collectFormDataWithOutDiscount = function () {
        if ("undefined" == typeof Calculator) return !1;

            var e = +$("input[name='number_rooms']").val(),
                t = +$("input[name='number_bath_rooms']").val(),
                n = "";
            $("input[name='number_window']").length && (n = +$("input[name='number_window']").val());
            var i = $(".type input[name='type']:checked").val(),
                o = $(".cicle input[name='cicle']:checked").val(),
                r = [];
            $(".service-item.active .inputs").each(function () {
                r.push({ id: $(this).find(".service-name").val(), time: $(this).find(".service-name").attr("data-time"), unit: $(this).find(".service-value").val() });
            });

            var a = (points = !1);
            var sum = recalculates(e, t, i, o, r,a,0, 1, n,0);

            return +sum;


    },
    collectFormDataWithoutTimeout = function (e) {
        var t = +$("input[name='number_rooms']").val(),
            n = +$("input[name='number_bath_rooms']").val(),
            i = "";
        $("input[name='number_window']").length && (i = +$("input[name='number_window']").val());
        var o = $(".type input[name='type']:checked").val(),
            r = $(".cicle input[name='cicle']:checked").val(),
            a = [];
        $(".service-item.active .inputs").each(function () {
            a.push({ id: $(this).find(".service-name").val(), unit: $(this).find(".service-value").val() });
        });
        var s = (points = !1);
        return recalculate(t, n, o, r, a, s, points, e, i);
    },
    clearDateInputs = function () {
        $("input[name='date']").val(""), $("input[name='time']").val(""), addDateAndTime();
    },
    addDateAndTime = function () {
        var e = $("input[name='date']").val(),
            t = $("input[name='time']").val(),
            n = "",
            i = new Date(),
            o = $(".order-total .date");
        if (("" === e && "" === t ? (o.find(".panel").empty().text(""), o.hide()) : o.show(), e)) {
            if (((e = e.split("-")), +i.getDate() == +e[0] && +i.getMonth() + 1 == +e[1] && +i.getFullYear() == +e[2])) return $("input[name='date']").val(""), o.is(":hidden") || o.hide(), !1;
            (n += +e[0]), (n += " " + app.month.split(",")[+e[1] - 1]), (n += " " + e[2] + " года, ");
        }
        t && (n += t), o.find(".panel").empty().text(n);
    },
    updateSidebarServicesInfo = function () {
        var e = [];
        if (
            ($(".service-item.active .inputs").each(function () {
                e.push({ id: $(this).find(".service-name").val(), count: $(this).find(".add-service-value").length ? $(this).find(".add-service-value").val() : $(this).find(".service-value").val() });
            }),
                e.length)
        )
            for (var t = 0; t < e.length; t++)
                $("#" + e[t].id)
                    .find(".js_service-count")
                    .text(e[t].count);
    };
$(document).ready(function () {
    collectFormData(), $(".order-total .cicle .panel").text($(".cicle input:checked").parents(".icr-label").find(".icr-text").text());
}),
    $("body").on("click", ".number-rooms i", function () {

        var e = $(this).attr("class"),
            t = +$("input[name='number_rooms']").val();
        (!t || t < 1 || "" === t) && (t = 1),
            "minus" === e ? t > 1 && ((t -= 1), $("input[name='number_rooms']").val(t)) : "plus" === e && ((t += 1), $("input[name='number_rooms']").val(t)),
            $("#number_rooms").val(
                t +
                " " +
                declension(
                    t,
                    {
                        ru: ["комната", "комнаты", "комнат"],
                        en: ["room", "rooms", "rooms"],
                        kz: ["бөлме", "бөлме", "бөлме"],
                    }[locale]
                )
            ),

            collectFormData(),

            clearDateInputs();
        if($(".points-params").attr("data-price") != 0 && $(".points-params").attr("data-price")){
            var summary = +collectFormDataWithOutDiscount();
            $('#old_price').html(summary + ' тг')
        }
    }),
    $("body").on("click", ".number-bath-rooms i", function () {
        var e = $(this).attr("class"),
            t = +$("input[name='number_bath_rooms']").val();
        (!t || t < 1 || "" === t) && (t = 1),
            "minus" === e ? t > 1 && ((t -= 1), $("input[name='number_bath_rooms']").val(t)) : "plus" === e && ((t += 1), $("input[name='number_bath_rooms']").val(t)),
            $("#number_bath_rooms").val(
                t +
                " " +
                declension(
                    t,
                    {
                        ru: ["санузел", "санузла", "санузлов"],
                        en: ["bathrooms", "bathrooms", "bathrooms"],
                        kz: ["жуынатын бөлме", "жуынатын бөлме", "жуынатын бөлме"],
                    }[locale]
                )
            ),
            collectFormData(),
            clearDateInputs();
        if($(".points-params").attr("data-price") != 0 && $(".points-params").attr("data-price")){
            var summary = +collectFormDataWithoutTimeout(true);
            $('#old_price').html(summary + ' тг')
        }
    }),
    $("body").on("click", ".number-window i", function () {
        var e = $(this).attr("class"),
            t = +$("input[name='number_window']").val();
        (!t || t < 0 || "" === t) && (t = 0),
            "minus" === e ? t > 0 && ((t -= 1), $("input[name='number_window']").val(t)) : "plus" === e && ((t += 1), $("input[name='number_window']").val(t)),
            $("#number_window").val(
                t +
                " " +
                declension(
                    t,
                    {
                        ru: ["окно", "окна", "окон"],
                        en: ["window", "windows", "windows"],
                        kz: ["терезе", "терезе", "терезе"],
                    }[locale]
                )
            ),
            collectFormData(),
            clearDateInputs();
        if($(".points-params").attr("data-price") != 0 && $(".points-params").attr("data-price")){
            var summary = +collectFormDataWithoutTimeout(true);
            $('#old_price').html(summary + ' тг')
        }
    }),
    $("body").on("change", "input[name='cicle']", function () {
        $(this).parents(".cicle").length && $(".order-total .cicle .panel").text($(this).parents(".radio").attr("data-label")), collectFormData(), clearDateInputs();
    }),
    $("body").on("change", "input[name='type']", function () {
        var e = "/cleaning";
        "general" === $(this).val() && (e = "/cleaning/general"), (location = e);
    }),
    $("body").on("click", ".service-item", function () {
        var e;

        clearDateInputs(),
            $(this).hasClass("active")
                ? ($(this).removeClass("active"),
                    (e = $(this).find("input").val()),
                    $(".attachible-services")
                        .find("#" + e)
                        .fadeOut(0),
                    $(this).find(".inputs input").prop("checked", !1),
                    $(this).parents(".item").find(".add-service-value-modal").fadeOut(0))
                : ($(this).addClass("active"),
                    (e = $(this).find("input").val()),
                    $(".attachible-services")
                        .find("#" + e)
                        .fadeIn(0),
                    $(this).find(".inputs input").prop("checked", !0),
                    $(this).parents(".item").find(".add-service-value-modal").fadeIn(0)),
            $(".attachible-services .item").is(":visible") ? $(".more-services").fadeIn(0) : $(".more-services").fadeOut(0),collectFormData();
        if($(".points-params").attr("data-price") != 0 && $(".points-params").attr("data-price")){
            var summary = 0 ;

            if(summary === 0){
                summary += parseInt(collectFormDataWithOutDiscount())

            }
            // console.log(collectFormDataWithoutTimeout())
            // debugger
            // console.log(summary)
            // summary = number_format(summary, 0, '', ' ');
            console.log(summary + 'tg')

            $('#old_price').html(summary + ' тг')
        }

        updateSidebarServicesInfo();
    }),
    $("body").on("change", "input[name='date'], input[name='time']", function () {
        return addDateAndTime();
    }),
    $("body").on("change", "input[name='email']", function () {
        if (!$(this).val()) return $(".auth-field").fadeOut(0), !1;
        var e = $(this).val();
        $.ajax({
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            url: "/cleaning/checkemail",
            type: "POST",
            data: { email: e },
            success: function (e) {
                400 === e.status && (app.notificate("error", e.msg), $(".auth-field").fadeOut(0)),
                300 === e.status && (app.notificate("warning", e.msg), $(".auth-field").fadeIn(0)),
                200 === e.status && (e.msg && "" !== e.msg && app.notificate("success", e.msg), $(".auth-field").fadeOut(0));
            },
        });
    }),
    $("body").on("click", ".fast-login", function () {
        $.ajax({
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            url: "/login/order",
            type: "POST",
            data: { email: $("input[name='email']").val(), password: $("input[name='password']").val() },
            success: function (e) {
                (e && 400 !== e.status && !1 !== e.success) ||
                app.notificate(
                    "error",
                    {
                        ru: "Неправильно заполнены поля e-mail и/или пароль",
                        en: "Incorrectly filled in e-mail and / or password",
                        kz: "Электрондық пошта және / немесе пароль дұрыс емес",
                    }[locale]
                ),
                200 === e.status &&
                !0 === e.success &&
                (app.notificate(
                    "success",
                    {
                        ru: "Вы успешно авторизовались!",
                        en: "You have successfully logged in!",
                        kz: "Сіз сәтті кірдіңіз!",
                    }[locale]
                ),
                    $(".auth-field").remove(),
                    $(".points").fadeIn(0),
                e.data &&
                ($("input[name='name']").val(e.data.name),
                    $("input[name='phone']").val(e.data.phone),
                    $("input[name='complex']").val(e.data.complex),
                    $("input[name='housing']").val(e.data.housing),
                    $("input[name='house']").val(e.data.house),
                    $("input[name='flat']").val(e.data.flat),
                    $("input[name='street']").val(e.data.street)));
            },
        });
    }),
    $("#order-cleaning input[name='discount']").on("change", function () {
        if (($('.choose-block input[name="discount"]').removeAttr("style"), onlyOneSaleTypeCanUse()))
            return (
                app.notificate(
                    "warning",
                    {
                        ru: "Можно использовать либо баллы либо промокод. Но не все вместе!",
                        en: "You can use either points or promo code. But not all together!",
                        kz: "Кез-келген нүктеден немесе промо-кодты пайдалана аласыз. Бірақ барлығы бірге емес!",
                    }[locale]
                ),
                    !1
            );
        var e = $(this).val();
        if (e === ''){
            var summary = +collectFormDataWithoutTimeout(true);
            $('#old_price').html('')
            $(".points-params").attr("data-price",0)
            $('.discount_info').html('')
            $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
            $("#order-cleaning input[name='points']").removeAttr("disabled")
            $("#order-cleaning input[name='points']").css("background-color", "#fff")

        }else {


            if ((e || "" !== e ? $("#order-cleaning input[name='points']").attr("disabled", !0).attr("style", "background: #ddd;") : $("#order-cleaning input[name='points']").removeAttr("disabled").removeAttr("style"), !e && "" === e))
                return $(".discount-params").removeAttr("data-price"), $(".discount-params").removeAttr("data-type"), collectFormData(), !1;
            $.ajax({
                headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")},
                url: "/cleaning/checkdiscount",
                type: "POST",
                data: {discount: e},
                success: function (data) {

                    if (data.status === 400) {
                        app.notificate('error', data.msg);
                        $('.choose-block input[name="points"]').attr("style", "border: 1px solid #ff8279;");
                        var summary = +collectFormDataWithoutTimeout(true);
                        $(".points-params").attr("data-price", data.data);
                        $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
                    }
                    if (data.status === 200) {
                        app.notificate('success', data.msg);

                        if (data.data) {

                            // get summary

                            var summary = +collectFormDataWithoutTimeout(true);
                            if($('#old_price').is(':empty')){
                                $('#old_price').append(summary + ' тг')
                            }
                            if(data.data.type === 1){
                                summary -= (1 * data.data.price);
                                $(".points-params").attr("data-price", data.data.price);
                                $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
                                if($('.discount_info').children().length === 0){
                                    if($('#local').val() === 'ru'){
                                        $('.discount_info').append(
                                            'Скидка по промокоду '+data.data.code+' <b>'+data.data.price+'тг</b> '
                                        )
                                    }else if($('#local').val() === 'kz'){
                                        $('.discount_info').append(
                                            'Промо-код бойынша жеңілдік '+data.data.code+' <b>'+data.data.price+'тг</b> '
                                        )
                                    }else{
                                        $('.discount_info').append(
                                            'Discount by promo code '+data.data.code+' <b>'+data.data.price+'тг</b>'
                                        )
                                    }

                                }
                            }else{
                                summary -= (data.data.price*summary);
                                $(".points-params").attr("data-price", data.data.price);
                                $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
                                // console.log($('#discount_info').html())
                                if($('.discount_info').children().length === 0){
                                    if($('#local').val() === 'ru'){
                                        $('.discount_info').append(
                                            'Скидка по промокоду '+data.data.code+' <b>'+data.data.price*100+'%</b> '
                                        )
                                    }else if($('#local').val() === 'kz'){
                                        $('.discount_info').append(
                                            'Промо-код бойынша жеңілдік '+data.data.code+' <b>'+data.data.price*100+'%</b> '
                                        )
                                    }else{
                                        $('.discount_info').append(
                                            'Discount by promo code '+data.data.code+' <b>'+data.data.price*100+'%</b>'
                                        )
                                    }

                                }
                            }

                        }
                    }else{
                        $(".points-params").attr("data-price", 0);
                        $('#old_price').html('')
                        $('.discount_info').html('')
                    }
                },
            });
        }
    }),
    $("#order-cleaning input[name='points']").on("change", function () {
        var t = +collectFormDataWithoutTimeout(!0);
        if (($('.choose-block input[name="points"]').removeAttr("style"), onlyOneSaleTypeCanUse()))
            return (
                app.notificate(
                    "warning",
                    {
                        ru: "Можно использовать либо баллы либо промокод. Но не все вместе!",
                        en: "You can use either points or promo code. But not all together!",
                        kz: "Кез-келген нүктеден немесе промо-кодты пайдалана аласыз. Бірақ барлығы бірге емес!",
                    }[locale]
                ),
                    !1
            );
        var e = $(this).val();
        if(e === ""){
            $(".order-total .total-summary b").text(number_format(t, 0, "", " ") + " тг.");
            $('#discountPromotion').html('');
            $('#old_price').html('')
        }
        if ((e || "" !== e ? $("#order-cleaning input[name='discount']").attr("disabled", !0).attr("style", "background: #ddd;") : $("#order-cleaning input[name='discount']").removeAttr("disabled").removeAttr("style"), !e && "" === e))
            return $(".points-params").removeAttr("data-price"), collectFormData(), !1;
        $.ajax({
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            url: "/cleaning/checkpoints",
            type: "POST",
            data: { points: e },
            success: function (e) {
                var t = +collectFormDataWithoutTimeout(!0);
                if (e.status === 200){
                    if($('#old_price').is(':empty')){
                        $('#old_price').append(t + ' тг')
                    }
                }else{
                    $('#old_price').html('')

                }
                if ((400 === e.status && (app.notificate("error", e.msg),
                    $('.choose-block input[name="points"]').attr("style", "border: 1px solid #ff8279;")),
                    $('#discountPromotion').html(''),$(".order-total .total-summary b").text(number_format(t, 0, "", " ") + " тг."),$(".points-params").attr("data-price", 0), 200 === e.status && (app.notificate("success", e.msg), e.data))) {
                    let discount = $('#points').val()
                    $('#discountPromotion').html('Скидка по баллам <b>-'+discount +' тг</b>')
                    var t = +collectFormDataWithoutTimeout(!0);
                    (t -= e.data), $(".points-params").attr("data-price", e.data), $(".order-total .total-summary b").text(number_format(t, 0, "", " ") + " тг.");
                }
            },
        });
    }),
    $("body").on("click", ".add-order", function () {
        if (!validate_form("order-cleaning", ["name", "email", "phone", "date", "time", "street", "house", "pay"]))
            return (
                app.notificate(
                    "warning",
                    {
                        ru: "Пожалуйста, заполните все обязательные поля!",
                        en: "Please fill in all required fields!",
                        kz: "Барлық міндетті өрістерді толтырыңыз!",
                    }[locale]
                ),
                    !1
            );
        $("input[name='elapsed']").val(calculateElapsedTime()),
            $.ajax({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                url: "/cleaning/order",
                type: "POST",
                data: $("#order-cleaning").serialize(),
                beforeSend: function () {
                    $(".add-order").attr("disabled", !0), $(".wrapper").addClass("loading");
                },
                success: function (e) {
                    $(".add-order").attr("disabled", !1),
                        $(".wrapper").removeClass("loading"),
                    400 === e.status && app.notificate("error", e.msg),
                    e.data &&
                    "" !== e.data &&
                    ($(".table-cell-class").removeClass("table-cell-class"),
                        $(".fixed-block").removeClass("fix"),
                        $(".faq-sidebar").removeAttr("style"),
                        $("#order-cleaning").remove(),
                        $(".order-total").fadeOut(0),
                        $(".order-total").empty(),
                        $(".order-result").html(e.data),
                        $(".js_remove").remove(),
                        scrollTopAfterOrder(),
                        app.notificate(
                            "success",
                            {
                                ru: "Заказ успешно сформирован!",
                                en: "Order successfully formed!",
                                kz: "Тапсырыс сәтті қалыптасты!",
                            }[locale]
                        ));
                },
            });
    }),
    $("body").on("click change", "input[name='date']", function (e) {
        var t = $(this).val(),
            n = calculateElapsedTime();
        return (
            !(!t || "" === t || !n || "" === n) &&
            ("click" === e.type
                ? ((dateCache.lastDate = t), !1)
                : ("change" !== e.type || dateCache.lastDate !== t) &&
                ($("input[name='time']").val(""),
                    void $.ajax({
                        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                        url: "/cleaning/checktime",
                        type: "POST",
                        data: { date: t, elapsed: n },
                        success: function (e) {
                            if (400 !== e.status) {
                                $('.choose-block input[name="date"]').removeAttr("style");
                                var t = e.data.t_from.split(":"),
                                    n = e.data.t_to.split(":"),
                                    i = [],
                                    o = +n[0];
                                if (e.data.t_from === e.data.t_to) i.push(e.data.t_from);
                                else for (var r = +t[0]; r <= o; r++) r === +t[0] ? i.push(r + ":" + t[1]) : i.push(r + ":00"), (r + 1 <= o || t[0] === n[0]) && i.push(r + ":30");
                                in_array(+n[0] + ":" + n[1], i) || i.push(n[0] + ":" + n[1]),
                                    $('.choose-block input[name="time"]').prop("disabled", !1),
                                    $('.choose-block input[name="time"]').datetimepicker({ datepicker: !1, allowTimes: i, format: "H:i" });
                            }
                            400 === e.status &&
                            (app.notificate("error", e.msg),
                                $('.choose-block input[name="time"]').val(""),
                                $('.choose-block input[name="time"]').prop("disabled", !0),
                                (dateCache.lastDate = ""),
                                $('.choose-block input[name="date"]').attr("style", "border: 1px solid #ff8279;"));
                        },
                    })))
        );
    }),
    $("body").on("click", ".set-service-value", function () {
        var e = $(this).parents(".add-service-value-modal"),
            t = e.find("input").val();
        (!t || "" === t || t <= 0) && (t = 1),
            $(this).parents(".item").find(".service-item .service-value").val(t),
            $(this).parents(".item").find(".inputs input").attr("checked", !0),
            e.fadeOut(0),
            collectFormData(),
            updateSidebarServicesInfo();
    }),
    $("body").on("click", ".set-service-value", function () {
        var e = $(this).parents(".add-service-value-modal"),
            t = e.find("input").val();
        (!t || "" === t || t <= 0) && (t = 1),
            $(this).parents(".item").find(".service-item .service-value").val(t),
            $(this).parents(".item").find(".inputs input").attr("checked", !0),
            e.fadeOut(0),
            collectFormData(),
            updateSidebarServicesInfo();
    }),
    $("#order-worker").on("click", ".send-order-workers", function () {
        if (!validate_form("order-worker", ["schedule", "weight", "name", "phone"]))
            return (
                app.notificate(
                    "warning",
                    {
                        ru: "Пожалуйста, заполните все обязательные поля!",
                        en: "Please fill in all required fields!",
                        kz: "Барлық міндетті өрістерді толтырыңыз!",
                    }[locale]
                ),
                    !1
            );
        $.ajax({
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            url: "/workers/order",
            type: "POST",
            data: $("#order-worker").serialize(),
            beforeSend: function () {
                $(".send-order-workers").attr("disabled", !0), $(".wrapper").addClass("loading");
            },
            success: function (e) {
                $(".wrapper").removeClass("loading"),
                400 === e.status && (app.notificate("error", e.msg), $(".send-order-workers").attr("disabled", !1)),
                e.data &&
                "" !== e.data &&
                ($("#order-worker").remove(),
                    $(".js_remove").remove(),
                    $(".order-result").html(e.data),
                    scrollTopAfterOrder(),
                    app.notificate(
                        "success",
                        {
                            ru: "Ваша заявка успешно отправлена!",
                            en: "Your application has been sent successfully!",
                            kz: "Сіздің өтінішіңіз сәтті жіберілді!",
                        }[locale]
                    ));
            },
        });
    }),

$("#order-worker input[name='points']").on("change", function () {
    if (($('#order-worker input[name="points"]').removeAttr("style"), onlyOneSaleTypeCanUse()))
        return (
            app.notificate(
                "warning",
                {
                    ru: "Можно использовать либо баллы либо промокод. Но не все вместе!",
                    en: "You can use either points or promo code. But not all together!",
                    kz: "Кез-келген нүктеден немесе промо-кодты пайдалана аласыз. Бірақ барлығы бірге емес!",
                }[locale]
            ),
                !1
        );
    var e = $(this).val();
    if ((e || "" !== e ? $("#order-worker input[name='discount']").attr("disabled", !0).attr("style", "background: #ddd;") : $("#order-worker input[name='discount']").removeAttr("disabled").removeAttr("style"), !e && "" === e)) return !1;
    $.ajax({
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
        url: "/workers/checkpoints",
        type: "POST",
        data: { points: e },
        success: function (e) {
            400 === e.status && (app.notificate("error", e.msg), $('#order-worker input[name="points"]').attr("style", "border: 1px solid #ff8279;")), 200 === e.status && app.notificate("success", e.msg);
        },
    });
}),
    $("body").on("change", "#order-worker input[name='email']", function () {
        if (!$(this).val()) return $(".auth-field").fadeOut(0), !1;
        var e = $(this).val();
        $.ajax({
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            url: "/workers/checkemail",
            type: "POST",
            data: { email: e },
            success: function (e) {
                400 === e.status && (app.notificate("error", e.msg), $(".auth-field").fadeOut(0)),
                300 === e.status && (app.notificate("warning", e.msg), $(".auth-field").fadeIn(0)),
                200 === e.status && (e.msg && "" != e.msg && app.notificate("success", e.msg), $(".auth-field").fadeOut(0));
            },
        });
    }),
    $("body").on("click", ".fast-login", function () {
        $.ajax({
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            url: "/login/order",
            type: "POST",
            data: { email: $("input[name='email']").val(), password: $("input[name='password']").val() },
            success: function (e) {
                (e && 400 !== e.status && !1 !== e.success) ||
                app.notificate(
                    "error",
                    {
                        ru: "Неправильно заполнены поля e-mail и/или пароль",
                        en: "Incorrectly filled in e-mail and / or password",
                        kz: "Электрондық пошта және / немесе пароль дұрыс емес",
                    }[locale]
                ),
                200 === e.status &&
                !0 === e.success &&
                (app.notificate(
                    "success",
                    {
                        ru: "Вы успешно авторизовались!",
                        en: "You have successfully logged in!",
                        kz: "Сіз сәтті кірдіңіз!",
                    }[locale]
                ),
                    $(".auth-field").remove(),
                    $(".points").fadeIn(0),
                e.data &&
                ($("input[name='name']").val(e.data.name),
                    $("input[name='phone']").val(e.data.phone),
                    $("input[name='complex']").val(e.data.complex),
                    $("input[name='housing']").val(e.data.housing),
                    $("input[name='house']").val(e.data.house),
                    $("input[name='flat']").val(e.data.flat),
                    $("input[name='street']").val(e.data.street)));
            },
        });
    });
    window.onload = function() {


        if($("input[name='discount']").val() !== ''){
            setTimeout(function () {
                var e = $("input[name='discount']").val()

                if (e !== undefined) {



                    // if ((e || "" !== e ? $("#order-cleaning input[name='points']").attr("disabled", !0).attr("style", "background: #ddd;") : $("#order-cleaning input[name='points']").removeAttr("disabled").removeAttr("style"), !e && "" === e))
                    //     return $(".discount-params").removeAttr("data-price"), $(".discount-params").removeAttr("data-type"), collectFormData(), !1;
                    $.ajax({
                        headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")},
                        url: "/cleaning/checkdiscount",
                        type: "POST",
                        data: {discount: e},
                        success: function (data) {
                            console.log(data)
                            if (data.status === 400) {
                                app.notificate('error', data.msg);
                                $('.choose-block input[name="points"]').attr("style", "border: 1px solid #ff8279;");

                                var summary = +collectFormDataWithoutTimeout(true);
                                $(".points-params").attr("data-price", data.data);
                                $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
                            }
                            if (data.status === 200) {
                                app.notificate('success', data.msg);

                                if (data.data) {

                                    // get summary
                                    var summary = +collectFormDataWithoutTimeout(true);
                                    $('#old_price').append(summary + ' тг')

                                    console.log(data)
                                    summary -= (1 * data.data.price);

                                    $(".points-params").attr("data-price", data.data.price);
                                    $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');

                                }
                            }
                        },
                    });
                }
            }, 1000)
        }
    }

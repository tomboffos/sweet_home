(function($) {
	
$(document).ready(function() {
	
$('.reviews-slider').bxSlider({
	slideMargin: 15,
	preloadImages: 'all',
	controls: false,
	maxSlides: 4,
	moveSlides: 4,
	slideWidth: 270
});

});

})(jQuery);
// send order from workers page
$("#order-worker").on("click", ".send-order-workers", function() {

    if (!validate_form('order-worker', ['schedule', 'weight', 'name', 'phone'])) {
        app.notificate('warning', 'Пожалуйста, заполните все обязательные поля!');
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/workers/order",
        type: "POST",
        data: $("#order-worker").serialize(),
        beforeSend: function() {
            $(".send-order-workers").attr('disabled', true);
            $(".wrapper").addClass('loading');
        },
        success: function(data) {
            $(".wrapper").removeClass('loading');
            if (data.status === 400) {
                app.notificate('error', data.msg);
                $(".send-order-workers").attr('disabled', false);
            }
            if (data.data && data.data !== '') {
                $("#order-worker").remove();
                $(".js_remove").remove();
                $(".order-result").html(data.data);
                scrollTopAfterOrder();
                app.notificate('success', 'Ваша заявка успешно отправлена!');
            }
        }
    });
});

// check discount code
$("#order-worker input[name='discount']").on("change", function() {

    $('#order-worker input[name="discount"]').removeAttr("style");

    if (onlyOneSaleTypeCanUse()) {
        app.notificate('warning', 'Можно использовать либо баллы либо промокод. Но не все вместе!');
        return false;
    }

    var discount = $(this).val();

    if (discount || discount !== '') {
        $("#order-worker input[name='points']").attr("disabled", true).attr("style", "background: #ddd;");
    } else {
        $("#order-worker input[name='points']").removeAttr("disabled").removeAttr("style");
    }

    if (!discount && discount === '') {
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/workers/checkdiscount",
        type: "POST",
        data: { discount: discount },
        success: function(data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
                $('#order-worker input[name="discount"]').attr("style", "border: 1px solid #ff8279;");
            }
            if (data.status === 200) {
                app.notificate('success', data.msg);

            }
        }
    });
});

// check points
$("#order-worker input[name='points']").on("change", function() {

    $('#order-worker input[name="points"]').removeAttr("style");

    if (onlyOneSaleTypeCanUse()) {
        app.notificate('warning', 'Можно использовать либо баллы либо промокод. Но не все вместе!');
        return false;
    }

    var points = $(this).val();

    if (points || points !== '') {
        $("#order-worker input[name='discount']").attr("disabled", true).attr("style", "background: #ddd;");
    } else {
        $("#order-worker input[name='discount']").removeAttr("disabled").removeAttr("style");
    }

    if (!points && points === '') {
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/workers/checkpoints",
        type: "POST",
        data: { points: points },
        success: function(data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
                $('#order-worker input[name="points"]').attr("style", "border: 1px solid #ff8279;");
            }
            if (data.status === 200) {
                app.notificate('success', data.msg);
            }
        }
    });
});

// check email
$("body").on("change", "#order-worker input[name='email']", function() {
    if (!$(this).val()) {
        $(".auth-field").fadeOut(0);
        return false;
    }

    var email = $(this).val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/workers/checkemail",
        type: "POST",
        data: { email: email },
        success: function(data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
                $(".auth-field").fadeOut(0);
            }
            if (data.status === 300) {
                app.notificate('warning', data.msg);
                $(".auth-field").fadeIn(0);
            }
            if (data.status === 200) {
                if (data.msg && data.msg != '') {
                    app.notificate('success', data.msg);
                }
                $(".auth-field").fadeOut(0);
            }
        }
    });
});

// auth via modal
$("body").on("click", ".fast-login", function() {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/login/order",
        type: "POST",
        data: { email: $("input[name='email']").val(), password: $("input[name='password']").val() },
        success: function(data) {
            if (!data || data.status === 400 || data.success === false) {
                app.notificate('error', 'Неправильно заполнены поля e-mail и/или пароль');
            }

            if (data.status === 200 && data.success === true) {
                app.notificate('success', 'Вы успешно авторизовались!');
                $(".auth-field").remove();
                $(".points").fadeIn(0);

                if (data.data) {
                    $("input[name='name']").val(data.data.name);
                    $("input[name='phone']").val(data.data.phone);
                    $("input[name='complex']").val(data.data.complex);
                    $("input[name='housing']").val(data.data.housing);
                    $("input[name='house']").val(data.data.house);
                    $("input[name='flat']").val(data.data.flat);
                    $("input[name='street']").val(data.data.street);
                }
            }
        }
    });
});
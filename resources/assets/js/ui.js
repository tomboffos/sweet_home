// main config
var app = {
    animationSpeed: 100,

    // view notifications
    notificate: function (type, message) {
        var data = {
            view: function (type, message) {
                $(".notificate-add-" + type).html("<p>" + message + "</p>");
                $(".notificate-add-" + type).fadeIn(200);

                setTimeout(function () {
                    $(".notificate-add-" + type).fadeOut(200);
                }, 5000);
            },
            info: function (message) {
                return this.view(message, 'info');
            },
            warning: function (message) {
                return this.view(message, 'warning');
            },
            success: function (message) {
                return this.view(message, 'success');
            },
            error: function (message) {
                return this.view(message, 'error');
            }
        }
        return data.view(type, message);
    },

    // monthes
    month: 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря'
};

// php in_array eq. function
function in_array(needle, haystack, strict) {
    var found = false,
        key, strict = !!strict;
    for (key in haystack) {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
            found = true;
            break;
        }
    }
    return found;
}

// php number format eq. function
function number_format(number, decimals, dec_point, thousands_sep) {

    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',

        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };

    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');

    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }

    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }

    return s.join(dec);
}

// fix sidebar
var dinamicBlock = function () {

    if ($(document).width() < 992 || !$(".fixed-block").length) {
        return false;
    }

    // use fix btn only on order page
    if ($(".order-total").length) {
        if (!$(".fix-btn").hasClass('active')) {
            return false;
        }
    }

    var dinamicBlockHeight = +$(".order-total").outerHeight() + 100,
        topPos = $('.sidebar').offset().top,
        top = $(document).scrollTop(),
        pip = $('.skills').offset().top,
        $dinamicBlock = $('.fixed-block');

    // if in area between header and footer
    if (top > topPos && top < pip - dinamicBlockHeight) {
        $dinamicBlock.addClass('fix').removeAttr("style");
        $dinamicBlock.find('.fix-btn').removeAttr("style");
    }
    // if footer area
    else if (top > pip - dinamicBlockHeight) {
        $dinamicBlock.removeClass('fix').css({'position': 'absolute', 'bottom': '0', 'margin-bottom': '0'});
        $dinamicBlock.find('.fix-btn').css({'margin-right': '0', 'right': '0'});
    } else {
        $dinamicBlock.removeClass('fix');
    }

    // hide or show faq block
    if (top > pip - (+$(".order-total").outerHeight() + $(".faq-sidebar").outerHeight() + 120)) {
        $(".faq-sidebar").attr("style", "position: absolute; display: none;");
    } else {
        $(".faq-sidebar").removeAttr("style");
    }
};

// init skills on home page
var skillsOnHomeSlider = function () {

    if ($("#skills").length) {

        var options = {
            items: 3,
            itemsDesktop: [1920, 3],
            itemsTablet: [991, 1],
            pagination: true,
        };

        //Initialize Plugin
        $("#skills").owlCarousel(options);

        //get carousel instance data and store it in variable owl
        var owl = $("#skills").data('owlCarousel');

        if ($(document).width() < 991) {
            owl.reinit(options);
        } else {
            owl.destroy();
        }
    }
};

$(document).ready(function () {
    if ($(document).width() < 768) {
        $("#toggle-primary-menu").addClass("not-active");
    }

    // decorate radio boxes
    if ($("input[type='radio']").length) {
        $(".radio").find("label>input[type='radio']").ionCheckRadio();
    }

    // add input phone mask
    $("input[name='phone']").mask("+7 (999) 999-99-99");

    if ($('.choose-block').length && $('.cleaning-order-page').length) {
        $.datetimepicker.setLocale('ru');

        // choose data and time when order edit
        $('.choose-block input[name="date"]').datetimepicker({
            timepicker: false,
            startDate: '+1971/05/01',
            minDate: (Schedule.enableOrder > 0 ? '-1969/12/31' : '-1969/12/30'),
            format: 'd-m-Y',
            disabledWeekDays: Schedule.disabledWeekDays,
        });

        var c_from = +Schedule.minTime.split(':')[0],
            c_top = +Schedule.maxTime.split(':')[0],
            allowTimes = [];

        for (var i = c_from; i <= c_top; i++) {
            allowTimes.push(i + ":00");
        }

        $('.choose-block input[name="time"]').datetimepicker({
            datepicker: false,
            allowTimes: allowTimes,
            format: 'H:i'
        });
    }

    // toggle tooltip in user page
    if ($(".what-is-this").length) {
        $('.what-is-this').popover({
            container: 'body'
        });
    }

    // toggle tooltip in cleaner page
    if ($(".raiting").length) {
        $('.raiting').popover({
            container: 'body'
        });
    }

    // decorate radio boxes in user index page
    if ($("#subscribe").length) {
        $("input[type='checkbox']").ionCheckRadio();
    }

    // decorate radio boxes in user add testimonal
    if ($("#add-testimonal").length) {
        $("input[type='checkbox']").ionCheckRadio();
    }

    // add expand btn on home page
    expandBtnOnHome();

    // skills slider on home
    skillsOnHomeSlider();

    if ($('#testimonal').length && location.hash && location.hash === '#testimonal') {
        $('html, body').animate({scrollTop: $('#testimonal').offset().top}, 700);
    }

    if ($("#section-slider").length) {
        $("#section-slider").owlCarousel({
            items: 1,
            itemsDesktop: [1920, 1],
            itemsTablet: [991, 1],
            pagination: true,
            navigation: true,
            autoPlay: true,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        });
    }

    $('[data-toggle="tooltip"]').tooltip();
});

$(window).resize(function () {
    if ($(document).width() < 768) {
        $("#toggle-primary-menu").addClass("not-active");
        if ($(".primary-menu .has-childs.active").length) {
            $(".primary-menu .has-childs.active").removeClass("active");
            $(".primary-menu .has-childs ul").slideUp(app.animationSpeed);
        }
    }

    // dinamic block
    dinamicBlock();

    // skills slider on home
    skillsOnHomeSlider();
});

$(window).scroll(function () {

    // dinamic block
    dinamicBlock();
});

// fix btn
$(".fix-btn").on("click", function () {
    if ($(this).hasClass("active")) {
        $('.fixed-block').removeClass("fix");
        $('.fixed-block').removeAttr("style");
        $(this).removeClass("active");
    } else {
        $(this).addClass("active");
    }
});

// toggle top menu
$("#toggle-menu").on("click", function () {
    if ($(".mobile-menu").is(":hidden")) {
        $(".mobile-menu").slideDown(app.animationSpeed);
    } else {
        $(".mobile-menu").slideUp(app.animationSpeed);
    }
});

// toggle primary top menu
$("#toggle-primary-menu").on("click", function () {
    if ($(".primary-menu>ul").is(":hidden")) {
        $(this).removeClass("not-active");
        $(".primary-menu>ul").slideDown(app.animationSpeed);
        $(".primary-menu .title").fadeOut(app.animationSpeed);
    } else {
        $(this).addClass("not-active");
        $(".primary-menu>ul").slideUp(app.animationSpeed);
        $(".primary-menu .title").fadeIn(app.animationSpeed);

        if ($(".primary-menu .has-childs.active").length) {
            $(".primary-menu .has-childs.active").removeClass("active");
            $(".primary-menu .has-childs ul").slideUp(app.animationSpeed);
        }
    }
});

// toggle primary top menu childs
$(".has-childs").on("click", function () {
    if ($(this).find("ul").is(":hidden")) {
        $(this).addClass("active");
        $(this).find("ul").slideDown(app.animationSpeed);
    } else {
        $(this).removeClass("active");
        $(this).find("ul").slideUp(app.animationSpeed);
    }
});

// toggle faq
$(".open-answer").on("click", function () {
    if ($(this).parents(".question-block").find(".answer-block").is(":hidden")) {
        $(".question-block").find(".answer-block").slideUp(app.animationSpeed);
    }
    $(this).parents(".question-block").find(".answer-block").slideDown(app.animationSpeed);
});

// hide modals when switch to another modal
$(".close-this-modal").click(function () {
    var id = $(this).parents(".modal-auth").attr("id");
    $('#' + id).modal('hide');
});

// auth via modal
function modalAuth() {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/login/ajax",
        type: "POST",
        data: $("#modalAuthForm").serialize(),
        success: function (data) {
            if (!data || data.status === 400 || data.success === false) {
                app.notificate('error', 'Неправильно заполнены поля e-mail и/или пароль');
            }

            if (data.status === 200 && data.success === true) {
                location.reload();
            }
        }
    });
}

// register via modal
function modalRegister() {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/register/ajax",
        type: "POST",
        data: $("#modalRegistrationForm").serialize(),
        success: function (data) {
            if (data.status === 400 || data.success === false) {

                var out = '';

                if (data.errors.email && data.errors.email.length > 0) {
                    for (var i = 0; i < data.errors.email.length; i++) {
                        out += data.errors.email[i] + '<br />';
                    }
                }

                if (data.errors.password && data.errors.password.length > 0) {
                    for (var k = 0; k < data.errors.password.length; k++) {
                        out += data.errors.password[k] + '<br />';
                    }
                }

                app.notificate('error', out);
            }

            if (data.status === 200 && data.success === true) {

                app.notificate('success', 'Вы успешно зарегистрировались!<br />Сейчас мы обновим страницу, чтобы авторизовать вас!');

                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        }
    });
}

// reset password
function modalResetPassword() {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/password/reset/ajax",
        type: "POST",
        data: $("#modalForgotForm").serialize(),
        success: function (data) {
            if (data.status === 400 && data.success === false) {
                app.notificate('error', 'Неправильно заполнены поля e-mail и/или пароль!');
            }
            if (data.status === 400 && data.success === 'none') {
                app.notificate('error', data.msg);
            }
            if (data.status === 200 && data.success === true) {
                $("#modalForgotForm").remove();
                $("#modalForgotPass .modal-body").html('<p>На указанный email отправлено письмо с инструкциями для восстановлению пароля!</p>');
                app.notificate('success', data.msg);
            }
        }
    });
}

// validate form function
var validate_form = function (form, fields) {

    var errors = [];

    // clear input and textarea styles
    $("#" + form + " input").removeAttr("style");
    $("#" + form + " textarea").removeAttr("style");

    // cheack inputs
    $("#" + form + " input").each(function (i) {
        if (!$(this).val() && (in_array($(this).attr("name"), fields) || $(this).attr("required"))) {
            $(this).css("border", "1px solid red");
            errors.push(i);
        }
    });

    // cheack textareas
    $("#" + form + " textarea").each(function (i) {
        if (!$(this).val() && (in_array($(this).attr("name"), fields) || $(this).attr("required"))) {
            $(this).css("border", "1px solid red");
            errors.push(i);
        }
    });

    if (errors.length) {
        return false;
    }

    return true;
};

// check if both types are use
var onlyOneSaleTypeCanUse = function () {
    if ($("input[name='discount']").val() && $("input[name='points']").val()) {
        return true;
    }

    return false;
};

// click vote in testimonals
$("body").on("click", ".vote-block i", function () {

    var id = $(this).attr("data-vote"),
        $vote_block = $(this).parents(".vote-block");

    $vote_block.find("i").removeClass("active");
    $vote_block.find("input").val(id);

    $vote_block.find("i").each(function (i) {
        i++;
        if (i <= id) {
            $(this).addClass("active");
        }
    });
});

// validate vote in testimonals
$(document).on("click", ".add-testimonal-btn", function (e) {

    e.preventDefault();

    var empty = [];

    $(".vote-block").find("input").each(function (i) {
        if (!$(this).val() || $(this).val() === '') {
            empty.push(i);
        }
    });

    if (empty.length) {

        app.notificate('warning', 'Пожалуйста, оцените заказ.');

        $('#add-testimonal').submit(function () {
            return false;
        });

        return false;
    }

    $('#add-testimonal')[0].submit();
});

// delete cleaner from favorite or black list
var deleteCleaner = function (cleaner, type) {

    if (!type || type === '' || !cleaner || cleaner === '') {
        return false;
    }

    if (!confirm("Вы уверены?")) {
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/user/" + type,
        type: "POST",
        data: {cleaner: cleaner},
        success: function (data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
            }
            if (data.status === 200) {
                $("#cleaner-" + cleaner).remove();
                app.notificate('success', data.msg);
            }
        }
    });
};

// scroll top after send order
var scrollTopAfterOrder = function () {

    if ($(".fixed-block").length) {
        $(".fixed-block").removeAttr('style');
    }

    var element = $("#scroll-here");
    if (element.length == 1) {
        $('html, body').animate({scrollTop: element.offset().top}, 700);
    }
    return false;
};

// call me form
function modalCallMeForm() {
    if (!validate_form('modalCallMeForm', ['phone'])) {
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/form/handle",
        type: "POST",
        data: $("#modalCallMeForm").serialize(),
        success: function (data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
            }
            if (data.status === 200) {
                $("#modalCallMeForm").remove();
                $("#modalCallMe .modal-body").html('<p>Ваша заявка отправлена! Мы свяжемся с вами в ближайшее время!</p>');
                app.notificate('success', data.msg);
            }
        }
    });
}

// add expand btn on home page
var expandBtnOnHome = function () {
    if ($(".why-we").length) {
        $(".why-we .item").each(function (i) {
            var short = $(this).find(".short");
            if (short.height() > 150) {
                $(this).find(".info-block").addClass("expanding-block");
                $(this).find(".expand-block").fadeIn(0);
            }
        });
    }
};

// expand block on home page
$("body").on("click", ".expand-block", function () {
    if (!$(this).hasClass("active")) {
        $(this).addClass("active");
        $(this).text("Скрыть подробности");
        $(this).parents(".why-we .item").find(".info-block").animate({maxHeight: "100%"}, 0);
    } else {
        $(this).removeClass("active");
        $(this).text("Показать полностью");
        $(this).parents(".why-we .item").find(".info-block").animate({maxHeight: "150"}, 0);
    }
});

// scroll to workers btn on home page
$("body").on("click", '.scroll-to-workers', function () {
    if ($('.other-services').length) {
        $('html, body').animate({scrollTop: $('.other-services').offset().top}, 700);
    }
    return false;
});

$(document).on('click', '.js_scroll-to', function (e) {
    e.preventDefault();
    var id = $($(this).data('scroll'));

    if (id.length) {
        $('html, body').animate({scrollTop: id.offset().top}, 700);
    }
});

// add cleaner to favorite
$("body").on("click", '.cleaner-action', function () {
    var action = $(this).attr("data-action"),
        cleaner = +$(this).attr("data-cleaner");

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/cleaners/action",
        type: "POST",
        data: {action: action, cleaner: cleaner},
        success: function (data) {
            if (data.status === 400 && data.success === false) {
                app.notificate('error', 'Ошибка! Пожалуйста, сообщите администрации!');
            }
            if (data.status === 200 && data.success === true) {
                location.reload();
            }
        }
    });
});

$('.dropdown-toggle').dropdown();
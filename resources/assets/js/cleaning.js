// date input cache
var dateCache = {
    lastDate: ''
};

// declension for rooms and bath rooms
var declension = function(num, expressions) {
    var result,
        count = num % 100;

    if (count >= 5 && count <= 20) {
        result = expressions['2'];
    } else {
        count = count % 10;
        if (count == 1) {
            result = expressions['0'];
        } else if (count >= 2 && count <= 4) {
            result = expressions['1'];
        } else {
            result = expressions['2'];
        }
    }
    return result;
};

// recalculate order
var recalculate = function(number_rooms, number_bath_rooms, type, cicle, service_item, sale, points, param, number_window) {

    if (!number_rooms || !number_bath_rooms || !type || !cicle || !service_item) {
        return false;
    }

    // base price
    var summary = Calculator.main.base_price;

    // rooms
    if (number_rooms > Calculator.main.number_room_elevated_price) {
        summary += Calculator.main.one_room_price * Calculator.main.number_room_elevated_price + (number_rooms - Calculator.main.number_room_elevated_price) * Calculator.main.one_room_elevated_price;
    } else {
        summary += Calculator.main.one_room_price * number_rooms;
    }

    // bathrooms
    summary += Calculator.main.one_bathroom_price * number_bath_rooms;

    // windows (general cleaning)
    if (typeof number_window !== 'undefined' && number_window !== '') {
        summary += Calculator.main.one_window_price * number_window;
    }

    // services
    if (service_item.length) {

        // rotate choosent services
        for (var i = 0; i < service_item.length; i++) {

            // rotate bellow all attributes of current service
            for (var key in Calculator.services) {
                if (Calculator.services[key].id === service_item[i].id) {

                    if (Calculator.services[key].pay === 0) {
                        summary += Calculator.services[key].price;
                    } else if (Calculator.services[key].pay === 1) {
                        summary += Calculator.services[key].price * service_item[i].unit;
                    } else if (Calculator.services[key].pay === 2) {
                        summary += Calculator.services[key].price * service_item[i].unit;
                    }
                }
            }
        }

        // if additional cleaner
        for (var i = 0; i < service_item.length; i++) {
            if (service_item[i].id === 'additional_cleaner') {
                summary += Calculator.additional_cleaner;
            }
        }
    }

    // coefficient of type; rotate bellow all available types
    for (var t_key in Calculator.type) {
        if (Calculator.type[t_key].id === type) {
            summary = number_format(summary * Calculator.type[t_key].ratio, 0, '', '');
        }
    }

    // coefficient of cicle; rotate bellow all available cicles
    for (var c_key in Calculator.cicle) {
        if (Calculator.cicle[c_key].id === cicle) {
            summary = number_format(summary * Calculator.cicle[c_key].ratio, 0, '', '');
        }
    }

    // minus discount
    if (sale) {

        // percent discount type
        if (sale.type === 0) {
            summary -= (summary * sale.price);
        }

        // amount discount type
        if (sale.type === 1) {
            summary -= sale.price;
        }
    }

    // minus points
    else if (points) {
        summary -= points;
    }

    // add/remove additional free cleaner
    if (summary >= Calculator.additional_cleaner_border) {
        if ($('.js_additional-cleaner').hasClass('active')) {
            if ((summary - Calculator.additional_cleaner) >= Calculator.additional_cleaner_border && !$('.js_additional-free-cleaner').hasClass('active')) {
                return $('.js_additional-free-cleaner').click();
            }
        } else if (!$('.js_additional-free-cleaner').hasClass('active')) {
            return $('.js_additional-free-cleaner').click();
        }
    } else if (summary < Calculator.additional_cleaner_border && $('.js_additional-free-cleaner').hasClass('active')) {
        return $('.js_additional-free-cleaner').click();
    }

    // return summary
    if (typeof param !== 'undefined' && !param) {
        $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
    } else {
        return summary;
    }
};

// summ time for cleaning
var summTime = function(time, h, m, s) {
    time.setHours(
        time.getHours() + +h,
        time.getMinutes() + +m,
        time.getSeconds() + +s
    );
};

// get time for cleaning
var getSumTime = function(time) {
    return time.getHours() + ':' + time.getMinutes();
};

// time calculator function
var timeCalculator = function(number_rooms, number_bath_rooms, service_item, number_window) {
    var cleaningTime = new Date();
    cleaningTime.setFullYear(0);
    cleaningTime.setMonth(0);
    cleaningTime.setDate(0);
    cleaningTime.setHours(0, 0, 0);

    // base time
    summTime(cleaningTime, Calculator.main.base_time.split(':')[0], Calculator.main.base_time.split(':')[1], 0);

    // time for rooms
    for (var i = 0; i < number_rooms; i++) {
        summTime(cleaningTime, Calculator.main.room_time.split(':')[0], Calculator.main.room_time.split(':')[1], 0);
    }

    // time for bathrooms
    for (var i = 0; i < number_bath_rooms; i++) {
        summTime(cleaningTime, Calculator.main.bathroom_time.split(':')[0], Calculator.main.bathroom_time.split(':')[1], 0);
    }

    // windows (general cleaning)
    if (typeof number_window !== 'undefined' && number_window !== '') {
        for (var i = 0; i < number_window; i++) {
            summTime(cleaningTime, Calculator.main.window_time.split(':')[0], Calculator.main.window_time.split(':')[1], 0);
        }
    }

    var cleaners_num = 1;

    // time for services
    if (service_item.length) {
        for (var i = 0; i < service_item.length; i++) {
            if (service_item[i].id !== 'additional_cleaner' && service_item[i].id !== 'additional_free_cleaner') {
                for (var k = 0; k < +service_item[i].unit; k++) {
                    summTime(cleaningTime, service_item[i].time.split(':')[0], service_item[i].time.split(':')[1], 0);
                }
            }
        }
        for (var i = 0; i < service_item.length; i++) {
            if (service_item[i].id === 'additional_cleaner') {
                cleaners_num += 1;
            }
        }
        for (var i = 0; i < service_item.length; i++) {
            if (service_item[i].id === 'additional_free_cleaner') {
                cleaners_num += 1;
            }
        }
    }

    if (cleaners_num > 1) {
        var minutes = +cleaningTime.getMinutes() + (+cleaningTime.getHours() * 60);

        if (minutes > 0) {
            minutes = Math.floor(minutes / cleaners_num);
        }

        cleaningTime.setHours(Math.floor(minutes / 60), Math.floor(minutes % 60), 0);
    }

    return cleaningTime;
};

// calculate time
var calculateTime = function(number_rooms, number_bath_rooms, service_item, number_window) {

    cleaningTime = timeCalculator(number_rooms, number_bath_rooms, service_item, number_window);

    var timeHtml = cleaningTime.getHours() + ' ';
    timeHtml += declension(+cleaningTime.getHours(), {
        'ru' : ['час', 'часа', 'часов'],
        'en' : ['hour', 'hour', 'hours'],
        'kz' : ['сағат', 'сағат', 'сағат'],
    }[locale]);
    timeHtml += (+cleaningTime.getMinutes() > 0 ?
        ' и ' + cleaningTime.getMinutes() + declension(+cleaningTime.getMinutes(), ' ' + {
        'ru' : ['минута', 'минуты', 'минут'],
        'en' : ['minute', 'minutes', 'minutes'],
        'kz' : ['минутына', 'минутына', 'минутына'],
    }[locale]) : '');

    $('.order-total .item.time').find('.panel').text(timeHtml);
};

// get elapsed time
var calculateElapsedTime = function() {

    // rooms number
    var number_rooms = +$("input[name='number_rooms']").val();

    // bath rooms number
    var number_bath_rooms = +$("input[name='number_bath_rooms']").val();

    // window number (general cleaning)
    if ($("input[name='number_window']").length) {
        var number_window = +$("input[name='number_window']").val();
    } else {
        var number_window = '';
    }

    // service item
    var service_item = [];

    $(".service-item.active .inputs").each(function() {
        service_item.push({
            id: $(this).find(".service-name").val(),
            time: $(this).find(".service-name").attr('data-time'),
            unit: $(this).find(".service-value").val()
        });
    });

    cleaningTime = timeCalculator(number_rooms, number_bath_rooms, service_item, number_window);

    time = cleaningTime.getHours() + ':' + (+cleaningTime.getMinutes() > 0 ? cleaningTime.getMinutes() : '00');

    return time;
};

// collectFormData order
var collectFormData = function() {

    if (typeof Calculator === 'undefined') {
        return false;
    }

    setTimeout(function() {

        // rooms number
        var number_rooms = +$("input[name='number_rooms']").val();

        // bath rooms number
        var number_bath_rooms = +$("input[name='number_bath_rooms']").val();

        // window number (general cleaning)
        var number_window = '';
        if ($("input[name='number_window']").length) {
            number_window = +$("input[name='number_window']").val();
        }

        // get checked type
        var type = $(".type input[name='type']:checked").val();

        // get checked type
        var cicle = $(".cicle input[name='cicle']:checked").val();

        // service item
        var service_item = [];

        $(".service-item.active .inputs").each(function() {
            service_item.push({
                id: $(this).find(".service-name").val(),
                time: $(this).find(".service-name").attr('data-time'),
                unit: $(this).find(".service-value").val()
            });
        });

        var sale = points = false;

        // discount && points
        if ($(".discount-params").attr('data-type') && $(".discount-params").attr('data-price')) {

            sale = {
                price: $(".discount-params").attr('data-price'),
                type: +$(".discount-params").attr('data-type')
            };

        } else if ($(".points-params").attr('data-price')) {
            points = +$(".points-params").attr('data-price');
        }

        // calculate time
        calculateTime(number_rooms, number_bath_rooms, service_item, number_window);

        // calculate total summ
        return recalculate(number_rooms, number_bath_rooms, type, cicle, service_item, sale, points, false, number_window);

    }, 300);
};

// collect Form Data Without Timeout (when Points or Checkout send)
var collectFormDataWithoutTimeout = function(param) {

    // rooms number
    var number_rooms = +$("input[name='number_rooms']").val();

    // bath rooms number
    var number_bath_rooms = +$("input[name='number_bath_rooms']").val();

    // window number (general cleaning)
    var number_window = '';
    if ($("input[name='number_window']").length) {
        number_window = +$("input[name='number_window']").val();
    }

    // get checked type
    var type = $(".type input[name='type']:checked").val();

    // get checked type
    var cicle = $(".cicle input[name='cicle']:checked").val();

    // service item
    var service_item = [];

    $(".service-item.active .inputs").each(function() {
        service_item.push({
            id: $(this).find(".service-name").val(),
            unit: $(this).find(".service-value").val()
        });
    });

    var sale = points = false;

    // calculate total summ
    return recalculate(number_rooms, number_bath_rooms, type, cicle, service_item, sale, points, param, number_window);
};

// clear date and time inputs when change something on page
var clearDateInputs = function() {
    $("input[name='date']").val('');
    $("input[name='time']").val('');
    addDateAndTime();
};

// add date and time to order block
var addDateAndTime = function() {
    var date = $("input[name='date']").val(),
        time = $("input[name='time']").val(),
        str = '',
        current_date = new Date(),
        blk = $(".order-total .date");

    if (date === '' && time === '') {
        blk.find('.panel').empty().text('');
        blk.hide();
    } else {
        blk.show();
    }

    if (date) {

        date = date.split('-');

        if (+current_date.getDate() === +date[0] &&
            (+current_date.getMonth() + 1) === +date[1] &&
            +current_date.getFullYear() === +date[2]) {
            $("input[name='date']").val('');
            if (!blk.is(":hidden")) {
                blk.hide();
            }
            return false;
        }

        str += +date[0];
        str += ' ' + app.month.split(',')[+date[1] - 1];
        str += ' ' + date[2] + ' года, ';
    }

    if (time) {
        str += time;
    }

    blk.find('.panel').empty().text(str);
};

// update prices and counts in sidebar
var updateSidebarServicesInfo = function() {

    var services = [];

    $(".service-item.active .inputs").each(function() {
        services.push({
            id: $(this).find(".service-name").val(),
            count: ($(this).find('.add-service-value').length ? $(this).find(".add-service-value").val() : $(this).find(".service-value").val())
        });
    });

    if (services.length) {
        for (var i = 0; i < services.length; i++) {
            $('#' + services[i].id).find('.js_service-count').text(services[i].count);
        }
    }
};

$(document).ready(function() {

    // get summary of default values
    collectFormData();

    // add default cicle
    $(".order-total .cicle .panel").text($(".cicle input:checked").parents(".icr-label").find(".icr-text").text());
});

// add number rooms
$("body").on("click", ".number-rooms i", function() {
    var action = $(this).attr("class"),
        value = +$("input[name='number_rooms']").val();

    if (!value || value < 1 || value === '') {
        value = 1;
    }

    // change value
    if (action === 'minus') {
        if (value > 1) {
            value -= 1;
            $("input[name='number_rooms']").val(value);
        }
    } else if (action === 'plus') {
        value += 1;
        $("input[name='number_rooms']").val(value);
    }

    // change text
    $("#number_rooms").val(value + ' ' + declension(value, {
        'ru' : ['комната', 'комнаты', 'комнат'],
        'en' : ['room', 'room', 'rooms'],
        'kz' : ['бөлме', 'бөлме', 'бөлме'],
    }[locale]));

    // collectFormData
    collectFormData();

    clearDateInputs();
});

// add number bath rooms
$("body").on("click", ".number-bath-rooms i", function() {
    var action = $(this).attr("class"),
        value = +$("input[name='number_bath_rooms']").val();

    if (!value || value < 1 || value === '') {
        value = 1;
    }

    // change value
    if (action === 'minus') {
        if (value > 1) {
            value -= 1;
            $("input[name='number_bath_rooms']").val(value);
        }
    } else if (action === 'plus') {
        value += 1;
        $("input[name='number_bath_rooms']").val(value);
    }

    // change text
    $("#number_bath_rooms").val(value + ' ' + declension(value, {
        'ru' : ['санузел', 'санузла', 'санузлов'],
        'en' : ['bathroom', 'bathroom', 'bathrooms'],
        'kz' : ['жуынатын бөлме', 'жуынатын бөлме', 'жуынатын бөлме'],
    }[locale]));

    // collectFormData
    collectFormData();

    clearDateInputs();
});

// add number windows (general cleaning)
$("body").on("click", ".number-window i", function() {
    var action = $(this).attr("class"),
        value = +$("input[name='number_window']").val();

    if (!value || value < 1 || value === '') {
        value = 1;
    }

    // change value
    if (action === 'minus') {
        if (value > 1) {
            value -= 1;
            $("input[name='number_window']").val(value);
        }
    } else if (action === 'plus') {
        value += 1;
        $("input[name='number_window']").val(value);
    }

    // change text
    $("#number_window").val(value + ' ' + declension(value, {
        'ru' : ["окно","окна","окон"],
        'en' : ['window', 'window', 'window'],
        'kz' : ['терезе', 'терезе', 'терезе'],
    }[locale]));

    // collectFormData
    collectFormData();

    clearDateInputs();
});

// add cicle data
$("body").on("change", "input[name='cicle']", function() {

    // add text to order-total block
    if ($(this).parents('.cicle').length) {
        $(".order-total .cicle .panel").text($(this).parents('.radio').attr('data-label'));
    }

    // collectFormData
    collectFormData();

    clearDateInputs();
});

// add type data
$("body").on("change", "input[name='type']", function() {

    var url = '/cleaning';

    if ($(this).val() === 'general') {
        url = '/cleaning/general';
    }

    location = url;
});

// add services data
$("body").on("click", ".service-item", function() {

    var id;

    clearDateInputs();

    if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        id = $(this).find("input").val();
        $(".attachible-services").find("#" + id).fadeOut(0);
        $(this).find(".inputs input").prop("checked", false);
        $(this).parents(".item").find(".add-service-value-modal").fadeOut(0);
    } else {
        $(this).addClass("active");
        id = $(this).find("input").val();
        $(".attachible-services").find("#" + id).fadeIn(0);
        $(this).find(".inputs input").prop("checked", true);
        $(this).parents(".item").find(".add-service-value-modal").fadeIn(0);
    }

    if ($(".attachible-services .item").is(":visible")) {
        $(".more-services").fadeIn(0);
    } else {
        $(".more-services").fadeOut(0);
    }

    // collectFormData
    collectFormData();

    // update prices and counts in sidebar
    updateSidebarServicesInfo();
});

// add date
$("body").on("change", "input[name='date'], input[name='time']", function() {
    return addDateAndTime();
});

// check email
$("body").on("change", "input[name='email']", function() {
    if (!$(this).val()) {
        $(".auth-field").fadeOut(0);
        return false;
    }

    var email = $(this).val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/cleaning/checkemail",
        type: "POST",
        data: { email: email },
        success: function(data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
                $(".auth-field").fadeOut(0);
            }
            if (data.status === 300) {
                app.notificate('warning', data.msg);
                $(".auth-field").fadeIn(0);
            }
            if (data.status === 200) {
                if (data.msg && data.msg !== '') {
                    app.notificate('success', data.msg);
                }
                $(".auth-field").fadeOut(0);
            }
        }
    });
});

// auth via modal
$("body").on("click", ".fast-login", function() {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/login/order",
        type: "POST",
        data: { email: $("input[name='email']").val(), password: $("input[name='password']").val() },
        success: function(data) {
            if (!data || data.status === 400 || data.success === false) {
                app.notificate('error', {
                    'ru' : 'Неправильно заполнены поля e-mail и/или пароль',
                    'en' : 'Incorrectly filled in e-mail and / or password',
                    'kz' : 'Электрондық пошта және / немесе пароль дұрыс емес',
                }[locale]);
            }

            if (data.status === 200 && data.success === true) {
                app.notificate('success', {
                    'ru' : 'Вы успешно авторизовались!',
                    'en' : 'You have successfully logged in!',
                    'kz' : 'Сіз сәтті кірдіңіз!',
                }[locale]);
                $(".auth-field").remove();
                $(".points").fadeIn(0);

                if (data.data) {
                    $("input[name='name']").val(data.data.name);
                    $("input[name='phone']").val(data.data.phone);
                    $("input[name='complex']").val(data.data.complex);
                    $("input[name='housing']").val(data.data.housing);
                    $("input[name='house']").val(data.data.house);
                    $("input[name='flat']").val(data.data.flat);
                    $("input[name='street']").val(data.data.street);
                }
            }
        }
    });
});

// check discount code
$("#order-cleaning input[name='discount']").on("change", function() {

    $('.choose-block input[name="discount"]').removeAttr("style");

    if (onlyOneSaleTypeCanUse()) {
        app.notificate('warning', {
            'ru' : 'Можно использовать либо баллы либо промокод. Но не все вместе!',
            'en' : 'You can use either points or promo code. But not all together!',
            'kz' : 'Кез-келген нүктеден немесе промо-кодты пайдалана аласыз. Бірақ барлығы бірге емес!',
        }[locale]);
        return false;
    }

    var discount = $(this).val();


    if (discount || discount !== '') {
        $("#order-cleaning input[name='points']").attr("disabled", true).attr("style", "background: #ddd;");
    } else {
        $("#order-cleaning input[name='points']").removeAttr("disabled").removeAttr("style");
    }

    if (!discount && discount === '') {
        $(".discount-params").removeAttr("data-price");
        $(".discount-params").removeAttr("data-type");
        collectFormData();
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/cleaning/checkdiscount",
        type: "POST",
        data: { discount: discount },
        success: function(data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
                $('.choose-block input[name="discount"]').attr("style", "border: 1px solid #ff8279;");
                var summary = collectFormDataWithoutTimeout(true);
                $(".discount-params").attr("data-price", summary);
                $(".discount-params").attr("data-type", data.data.type);
                $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
            }
            if (data.status === 200) {


                app.notificate('success', data.msg);

                $('#discount_info').html(
                    'Скидка по промокоду '+ discount+' <b>'+$('#percent').val()+'%</b>'
                )
                $('#discount_info').style('css','block')
                if (data.data) {

                    // get summary
                    var summary = collectFormDataWithoutTimeout(true);

                    // percent discount type

                    summary -= (1 * data.data.amount);


                    // amount discount type




                    $(".discount-params").attr("data-price", summary);
                    $(".discount-params").attr("data-type", data.data.type);
                    $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');


                }
            }
        }
    });
});

// check points
$("input[name='points']").on("change", function() {

    $('.choose-block input[name="points"]').removeAttr("style");

    if (onlyOneSaleTypeCanUse()) {
        app.notificate('warning', {
            'ru' : 'Можно использовать либо баллы либо промокод. Но не все вместе!',
            'en' : 'You can use either points or promo code. But not all together!',
            'kz' : 'Кез-келген нүктеден немесе промо-кодты пайдалана аласыз. Бірақ барлығы бірге емес!',
        }[locale]);
        return false;
    }

    var points = $(this).val();

    if (points || points !== '') {
        $("#order-cleaning input[name='discount']").attr("disabled", true).attr("style", "background: #ddd;");
    } else {
        $("#order-cleaning input[name='discount']").removeAttr("disabled").removeAttr("style");
    }

    if (!points && points === '') {
        $(".points-params").removeAttr("data-price");
        collectFormData();
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/cleaning/checkpoints",
        type: "POST",
        data: { points: points },
        success: function(data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
                $('.choose-block input[name="points"]').attr("style", "border: 1px solid #ff8279;");
            }
            if (data.status === 200) {
                app.notificate('success', data.msg);

                if (data.data) {

                    // get summary
                    var summary = +collectFormDataWithoutTimeout(true);

                    summary -= data.data;

                    $(".points-params").attr("data-price", data.data);
                    $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
                }
            }
        }
    });
});

$('a#allPoint').on('click',function(){


    var points = $('#points').val();


    if (points || points !== '') {
        $("#order-cleaning input[name='discount']").attr("disabled", true).attr("style", "background: #ddd;");
    } else {
        $("#order-cleaning input[name='discount']").removeAttr("disabled").removeAttr("style");
    }

    if (!points && points === '') {
        $(".points-params").removeAttr("data-price");
        collectFormData();
        return false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/cleaning/checkpoints",
        type: "POST",
        data: { points: points },
        success: function(data) {
            if (data.status === 400) {
                app.notificate('error', data.msg);
                $('.choose-block input[name="points"]').attr("style", "border: 1px solid #ff8279;");
            }
            if (data.status === 200) {
                app.notificate('success', data.msg);

                if (data.data) {

                    // get summary
                    var summary = +collectFormDataWithoutTimeout(true);

                    summary -= data.data;

                    $(".points-params").attr("data-price", data.data);
                    $(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
                }
            }
        }
    });
})
// add order
$("body").on("click", ".add-order", function() {

    if (!validate_form('order-cleaning', ['name', 'email', 'phone', 'date', 'time', 'street', 'house', 'pay'])) {
        app.notificate('warning', {
            'ru' : 'Пожалуйста, заполните все обязательные поля!',
            'en' : 'Please fill in all required fields!',
            'kz' : 'Барлық міндетті өрістерді толтырыңыз!',
        }[locale]);
        return false;
    }

    // add elapsed time
    $("input[name='elapsed']").val(calculateElapsedTime());

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/cleaning/order",
        type: "POST",
        data: $("#order-cleaning").serialize(),
        beforeSend: function() {
            $(".add-order").attr('disabled', true);
            $(".wrapper").addClass('loading');
        },
        success: function(data) {

            $(".add-order").attr('disabled', false);
            $(".wrapper").removeClass('loading');

            if (data.status === 400) {
                app.notificate('error', data.msg);
            }

            if (data.data && data.data !== '') {
                $(".table-cell-class").removeClass("table-cell-class");
                $(".fixed-block").removeClass("fix");
                $(".faq-sidebar").removeAttr("style");
                $("#order-cleaning").remove();
                $(".order-total").fadeOut(0);
                $(".order-total").empty();
                $(".order-result").html(data.data);
                $(".js_remove").remove();
                scrollTopAfterOrder();
                app.notificate('success', {
                    'ru' : 'Заказ успешно сформирован!',
                    'en' : 'Order successfully formed!',
                    'kz' : 'Тапсырыс сәтті қалыптасты!',
                }[locale]);
            }
        }
    });
});

// check available time for cleaning
$("body").on("click change", "input[name='date']", function(e) {

    var date = $(this).val(),
        currentElapsedTime = calculateElapsedTime();

    if (!date || date === '' || !currentElapsedTime || currentElapsedTime === '') {
        return false;
    }

    if (e.type === 'click') {
        dateCache.lastDate = date;
        return false;
    }

    if (e.type === 'change') {
        if (dateCache.lastDate === date) {
            return false;
        }
    }

    $("input[name='time']").val('');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/cleaning/checktime",
        type: "POST",
        data: { date: date, elapsed: currentElapsedTime },
        success: function(data) {

            if (data.status !== 400) {

                $('.choose-block input[name="date"]').removeAttr("style");

                var t_from = data.data.t_from.split(':'),
                    t_to = data.data.t_to.split(':'),
                    allowTimes = [],
                    limit = +t_to[0];

                if (data.data.t_from === data.data.t_to) {
                    allowTimes.push(data.data.t_from);
                } else {
                    for (var i = +t_from[0]; i <= limit; i++) {

                        if (i === +t_from[0]) {
                            allowTimes.push(i + ':' + t_from[1]);
                        } else {
                            allowTimes.push(i + ':00');
                        }

                        if ((i + 1) <= limit || t_from[0] === t_to[0]) {
                            allowTimes.push(i + ':30');
                        }
                    }
                }

                if (!in_array(+t_to[0] + ':' + t_to[1], allowTimes)) {
                    allowTimes.push(t_to[0] + ':' + t_to[1]);
                }

                $('.choose-block input[name="time"]').prop("disabled", false);

                $('.choose-block input[name="time"]').datetimepicker({
                    datepicker: false,
                    allowTimes: allowTimes,
                    format: 'H:i'
                });
            }

            if (data.status === 400) {
                app.notificate('error', data.msg);
                $('.choose-block input[name="time"]').val('');
                $('.choose-block input[name="time"]').prop("disabled", true);
                dateCache.lastDate = '';
                $('.choose-block input[name="date"]').attr("style", "border: 1px solid #ff8279;");
            }
        }
    });
});

// set aditional service value (counts/hours)
$("body").on("click", ".set-service-value", function() {
    var $modal = $(this).parents(".add-service-value-modal"),
        value = $modal.find("input").val();

    if (!value || value === '' || value <= 0) {
        value = 1;
    }

    $(this).parents(".item").find(".service-item .service-value").val(value);
    $(this).parents(".item").find(".inputs input").attr("checked", true);
    $modal.fadeOut(0);

    // collectFormData
    collectFormData();

    // update prices and counts in sidebar
    updateSidebarServicesInfo();
});

// change 
$("body").on("click", ".set-service-value", function() {
    var $modal = $(this).parents(".add-service-value-modal"),
        value = $modal.find("input").val();

    if (!value || value === '' || value <= 0) {
        value = 1;
    }

    $(this).parents(".item").find(".service-item .service-value").val(value);
    $(this).parents(".item").find(".inputs input").attr("checked", true);
    $modal.fadeOut(0);

    // collectFormData
    collectFormData();

    // update prices and counts in sidebar
    updateSidebarServicesInfo();
});

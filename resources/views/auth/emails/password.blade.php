<html>
	<head>
		<title>Восстановление пароля</title>
	</head>
	<body>
		<p>Здравствуйте!</p>

		<p>На сайте http://homesweet.kz/ был сделан запрос на восстановление пароля.</p>

		<p>Если вы не запрашивали изменение пароля на сайте, просто проигнорируйте данное письмо.</p>

		<p>Если Вам действительно необходимо восстановить пароль, перейдите по ссылке: <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a></p>

	</body>
</html>
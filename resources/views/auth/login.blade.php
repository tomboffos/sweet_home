@extends('front.layout')

@section('content')
<section class="page-top">
    
    @include('front.menu')

    <div class="breadcrumbs">
        <div class="container">
            <div class="col-md-12">
                <h1>Авторизация</h1>

                <ul>
                    <li><a href="{{ url('/') }}">Главная</a></li>
                    <li>Авторизация</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page">

    <div class="container entry-content">
        <div class="row">
            <div class="col-md-9">
                <div class="entry-text">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Пароль</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Запомнить меня
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-sign-in"></i> Войти
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Забыли пароль?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3">
                <aside class="sidebar">
                    <div class="menu-list">
                        <ul>
                        <li class="active"><a href="{{ url('register') }}">Авторизация</a></li>
                        <li><a href="{{ url('register') }}">Регистрация</a></li>
                        <li><a href="{{ url('password/reset') }}">Напомнить пароль</a></li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </div>

    @include('front.footer-skills')

</main><!-- .content -->
@endsection

@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Настройки обычного калькулятора</h1>
        <div class="alert alert-danger" role="alert">Внимание! Изменения в данном разделе повлекут изменения при редактировании заказов!</div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/calc") }}" class="post-edit" method="post">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Тип уборки</a></li>
                <li><a href="#tab-3" data-toggle="tab">Дополнительные услуги</a></li>
                <li><a href="#tab-4" data-toggle="tab">Частота уборки</a></li>
            </ul>

            <div class="tab-content">

                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('base_price') ? ' has-error' : '' }}">
                                <label for="base_price">Базовая стоимость</label>
                                <input type="text" class="form-control" name="base_price" id="base_price" placeholder="Базовая стоимость" value="{{ $data['calc_main']['base_price'] }}">
                                @if ($errors->has('base_price'))
                                <span class="help-block"> <strong>{{ $errors->first('base_price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('base_time') ? ' has-error' : '' }}">
                                <label for="base_time">Базовое время</label>
                                <input type="text" class="form-control" name="base_time" id="base_time" placeholder="Базовое время" value="{{ $data['calc_main']['base_time'] }}">
                                <i>Формат записи: 2:00 - два часа, 3:30 - 3 часа 30 мин</i>
                                @if ($errors->has('base_time'))
                                <span class="help-block"> <strong>{{ $errors->first('base_time') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('number_room_elevated_price') ? ' has-error' : '' }}">
                                <label for="number_room_elevated_price">C какого количества комнат начинает действовать повышенный тариф</label>
                                <input type="text" class="form-control" name="number_room_elevated_price" id="number_room_elevated_price" placeholder="" value="{{ $data['calc_main']['number_room_elevated_price'] }}">
                                @if ($errors->has('number_room_elevated_price'))
                                <span class="help-block"> <strong>{{ $errors->first('number_room_elevated_price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('one_room_price') ? ' has-error' : '' }}">
                                <label for="one_room_price">Стоимость уборки 1 комнаты (до {{ $data['calc_main']['number_room_elevated_price'] }}х комнат)</label>
                                <input type="text" class="form-control" name="one_room_price" id="one_room_price" placeholder="Стоимость уборки 1 комнаты" value="{{ $data['calc_main']['one_room_price'] }}">
                                @if ($errors->has('one_room_price'))
                                <span class="help-block"> <strong>{{ $errors->first('one_room_price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('room_time') ? ' has-error' : '' }}">
                                <label for="room_time">Время уборки 1 комнаты</label>
                                <input type="text" class="form-control" name="room_time" id="room_time" placeholder="Базовое время" value="{{ $data['calc_main']['room_time'] }}">
                                <i>Формат записи: 2:00 - два часа, 3:30 - 3 часа 30 мин</i>
                                @if ($errors->has('room_time'))
                                <span class="help-block"> <strong>{{ $errors->first('room_time') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('one_room_elevated_price') ? ' has-error' : '' }}">
                                <label for="one_room_price">Стоимость уборки 1 комнаты ({{ $data['calc_main']['number_room_elevated_price'] }} и более комнат)</label>
                                <input type="text" class="form-control" name="one_room_elevated_price" id="one_room_elevated_price" placeholder="Стоимость уборки 1 комнаты" value="{{ $data['calc_main']['one_room_elevated_price'] }}">
                                @if ($errors->has('one_room_elevated_price'))
                                <span class="help-block"> <strong>{{ $errors->first('one_room_elevated_price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('one_bathroom_price') ? ' has-error' : '' }}">
                                <label for="one_bathroom_price">Стоимость уборки 1 санузла</label>
                                <input type="text" class="form-control" name="one_bathroom_price" id="one_bathroom_price" placeholder="Стоимость уборки 1 санузла" value="{{ $data['calc_main']['one_bathroom_price'] }}">
                                @if ($errors->has('one_bathroom_price'))
                                <span class="help-block"> <strong>{{ $errors->first('one_bathroom_price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('bathroom_time') ? ' has-error' : '' }}">
                                <label for="bathroom_time">Время уборки 1 санузла</label>
                                <input type="text" class="form-control" name="bathroom_time" id="bathroom_time" placeholder="Базовое время" value="{{ $data['calc_main']['bathroom_time'] }}">
                                <i>Формат записи: 2:00 - два часа, 3:30 - 3 часа 30 мин</i>
                                @if ($errors->has('bathroom_time'))
                                <span class="help-block"> <strong>{{ $errors->first('bathroom_time') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">                            
                            <div class="form-group">
                                <label for="calc_additional_cleaner">Наценка за дополнительного клинера</label>
                                <div class="form-group {{ $errors->has('calc_additional_cleaner') ? ' has-error' : '' }}">
                                    <input class="form-control" name="calc_additional_cleaner" type="text" value="{{ $data['calc_additional_cleaner'] }}">
                                    @if ($errors->has('calc_additional_cleaner'))
                                    <span class="help-block"> <strong>{{ $errors->first('calc_additional_cleaner') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="calc_additional_cleaner_border">Порог суммы заказа, после которого будет добавлен доп. клинер</label>
                                <div class="form-group {{ $errors->has('calc_additional_cleaner_border') ? ' has-error' : '' }}">
                                    <input class="form-control" name="calc_additional_cleaner_border" type="text" value="{{ $data['calc_additional_cleaner_border'] }}">
                                    @if ($errors->has('calc_additional_cleaner_border'))
                                    <span class="help-block"> <strong>{{ $errors->first('calc_additional_cleaner_border') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Type --}}
                <div class="tab-pane fade" id="tab-2">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="cleanType" class="form-group">
                                <p><label for="clean_type">Тип уборки</label></p>
                                <table class="table table-striped table-bordered table-hover type-field-zone">
                                <thead>
                                    <tr>
                                        <th class="text-center">Тип</th>
                                        <th class="text-center">Название</th>
                                        <th class="text-center">Значение</th>
                                        <th class="text-center" style="width: 10%;">Удалить</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($data['calc_type']) && count($data['calc_type']))
                                @foreach($data['calc_type'] as $type)
                                    <tr class="added">
                                        <td>
                                            <select name="clean_type_label[]" class="form-control">
                                                <option value="standart" @if($type['label'] === 'standart') selected @endif>Калькулятор обычной уборки</option>
                                                <option value="general" @if($type['label'] === 'general') selected @endif>Калькулятор генеральной уборки</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" name="clean_type_name[]" placeholder="Название типа уборки" value="{{ $type['name'] }}"></td>
                                        <td><input type="text" class="form-control" name="clean_type_k[]" placeholder="Коэфициент" value="{{ $type['ratio'] }}"></td>
                                        <td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                                </table>
                                <p><button type="button" id="addFieldType" class="btn btn-xs btn-success">добавить поле</button></p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- More services --}}
                <div class="tab-pane fade" id="tab-3">
                    <div class="row">
                        <div class="col-md-9">
                            <div id="cleanServices" class="form-group">
                                <p><label for="clean_services">Дополнительные услуги</label></p>
                                <table class="table table-striped table-bordered table-hover type-field-zone">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 30%;">Название</th>
                                        <th class="text-center">Значение</th>
                                        <th class="text-center" style="width: 25%;">Тип оплаты</th>
                                        <th class="text-center" style="width: 25%;">Изображение</th>
                                        <th class="text-center" style="width: 10%;">Время</th>
                                        <th class="text-center" style="width: 10%;">Удалить</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($data['calc_services']) && count($data['calc_services']))
                                @foreach($data['calc_services'] as $service)
                                    <tr class="added">
                                        <td style="width: 30%;"><input type="text" class="form-control" name="clean_services_name[]" placeholder="Название услуги" value="{{ $service['name'] }}"></td>
                                        <td><input type="text" class="form-control" name="clean_services_price[]" placeholder="Цена" value="{{ $service['price'] }}"></td>
                                        <td style="width: 25%;">
                                            <select name="clean_services_pay[]" class="form-control">
                                            @if(isset($services_pay) && count($services_pay))
                                            @foreach($services_pay as $k => $pay)
                                                <option value="{{ $k }}" @if($k == $service['pay']) selected @endif>{{ $pay }}</option>
                                            @endforeach
                                            @endif
                                            </select>
                                        </td>
                                        <td style="width: 25%;">
                                            <select name="clean_services_icon[]" class="form-control">
                                            @if(isset($services_icons) && count($services_icons))
                                            @foreach($services_icons as $k => $pay)
                                                <option value="{{ $k }}" @if($k == $service['icon']) selected @endif>{{ $pay }}</option>
                                            @endforeach
                                            @endif
                                            </select>
                                        </td>
                                        <td style="width: 10%;"><input type="text" class="form-control" name="clean_services_time[]" placeholder="Время" value="{{ $service['time'] }}"></td>
                                        <td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                                </table>
                                <p><button type="button" id="addFieldService" class="btn btn-xs btn-success">добавить поле</button></p>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Cicle cleaning --}}
                <div class="tab-pane fade" id="tab-4">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="cleanCicle" class="form-group">
                                <p><label for="clean_Cicle">Частота уборки</label></p>
                                <table class="table table-striped table-bordered table-hover type-field-zone">
                                <thead>
                                    <tr>
                                        <th class="text-center">Название</th>
                                        <th class="text-center">Значение</th>
                                        <th class="text-center" style="width: 10%;">Удалить</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($data['calc_cicle']) && count($data['calc_cicle']))
                                @foreach($data['calc_cicle'] as $cicle)
                                    <tr class="added">
                                        <td><input type="text" class="form-control" name="clean_cicle_name[]" placeholder="Название" value="{{ $cicle['name'] }}"></td>
                                        <td><input type="text" class="form-control" name="clean_cicle_price[]" placeholder="Коэфициент" value="{{ $cicle['ratio'] }}"></td>
                                        <td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                                </table>
                                <p><button type="button" id="addFieldCicle" class="btn btn-xs btn-success">добавить поле</button></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <p><br><hr></p>

            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

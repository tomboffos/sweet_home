@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Заказ на подбор персонала №{{ $data->id }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form id="orderWorker" action="{{ url ("/manager/orders/workers/$data->id") }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            {{-- Main --}}
            <div class="panel main-order-info panel-green">
                <div class="panel-heading">Информация о заказе</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                                <label for="worker_id">Специальность</label>
                                <select name="worker_id" id="worker_id" class="form-control">
                                    @if(isset($professions) && count($professions))
                                    @foreach($professions as $item)
                                    <option value="{{ $item->id }}" @if((int) $item->id === (int) $data->worker_id) selected @endif>{{ $item->name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <i>Если не отображается какая-нибудь специальность, <a href="{{ url('/manager/workers/cats') }}" title="Открыть список специальностей в новой вкладке" target="_blank">убедитесь что она включена</a>!</i>
                                @if ($errors->has('text'))
                                <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                                <label for="form_type">Тип формы</label>
                                <select name="form_type" id="form_type" class="form-control">
                                    @foreach($form_types as $k => $form_type)
                                    <option value="{{ $k }}" @if((int) $data->type === (int) $k) selected @endif>{{ $form_type }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('form_type'))
                                <span class="help-block"> <strong>{{ $errors->first('form_type') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                                <label for="simple_text">Полное описание</label>
                                <textarea type="text" name="text" id="simple_text" class="form-control" placeholder="Полное описание">{{ $data->text }}</textarea>
                                @if ($errors->has('text'))
                                <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('requirements') ? ' has-error' : '' }}">
                                <label for="requirements">
                                    @if($data->type == 0)
                                    Возраст ребёнка (детей)
                                    @elseif($data->type == 1)
                                    Расскажите о подопечном (возраст, пол, необходимая помощь)
                                    @elseif($data->type == 2)
                                    Предпочитаемая кухня
                                    @elseif($data->type == 3)
                                    Площадь квартиры / дома
                                    @else
                                    Требования к кандидату
                                    @endif
                                </label>
                                <textarea type="text" name="requirements" id="requirements" class="form-control simple_text">{{ $data->requirements }}</textarea>
                                @if ($errors->has('requirements'))
                                <span class="help-block"> <strong>{{ $errors->first('requirements') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('schedule') ? ' has-error' : '' }}">
                                <label for="worker_schedule">График работы</label>
                                <textarea type="text" name="schedule" id="worker_schedule" class="form-control simple_text" placeholder="График работы">{{ $data->schedule }}</textarea>
                                @if ($errors->has('schedule'))
                                <span class="help-block"> <strong>{{ $errors->first('schedule') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        @if(!is_null($data->address) && !empty($data->address))
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="worker_address">Адрес</label>
                                <textarea type="text" name="address" id="worker_address" class="form-control simple_text" placeholder="Адрес">{{ $data->address }}</textarea>
                                @if ($errors->has('address'))
                                <span class="help-block"> <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @endif

                        @if(!is_null($data->wage) && !empty($data->wage))
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('wage') ? ' has-error' : '' }}">
                                <label for="worker_address">Предполагаемая заработная плата</label>
                                <input type="text" name="wage" id="worker_address" class="form-control simple_text" placeholder="Предполагаемая заработная плата" value="{{ $data->wage }}">
                                @if ($errors->has('wage'))
                                <span class="help-block"> <strong>{{ $errors->first('wage') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>

            {{-- Client --}}
            @if(!is_null($data->client_id))
            <div class="panel panel-info">
                <div class="panel-heading">Информация о клиенте</div>
                <div class="panel-body">

                    <h5>Клиент</h5>
                    <ul class="list-group">
                        <li class="list-group-item"><b>Имя:</b> <a href="{{ url("/manager/users/clients/$user->id") }}" title="Смотреть подробную информацию в новой вкладке" target="_blank">{{ $user->name }}</a></li>
                        <li class="list-group-item"><b>Email:</b> {{ $user->email }}</li>
                        <li class="list-group-item"><b>Телефон:</b> {{ $user->phone }}</li>
                    </ul>
                    <hr>

                    <h5>Адрес</h5>
                    <ul class="list-group">
                        <li class="list-group-item"><b>Город: </b>{{ $user->city }}</li>
                        <li class="list-group-item"><b>Улица: </b>{{ $user->street }}</li>
                        <li class="list-group-item"><b>Жилой комплекс: </b>{{ $user->complex }}</li>
                        <li class="list-group-item"><b>Корпус: </b>{{ $user->housing }}</li>
                        <li class="list-group-item"><b>Дом: </b>{{ $user->house }}</li>
                        <li class="list-group-item"><b>Квартира: </b>{{ $user->flat }}</li>
                    </ul>
                    <input type="hidden" name="client_id" value="{{ $data->client_id }}">

                </div>
            </div>
            @endif

            @if(!is_null($data->guest))
            <div class="panel panel-info">
                <div class="panel-heading">Информация о клиенте</div>
                <div class="panel-body">
                    <h5>Клиент -  Гость</h5>
                    <ul class="list-group">
                        <li class="list-group-item"><b>Имя:</b> {{ $user->name }}</li>
                        <li class="list-group-item"><b>Телефон:</b> {{ $user->phone }}</li>
                    </ul>
                </div>
            </div>
            @endif

            {{-- Payment --}}
            <div class="panel panel-info">
                <div class="panel-heading">Информация об оплате</div>
                <div class="panel-body">

                    <h5>Способ оплаты:</h5>
                    <div class="row">
                        <div class="col-md-4">
                            @if(isset($payment_methods) && count($payment_methods))
                            <select name="payment_type" class="payment_type form-control">
                            @foreach($payment_methods as $type => $pay_method)
                                <option value="{{ $type }}" @if($type === $data->payment_type) selected @endif>{{ $pay_method }}</option>
                            @endforeach
                            </select>
                            @endif
                        </div>
                    </div>

                    <hr>

                    <h5>Использованный промокод:</h5>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="discount-order form-control" name="discount_code" value="{{ isset($discount->code) ? $discount->code : '' }}">
                            @if(isset($discount->price))
                                <i>Скидка: <b>{{ $discount->price }}</b> {{ $discount->type === 0 ? '%' : 'тенге' }}</i>
                            @endif
                        </div>
                    </div>

                    <input type="hidden" name="discount_id" value="{{ isset($discount->id) ? $discount->id : '' }}">

                    <hr>

                    <h5>Использованные баллы (ед.):</h5>

                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="points-order form-control" name="points" value="{{ $data->points ? $data->points : 0 }}">
                        </div>
                    </div>

                    <hr>

                    <h5>Итоговая сумма (тенге):</h5>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="total-order form-control" name="total" value="{{ $data->total }}">
                        </div>
                    </div>
                </div>
            </div>

            {{-- Order status --}}
            <div class="panel panel-info">
                <div class="panel-heading">Статус заказа и оплаты</div>
                <div class="panel-body">
                    <h5>Текущий статус заказа:</h5>
                    
                    <div class="row">
                        <div class="col-md-4">
                            @if($data->status === 0)
                                <span class="label label-warning">{{ $order_status[$data->status] }}</span>
                            @elseif($data->status === 1)
                                <span class="label label-info">{{ $order_status[$data->status] }}</span>
                            @elseif($data->status === 2)
                                <span class="label label-success">{{ $order_status[$data->status] }}</span>
                            @elseif($data->status === 3)
                                <span class="label label-danger">{{ $order_status[$data->status] }}</span>
                            @endif
                        </div>
                    </div>

                    <hr>

                    <h5>Статус оплаты:</h5>
                    <div class="row">
                        <div class="col-md-4">
                            @if($data->payment_status === 1)
                                <span class="label label-success">оплачен</span>
                            @else
                                <span class="label label-warning">в ожидании</span>
                            @endif
                        </div>
                    </div>

                    <hr>

                    <div class="form-group{{ $errors->has('order_status') ? ' has-error' : '' }}">
                        <label for="order_status">Статус заказа</label>
                        <select id="order_status" name="order_status" class="form-control">
                        @if(isset($order_status) && count($order_status))
                        @foreach($order_status as $n => $status)
                            <option value="{{ $n }}" @if($n === $data->status) selected @endif>{{ $status }}</option>
                        @endforeach
                        @endif
                        </select>
                        @if ($errors->has('order_status'))
                        <span class="help-block"> <strong>{{ $errors->first('order_status') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('payment_status') ? ' has-error' : '' }}">
                        <label for="payment_status">Статус оплаты</label>
                        <select id="payment_status" name="payment_status" class="form-control">
                        @if(isset($payment_status) && count($payment_status))
                        @foreach($payment_status as $n => $payment_status)
                            <option value="{{ $n }}" @if($n === $data->payment_status) selected @endif>{{ $payment_status }}</option>
                        @endforeach
                        @endif
                        </select>
                        @if ($errors->has('payment_status'))
                        <span class="help-block"> <strong>{{ $errors->first('payment_status') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <a href="{{ url ('/manager/orders/workers') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url ("/manager/orders/workers/delete/$data->id") }}" class="btn btn-danger pull-right delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@endsection

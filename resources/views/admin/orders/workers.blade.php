@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Заказы на подбор персонала</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Список заказов
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            @if(isset($data) && !empty($data) && count($data))
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 5%;">№</th>
                            <th>Заказ</th>
                            <th class="text-center">Способ оплаты</th>
                            <th class="text-center">Статус оплаты</th>
                            <th class="text-center">Статус</th>
                            <th class="text-center">Добавлен</th>
                            <th class="text-center" style="width: 10%;">Удалить</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $k => $item)
                        <tr>
                            <td class="text-center" style="width: 5%;">{{ ++$k }}</td>
                            <td><a href="{{ url("/manager/orders/workers/$item->id") }}">Заказ №{{ $item->id }}</a></td>

                            <td class="text-center" style="width: 15%;">
                            @if(isset($payment_methods) && count($payment_methods))
                            @foreach($payment_methods as $type => $pay_method)
                                @if($type === $item->payment_type) {{ $pay_method }} @endif
                            @endforeach
                            @endif
                            </td>

                            <td class="text-center" style="width: 10%;">
                            @if($item->payment_status === 1)
                                <span class="label label-success">оплачен</span>
                            @else
                                <span class="label label-warning">в ожидании</span>
                            @endif
                            </td>

                            <td class="text-center" style="width: 5%;">
                            @if($item->status === 0)
                                <span class="label label-warning">{{ $order_status[$item->status] }}</span>
                            @elseif($item->status === 1)
                                <span class="label label-info">{{ $order_status[$item->status] }}</span>
                            @elseif($item->status === 2)
                                <span class="label label-success">{{ $order_status[$item->status] }}</span>
                            @elseif($item->status === 3)
                                <span class="label label-danger">{{ $order_status[$item->status] }}</span>
                            @endif
                            </td>

                            <td class="text-center" style="width: 12%;">{!! $item->created_at->format('d F Y') !!} г.</td>

                            <td class="text-center" style="width: 10%;"><a href="{{ url("/manager/orders/workers/delete/$item->id") }}" class="delete">Удалить</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
            <p>Записей нет.</p>
            @endif

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

        {!! $data->render() !!}

    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Заказ на уборку №{{ $data->id }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form id="orderCleaner" action="{{ url ("/manager/orders/cleaners/$data->id") }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="order_id" value="{{ $data->id }}">
            <input type="hidden" name="order_label" value="{{ $data->label }}">
            <input type="hidden" name="elapsed" value="{{ date('H:i:s', strtotime($data->elapsed)) }}">

            {{-- Main --}}
            <div class="panel main-order-info panel-green">
                <div class="panel-heading">Информация о заказе</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 ajax-changeble">
                            <p><label>Основная информация</label></p>
                            <table class="table table-bordered type-field-zone">
                            <thead>
                                <tr>
                                    <th>{{ trans('messages.cleaning_type') }}</th>
                                    <th>Количество комнат</th>
                                    <th>Количество санузлов</th>
                                    @if(!empty($data->label === 'general')) <th>Количество окон</th> @endif
                                    <th>Частота</th>
                                    <th>Дата</th>
                                    <th>Время</th>
                                    <th>Затрачиваемое время</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width: 15%;">
                                        <select name="clean_type" class="form-control input-sm" disabled>
                                        @if(isset($calculator['type']) && count($calculator['type']))
                                        @foreach($calculator['type'] as $k => $type)
                                        <option value="{{ $type['label'] }}" @if(isset($data->type) && isset($data->type->label) && $type['label'] === $data->type->label) selected @endif>{{ $type['name'] }}</option>
                                        @endforeach
                                        @endif
                                        </select>
                                    </td>
                                    <td style="width: 15%;"><input type="text" class="form-control input-sm" name="clean_number_rooms" required placeholder="Количество комнат" value="{{ $data->number_rooms }}"></td>
                                    <td style="width: 15%;"><input type="text" class="form-control input-sm" name="clean_number_bathrooms" required placeholder="Количество санузлов" value="{{ $data->number_bathrooms }}"></td>
                                    @if(!empty($data->label === 'general'))
                                    <td style="width: 15%;"><input type="text" class="form-control input-sm" name="clean_number_window" required placeholder="Количество окон" value="{{ $data->number_window }}"></td>
                                    @endif
                                    <td style="width: 15%;">
                                        <select name="clean_cicle" class="form-control input-sm">
                                        @if(isset($calculator['cicle']) && count($calculator['cicle']))
                                        @foreach($calculator['cicle'] as $k => $cicle)
                                        <option value="{{ $cicle['name'] }}" @if(isset($data->cicle) && $cicle['name'] === $data->cicle->name) selected @endif>{{ $cicle['name'] }}</option>
                                        @endforeach
                                        @endif
                                        </select>
                                    </td>
                                    <td style="width: 12%;"><input type="text" class="form-control input-sm" name="clean_date" placeholder="Дата" value="{{ $data->cl_date->format('d-m-Y') }}"></td>
                                    <td style="width: 8%;"><input type="text" class="form-control input-sm" name="clean_time" placeholder="Время" value="{{ $data->cl_time->format('H:i') }}"></td>
                                    <td style="width: 5%;"><input type="text" class="form-control input-sm" name="elapsed" placeholder="Затрачиваемое время" value="{{ $data->elapsed->format('H:i') }}"></td>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="cleanServicesOrder" class="form-group ajax-changeble">
                                <p><label>{{ trans('messages.additional_services') }}</label></p>
                                <table class="table table-striped table-bordered table-hover type-field-zone">
                                <thead>
                                    <tr>
                                        <th class="text-center">Название</th>
                                        <th class="text-center">Значение</th>
                                        <th class="text-center">Кол-во</th>
                                        <th class="text-center">Время</th>
                                        <th class="text-center">Тип оплаты</th>
                                        <th class="text-center" style="width: 10%;">Удалить</th>
                                    </tr>
                                </thead>
                                <tbody>
                                 @if(isset($data->services) && count($data->services))
                                    @foreach($data->services as $k => $service)

                                    <tr class="added">
                                        <td><input type="text" class="form-control" name="clean_services_name[]" placeholder="Название услуги" value="{{ $service->name }}"></td>
                                        <td style="width: 12%;"><input type="text" class="form-control" name="clean_services_price[]" placeholder="Цена" value="{{ $service->price }}"></td>
                                        <td style="width: 10%;"><input type="text" class="form-control" name="clean_services_parts[]" placeholder="Количество" value="{{ $service->unit }}"></td>
                                        <td style="width: 10%;"><input title="0.5 - 30 минут, 1.5 - 1 час и 30 минут ..." type="text" class="form-control" name="clean_services_time[]"
                                        placeholder="Время" value="{{ $service->time }}"></td>
                                        <td style="width: 12%;">
                                            <select name="clean_services_pay[]" class="form-control">
                                            @if(isset($services_pay) && count($services_pay))
                                            @foreach($services_pay as $k => $pay)
                                                <option value="{{ $k }}" @if($service->pay === $k) selected @endif>{{ $pay }}</option>
                                            @endforeach
                                            @endif
                                            </select>
                                        </td>
                                        <td class="text-center"><button type="button" class="btn btn-xs btn-danger delete-clean-row">убрать</button></td>
                                    </tr>

                                    @endforeach
                                @endif
                                </tbody>
                                </table>
                                <p><button type="button" id="addFieldServiceOrder" class="btn btn-xs btn-success">добавить поле</button></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Client --}}
            <div class="panel panel-info">
                <div class="panel-heading">Информация о клиенте</div>
                <div class="panel-body">
                    <h5>Клиент</h4>
                    <ul class="list-group">
                        <li class="list-group-item"><b>Имя:</b> <a href="{{ url("/manager/users/clients/$user->id") }}" title="Смотреть подробную информацию в новой вкладке" target="_blank">{{ $user->name }}</a></li>
                        <li class="list-group-item"><b>Email:</b> {{ $user->email }}</li>
                        <li class="list-group-item"><b>Телефон:</b> {{ $user->phone }}</li>
                    </ul>
                    <hr>

                    <h5>Адрес</h5>
                    <ul class="list-group">
                        <li class="list-group-item"><b>Город: </b>{{ $user->city }}</li>
                        <li class="list-group-item"><b>Улица: </b>{{ $user->street }}</li>
                        <li class="list-group-item"><b>Жилой комплекс: </b>{{ $user->complex }}</li>
                        <li class="list-group-item"><b>Корпус: </b>{{ $user->housing }}</li>
                        <li class="list-group-item"><b>Дом: </b>{{ $user->house }}</li>
                        <li class="list-group-item"><b>Квартира: </b>{{ $user->flat }}</li>
                    </ul>
                    <input type="hidden" name="client_id" value="{{ $data->client_id }}">
                    <hr>

                    <p><label>Комментарий к заказу:</label></p>
                    <textarea name="comment" id="comment" class="form-control" rows="4">{{ $data->comment }}</textarea>
                </div>
            </div>

            {{-- Payment --}}
            <div class="panel panel-info">
                <div class="panel-heading">Информация об оплате</div>
                <div class="panel-body">

                    <h5>Способ оплаты:</h5>

                    <div class="row">
                        <div class="col-md-4">
                            @if(isset($payment_methods) && count($payment_methods))
                            <select name="payment_type" class="payment_type form-control">
                            @foreach($payment_methods as $type => $pay_method)
                                <option value="{{ $type }}" @if($type === $data->payment_type) selected @endif>{{ $pay_method }}</option>
                            @endforeach
                            </select>
                            @endif
                        </div>
                    </div>

                    <hr>

                    <h5>Использованный промокод:</h5>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="discount-order form-control" name="discount_code" value="{{ isset($discount->code) ? $discount->code : '' }}">
                        </div>
                    </div>

                    @if(isset($discount->price))
                        <i>Cкидка: <b>{{ $discount->type === 0 ? ($discount->price*100).' %' : $discount->price.' тенге' }}</b></i>
                    @endif

                    <input type="hidden" name="discount_id" value="{{ isset($discount->id) ? $discount->id : '' }}">

                    <hr>

                    <h5>Использованные баллы (ед.):</h5>

                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="points-order form-control" name="points" value="{{ $data->points ? $data->points : 0 }}">
                        </div>
                    </div>

                    <hr>

                    <h5>Итоговая сумма (тенге):</h5>

                    <div class="row">
                        <div class="col-md-4">
                           <input type="text" class="total-order form-control" name="total" value="{{$data->total}}">
                        </div>
                    </div>

                    @if($data->payment_type > 0 && $data->payment_status != 1)
                        @if(isset($clientCard) && !empty($clientCard->card_id) && $clientCard->approve === 0 && !is_null($clientCard->card_id) && !is_null($clientCard->card_mask))
                            <a class="btn btn-success btn-sm" href="{{ url('manager/orders/cleaners/kazkom/pay') }}/{{ $data->id }}">Списать сумму {{ $data->total }} тенге</a>
                        @elseif(isset($clientCard) && $clientCard->approve === 1)
                            <p class="alert alert-danger">Клиент привязал карту к аккаунту, но не активировал ее.</p>
                        @else
                            <p class="alert alert-danger">Клиент не привязал карту к аккаунту.</p>
                        @endif
                    @endif
                </div>
            </div>

            @if(!empty($data->additional))
            {{-- Additional Cleaners --}}
            <div class="panel panel-info">
                <div class="panel-heading">Дополнительные клинеры</div>
                <div class="panel-body">
                    @php $data->additional = json_decode($data->additional, true); @endphp

                    <div class="list-group">

                    @if(isset($data->additional['free_cleaner']))
                    <p class="list-group-item">Автоматически добавлен дополнительный клинер (сумма заказа выше {{ $calculator['additional_cleaner_border'] }} тенге).</p>
                    @endif

                    @if(isset($data->additional['paid_cleaner']))
                    <p class="list-group-item">Клиент добавил дополнительного клинера. Наценка составила: {{ number_format($data->additional['paid_cleaner']['price'], 0, '', ' ') }} тг.</p>
                    @endif

                    </div>
                </div>
            </div>
            @endif

            {{-- Cleaners --}}
            <div class="panel panel-info">
                <div class="panel-heading">Выберите клинера для этого заказа</div>
                <div class="panel-body">
                @if(isset($cleaners) && count($cleaners))
                <div class="orders-list-cleaner">
                    <div class="list-group">
                    @foreach($cleaners as $cleaner)
                        <span id="cleaner-id-{{ $cleaner['cleaner_id'] }}" class="list-group-item cleaner-item">
                            @if(isset($cleaner['bad']))
                                <i class="black-cleaner" title="Клинер в черном списке у клиента"></i>
                            @endif
                            @if(isset($cleaner['favorite']))
                                <i class="favorite-cleaner" title="Клинер в избранном у клиента"></i>
                            @endif

                            <a href="{{ url("/manager/cleaners/$cleaner[cleaner_id]") }}" target="_blank" title="Посмотреть подробную информацию о клинере в новой вкладе">{{ $cleaner['name'] }}</a>

                            <span class="pull-right text-muted choose-cleaner" data-cleaner="{{ $cleaner['cleaner_id'] }}">Выбрать</span>
                        </span>
                    @endforeach
                    </div>
                </div>
                <input type="hidden" name="cleaner" value="{{ $data->cleaner_id ? $data->cleaner_id : '' }}">
                @else
                <p>Нет доступных клинеров для данного заказа (возможно не совпадает время или все клинеры заняты).</p>
                @endif
                </div>
            </div>

            {{-- Order status --}}
            <div class="panel panel-info">
                <div class="panel-heading">Статус заказа и оплаты</div>
                <div class="panel-body">
                    <h5>Текущий статус заказа:</h5>
                    <div class="row">
                        <div class="col-md-4">
                            @if($data->status === 0)
                                <span class="label label-warning">{{ $order_status[$data->status] }}</span>
                            @elseif($data->status === 1)
                                <span class="label label-info">{{ $order_status[$data->status] }}</span>
                            @elseif($data->status === 2)
                                <span class="label label-success">{{ $order_status[$data->status] }}</span>
                            @elseif($data->status === 3)
                                <span class="label label-danger">{{ $order_status[$data->status] }}</span>
                            @endif
                        </div>
                    </div>

                    <hr>

                    <h5>Статус оплаты:</h5>

                    <div class="row">
                        <div class="col-md-4">
                            <p>@if($data->payment_status === 1)
                                <span class="label label-success">оплачен</span>
                            @else
                                <span class="label label-warning">в ожидании</span>
                            @endif
                            </p>
                        </div>
                    </div>

                    <hr>
                    
                    <div class="form-group{{ $errors->has('order_status') ? ' has-error' : '' }}">
                        <label for="order_status">Статус заказа</label>
                        <select id="order_status" name="order_status" class="form-control">
                        @if(isset($order_status) && count($order_status))
                        @foreach($order_status as $n => $status)
                            <option value="{{ $n }}" @if($n === $data->status) selected @endif>{{ $status }}</option>
                        @endforeach
                        @endif
                        </select>
                        @if ($errors->has('order_status'))
                        <span class="help-block"> <strong>{{ $errors->first('order_status') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('payment_status') ? ' has-error' : '' }}">
                        <label for="payment_status">Статус оплаты</label>
                        <select id="payment_status" name="payment_status" class="form-control">
                        @if(isset($payment_status) && count($payment_status))
                        @foreach($payment_status as $n => $payment_status)
                            <option value="{{ $n }}" @if($n === $data->payment_status) selected @endif>{{ $payment_status }}</option>
                        @endforeach
                        @endif
                        </select>
                        @if ($errors->has('payment_status'))
                        <span class="help-block"> <strong>{{ $errors->first('payment_status') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="send_order_complete"> Отправить письмо клиенту о завершении заказа?
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <span id="copy-order" class="btn btn-primary">Создать заказ на основе данного</span>
            <a href="{{ url ('/manager/orders/cleaners') }}" class="btn btn-default">Вернуться</a>

            <a href="{{ url ("/manager/orders/cleaners/delete/$data->id") }}" class="btn btn-danger pull-right delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@endsection

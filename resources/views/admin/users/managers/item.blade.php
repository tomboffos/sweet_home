@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование администратора</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/users/managers/$data->id") }}" class="post-edit" method="post">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Имя</label>
                <input type="text" class="form-control" name="name" required id="name" placeholder="Имя" value="{{ $data->name }}">
                @if ($errors->has('name'))
                <span class="help-block"> <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group disabled">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" value="{{ $data->email }}" name="email">
            </div>

            <div class="form-group disabled">
                <label for="created_at">Зарегистрирован</label>
                <input type="text" class="form-control" id="created_at" placeholder="Зарегистрирован" value="{{ $data->created_at }}" disabled>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password">Пароль (если укажите, старый будет заменен на новый)</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Пароль" value="">
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm">Повторите пароль</label>
                <input type="password" class="form-control" name="password_confirmation" id="password-confirm" placeholder="Пароль">
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <a href="{{ url ('/manager/users/managers') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url ("/manager/users/managers/delete/$data->id") }}" class="btn btn-danger pull-right delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
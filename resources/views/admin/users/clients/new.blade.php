@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Добавить клиента</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ('/manager/users/clients/new') }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Контакты и адрес</a></li>
            </ul>

            <div class="tab-content">
                
                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" name="name" required id="name" placeholder="Имя" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                        <span class="help-block"> <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group disabled">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" required name="email" placeholder="Email" value="{{ old('email') }}">
                    </div>
                     <div class="form-group disabled">
                        <label for="points">Баллы</label>
                        <input type="text" class="form-control" id="points" name="points" placeholder="Баллы" value="{{ old('points') }}">
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Пароль</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Пароль">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm">Повторите пароль</label>
                        <input type="password" class="form-control" name="password_confirmation" id="password-confirm" placeholder="Пароль">
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                     <div class="checkbox{{ $errors->has('subscribe') ? ' has-error' : '' }}">
                        <label for="subscribe">
                        <input type="checkbox" name="subscribe" id="subscribe" checked> Подписка на рассылку

                        </label>
                        @if ($errors->has('subscribe'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subscribe') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{-- Address --}}
                <div class="tab-pane fade" id="tab-2">
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone">Телефон</label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Телефон" value="{{ old('phone') }}">
                        @if ($errors->has('phone'))
                        <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <label for="city">Город</label>
                        <input type="text" class="form-control" name="city" id="city" placeholder="Город" value="Алматы">
                        @if ($errors->has('city'))
                        <span class="help-block"> <strong>{{ $errors->first('city') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                        <label for="street">Улица</label>
                        <input type="text" class="form-control" name="street" id="street" placeholder="Улица">
                        @if ($errors->has('street'))
                        <span class="help-block"> <strong>{{ $errors->first('street') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('complex') ? ' has-error' : '' }}">
                        <label for="complex">Жилой комплекс</label>
                        <input type="text" class="form-control" name="complex" id="complex" placeholder="Жилой комплекс">
                        @if ($errors->has('complex'))
                        <span class="help-block"> <strong>{{ $errors->first('complex') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('housing') ? ' has-error' : '' }}">
                        <label for="housing">Корпус</label>
                        <input type="text" class="form-control" name="housing" id="housing" placeholder="Корпус">
                        @if ($errors->has('housing'))
                        <span class="help-block"> <strong>{{ $errors->first('housing') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('house') ? ' has-error' : '' }}">
                        <label for="house">Дом</label>
                        <input type="text" class="form-control" name="house" id="house" placeholder="Дом">
                        @if ($errors->has('house'))
                        <span class="help-block"> <strong>{{ $errors->first('house') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('flat') ? ' has-error' : '' }}">
                        <label for="flat">Квартира</label>
                        <input type="text" class="form-control" name="flat" id="flat" placeholder="Квартира">
                        @if ($errors->has('flat'))
                        <span class="help-block"> <strong>{{ $errors->first('flat') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('rooms_number') ? ' has-error' : '' }}">
                        <label for="rooms_number">Количество комнат</label>
                        <input type="text" class="form-control" name="rooms_number" id="rooms_number" placeholder="Количество комнат">
                        @if ($errors->has('rooms_number'))
                        <span class="help-block"> <strong>{{ $errors->first('rooms_number') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('bath_rooms_number') ? ' has-error' : '' }}">
                        <label for="bath_rooms_number">Количество санузлов</label>
                        <input type="text" class="form-control" name="bath_rooms_number" id="bath_rooms_number" placeholder="Количество санузлов">
                        @if ($errors->has('bath_rooms_number'))
                        <span class="help-block"> <strong>{{ $errors->first('bath_rooms_number') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <a href="{{ url('/manager/users/clients') }}" class="btn btn-default">Отмена</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
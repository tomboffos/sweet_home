@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Клиенты сайта</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Список клиентов
                <div class="pull-right">
                    <div class="btn-group">
                        <a href="{{ url('/manager/users/clients/export/subscribes') }}" target="_blank" class="btn btn-success btn-xs">
                            Экспорт подписчиков
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="{{ url('/manager/users/clients/export/all') }}" target="_blank" class="btn btn-success btn-xs">
                            Экспорт всех
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="{{ url('/manager/users/clients/new') }}" class="btn btn-success btn-xs">
                            Добавить клиента
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            @if(isset($data) && !empty($data) && count($data))
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 5%;">№</th>
                            <th>Email</th>
                            <th class="text-center">Добавлено</th>
                            <th class="text-center" style="width: 10%;">Удалить</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $k => $item)
                        <tr>
                            <td class="text-center" style="width: 5%;">{{ ++$k }}</td>
                            <td><a href="{{ url("/manager/users/clients/$item->id") }}">{{ $item->email }}</a></td>
                            <td class="text-center" style="width: 12%;">{!! $item->created_at->format('d F Y') !!} г.</td>
                            <td class="text-center" style="width: 10%;"><a href="{{ url("/manager/users/clients/delete/$item->id") }}" class="delete">Удалить</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
            <p>Записей нет.</p>
            @endif

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

        {!! $data->render() !!}

    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
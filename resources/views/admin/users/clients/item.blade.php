@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование клиента</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/users/clients/$data->id") }}" class="post-edit" method="post">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Платежные данные</a></li>
                <li><a href="#tab-3" data-toggle="tab">Контакты и адрес</a></li>
                <li><a href="#tab-4" data-toggle="tab">Заказы на уборку</a></li>
                <li><a href="#tab-5" data-toggle="tab">Заказы на подбор персонала</a></li>
                <li><a href="#tab-6" data-toggle="tab">Отзывы</a></li>
                <li><a href="#tab-7" data-toggle="tab">Избранное и черный список</a></li>
            </ul>

            <div class="tab-content">
                
                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" name="name" required id="name" placeholder="Имя" value="{{ $data->name }}">
                        @if ($errors->has('name'))
                        <span class="help-block"> <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group disabled">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" required value="{{ $data->email }}" name="email" placeholder="Email">
                    </div>
                     <div class="form-group disabled">
                        <label for="points">Баллы</label>
                        <input type="text" class="form-control" id="points" value="{{ $data->points }}" name="points" placeholder="Баллы">
                    </div>
                    <div class="form-group disabled">
                        <label for="created_at">Зарегистрирован</label>
                        <input type="text" class="form-control" id="created_at" placeholder="Зарегистрирован" value="{{ $data->created_at }}" disabled>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Пароль (если укажите, старый будет заменен на новый)</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Пароль" value="">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm">Повторите пароль</label>
                        <input type="password" class="form-control" name="password_confirmation" id="password-confirm" placeholder="Пароль">
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                     <div class="checkbox{{ $errors->has('subscribe') ? ' has-error' : '' }}">
                        <label for="subscribe">
                        <input type="checkbox" name="subscribe" id="subscribe" @if($data->subscribe === 1) checked @endif> Подписка на рассылку

                        </label>
                        @if ($errors->has('subscribe'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subscribe') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{-- Payment --}}
                <div class="tab-pane fade" id="tab-2">
                    @if(isset($clientCard) && !empty($clientCard->card_id) && $clientCard->approve === 0 && !is_null($clientCard->card_id) && !is_null($clientCard->card_mask))
                        <p class="alert alert-success">Клиент привязал и активировал карту. {{ $clientCard->card_mask }}</p>
                    @elseif(isset($clientCard) && $clientCard->approve === 1)
                        <p class="alert alert-info">Клиент привязал карту к аккаунту, но еще не активировал ее.</p>
                    @else
                        <p class="alert alert-danger">Клиент не привязал карту к аккаунту.</p>
                    @endif
                </div>

                {{-- Address --}}
                <div class="tab-pane fade" id="tab-3">
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone">Телефон</label>
                        <input type="text" class="form-control" name="phone" id="phone" value="{{ $data->phone }}" placeholder="Телефон">
                        @if ($errors->has('phone'))
                        <span class="help-block"> <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <label for="city">Город</label>
                        <input type="text" class="form-control" name="city" id="city" value="{{ $data->city }}" placeholder="Город">
                        @if ($errors->has('city'))
                        <span class="help-block"> <strong>{{ $errors->first('city') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                        <label for="street">Улица</label>
                        <input type="text" class="form-control" name="street" id="street" value="{{ $data->street }}" placeholder="Улица">
                        @if ($errors->has('street'))
                        <span class="help-block"> <strong>{{ $errors->first('street') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('complex') ? ' has-error' : '' }}">
                        <label for="complex">Жилой комплекс</label>
                        <input type="text" class="form-control" name="complex" id="complex" value="{{ $data->complex }}" placeholder="Жилой комплекс">
                        @if ($errors->has('complex'))
                        <span class="help-block"> <strong>{{ $errors->first('complex') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('housing') ? ' has-error' : '' }}">
                        <label for="housing">Корпус</label>
                        <input type="text" class="form-control" name="housing" id="housing" value="{{ $data->housing }}" placeholder="Корпус">
                        @if ($errors->has('housing'))
                        <span class="help-block"> <strong>{{ $errors->first('housing') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('house') ? ' has-error' : '' }}">
                        <label for="house">Дом</label>
                        <input type="text" class="form-control" name="house" id="house" value="{{ $data->house }}" placeholder="Дом">
                        @if ($errors->has('house'))
                        <span class="help-block"> <strong>{{ $errors->first('house') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('flat') ? ' has-error' : '' }}">
                        <label for="flat">Квартира</label>
                        <input type="text" class="form-control" name="flat" id="flat" value="{{ $data->flat }}" placeholder="Квартира">
                        @if ($errors->has('flat'))
                        <span class="help-block"> <strong>{{ $errors->first('flat') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('rooms_number') ? ' has-error' : '' }}">
                        <label for="rooms_number">Количество комнат</label>
                        <input type="text" class="form-control" name="rooms_number" id="rooms_number" value="{{ $data->rooms_number }}" placeholder="Количество комнат">
                        @if ($errors->has('rooms_number'))
                        <span class="help-block"> <strong>{{ $errors->first('rooms_number') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('bath_rooms_number') ? ' has-error' : '' }}">
                        <label for="bath_rooms_number">Количество санузлов</label>
                        <input type="text" class="form-control" name="bath_rooms_number" id="bath_rooms_number" value="{{ $data->bath_rooms_number }}" placeholder="Количество санузлов">
                        @if ($errors->has('bath_rooms_number'))
                        <span class="help-block"> <strong>{{ $errors->first('bath_rooms_number') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                {{-- Orders Cleaners --}}
                <div class="tab-pane fade" id="tab-4">
                    <div class="form-group">
                    <label>Список заказов на уборку:</label>
                        @if(isset($ordersCleaners) && count($ordersCleaners))
                        <div class="orders-list-cleaner">
                            <div class="list-group">
                            @foreach($ordersCleaners as $orderCleaner)
                                <a href="{{ url("/manager/orders/cleaners/$orderCleaner->id") }}" title="Открыть заказ" class="list-group-item">
                                    Заказ №{{ $orderCleaner->id }}
                                    <span class="pull-right text-muted small"><em>Дата: {!! $orderCleaner->created_at->format('d F Y') !!} | Время: {!! $orderCleaner->created_at->format('H:i') !!}</em>
                                    </span>
                                </a>
                            @endforeach
                            </div>
                        </div>
                        @else
                        <p>Заказов на уборку пока не было.</p>
                        @endif
                    </div>
                </div>

                {{-- Orders Workers --}}
                <div class="tab-pane fade" id="tab-5">
                    <div class="form-group">
                    <label>Список заказов на подбор персонала:</label>
                        @if(isset($ordersWorkers) && count($ordersWorkers))
                        <div class="orders-list-cleaner">
                            <div class="list-group">
                            @foreach($ordersWorkers as $orderWorker)
                                <a href="{{ url("/manager/orders/workers/$orderWorker->id") }}" title="Открыть заказ" class="list-group-item">
                                    Заказ №{{ $orderWorker->id }}
                                    <span class="pull-right text-muted small"><em>Дата: {!! $orderWorker->created_at->format('d F Y') !!} | Время: {!! $orderWorker->created_at->format('H:i') !!}</em>
                                    </span>
                                </a>
                            @endforeach
                            </div>
                        </div>
                        @else
                        <p>Заказов на подбор персонала пока не было.</p>
                        @endif
                    </div>
                </div>

                {{-- Testimonals --}}
                <div class="tab-pane fade" id="tab-6">
                    <div class="form-group">
                        @if(isset($testimonals) && count($testimonals))
                        <label for="">Список отзывов клиента:</label>
                        <div class="orders-list-cleaner">
                            <div class="list-group">
                            @foreach($testimonals as $testimonal)
                                <a href="{{ url("/manager/testimonals/$testimonal->id") }}" title="Открыть заказ" class="list-group-item">
                                    Отзыв к заказу №{{ $testimonal->order_id }}
                                    <span class="pull-right text-muted small"><em>Дата: {!! $testimonal->created_at->format('d F Y') !!} | Время: {!! $testimonal->created_at->format('H:i') !!}</em>
                                    </span>
                                </a>
                            @endforeach
                            </div>
                        </div>
                        @else
                        <p>Отзывов пока не было.</p>
                        @endif
                    </div>
                </div>

                {{-- White && Black lists --}}
                <div class="tab-pane fade" id="tab-7">
                    <div class="form-group">
                        @if(isset($bad_cleaners) && count($bad_cleaners))
                        <label for="">У данного клиента в черном списке следующие клинеры:</label>
                        <div class="orders-list-cleaner">
                            <div class="list-group">
                            @foreach($bad_cleaners as $cleaner)
                                <a href="{{ url("/manager/cleaners/$cleaner->id") }}" title="Смотреть информацию о клинере" class="list-group-item">
                                    {{ $cleaner->name }}
                                </a>
                            @endforeach
                            </div>
                        </div>
                        @else
                        <p>Черный список пуст.</p>
                        @endif
                    </div>

                     <div class="form-group">
                        @if(isset($favorite_cleaners) && count($favorite_cleaners))
                        <label for="">У данного клиента в избранном следующие клинеры:</label>
                        <div class="orders-list-cleaner">
                            <div class="list-group">
                            @foreach($favorite_cleaners as $cleaner)
                                <a href="{{ url("/manager/cleaners/$cleaner->id") }}" title="Смотреть информацию о клинере" class="list-group-item">
                                    {{ $cleaner->name }}
                                </a>
                            @endforeach
                            </div>
                        </div>
                        @else
                        <p>В избранном пусто.</p>
                        @endif
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <a href="{{ url ('/manager/users/clients') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url ("/manager/users/clients/delete/$data->id") }}" class="btn btn-danger pull-right delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
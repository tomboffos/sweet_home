@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Добавить промокод</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ('/manager/discounts/new') }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                <label for="code">Код</label>
                <input type="text" class="form-control" name="code" required id="code" placeholder="Код" value="{{ old('code') }}">
                @if ($errors->has('code'))
                <span class="help-block"> <strong>{{ $errors->first('code') }}</strong>
                </span>
                @endif
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                        <label for="category">Категория</label>
                        <select name="category" id="category" class="form-control">
                        @foreach($categories as $k => $category)
                            <option value="{{ $k }}">{{ $category }}</option>
                        @endforeach
                        </select>
                        @if ($errors->has('category'))
                        <span class="help-block"> <strong>{{ $errors->first('category') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type">Тип</label>
                        <select name="type" id="type" class="form-control">
                        @foreach($types as $k => $type)
                            <option value="{{ $k }}">{{ $type }}</option>
                        @endforeach
                        </select>
                        @if ($errors->has('type'))
                        <span class="help-block"> <strong>{{ $errors->first('type') }}</strong>
                        </span>
                        @endif
                    </div>                    
                </div>
                <div class="col-md-2">
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        <label for="price">Значение</label>
                        <input type="text" class="form-control" name="price" required id="price" placeholder="Значение" value="{{ old('price') }}">
                        @if ($errors->has('price'))
                        <span class="help-block"> <strong>{{ $errors->first('price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group{{ $errors->has('finish') ? ' has-error' : '' }}">
                        <div class="input-group"><label for="from">Дата окончания</label></div>
                        <div class="input-group input-append discount-date">
                            <input class="form-control add-on" name="finish" type="text" value="">
                            <span class="input-group-addon add-on">
                            <i class="fa fa-clock-o"></i>
                            </span>
                        </div>
                        @if ($errors->has('finish'))
                        <span class="help-block"> <strong>{{ $errors->first('finish') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>                
                <div class="col-md-2">
                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="used">Кол-во применений</label>
                        <select name="used" id="used" class="form-control">
                        <option value="0">Сколько угодно раз</option>
                        <option value="1">Один раз</option>
                        </select>
                        @if ($errors->has('used'))
                        <span class="help-block"> <strong>{{ $errors->first('used') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">

            <a href="{{ url('/manager/discounts') }}" class="btn btn-default">Отмена</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование промокода</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/discounts/$data->id") }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Заказы на уборку</a></li>
                <li><a href="#tab-3" data-toggle="tab">Заказы на подбор персонала</a></li>
            </ul>

            <div class="tab-content">
                
                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                        <label for="code">Код</label>
                        <input type="text" class="form-control" name="code" required id="code" placeholder="Код" value="{{ $data->code }}">
                        @if ($errors->has('code'))
                        <span class="help-block"> <strong>{{ $errors->first('code') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                <label for="category">Категория</label>
                                <select name="category" id="category" class="form-control">
                                @foreach($categories as $k => $category)
                                    <option value="{{ $k }}" @if($k === $data->category) selected @endif>{{ $category }}</option>
                                @endforeach
                                </select>
                                @if ($errors->has('category'))
                                <span class="help-block"> <strong>{{ $errors->first('category') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label for="type">Тип</label>
                                <select name="type" id="type" class="form-control">
                                @foreach($types as $k => $type)
                                    <option value="{{ $k }}" @if($k === $data->type) selected @endif>{{ $type }}</option>
                                @endforeach
                                </select>
                                @if ($errors->has('type'))
                                <span class="help-block"> <strong>{{ $errors->first('type') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="price">Значение</label>
                                <input type="text" class="form-control" name="price" required id="price" placeholder="Значение" value="{{ $data->price }}">
                                @if ($errors->has('price'))
                                <span class="help-block"> <strong>{{ $errors->first('price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('finish') ? ' has-error' : '' }}">
                                <div class="input-group"><label for="from">Дата окончания</label></div>
                                <div class="input-group input-append discount-date">
                                    <input class="form-control add-on" name="finish" type="text" value="{{ isset($data->finish) && !empty($data->finish) ? $data->finish : '' }}">
                                    <span class="input-group-addon add-on">
                                    <i class="fa fa-clock-o"></i>
                                    </span>
                                </div>
                                @if ($errors->has('finish'))
                                <span class="help-block"> <strong>{{ $errors->first('finish') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label for="used">Кол-во применений</label>
                                <select name="used" id="used" class="form-control">
                                <option value="0" @if((int) $data->used === 0) selected @endif>Сколько угодно раз</option>
                                <option value="1" @if((int) $data->used === 1) selected @endif>Один раз</option>
                                </select>
                                @if ($errors->has('used'))
                                <span class="help-block"> <strong>{{ $errors->first('used') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group disabled">
                                <label for="created_at">Добавлен</label>
                                <input type="text" class="form-control" id="created_at" placeholder="Зарегистрирован" value="{{ $data->created_at }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Orders Cleaners --}}
                <div class="tab-pane fade" id="tab-2">
                    <div class="form-group">
                        @if(isset($ordersCleaners) && count($ordersCleaners))
                        <label for="">Список заказов, в которых был использован данный код:</label>
                        <div class="orders-list-cleaner">
                            <div class="list-group">
                            @foreach($ordersCleaners as $orderCleaner)
                                <a href="{{ url("/manager/orders/cleaners/$orderCleaner->id") }}" title="Открыть заказ" class="list-group-item">
                                    Заказ №{{ $orderCleaner->id }}
                                    <span class="pull-right text-muted small"><em>Дата: {!! $orderCleaner->created_at->format('d F Y') !!} | Время: {!! $orderCleaner->created_at->format('H:i') !!}</em>
                                    </span>
                                </a>
                            @endforeach
                            </div>
                        </div>
                        @else
                        <p>Промокод пока не был использован.</p>
                        @endif
                    </div>
                </div>

                {{-- Orders Workers --}}
                <div class="tab-pane fade" id="tab-3">
                    <div class="form-group">
                        @if(isset($ordersWorkers) && count($ordersWorkers))
                        <label for="">Список заказов, в которых был использован данный код:</label>
                        <div class="orders-list-cleaner">
                            <div class="list-group">
                            @foreach($ordersWorkers as $orderWorker)
                                <a href="{{ url("/manager/orders/workers/$orderWorker->id") }}" title="Открыть заказ" class="list-group-item">
                                    Заказ №{{ $orderWorker->id }}
                                    <span class="pull-right text-muted small"><em>Дата: {!! $orderWorker->created_at->format('d F Y') !!} | Время: {!! $orderWorker->created_at->format('H:i') !!}</em>
                                    </span>
                                </a>
                            @endforeach
                            </div>
                        </div>
                        @else
                        <p>Промокод пока не был использован.</p>
                        @endif
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            @if($data->status === 1)
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">
            @else
            <button type="button" id="status" class="btn btn-default">Выключено</button>
            <input type="hidden" name="status" value="0">
            @endif

            <a href="{{ url ('/manager/discounts') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url ("/manager/discounts/delete/$data->id") }}" class="btn btn-danger pull-right delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
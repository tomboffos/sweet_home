@extends('admin.layout')



@section('content')

<div class="row ">

    <div class="col-lg-8">
        @if(!$claim)
        <h1 class="page-header">Промокоды</h1>
        @else
            <h1 class="page-header">Реферальные коды</h1>
        @endif
            <div class="d-flex">
            <a href="{{route('promocode')}}" class="btn btn-success" style="margin-bottom: 10px; margin-right: 10px;">Промокоды</a>
            <a href="{{route('discounts')}}" class="btn btn-primary" style="margin-bottom: 10px;">Реферальные коды</a>

        </div>
    </div>


    <!-- /.col-lg-12 -->

</div>

<!-- /.row -->



<div class="row">

    <div class="col-lg-12">

        <div class="panel panel-info">

            <div class="panel-heading">

                Список промокодов

                <div class="pull-right">

                    <div class="btn-group">
                    @if(!$claim )
                        <a href="{{ url('/manager/discounts/new') }}" class="btn btn-success btn-xs">

                            Добавить промокод

                        </a>
                    @endif
                    </div>

                </div>

            </div>

            <!-- /.panel-heading -->

            <div class="panel-body">

            @if(isset($data) && !empty($data) && count($data))

                <table class="table table-striped table-bordered table-hover">

                    <thead>

                        <tr>

                            <th class="text-center" style="width: 5%;">№</th>

                            <th>Код</th>

                            <th class="text-center">Категория</th>

                            <th class="text-center">Скидка</th>

                            <th class="text-center">Действует до</th>

                            <th class="text-center">Добавлено</th>

                            <th class="text-center">Статус</th>

                            <th class="text-center" style="width: 10%;">Удалить</th>

                        </tr>

                    </thead>

                    <tbody>

                    @foreach($data as $k => $item)

                        <tr>

                            <td class="text-center" style="width: 5%;">{{ ++$k }}</td>

                            <td><a href="{{ url("/manager/discounts/$item->id") }}">{{ $item->code }}</a></td>

                            <td class="text-center"><a href="{{ url("/manager/") }}/{{ $item->category === 0 ? 'cleaners' : 'workers' }}">{{ $item->category === 0 ? 'Уборка' : 'Подбор персонала' }}</a></td>

                            <td class="text-center">{{ $item->type === 0 ? ($item->price*100).' %' : $item->price.' тенге' }}</td>

                            <td class="text-center" style="width: 12%;">{{ date('d-m-Y', strtotime($item->finish)) }} г.</td>

                            <td class="text-center" style="width: 12%;">{!! $item->created_at->format('d F Y') !!} г.</td>

                            <td class="text-center" style="width: 8%;">

                                @if($item->status === 1)

                                <span class="label label-success">Включено</span>

                                @else

                                    <span class="label label-danger">Выключено</span>

                                @endif

                            </td>

                            <td class="text-center" style="width: 10%;"><a href="{{ url("/manager/discounts/delete/$item->id") }}" class="delete">Удалить</a></td>

                        </tr>

                    @endforeach

                    </tbody>

                </table>

            @else

            <p>Записей нет.</p>

            @endif



            </div>

            <!-- /.panel-body -->

        </div>

        <!-- /.panel -->



        {!! $data->render() !!}



    </div>

    <!-- /.col-lg-12 -->

</div>

<!-- /.row -->

@endsection
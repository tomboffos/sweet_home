@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Настройки главной страницы</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/edit") }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Первый блок</a></li>
                <li><a href="#tab-2" data-toggle="tab">Второй блок</a></li>
                <li><a href="#tab-3" data-toggle="tab">Третий блок</a></li>
                <li><a href="#tab-4" data-toggle="tab">Верхний баннер</a></li>
                <li><a href="#tab-5" data-toggle="tab">Промежуточный баннер</a></li>
            </ul>

            <div class="tab-content">

                {{-- 1 --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="index_title">Заголовок на главной странице</label>
                                <input type="text" class="form-control" required name="index[title]" id="index_title" placeholder="Заголовок" value="{{ $data['title'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="index_short">Короткое описание</label>
                                <textarea class="form-control" cols="5" placeholder="Короткое описание" name="index[short]" id="index_short">{{ $data['short'] }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 2 --}}
                <div class="tab-pane fade" id="tab-2">

                    <div class="row">
                        <div class="col-md-5">
                            <label for="index_short">Блок: Преимущества</label>
                                @foreach($data['skills'] as $key => $skill)
                                <div class="form-group">
                                    <input type="text" class="form-control" name="index[skills][{{ $key }}][title]" placeholder="Заголовок" value="{{ $skill['title'] }}">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" cols="5" placeholder="Короткое описание" name="index[skills][{{ $key }}][short]">{{ $skill['short'] }}</textarea>
                                </div>
                                <br>
                                @endforeach
                        </div>
                    </div>
                </div>

                {{-- 3 --}}
                <div class="tab-pane fade" id="tab-3">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="index_short">Блок: Почему нам стоит доверять</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="index[why_title]" placeholder="Почему нам стоит доверять" value="{{ $data['why_title'] }}">
                                </div>
                                @foreach($data['why_items'] as $key => $why_item)
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                           name="index[why_items][{{ $key }}][title]"
                                           placeholder="Заголовок"
                                           value="{{ $why_item['title'] }}">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control"
                                              cols="5"
                                              placeholder="Короткое описание"
                                              name="index[why_items][{{ $key }}][short]">{{ $why_item['short'] }}
                                    </textarea>
                                </div>
                                <br>
                                @endforeach
                        </div>
                    </div>
                </div>

                {{-- 4 --}}
                <div class="tab-pane fade" id="tab-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="text">Содержимое верхнего баннера</label>
                                <textarea class="form-control simple_text"
                                          name="index[banners][top][title]"
                                          id="simple_text_1">
                                    {!! $data['banners']['top']['title'] !!}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="index[banners][top][url]">Ссылка для верхнего баннера</label>
                                <input type="text"
                                       class="form-control"
                                       id="index[banners][top][url]"
                                       name="index[banners][top][url]"
                                       placeholder="http://google.com"
                                       value="{{ $data['banners']['top']['url'] }}">
                            </div>
                            <div class="form-group">
                                <label for="index[banners][top][banner]">Изображение для верхнего баннера</label>
                                <input type="file"
                                       class="form-control"
                                       id="index[banners][top][banner]"
                                       name="index[banners][top][banner]">
                                <input type="hidden" name="index[banners][top][current_banner]" value="{{ $data['banners']['top']['banner'] }}">
                            </div>
                            @if(!empty($data['banners']['top']['banner']))
                                <div class="form-group">
                                    <a href="{{ url("/data/banners/{$data['banners']['top']['banner']}") }}" target="_blank">{{ $data['banners']['top']['banner'] }}</a>
                                </div>
                                <div class="checkbox">
                                    <label for="index[banners][top][delete][banner]">
                                        <input type="checkbox"
                                               id="index[banners][top][delete][banner]"
                                               name="index[banners][top][delete][banner]"
                                               value="{{ $data['banners']['top']['banner'] }}"> Удалить изображение?
                                    </label>
                                </div>
                            @endif
                            <div class="checkbox">
                                <label for="index[banners][top][status]">
                                    <input type="checkbox"
                                           id="index[banners][top][status]"
                                           name="index[banners][top][status]"
                                           @if((int) $data['banners']['top']['status'] === 1) checked @endif> Включить?
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- 5 --}}
                <div class="tab-pane fade" id="tab-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="index[banners][inner][title]">Содержимое промежуточного баннера</label>
                                <textarea class="form-control simple_text"
                                          name="index[banners][inner][title]"
                                          id="simple_text_2">
                                    {!! $data['banners']['inner']['title'] !!}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="index[banners][inner][url]">Ссылка для промежуточного баннера</label>
                                <input type="text"
                                       class="form-control"
                                       id="index[banners][inner][url]"
                                       name="index[banners][inner][url]"
                                       placeholder="http://google.com"
                                       value="{{ $data['banners']['inner']['url'] }}">
                            </div>
                            <div class="form-group">
                                <label for="index[banners][inner][banner]">Изображение для промежуточного баннера</label>
                                <input type="file"
                                       class="form-control"
                                       id="index[banners][inner][banner]"
                                       name="index[banners][inner][banner]">
                                <input type="hidden" name="index[banners][inner][current_banner]" value="{{ $data['banners']['inner']['banner'] }}">
                            </div>
                            @if(!empty($data['banners']['inner']['banner']))
                                <div class="form-group">
                                    <a href="{{ url("/data/banners/{$data['banners']['inner']['banner']}") }}" target="_blank">{{ $data['banners']['inner']['banner'] }}</a>
                                </div>
                                <div class="checkbox">
                                    <label for="index[banners][inner][delete][banner]">
                                        <input type="checkbox"
                                               id="index[banners][inner][delete][banner]"
                                               name="index[banners][inner][delete][banner]"
                                               value="{{ $data['banners']['inner']['banner'] }}"> Удалить изображение?
                                    </label>
                                </div>
                            @endif
                            <div class="checkbox">
                                <label for="index[banners][inner][status]">
                                    <input type="checkbox"
                                           id="index[banners][inner][status]"
                                           name="index[banners][inner][status]"
                                           @if((int) $data['banners']['inner']['status'] === 1) checked @endif> Включить?
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Настройки сайта</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/settings") }}" class="post-edit" method="post">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Контакты</a></li>
                <li><a href="#tab-3" data-toggle="tab">Баллы</a></li>
                <li><a href="#tab-4" data-toggle="tab">Мета теги</a></li>

            </ul>

            <div class="tab-content">

                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group{{ $errors->has('posts_per_page') ? ' has-error' : '' }}">
                                <label for="posts_per_page">Количество записей на странице</label>
                                <input type="text" class="form-control" required name="posts_per_page" id="posts_per_page" placeholder="Количество записей на странице" value="{{ $data['posts_per_page'] }}">
                                @if ($errors->has('posts_per_page'))
                                <span class="help-block"> <strong>{{ $errors->first('posts_per_page') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group{{ $errors->has('points_currency') ? ' has-error' : '' }}">
                                <label for="points_currency">Укажите скольким тенге равен 1 балл</label>
                                <input type="text" class="form-control" required name="points_currency" id="points_currency" placeholder="Значение" value="{{ $data['points_currency'] }}">
                                <i>1 балл = значение данного поля</i>
                                @if ($errors->has('points_currency'))
                                <span class="help-block"> <strong>{{ $errors->first('points_currency') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="points_currency">Граница времени для графика клинеров</label>
                                <div class="input-group schedule-date {{ $errors->has('cleaning_time_border') ? ' has-error' : '' }}">
                                    <input class="form-control add-on" name="cleaning_time_border" type="text" value="{{ $data['cleaning_time_border'] }}">
                                    <span class="input-group-addon add-on">
                                    <i class="fa fa-clock-o"></i>
                                    </span>
                                    @if ($errors->has('cleaning_time_border'))
                                    <span class="help-block"> <strong>{{ $errors->first('cleaning_time_border') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Contacts --}}
                <div class="tab-pane fade" id="tab-2">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('emails') ? ' has-error' : '' }}">
                                <label for="emails">Укажите email для уведомлений</label>
                                <input type="text" class="form-control" name="emails" id="emails" placeholder="Значение" value="{{ $data['emails'] }}">
                                <i>можно указать несколько, через запятую (не больше 2х)</i>
                                @if ($errors->has('emails'))
                                <span class="help-block"> <strong>{{ $errors->first('emails') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('phones') ? ' has-error' : '' }}">
                                <label for="phones">Телефоны</label>
                                <input type="text" class="form-control" name="phones" id="phones" placeholder="Значение" value="{{ $data['phones'] }}">
                                <i>можно указать несколько, через запятую (не больше 2х)</i>
                                @if ($errors->has('phones'))
                                <span class="help-block"> <strong>{{ $errors->first('phones') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="social_vk">Вконтакте</label>
                                <input type="text" class="form-control" name="social[vk]" id="social_vk" placeholder="Ссылка на социальную сеть" value="{{ $data['social']['vk'] }}">
                            </div>
                            <div class="form-group">
                                <label for="social_instagram">Instagram</label>
                                <input type="text" class="form-control" name="social[in]" id="social_instagram" placeholder="Сылка на аккаунт" value="{{ $data['social']['in'] }}">
                            </div>
                            <div class="form-group">
                                <label for="social_facebook">Facebook</label>
                                <input type="text" class="form-control" name="social[fa]" id="social_facebook" placeholder="Ссылка на социальную сеть" value="{{ $data['social']['fa'] }}">
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Points --}}
                <div class="tab-pane fade" id="tab-3">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Бонусные баллы, получаемые партнёром</label>
                                <input type="number" class="form-control" name="discount" placeholder="Введите скидка на промокод в процентах" value="{{$discount_second->amount}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">

                            <div class="form-group{{ $errors->has('testimonal') ? ' has-error' : '' }}">
                                <label for="testimonal">Укажите сколько баллов начислять за добавление отзыва к заказу</label>

                                    <input type="text" class="form-control" required name="testimonal" id="testimonal" placeholder="Значение" value="{{$data['testimonal']}}">
                                @if ($errors->has('testimonal'))
                                <span class="help-block"> <strong>{{ $errors->first('testimonal') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Скидка получаемая партнером</label>
                                <input type="number" class="form-control" name="bonus" placeholder="Введите бонус " value="{{$discount_first->amount}}">

                            </div>
                        </div>
                    </div>
                </div>

                {{-- Meta --}}
                <div class="tab-pane fade" id="tab-4">
                    @if(isset($meta) && count($meta))
                        @foreach($meta as $key => $tag)
                        <div class="row">
                            <div class="col-md-6">
                                <label>{{ $tag['label'] }}</label>
                                <input type="hidden" class="form-control" name="meta[{{ $key }}][label]" value="{{ $tag['label'] }}">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="meta[{{ $key }}][title]" placeholder="Заголовок" value="{{ $tag['title'] }}">
                                </div>
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="meta[{{ $key }}][meta_title]" placeholder="Мета заголовок" value="{{ $tag['meta_title'] }}">
                                </div>
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="meta[{{ $key }}][meta_description]" placeholder="Мета описание" value="{{ $tag['meta_description'] }}">
                                </div>
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="meta[{{ $key }}][meta_keywords]" placeholder="Мета ключевые слова" value="{{ $tag['meta_keywords'] }}">
                                </div>
                            </div>
                        </div>
                        <hr>
                        @endforeach
                    @endif
                </div>


            </div>

            <button type="submit" id="main_submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<script>

</script>
<!-- /.row -->
@endsection

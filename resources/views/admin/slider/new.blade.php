@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Добавить слайд</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ('/manager/slider/new') }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
            </ul>

            <div class="tab-content">

                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title">Заголовок</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Заголовок">
                                @if ($errors->has('title'))
                                <span class="help-block"> <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('sort') ? ' has-error' : '' }}">
                                <label for="sort">Порядок</label>
                                <input type="text" class="form-control" name="sort" id="sort" placeholder="Порядок" value="0">
                                @if ($errors->has('sort'))
                                <span class="help-block"> <strong>{{ $errors->first('sort') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                <label for="url">Ссылка</label>
                                <input type="text" class="form-control" name="url" id="url" placeholder="Ссылка">
                                @if ($errors->has('url'))
                                <span class="help-block"> <strong>{{ $errors->first('url') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="thumbnail">Изображение</label>
                                <input type="file" class="form-control" id="thumbnail" name="thumbnail">
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                        <label for="text">Текст</label>
                        <textarea type="text" class="form-control" name="text" id="text" placeholder="Текст"></textarea>
                        @if ($errors->has('text'))
                        <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">

            <a href="{{ url('/manager/slider') }}" class="btn btn-default">Отмена</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

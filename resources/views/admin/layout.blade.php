<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Панель администрирования - HomeSweet</title>
    <link href="{{ url('/') }}/assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/css/sidebarmenu.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/css/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/common/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/assets/common/css/toastr.min.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/assets/common/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/assets/common/css/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/manager') }}">HomeSweet Администратор</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user"></i>{{ Auth::user()->name }} <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ url('/manager/users/managers/') }}/{{ Auth::user()->id }}"><i class="fa fa-gear"></i> Настройки</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> Выйти</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <li><a href="{{ url('/') }}" target="_blank" title="Откроется в новом окне">Перейти на сайт <i class="fa fa-sign-out"></i></a></li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        @if(isset($sidebar) && count($sidebar))
                            <?php $current_url = Route::getFacadeRoot()->current()->uri(); ?>
                            @foreach($sidebar as $text => $link)
                            <li>
                                <a href="{{ url('/') }}/{{ $link[1] }}"
                                @if($current_url === $link[1]) class="active" @endif>
                                <i class="fa {{ $link[0] }}"></i> {{ $text }}

                                @if(isset($link['child']) && count($link['child']))
                                <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    @foreach($link['child'] as $child_link => $child_text)
                                    <li><a href="{{ url('/') }}/{{ $child_link }}">{{ $child_text }}</a></li>
                                    @endforeach
                                </ul>
                                @else
                                </a>
                                @endif

                            </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
           @yield('content')
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!-- JS -->
<script src="{{ url('/') }}/assets/common/js/jquery.min.js"></script>
<script src="{{ url('/') }}/assets/admin/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/assets/admin/js/metisMenu.min.js"></script>
<script src="{{ url('/') }}/assets/admin/plugins/ckeditor/ckeditor.js"></script>
<script src="{{ url('/') }}/assets/common/js/toastr.min.js"></script>
<script src="{{ url('/') }}/assets/common/js/lightbox.min.js"></script>
<script src="{{ url('/') }}/assets/common/js/phoneinput.js"></script>
<script src="{{ url('/') }}/assets/common/js/jquery.datetimepicker.full.min.js"></script>
{{-- <script src="{{ url('/') }}/assets/admin/js/main.min.js"></script> --}}
<script src="{{ url('/') }}/assets/admin/js/main.js"></script>

@if(Session::has('message'))
<script>
var type = "{{ Session::get('alert-type', 'error') }}";
switch(type){
    case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;

    case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;

    case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;

    case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
}
</script>
@endif

</body>
</html>

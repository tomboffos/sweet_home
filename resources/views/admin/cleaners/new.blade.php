@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Добавить клинера</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ('/manager/cleaners/new') }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Изображение</a></li>
                <li><a href="#tab-3" data-toggle="tab">График работы</a></li>
            </ul>

            <div class="tab-content">
                
                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" name="name" required id="name" placeholder="Имя">
                        @if ($errors->has('name'))
                        <span class="help-block"> <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                        <label for="simple_text">Информация для клиентов</label>
                        <textarea type="text" class="form-control" name="text" id="simple_text" placeholder="Информация для клиентов"></textarea>
                        @if ($errors->has('text'))
                        <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                        <label for="comment">Комментарий (не отображается клиенту)</label>
                        <textarea type="text" class="form-control" name="comment" id="comment" placeholder="Информация для клиентов"></textarea>
                        @if ($errors->has('comment'))
                        <span class="help-block"> <strong>{{ $errors->first('comment') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                {{-- Image --}}
                <div class="tab-pane fade" id="tab-2">
                    <div class="form-group {{ $errors->has('img') ? ' has-error' : '' }}">
                        <label for="img">Фото</label>
                        <input type="file" name="img" id="img" class="form-control"> 
                        @if ($errors->has('img'))
                            <span class="help-block">
                                <strong>{{ $errors->first('img') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>

                {{-- Schedule --}}
                <div class="tab-pane fade" id="tab-3">
                   <div class="row">
                        <div class="col-md-2">
                            <div class="input-group"><label for="from">Начало</label></div>
                            <div class="input-group input-append schedule-date">
                                <input class="form-control add-on" data-format="hh:mm" name="from" type="text" value="08:00">
                                <span class="input-group-addon add-on">
                                <i class="fa fa-clock-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group"><label for="to">До</label></div>
                            <div class="input-group input-append schedule-date">
                                <input class="form-control add-on" data-format="hh:mm" name="to" type="text" value="18:00">
                                <span class="input-group-addon add-on">
                                <i class="fa fa-clock-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group"><label for="t_to_finish">До (макс. время)</label></div>
                            <div class="input-group input-append schedule-date">
                                <input class="form-control add-on" name="to_finish" type="text" value="18:00">
                                <span class="input-group-addon add-on">
                                <i class="fa fa-clock-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group disabled">
                        <table class="table week-days" style="width:250px;">
                            <thead>
                                <th>Пн</th>
                                <th>Вт</th>
                                <th>Ср</th>
                                <th>Чт</th>
                                <th>Пт</th>
                                <th>Сб</th>
                                <th>Вс</th>
                            </thead>
                            <tbody>
                                <td><input type="checkbox" name="mon"></td>
                                <td><input type="checkbox" name="tue"></td>
                                <td><input type="checkbox" name="wed"></td>
                                <td><input type="checkbox" name="thu"></td>
                                <td><input type="checkbox" name="fri"></td>
                                <td><input type="checkbox" name="sat"></td>
                                <td><input type="checkbox" name="sun"></td>
                            </tbody>
                        </table>                        
                    </div>
                    <p><br></p>
                </div>

            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">

            <a href="{{ url('/manager/cleaners') }}" class="btn btn-default">Отмена</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
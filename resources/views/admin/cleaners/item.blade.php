@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование клинера</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/cleaners/$data->id") }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Изображение</a></li>
                <li><a href="#tab-3" data-toggle="tab">Список заказов</a></li>
                <li><a href="#tab-4" data-toggle="tab">График работы</a></li>
                <li><a href="#tab-5" data-toggle="tab">Рейтинг</a></li>
                <li><a href="#tab-6" data-toggle="tab">Дополнительно</a></li>
            </ul>

            <div class="tab-content">
                
                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" name="name" required id="name" placeholder="Имя" value="{{ $data->name }}">
                        @if ($errors->has('name'))
                        <span class="help-block"> <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                        <label for="simple_text">Информация для клиентов</label>
                        <textarea type="text" class="form-control" name="text" id="simple_text" placeholder="Информация для клиентов">{{ $data->text }}</textarea>
                        @if ($errors->has('text'))
                        <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                        <label for="comment">Комментарий (не отображается клиенту)</label>
                        <textarea type="text" class="form-control" name="comment" id="comment" placeholder="Информация для клиентов">{{ $data->comment }}</textarea>
                        @if ($errors->has('comment'))
                        <span class="help-block"> <strong>{{ $errors->first('comment') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                {{-- Image --}}
                <div class="tab-pane fade" id="tab-2">
                    @if(isset($data->img) && !empty($data->img))

                        <span class="title-blk">Текущее изображение</span>
                        <div class="img-thumbnail">
                            <a href="{{ url('/data/cleaners') }}/{{ $data->id }}/{{ $data->img }}" rel="lightbox" class="img"><img src="{{ url('/data/cleaners') }}/{{ $data->id }}/{{ $data->img }}" alt=""></a>
                        </div>
                        <button type="button" id="delete_thumb" class="btn btn-default">Удалить изображение</button>
                        <input type="hidden" name="delete_thumb" value="0">
                        <input type="hidden" name="old_image" value="{{ $data->img }}">
                    @endif

                    <div class="form-group {{ $errors->has('img') ? ' has-error' : '' }}">
                        <label for="img">Фото</label>
                        <input type="file" name="img" id="img" class="form-control"> 
                        @if ($errors->has('img'))
                            <span class="help-block">
                                <strong>{{ $errors->first('img') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
                
                {{-- Orders --}}
                <div class="tab-pane fade" id="tab-3">
                    <div class="form-group">
                        @if(isset($orders) && count($orders))
                        <div class="orders-list-cleaner">
                            <div class="list-group">
                            @foreach($orders as $order)
                                <a href="{{ url("/manager/orders/cleaners/$order->id") }}" title="Открыть заказ" class="list-group-item">
                                    Заказ №{{ $order->id }}
                                    <span class="pull-right text-muted small"><em>Дата уборки: {!! $order->cl_date->format('d F Y') !!} {!! $order->cl_time->format('H:i') !!} | Время на уборку: {{ $order->elapsed->format('H') }} ч. и {{ $order->elapsed->format('i') }} м. | Заказ создан: {!! $order->created_at->format('H:i') !!}</em>
                                    </span>
                                </a>
                            @endforeach
                            </div>
                        </div>
                        @else
                        <p>Заказов пока не было.</p>
                        @endif
                    </div>
                </div>

                {{-- Schedule --}}
                <div class="tab-pane fade" id="tab-4">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="input-group"><label for="from">Начало</label></div>
                            <div class="input-group input-append schedule-date">
                                <input class="form-control add-on" name="from" type="text" value="{{ isset($schedule->t_from) ? $schedule->t_from : '08:00' }}">
                                <span class="input-group-addon add-on">
                                <i class="fa fa-clock-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group"><label for="to">До</label></div>
                            <div class="input-group input-append schedule-date">
                                <input class="form-control add-on" name="to" type="text" value="{{ isset($schedule->t_to) ? $schedule->t_to : '18:00' }}">
                                <span class="input-group-addon add-on">
                                <i class="fa fa-clock-o"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group"><label for="t_to_finish">До (макс. время)</label></div>
                            <div class="input-group input-append schedule-date">
                                <input class="form-control add-on" name="to_finish" type="text" value="{{ isset($schedule->t_to_finish) ? $schedule->t_to_finish : '18:00' }}">
                                <span class="input-group-addon add-on">
                                <i class="fa fa-clock-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group disabled">
                        <table class="table week-days" style="width:250px;">
                            <thead>
                                <th>Пн</th>
                                <th>Вт</th>
                                <th>Ср</th>
                                <th>Чт</th>
                                <th>Пт</th>
                                <th>Сб</th>
                                <th>Вс</th>
                            </thead>
                            <tbody>
                                <td><input type="checkbox" name="mon" @if($schedule->mon) checked @endif></td>
                                <td><input type="checkbox" name="tue" @if($schedule->tue) checked @endif></td>
                                <td><input type="checkbox" name="wed" @if($schedule->wed) checked @endif></td>
                                <td><input type="checkbox" name="thu" @if($schedule->thu) checked @endif></td>
                                <td><input type="checkbox" name="fri" @if($schedule->fri) checked @endif></td>
                                <td><input type="checkbox" name="sat" @if($schedule->sat) checked @endif></td>
                                <td><input type="checkbox" name="sun" @if($schedule->sun) checked @endif></td>
                            </tbody>
                        </table>                        
                    </div>
                    <p><br></p>
                </div>

                {{-- Rating --}}
                <div class="tab-pane fade" id="tab-5">
                    <div class="form-group disabled">
                        <label for="created_at">Текущий рейтинг</label>
                        @if(isset($rating) && !empty($rating))
                        <input type="text" class="form-control" id="created_at" value="{{ $rating }}" disabled>
                        @else
                        <p>У данного клинера пока нет рейтинга.</p>
                        @endif
                    </div>
                </div>

                {{-- Date --}}
                <div class="tab-pane fade" id="tab-6">
                   <div class="form-group disabled">
                        <label for="created_at">Зарегистрирован</label>
                        <input type="text" class="form-control" id="created_at" value="{{ $data->created_at }}" disabled>
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            @if($data->status === 1)
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">
            @else
            <button type="button" id="status" class="btn btn-default">Выключено</button>
            <input type="hidden" name="status" value="0">
            @endif

            <a href="{{ url ('/manager/cleaners') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url ("/manager/cleaners/delete/$data->id") }}" class="btn btn-danger pull-right delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
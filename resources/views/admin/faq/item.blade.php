@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование вопроса</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/faq/$data->id") }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Мета теги</a></li>
            </ul>

            <div class="tab-content">
                
                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title">Вопрос</label>
                        <input type="text" class="form-control" name="title" id="title" required placeholder="Вопрос" value="{{ $data->title }}">
                        @if ($errors->has('title'))
                        <span class="help-block"> <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                <label for="category">Раздел</label>
                                <select name="category" id="category" class="form-control">
                                @if(isset($faq_categories) && count($faq_categories))
                                    @foreach($faq_categories as $faq_category)
                                        @if($faq_category->id === $data->category)
                                            <option value="{{ $faq_category->id }}" selected>{{ $faq_category->title }}</option>
                                        @else
                                            <option value="{{ $faq_category->id }}">{{ $faq_category->title }}</option>
                                        @endif
                                    @endforeach
                                @endif
                                </select>
                                @if ($errors->has('category'))
                                <span class="help-block"> <strong>{{ $errors->first('category') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group{{ $errors->has('sort') ? ' has-error' : '' }}">
                                <label for="sort">Порядок</label>
                                <input type="text" class="form-control" name="sort" id="sort" placeholder="Порядок" value="{{ $data->sort }}">
                                @if ($errors->has('sort'))
                                <span class="help-block"> <strong>{{ $errors->first('sort') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                        <label for="text">Текст</label>
                        <textarea type="text" class="form-control" name="text" id="text" required placeholder="Текст">{{ $data->text }}</textarea>
                        @if ($errors->has('text'))
                        <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                {{-- Meta --}}
                <div class="tab-pane fade" id="tab-2">

                    <div class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
                        <label for="meta_title">Мета заголовок</label>
                        <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Мета заголовок" value="{{ $data->meta_title }}">
                        @if ($errors->has('meta_title'))
                        <span class="help-block"> <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                        <label for="meta_description">Мета описание</label>
                        <input type="text" class="form-control" name="meta_description" id="meta_description" placeholder="Мета описание" value="{{ $data->meta_description }}">
                        @if ($errors->has('meta_description'))
                        <span class="help-block"> <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                        <label for="slug">ЧПУ</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="ЧПУ" value="{{ $data->slug }}">
                        @if ($errors->has('slug'))
                        <span class="help-block"> <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            @if($data->status === 1)
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">
            @else
            <button type="button" id="status" class="btn btn-default">Выключено</button>
            <input type="hidden" name="status" value="0">
            @endif

            <a href="{{ url('/manager/faq') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url("/manager/faq/delete/$data->id") }}" class="pull-right btn btn-danger delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
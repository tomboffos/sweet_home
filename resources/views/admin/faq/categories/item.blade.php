@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование категории</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/faq/cats/$data->id") }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Мета теги</a></li>
            </ul>

            <div class="tab-content">
                
                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title">Название</label>
                        <input type="text" class="form-control" name="title" required id="title" placeholder="Название" value="{{ $data->title }}">
                        @if ($errors->has('title'))
                        <span class="help-block"> <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                        <label for="category">Раздел</label>
                        <select name="category" id="category" class="form-control">
                            <option value="servis-v-celom" @if('servis-v-celom' === $data->category) selected @endif>Сервис в целом</option>
                            <option value="uborka" @if('uborka' === $data->category) selected @endif>Уборка</option>
                        @if(isset($categories) && count($categories))
                            @foreach($categories as $category)
                                <option value="{{ $category->slug }}" @if($category->slug === $data->category) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        @endif
                        </select>
                        @if ($errors->has('category'))
                        <span class="help-block"> <strong>{{ $errors->first('category') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                {{-- Meta --}}
                <div class="tab-pane fade" id="tab-2">

                    <div class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
                        <label for="meta_title">Мета заголовок</label>
                        <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Мета заголовок" value="{{ $data->meta_title }}">
                        @if ($errors->has('meta_title'))
                        <span class="help-block"> <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                        <label for="meta_description">Мета описание</label>
                        <input type="text" class="form-control" name="meta_description" id="meta_description" placeholder="Мета описание" value="{{ $data->meta_description }}">
                        @if ($errors->has('meta_description'))
                        <span class="help-block"> <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                        <label for="slug">ЧПУ</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="ЧПУ" value="{{ $data->slug }}">
                        @if ($errors->has('slug'))
                        <span class="help-block"> <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            @if($data->status === 1)
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">
            @else
            <button type="button" id="status" class="btn btn-default">Выключено</button>
            <input type="hidden" name="status" value="0">
            @endif
            
            <a href="{{ url ('/manager/faq/cats') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url ("/manager/faq/cats/delete/$data->id") }}" class="btn btn-danger pull-right delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
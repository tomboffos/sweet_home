@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Главная</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ $c_cleaning }}</div>
                        <div>Заказы на уборку</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('/manager/orders/cleaners') }}">
                <div class="panel-footer">
                    <span class="pull-left">Открыть</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-10 text-right">
                        <div class="huge">{{ $c_workers }}</div>
                        <div>Заказы на подбор персонала</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('/manager/orders/workers') }}">
                <div class="panel-footer">
                    <span class="pull-left">Открыть</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ $c_testimonals }}</div>
                        <div>Отзывы</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('/manager/testimonals') }}">
                <div class="panel-footer">
                    <span class="pull-left">Открыть</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ $c_user }}</div>
                        <div>Клиенты</div>
                    </div>
                </div>
            </div>
            <a href="{{ url('/manager/users/clients') }}">
                <div class="panel-footer">
                    <span class="pull-left">Открыть</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-12">
        <hr>

    </div>


</div>
<style>
    .form-group label{
        margin-bottom: 20px;
        margin-top: 20px;
    }
</style>
<!-- /.row -->

@endsection
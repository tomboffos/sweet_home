@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Добавить запись</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ('/manager/posts/new') }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Мета теги</a></li>
            </ul>

            <div class="tab-content">
                {{-- Main info --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" name="title" id="title" required placeholder="Заголовок">
                                @if ($errors->has('title'))
                                <span class="help-block"> <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('parent') ? ' has-error' : '' }}">
                                <label for="parent">Родительская</label>
                                <select name="parent" class="form-control">
                                    <option value="0">Нет</option>
                                    @foreach($parents as $parent)
                                        <option value="{{ $parent->id }}">{{ $parent->title }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('parent'))
                                <span class="help-block"> <strong>{{ $errors->first('parent') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                <label for="category">Категория</label>
                                <select name="category" class="form-control">
                                    <option value="0">Родительская</option>
                                    @foreach($categories as $categorySlug => $categoryTitle)
                                        <option value="{{ $categorySlug }}">{{ $categoryTitle }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                <span class="help-block"> <strong>{{ $errors->first('category') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('sort') ? ' has-error' : '' }}">
                                <label for="sort">Порядок</label>
                                <input type="text" class="form-control" name="sort" id="sort" placeholder="Порядок">
                                @if ($errors->has('sort'))
                                <span class="help-block"> <strong>{{ $errors->first('sort') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->
                            has('text') ? ' has-error' : '' }}">
                                <label for="text">Текст</label>
                                <textarea type="text" class="form-control" name="text" id="text" required placeholder="Текст"></textarea>
                                @if ($errors->has('text'))
                                <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('text_bottom') ? ' has-error' : '' }}">
                                <label for="text_bottom">Текст ниже формы</label>
                                <textarea type="text" class="form-control js_ckeditor" name="text_bottom" id="text_bottom" required placeholder="Текст"></textarea>
                                @if ($errors->has('text_bottom'))
                                <span class="help-block"> <strong>{{ $errors->first('text_bottom') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Meta --}}
                <div class="tab-pane fade" id="tab-2">
                    <div class="form-group{{ $errors->
                        has('meta_title') ? ' has-error' : '' }}">
                        <label for="meta_title">Мета заголовок</label>
                        <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Мета описание" value="">
                        @if ($errors->has('meta_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->
                        has('meta_description') ? ' has-error' : '' }}">
                        <label for="meta_description">Мета описание</label>
                        <input type="text" class="form-control" name="meta_description" id="meta_description" placeholder="Мета описание" value="">
                        @if ($errors->has('meta_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->
                        has('slug') ? ' has-error' : '' }}">
                        <label for="slug">Чпу (только латинскими буквами, тире, цифры)</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="Чпу" value="">
                        @if ($errors->has('slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>

            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">

            <a href="{{ url('/manager/posts') }}" class="btn btn-default">Отмена</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

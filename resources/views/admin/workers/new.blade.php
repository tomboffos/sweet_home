@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Добавить сотрудника</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ('/manager/workers/new') }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Изображение</a></li>
            </ul>

            <div class="tab-content">
                
                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" name="name" required id="name" placeholder="Имя">
                        @if ($errors->has('name'))
                        <span class="help-block"> <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>

                     <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                        <label for="category">Специальность</label>
                        <select name="category" id="category" class="form-control">
                            @if(isset($categories) && !empty($categories))
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                            @endif
                        </select>
                        @if ($errors->has('category'))
                        <span class="help-block"> <strong>{{ $errors->first('category') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                        <label for="simple_text">Информация для клиентов</label>
                        <textarea type="text" class="form-control" name="text" id="simple_text" placeholder="Информация для клиентов"></textarea>
                        @if ($errors->has('text'))
                        <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                        <label for="comment">Комментарий (не отображается клиенту)</label>
                        <textarea type="text" class="form-control" name="comment" id="comment" placeholder="Информация для клиентов"></textarea>
                        @if ($errors->has('comment'))
                        <span class="help-block"> <strong>{{ $errors->first('comment') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                {{-- Image --}}
                <div class="tab-pane fade" id="tab-2">
                    <div class="form-group {{ $errors->has('img') ? ' has-error' : '' }}">
                        <label for="img">Фото</label>
                        <input type="file" name="img" id="img" class="form-control"> 
                        @if ($errors->has('img'))
                            <span class="help-block">
                                <strong>{{ $errors->first('img') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">

            <a href="{{ url('/manager/workers') }}" class="btn btn-default">Отмена</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Добавить сотрудника</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ('/manager/workers/cats/new') }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Мета теги</a></li>
            </ul>

            <div class="tab-content">

                {{-- Main --}}
                <div class="tab-pane fade in active" id="tab-1">

                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name">Название</label>
                                <input type="text" class="form-control" name="name" required id="name" placeholder="Название">
                                @if ($errors->has('name'))
                                <span class="help-block"> <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('sort') ? ' has-error' : '' }}">
                                <label for="sort">Порядок</label>
                                <input type="text" class="form-control" name="sort" id="sort" placeholder="Порядок">
                                @if ($errors->has('sort'))
                                <span class="help-block"> <strong>{{ $errors->first('sort') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title">Название, отображаемое на сайте</label>
                                <input type="text" class="form-control" name="title" required id="title" placeholder="Название, отображаемое на сайте" value="{{ old('title') }}">
                                @if ($errors->has('title'))
                                <span class="help-block"> <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('form_type') ? ' has-error' : '' }}">
                                <label for="form_type">Выберите тип формы</label>
                                <select name="form_type" id="form_type" class="form-control">
                                    @foreach($form_types as $k => $form_type)
                                    <option value="{{ $k }}">{{ $form_type }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('form_type'))
                                <span class="help-block"> <strong>{{ $errors->first('form_type') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                        <label for="text">Описание</label>
                        <textarea type="text" class="form-control" name="text" id="text" placeholder="Название, отображаемое на сайте">{{ old('text') }}</textarea>
                        @if ($errors->has('text'))
                        <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                {{-- Meta --}}
                <div class="tab-pane fade" id="tab-2">

                    <div class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
                        <label for="meta_title">Мета заголовок</label>
                        <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Мета заголовок" value="{{ old('meta_title') }}">
                        @if ($errors->has('meta_title'))
                        <span class="help-block"> <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                        <label for="meta_description">Мета описание</label>
                        <input type="text" class="form-control" name="meta_description" id="meta_description" placeholder="Мета описание" value="{{ old('meta_description') }}">
                        @if ($errors->has('meta_description'))
                        <span class="help-block"> <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                        <label for="slug">ЧПУ</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="ЧПУ" value="{{ old('slug') }}">
                        @if ($errors->has('slug'))
                        <span class="help-block"> <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>

            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">

            <button type="button" id="menu" class="btn btn-success">Показывается в меню</button>
            <input type="hidden" name="menu" value="1">            <button type="button" id="footer-menu" class="btn btn-success">Показывается в подвале</button>            <input type="hidden" name="footer" value="1">

            <a href="{{ url('/manager/workers/cats') }}" class="btn btn-default">Отмена</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Транзакции</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Список транзакций
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            @if(isset($data) && !empty($data) && count($data))
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 5%;">№</th>
                            <th>ID абонента</th>
                            <th>ID карты абонента</th>
                            <th>Номер заказа</th>
                            <th>Сумма</th>
                            <th>Время (epay)</th>
                            <th>Сист. сообщение</th>
                            <th>Reference</th>
                            <th>int_reference</th>
                            <th>approval_code</th>
                            <th>Result</th>
                            <th class="text-center">Добавлено</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $k => $item)
                        <tr>
                            <td class="text-center" style="width: 5%;">{{ $item->id }}</td>
                            <td>{{ $item->abonent_id }}</td>
                            <td>{{ $item->card_id }}</td>
                            <td>{{ $item->OrderId }}</td>
                            <td>{{ $item->amount }}</td>
                            <td>{{ $item->ActionTime }}</td>
                            <td>{{ $item->message }}</td>
                            <td>{{ $item->Reference }}</td>
                            <td>{{ $item->int_reference }}</td>
                            <td>{{ $item->approval_code }}</td>
                            <td>{{ $item->Result }}</td>
                            <td class="text-center" style="width: 12%;">{!! $item->created_at->format('d F Y') !!} г.</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
            <p>Записей нет.</p>
            @endif

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

        {!! $data->render() !!}

    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
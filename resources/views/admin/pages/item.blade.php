@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование страницы</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/pages/$data->id") }}" class="post-edit" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Основное</a></li>
                <li><a href="#tab-2" data-toggle="tab">Мета теги</a></li>
            </ul>

            <div class="tab-content">
                {{-- Main info --}}
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" name="title" id="title" required placeholder="Заголовок" value="{{ $data->title }}">
                                @if ($errors->has('title'))
                                <span class="help-block"> <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('sort') ? ' has-error' : '' }}">
                                <label for="sort">Порядок</label>
                                <input type="text" class="form-control" name="sort" id="sort" placeholder="Порядок" value="{{ $data->sort }}">
                                @if ($errors->has('sort'))
                                <span class="help-block"> <strong>{{ $errors->first('sort') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                        <label for="text">Текст</label>
                        <textarea type="text" class="form-control" name="text" id="text" required placeholder="Текст">{{ $data->text }}</textarea>
                        @if ($errors->has('text'))
                        <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                {{-- Meta --}}
                <div class="tab-pane fade" id="tab-2">
                    <div class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
                        <label for="meta_title">Мета заголовок</label>
                        <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Мета описание" value="{{ $data->
                        meta_title }}">
                        @if ($errors->has('meta_title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_title') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                        <label for="meta_description">Мета описание</label>
                        <input type="text" class="form-control" name="meta_description" id="meta_description" placeholder="Мета описание" value="{{ $data->
                        meta_description }}">
                        @if ($errors->has('meta_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                        <label for="slug">Чпу (только латинскими буквами, тире, цифры)</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="Чпу" value="{{ $data->
                        slug }}">
                        @if ($errors->has('slug'))
                        <span class="help-block">
                            <strong>{{ $errors->first('slug') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>

            @if($data->status === 1)
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">
            @else
            <button type="button" id="status" class="btn btn-default">Выключено</button>
            <input type="hidden" name="status" value="0">
            @endif

            @if($data->menu === 1)
            <button type="button" id="menu" class="btn btn-success">Показывается в меню</button>
            <input type="hidden" name="menu" value="1">
            @else
            <button type="button" id="menu" class="btn btn-default">Не показывается в меню</button>
            <input type="hidden" name="menu" value="0">
            @endif

            @if($data->footer === 1)
            <button type="button" id="footer-menu" class="btn btn-success">Показывается в подвале</button>
            <input type="hidden" name="footer" value="1">
            @else
            <button type="button" id="footer-menu" class="btn btn-default">Не показывается в подвале</button>
            <input type="hidden" name="footer" value="0">
            @endif

            <a href="{{ url('/manager/pages') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url("/manager/pages/delete/$data->id") }}" class="pull-right btn btn-danger delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

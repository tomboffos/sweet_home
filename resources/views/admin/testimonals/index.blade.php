@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Отзывы клиентов</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Список отзывов
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            @if(isset($data) && !empty($data) && count($data))
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 5%;">№</th>
                            <th>Отзыв к заказу</th>
                            <th class="text-center">Рейтинг</th>
                            <th class="text-center">Добавлено</th>
                            <th class="text-center">Статус</th>
                            <th class="text-center" style="width: 10%;">Удалить</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $k => $item)
                        <tr>
                            <td class="text-center" style="width: 5%;">{{ ++$k }}</td>
                            <td><a href="{{ url("/manager/testimonals/$item->id") }}">Отзыв к заказу №{{ $item->order_id }}</a></td>                            
                            <td class="text-center" style="width: 5%;">{{ $item->rating }}</td>
                            <td class="text-center" style="width: 12%;">{!! $item->created_at->format('d F Y') !!} г.</td>
                            <td class="text-center" style="width: 8%;">
                                @if($item->status === 1)
                                <span class="label label-success">Включено</span>
                                @else
                                    <span class="label label-danger">Выключено</span>
                                @endif
                            </td>
                            <td class="text-center" style="width: 10%;"><a href="{{ url("/manager/testimonals/delete/$item->id") }}" class="delete">Удалить</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
            <p>Записей нет.</p>
            @endif

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

        {!! $data->render() !!}

    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection
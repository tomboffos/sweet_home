@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование отзыва</h1>
        @if(!empty($data->text))
        <div class="alert alert-info" role="alert">За данный отзыв клиент получил баллы. Если вы удалите или отключите отзыв, не забудьте вычесть баллы у клиента (если, конечно, это необходимо)!</div>
        @endif
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <ul class="error-list alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form action="{{ url ("/manager/testimonals/$data->id") }}" class="post-edit" method="post">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="name">Имя клиента</label>
                        <p><a class="btn btn-xs btn-info" href="{{ url("/manager/users/clients/$user->id") }}" title="Редактировать клиента">{{ $user->name }}</a></p>
                    </div>
                </div>
                @if($data->type === 0)
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="name">Клинер</label>
                        @if(isset($cleaner))
                        <p><a class="btn btn-xs btn-info" href="{{ url("/manager/cleaners/$cleaner->id") }}" title="Подробнее о клинере">{{ $cleaner->name }}</a></p>
                        @else
                            <p>Клинер не найден.</p>
                        @endif
                    </div>
                </div>
                @endif
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="name">Заказ</label>
                        @if($data->type === 0)
                        	<p><a class="btn btn-xs btn-info" href="{{ url("/manager/orders/cleaners/$order->id") }}" title="Смотреть заказ">Смотреть заказ №{{ $order->id }}</a></p>
                        @else
                        	<p><a class="btn btn-xs btn-info" href="{{ url("/manager/orders/workers/$order->id") }}" title="Смотреть заказ">Смотреть заказ №{{ $order->id }}</a></p>
                        @endif
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
            	@if($data->type === 0)
                <div class="col-md-1">
                    <div class="form-group{{ $errors->has('rating') ? ' has-error' : '' }}">
                        <label for="rating">Рейтинг</label>
                        <input type="text" class="form-control" name="rating" id="rating" value="{{ $data->rating }}">
                    </div>
                </div>
                @endif
                <div class="col-md-1">
                    <div class="form-group{{ $errors->has('sort') ? ' has-error' : '' }}">
                        <label for="sort">Порядок</label>
                        <input type="text" class="form-control" name="sort" id="sort" value="{{ $data->sort }}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group disabled">
                        <label for="created_at">Добавлен</label>
                        <input type="text" class="form-control" id="created_at" value="{{ $data->created_at }}" disabled>
                    </div>
                </div>
            </div>

            <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                <label for="simple_text">Текст</label>
                <textarea type="text" class="form-control" name="text" id="simple_text" required placeholder="Текст">{{ $data->text }}</textarea>
                @if ($errors->has('text'))
                <span class="help-block"> <strong>{{ $errors->first('text') }}</strong>
                </span>
                @endif
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
            @if($data->status === 1)
            <button type="button" id="status" class="btn btn-success">Включено</button>
            <input type="hidden" name="status" value="1">
            @else
            <button type="button" id="status" class="btn btn-default">Выключено</button>
            <input type="hidden" name="status" value="0">
            @endif
            <a href="{{ url ('/manager/testimonals') }}" class="btn btn-default">Вернуться</a>
            <a href="{{ url ("/manager/testimonals/delete/$data->id") }}" class="btn btn-danger pull-right delete">Удалить</a>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

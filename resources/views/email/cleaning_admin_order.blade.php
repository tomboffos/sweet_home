<html>
	<head>
		<title>{{ trans('messages.cleaners_order_added') }}</title>
	</head>
	<body>
		<p>{{ trans('messages.cleaners_order_added') }}</p>
		<p>Подробности указаны ниже:</p>
		<p></p>
		<p>Номер заказа: {{ $event->data->order_id }}</p>
		<p>Имя клиента: {{ $event->data->client_name }}</p>
		<p>Email клиента: {{ $event->data->client_email}}</p>
		<p>Сумма заказа: {{ number_format($event->data->order_total, 0, '', ' ') }} тенге</p>
		<p>Подробная информация о заказе указана в панели администрирования.</p>
	</body>
</html>
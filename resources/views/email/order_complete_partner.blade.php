<html>
<head>
    <title>{{ trans('messages.order_complete_subject') }}</title>
</head>
<body>
<p>Здравствуйте, {{ $partner->name }}!</p>
<p>
    По вашему промокоду был сделан новый заказ, и вам было начислено {{$bonus->amount}} баллов.
</p>
<p>
    У вас сейчас {{$partner->points}} баллов. Используйте их, чтобы получить скидку на уборку!
</p>
<p>
    <a href="{{ url('user/orders/cleaning') }}">в вашем личном кабинете</a>.</p>
</p>
<p>
    Ваш промокод {{$discount->code}}. Поделитесь им с друзьями, чтобы заработать больше баллов!
</p>
<p>
    Facebook: https://www.facebook.com/sharer/sharer.php?u={{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($partner->name))}}{{ $partner->id }}
</p>
<p>
    Instagram: https://vk.com/share.php?url=http://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($partner->name))}}{{ $partner->id }}
</p>
<p>
    Telegram: https://t.me/share/url?url=https://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($partner->name))}}{{ $partner->id }}
</p>
<p>
    Whatsapp: https://api.whatsapp.com/send?text=https://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($partner->name))}}{{ $partner->id }}
</p>
</body>
</html>

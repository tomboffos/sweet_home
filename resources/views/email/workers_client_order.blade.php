<html>
	<head>
		<title>{{ trans('messages.workers_order_added') }}</title>
	</head>
	<body>
		<p>Здравствуйте! Вы добавили заказ на подбор персонала.</p>
		<p></p>

		<p>Номер заказа: {{ $event->data->order_id }}</p>
		<p>Подробная информация о заказе указана <a href="{{ url('user/orders/workers') }}/{{ $event->data->order_id }}">в вашем личном кабинете</a>.</p>
	</body>
</html>
<html>
	<head>
		<title>{{ trans('messages.cleaners_order_added') }}</title>
	</head>
	<body>
		<p>Здравствуйте! Вы добавили заказ на уборку, подробности указаны ниже:</p>
		<p></p>
		<p>Номер заказа: {{ $event->data->order_id }}</p>
		<p>Сумма заказа:<s>{{$event->data->old_price != $event->data->order_total ? $event->data->old_price : ''}}</s> {{ number_format($event->data->order_total, 0, '', ' ') }} тенге</p>
		@if($event->data->old_price != $event->data->order_total && $event->data->discount_code != null)
			<p>Поздравляем, вы получили скидку {{$event->data->old_price - $event->data->order_total}} тенге по коду {{$event->data->discount_code}}!</p>
		@endif
		<p>Подробная информация о заказе указана <a href="{{ url('user/orders/cleaning') }}/{{ $event->data->order_id }}">в вашем личном кабинете</a>.</p>
		<p>
			Приглашая друзей, вы можете зарабатывать баллы и использовать их для получения скидки на уборку!
		</p>
		<p>
			Ваш реферальный код: {{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->data->client_name))}}{{ $event->data->user_id }}. Поделитесь кодом
		</p>
		<p>
			Facebook: https://www.facebook.com/sharer/sharer.php?u={{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->data->client_name))}}{{ $event->data->user_id }}
		</p>
		<p>
			Instagram: https://vk.com/share.php?url=http://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->data->client_name))}}{{ $event->data->user_id }}
		</p>
		<p>
			Telegram: https://t.me/share/url?url=https://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->data->client_name))}}{{ $event->data->user_id }}
		</p>
		<p>
			Whatsapp: https://api.whatsapp.com/send?text=https://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->data->client_name))}}{{ $event->data->user_id }}
		</p>
	</body>
</html>
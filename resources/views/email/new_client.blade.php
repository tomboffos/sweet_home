<html>
	<head>
		<title>{{ trans('messages.new_client_registered') }}</title>
	</head>
	<body>
		<p>{{ trans('messages.new_client_registered') }}</p>
		<p>Вы оставили заявку на сайте HomeSweet. Мы зарегистрировали для вас аккаунт и сгенерировали пароль, который можно легко поменять в своем личном кабинете!</p>
		
		<p><a href="{{ url('user') }}">Профиль</a></p>
		<p>Пароль: {{ $event->data->password }}</p>
	</body>
</html>
<html>
	<head>
		<title>{{ trans('messages.order_complete_subject') }}</title>
	</head>
	<body>
		<p>Здравствуйте, {{ $event->user->name }}!</p>
		<p>{{ trans('messages.your_order') }} №{{ $event->order->id }} выполнен {{ date('d-m-Y', strtotime($event->order->cl_date)) }} {{ date('H:i', strtotime($event->order->cl_time)) }}, клинер - {{ $event->cleaner->name }}.</p>
		<p></p>
		<p>Будем признательны, если вы оставите отзыв об уборке на сайте {{ url("/user/orders/cleaning/{$event->order->id}#testimonal") }}. А мы, в знак благодарности, начислим {{ $event->points_settings->value }} бонусов на ваш счет. Бонусы можно использовать при оплате следующего заказа.</p>
        <p></p>
        <p>Ваше мнение очень важно для нас!</p>
	</body>
</html>

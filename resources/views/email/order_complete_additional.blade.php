<html>
<head>
    <title>{{ trans('messages.order_complete_subject') }}</title>
</head>
<body>

<p>Здравствуйте, {{ $event->user->name }}!</p>
<p>
    Мы хотели бы рассказать вам о бонусной системе нашего сайта. Приглашая нового пользователя через нашу ссылку на сайт вы получаете бонус, который можете использовать для совершения для дальнейших заказов.
</p>
<p>{{ trans('messages.your_order') }} №{{ $event->order->id }} выполнен {{ date('d-m-Y', strtotime($event->order->cl_date)) }} {{ date('H:i', strtotime($event->order->cl_time)) }}.</p>
<p></p>
<p>Ваш код для бонусов {{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->user->name))}}{{ $event->user->id }}</p>
<p></p>
<p>
    Ссылка чтобы поделиться в соцсетях
</p>
<p>
    Facebook: https://www.facebook.com/sharer/sharer.php?u={{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->user->name))}}{{ $event->user->id }}
</p>
<p>
    Instagram: https://vk.com/share.php?url=http://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->user->name))}}{{ $event->user->id }}
</p>
<p>
    Telegram: https://t.me/share/url?url=https://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->user->name))}}{{ $event->user->id }}
</p>
<p>
    Whatsapp: https://api.whatsapp.com/send?text=https://{{request()->getHost()}}?ref={{ \Illuminate\Support\Str::upper(cyrillic_to_latin($event->user->name))}}{{ $event->user->id }}
</p>

</body>
</html>

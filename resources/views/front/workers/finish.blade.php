<!-- Facebook Pixel Event -->
<script>
  fbq('track', 'Purchase');
</script>
<!-- Event snippet for Заявка на подбор conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-799513118/v6acCMP08YsBEJ60nv0C',
      'transaction_id': ''
  });
</script>
<h4>Заказ успешно сформирован!</h4>

@if($new_user === true)
<p>Для вас создан аккаунт. Пароль был выслан на ваш почтовый ящик!</p>
@endif

<p>Детали заказа всегда можно <a href="{{ url('/user/orders/workers') }}/{{ $order->id }}">посмотреть</a> в личном кабинете.</p>
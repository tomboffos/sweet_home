<div class="clearfix"></div>

<h2>{{ trans('messages.submit_application') }}</h2>

<form id="order-worker" class="order-form">

    <input type="hidden" name="id" value="{{ $data->id }}">
    <input type="hidden" name="type" value="1">

    <div class="row choose-block common-inputs">
        <div class="col-md-12">
            <div class="title">{{ trans('messages.contacts') }}</div>

            <div class="inline">
                <div class="item">
                    <input type="text" required name="name" value="{{ $user->name }}" placeholder="{{ trans('messages.name') }}">
                </div>
                <div class="item">
                    <input type="text" required name="phone" value="{{ $user->phone }}" placeholder="{{ trans('messages.phone') }}">
                </div>
            </div>
            <p></p>

            <div class="inline">
                <div class="item">
                    <input type="email" name="email" placeholder="Email" value="{{ $user->email }}">
                </div>
                @if(!Auth::user())
                <div class="auth-field item" style="display: none;">
                    <input type="password" name="password" placeholder="{{ trans('messages.password') }}">
                </div>
                <div class="auth-field item" style="display: none;">
                    <span class="ajax-auth btn btn-success fast-login">Войти</span>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row choose-block common-inputs">
        <div class="col-md-9">
            <div class="title">{{ trans('messages.about_ward') }}</div>
            <textarea required name="requirements"></textarea>
        </div>
    </div>

    <div class="row choose-block common-inputs">
        <div class="col-md-9">
            <div class="title">{{ trans('messages.work_schedule') }}</div>
            <div class="inline">
                <div class="item">
                    <select name="schedule" id="schedule" required>
                        <option value="пятидневка">{{ trans('messages.five_days') }}</option>
                        <option value="посуточный">{{ trans('messages.daily') }}</option>
                        <option value="ночная сиделка">{{ trans('messages.night_nurse') }}</option>
                        <option value="другой график">{{ trans('messages.another_schedule') }}</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row choose-block common-inputs">
        <div class="col-md-9">
            <div class="title">{{ trans('messages.comments') }}</div>
            <textarea name="text"></textarea>
        </div>
    </div>

    <div class="row choose-block common-inputs">
        <div class="col-md-5">
            <div class="title">{{ trans('messages.add_promo_code') }}</div>

            <div class="inline">
                <div class="item">
                    <input type="text" name="discount">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button type="button" class="add-btn send-order-workers">{{ trans('messages.send') }}</button>
        </div>
    </div>
</form>

<div class="order-result"></div>
@extends('front.layout')

@section('content')

<section id="scroll-here" class="page-top">

	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.' . $data->title) }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.' . $data->title) }}</li>
				</ul>
			</div>
		</div>
	</div>

</section>

<main class="content page order-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
                    <div class="entry-typography entry-typography-top">
                        @if(App::isLocale('en'))
                            <p>Sweet Home agency performs staff recruitment services in Almaty.</p>
                        @endif
                        {!! $data->getText() !!}
                    </div>

					<div class="clearfix"></div>

					<form id="order-worker" class="order-form">
						<h2>{{ trans('messages.submit_application') }}</h2>
						<input type="hidden" name="id" value="{{ $data->id }}">
						<input type="hidden" name="type" value="3">

						<div class="row choose-block common-inputs">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.contacts') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" required name="name" value="{{ $user->name }}" placeholder="{{ trans('messages.name') }}">
									</div>
									<div class="item">
										<input type="text" required name="phone" value="{{ $user->phone }}" placeholder="{{ trans('messages.phone') }}">
									</div>
								</div>
								<p></p>

								<div class="inline">
									<div class="item">
										<input type="email" name="email" placeholder="Email" value="{{ $user->email }}">
									</div>
									@if(!Auth::user())
									<div class="auth-field item" style="display: none;">
										<input type="password" name="password" placeholder="{{ trans('messages.password') }}">
									</div>
									<div class="auth-field item" style="display: none;">
										<span class="ajax-auth btn btn-success fast-login">Войти</span>
									</div>
									@endif
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.apartment_area') }}</div>
								<div class="inline">
									<div class="item">
										<input type="text" required name="requirements">
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.work_schedule') }}</div>
								<div class="inline">
									<div class="item">
										<select name="schedule" id="schedule" required>
											<option value="пятидневка">{{ trans('messages.five_days') }}</option>
											<option value="3 раза в неделю">{{ trans('messages.3_days') }}</option>
											<option value="1 раз в неделю">{{ trans('messages.1_days') }}</option>
											<option value="другой график">{{ trans('messages.another_schedule') }}</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.comments') }}</div>
								<textarea name="text"></textarea>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-5">
								<div class="title">{{ trans('messages.add_promo_code') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" name="discount">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<button type="button" class="add-btn send-order-workers">{{ trans('messages.send') }}</button>
							</div>
						</div>
					</form>

					<div class="order-result"></div>

					<div class="entry-typography entry-typography-bottom">
						<h2>{{ trans('messages.price') }}</h2>
						<table>
							<thead>
								<tr>
									<th>{{ trans('messages.service') }}</th>
									<th>{{ trans('messages.price') }}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ trans('messages.permanent_housekeeper') }}</td>
									<td>{{ trans('messages._6_') }}</td>
								</tr>
							</tbody>
						</table>
                        @if (App::isLocale('ru'))
                        <h2>Почему стоит обращаться к нам</h2>
                        <p>Найти домработницу в наше время далеко не проблема. В интернете огромное количество людей, которые хотели бы работать в данной сфере. Главная сложность заключается в том, чтобы подобрать квалифицированного и надежного сотрудника. Вы без проблем найдете множество примеров, когда прислуга не просто халатно относилась к своим обязанностям, но и могла навредить заказчику и его близким. Наша компания предлагает клининговые услуги уже много лет. За это время нам удалось отобрать лучших профессионалов своего дела, которые могут похвастаться следующими достоинствами:</p>
                        <ul>
                            <li>Большой опыт работы.</li>
                            <li>Добросовестность.</li>
                            <li>Ответственность.</li>
                            <li>Скрупулезность.</li>
                            <li>Пунктуальность.</li>
                        </ul>
                        <p>Кроме того, вы можете быть уверены в сохранности своих вещей и конфиденциальности.</p>
                        @endif
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li><a href="{{ url('workers') }}">{{ trans('messages.staff_recruitment') }}</a></li>
							@foreach($categories as $item)
								<li @if($data->id === $item->id) class="active" @endif><a href="{{ url('workers') }}/{{ $item->id }}-{{ $item->slug }}">{!! trans('messages.' . $item->title) !!}</a></li>
							@endforeach
						</ul>
					</div>
					@if(isset($faq) && $faq && count($faq) && App::isLocale('ru'))
					<div class="block faq-sidebar">
						<div class="title">{{ trans('messages.q_n_a') }}</div>

						<ul>
							@foreach($faq as $item)
							<li><a href="{{ url('faq') }}/{{ $item->category }}-{{ isset($item->common) ? 'servis-v-celom' : $faq_category  }}/{{ $item->id }}-{{ $item->slug }}" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ trans('messages.' . $item->title) }}</a></li>
							@endforeach
						</ul>

						<a href="{{ url('faq') }}" class="more-faq" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ trans('messages.other_issues') }}</a>

						<div class="clearfix"></div>
					</div>
					@endif
				</aside>
			</div>
		</div>
		@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
			[
				'title' => trans('messages.main_page'),
				'url' => url('/'),
			],
			[
				'title' => trans('messages.' . $data->title),
				'url' => '',
			],
		]])
	</div>

	@include('front.footer-skills')

</main>
@endsection

@extends('front.layout')

@section('content')

<section id="scroll-here" class="page-top">

	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.' . $data->title) }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.' . $data->title) }}</li>
				</ul>
			</div>
		</div>
	</div>

</section>

<main class="content page order-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
                    <div class="entry-typography entry-typography-top">
                        @if(App::isLocale('en'))
                            <p>Sweet Home agency performs staff recruitment services in Almaty.</p>
                        @endif
                        {!! $data->getText() !!}
                    </div>

					<div class="clearfix"></div>

					<form id="order-worker" class="order-form">
						<h2>{{ trans('messages.submit_application') }}</h2>
						<input type="hidden" name="id" value="{{ $data->id }}">
						<input type="hidden" name="type" value="0">

						<div class="row choose-block common-inputs">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.contacts') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" required name="name" value="{{ $user->name }}" placeholder="{{ trans('messages.name') }}">
									</div>
									<div class="item">
										<input type="text" required name="phone" value="{{ $user->phone }}" placeholder="{{ trans('messages.phone') }}">
									</div>
								</div>
								<p></p>

								<div class="inline">
									<div class="item">
										<input type="email" name="email" placeholder="Email" value="{{ $user->email }}">
									</div>
									@if(!Auth::user())
									    <div class="auth-field item" style="display: none;">
										    <input type="password" name="password" placeholder="{{ trans('messages.password') }}">
									    </div>
									    <div class="auth-field item" style="display: none;">
										    <span class="ajax-auth btn btn-success fast-login">Войти</span>
									    </div>
									@endif
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.child_age') }}</div>
								<div class="inline">
									<div class="item">
										<input type="text" required name="requirements">
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.work_schedule') }}</div>
								<div class="inline">
									<div class="item">
										<select name="schedule" id="schedule" required>
											<option value="пятидневка">{{ trans('messages.five_days') }}</option>
											<option value="2 дня через 2">{{ trans('messages.2_after_2') }}</option>
											<option value="ночная няня">{{ trans('messages.night_babysitter') }}</option>
											<option value="другой график">{{ trans('messages.another_schedule') }}</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.comments') }}</div>
								<textarea name="text"></textarea>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-5">
								<div class="title">{{ trans('messages.add_promo_code') }}</div>
								<div class="inline">
									<div class="item">
										<input type="text" name="discount">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<button type="button" class="add-btn send-order-workers">{{ trans('messages.send') }}</button>
							</div>
						</div>
					</form>

					<div class="order-result"></div>

					<div class="entry-typography entry-typography-bottom">
						<h2>{{ trans('messages.price') }}</h2>
						<table>
							<thead>
								<tr>
									<th>{{ trans('messages.service') }}</th>
									<th>{{ trans('messages.price') }}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ trans('messages.babysitter') }}</td>
									<td>{{ trans('messages._80_') }}</td>
								</tr>
							</tbody>
						</table>
                        @if (App::isLocale('ru'))
<h2>Почему стоит обращаться к нам</h2>
<p>Главным достоинством нашей компании можно считать тот факт, что мы всегда тщательно отбираем работников в свой штат. Если мы предлагаем вам определенную кандидатуру, то вы можете спокойно доверить своего ребенка этому человеку. Мы гарантируем вам, что все няни от нашей компании – это:</p>
<ul>
    <li>Профессионалы, получившие необходимую подготовку.</li>
    <li>Люди с большим опытом работы в данной сфере.</li>
    <li>Настоящие психологи, которые умеют находить общий язык с любым ребенком.</li>
    <li>Ответственные и добросовестные сотрудники, которые всегда выполняют свои обязанности.</li>
</ul>
<p>Наем няни может быть как разовым, так и регулярным. Второй вариант отлично подойдет для деловых людей, которым приходится слишком много времени проводить на работе и они не могут уделить должного внимания воспитанию своих детей.</p>
                        @endif
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li><a href="{{ url('workers') }}">{{ trans('messages.staff_recruitment') }}</a></li>
							@foreach($categories as $item)
								<li @if($data->id === $item->id) class="active" @endif><a href="{{ url('workers') }}/{{ $item->id }}-{{ $item->slug }}">{{ trans('messages.' . $item->title) }}</a></li>
							@endforeach
						</ul>
					</div>
					@if(isset($faq) && $faq && count($faq) && App::isLocale('ru'))
					<div class="block faq-sidebar">
						<div class="title">{{ trans('messages.q_n_a') }}</div>

						<ul>
							@foreach($faq as $item)
							<li><a href="{{ url('faq') }}/{{ $item->category }}-{{ isset($item->common) ? 'servis-v-celom' : $faq_category  }}/{{ $item->id }}-{{ $item->slug }}" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ trans('messages.' . $item->title) }}</a></li>
							@endforeach
						</ul>

						<a href="{{ url('faq') }}" class="more-faq" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ trans('messages.other_issues') }}</a>

						<div class="clearfix"></div>
					</div>
					@endif
				</aside>
			</div>
		</div>
		@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
			[
				'title' => trans('messages.main_page'),
				'url' => url('/'),
			],
			[
                'title' => trans('messages.' . $data->title),
				'url' => '',
			],
		]])
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection

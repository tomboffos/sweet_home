@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.staff_recruitment') }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.staff_recruitment') }}</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page faq-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
				@if(isset($data) && count($data))
					<p>{{ trans('messages.choose_your_interest') }}:</p>
					<ul>
					    @foreach($data as $item)
						    <li><a href="{{ url('workers') }}/{{ $item->id }}-{{ $item->slug }}">{{ trans('messages.' . $item->title) }}</a></li>
					    @endforeach
					</ul>
				@endif
				</div>
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li class="active"><a href="{{ url('workers') }}">{{ trans('messages.staff_recruitment') }}</a></li>
							@foreach($data as $item)
								<li><a href="{{ url('workers') }}/{{ $item->id }}-{{ $item->slug }}">{{ trans('messages.' . $item->title) }}</a></li>
							@endforeach
						</ul>
					</div>
				</aside>
			</div>
		</div>
		@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
			[
				'title' => trans('messages.main_page'),
				'url' => url('/'),
			],
			[
				'title' => trans('messages.staff_recruitment'),
				'url' => '',
			],
		]])
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
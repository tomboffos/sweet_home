@extends(
    'front.layout',
    [
        'meta' => (object) [
            'meta_title' => !empty(trans('cook.meta_title')) ? trans('cook.meta_title') : ($meta ? $meta->meta_title : false),
            'meta_keywords' => !empty(trans('cook.meta_keywords')) ? trans('cook.meta_keywords') : ($meta ? $meta->meta_keywords : false),
            'meta_description' => !empty(trans('cook.meta_description')) ? trans('cook.meta_description') : ($meta ? $meta->meta_description : false),
        ]
    ]
)

@section('content')

<section id="scroll-here" class="page-top workers__top">
	<div class="workers__slide">
		<img src="{{ asset('assets/front/img/workers/cook/slide.jpg') }}" alt="" class="workers__slide-img">

		<div class="workers__slide-desc">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="workers__slide-ttl">{!! trans('cook.slide_ttl') !!}</div>
						<a href="#" class="btn-main btn-main-yellow workers__btn js_scroll-to" data-scroll="#order-worker">
							{{ trans('cook.slide_btn') }}
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.' . $data->title) }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.' . $data->title) }}</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<main class="content page order-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-12">
				<div class="entry-text">
					<div class="workers__intro">
						<img src="{{ asset('assets/front/img/lp-logo.jpg') }}" alt="" class="workers__intro-logo">

						<div class="workers__ttl text-center">
							{!! trans('cook.intro_ttl') !!}
						</div>

						<div class="workers__intro-txt text-center">
							<p>{!! trans('cook.intro_txt') !!}</p>
						</div>

						<a href="#"
						   class="btn-main btn-main-yellow workers__btn workers__intro-btn js_scroll-to" data-scroll="#order-worker">
							{{ trans('cook.intro_btn') }}
						</a>
					</div>

					<div class="workers__cols">
						<div class="workers__cols-i">
							<div class="workers__cols-ttl">
								{!! trans('cook.cols_1_ttl') !!}
							</div>
							<div class="workers__cols-price">
								{!! trans('cook.cols_1_price') !!}
							</div>
							<div class="workers__cols-img">
								<img src="{{ asset('assets/front/img/workers/cook/cook_1.jpg') }}"
									 alt="" class="workers__cols-image">
							</div>
							<div class="workers__cols-desc-ttl">
								{!! trans('cook.cols_1_desc_ttl') !!}
							</div>
							<ul class="workers__cols-desc-lst">
								{!! trans('cook.cols_1_desc_txt') !!}
							</ul>
						</div>

						<div class="workers__cols-i">
							<div class="workers__cols-ttl">
								{!! trans('cook.cols_2_ttl') !!}
							</div>
							<div class="workers__cols-price">
								{!! trans('cook.cols_2_price') !!}
							</div>
							<div class="workers__cols-img">
								<img src="{{ asset('assets/front/img/workers/cook/cook_2.jpg') }}"
									 alt="" class="workers__cols-image">
							</div>
							<div class="workers__cols-desc-ttl">
								{!! trans('cook.cols_2_desc_ttl') !!}
							</div>
							<ul class="workers__cols-desc-lst">
								{!! trans('cook.cols_2_desc_txt') !!}
							</ul>
						</div>

					</div>

					<div class="workers__cols">
						<a href="#"
						   class="btn-main btn-main-yellow workers__btn workers__cols-btn js_scroll-to" data-scroll="#order-worker">
							{{ trans('cook.cols_btn') }}
						</a>
					</div>

					<div class="workers__team">
						<div class="workers__ttl workers__team-ttl text-center">
							{!! trans('cook.team_ttl') !!}
						</div>

						<div class="workers__team-cnt">
							<div class="workers__team-i">
								<div class="workers__team-ic">
									<i class="fa fa-utensils workers__team-ic-fa"></i>
								</div>
								<div class="workers__team-txt">
									{!! trans('cook.team_txt_1') !!}
								</div>
							</div>

							<div class="workers__team-i">
								<div class="workers__team-ic">
									<i class="fa fa-pizza-slice workers__team-ic-fa"></i>
								</div>
								<div class="workers__team-txt">
									{!! trans('cook.team_txt_2') !!}
								</div>
							</div>

							<div class="workers__team-i">
								<div class="workers__team-ic">
									<i class="fa fa-carrot workers__team-ic-fa"></i>
								</div>
								<div class="workers__team-txt">
									{!! trans('cook.team_txt_3') !!}
								</div>
							</div>

							<div class="workers__team-i">
								<div class="workers__team-ic">
									<i class="fa fa-comment workers__team-ic-fa"></i>
								</div>
								<div class="workers__team-txt">
									{!! trans('cook.team_txt_4') !!}
								</div>
							</div>

							<div class="workers__team-i">
								<div class="workers__team-ic">
									<i class="fa fa-passport workers__team-ic-fa"></i>
								</div>
								<div class="workers__team-txt">
									{!! trans('cook.team_txt_5') !!}
								</div>
							</div>

							<div class="workers__team-i">
								<div class="workers__team-ic">
									<i class="fa fa-users workers__team-ic-fa"></i>
								</div>
								<div class="workers__team-txt">
									{!! trans('cook.team_txt_6') !!}
								</div>
							</div>

							<div class="workers__team-i">
								<div class="workers__team-ic">
									<i class="fa fa-check-double workers__team-ic-fa"></i>
								</div>
								<div class="workers__team-txt">
									{!! trans('cook.team_txt_7') !!}
								</div>
							</div>
						</div>
					</div>

					<div class="workers__steps">
						<div class="workers__ttl workers__steps-ttl text-center">
							{{ trans('cook.steps_ttl') }}
						</div>

						<div class="workers__steps-cnt">
							<div class="workers__steps-i">
								<div class="workers__steps-i-ttl">
									{!! trans('cook.steps_ttl_1') !!}
								</div>
								<div class="workers__steps-i-txt">
									{!! trans('cook.steps_txt_1') !!}
								</div>
							</div>

							<div class="workers__steps-i">
								<div class="workers__steps-i-ttl">
									{!! trans('cook.steps_ttl_2') !!}
								</div>
								<div class="workers__steps-i-txt">
									{!! trans('cook.steps_txt_2') !!}
								</div>
							</div>

							<div class="workers__steps-i">
								<div class="workers__steps-i-ttl">
									{!! trans('cook.steps_ttl_3') !!}
								</div>
								<div class="workers__steps-i-txt">
									{!! trans('cook.steps_txt_3') !!}
								</div>
							</div>

							<div class="workers__steps-i">
								<div class="workers__steps-i-ttl">
									{!! trans('cook.steps_ttl_4') !!}
								</div>
								<div class="workers__steps-i-txt">
									{!! trans('cook.steps_txt_4') !!}
								</div>
							</div>

							<div class="workers__steps-i">
								<div class="workers__steps-i-ttl workers__steps-i-ttl-last">
									{!! trans('cook.steps_ttl_5') !!}
								</div>
								<div class="workers__steps-i-txt">
									{!! trans('cook.steps_txt_5') !!}
								</div>
							</div>
						</div>

						<a href="#" class="btn-main btn-main-yellow workers__btn workers__steps-btn js_scroll-to" data-scroll="#order-worker">
							{{ trans('cook.steps_btn') }}
						</a>
					</div>

					<div class="workers__why">
						<div class="workers__ttl workers__why-ttl text-center">
							{!! trans('cook.why_ttl') !!}
						</div>

						<ul class="workers__why-lst">
							<li class="workers__why-i">
								<div class="workers__why-ic">
									<i class="fa fa-check-circle workers__why-ic-fa"></i>
								</div>
								<div class="workers__why-txt">
									{!! trans('cook.why_txt_1') !!}
								</div>
							</li>

							<li class="workers__why-i">
								<div class="workers__why-ic">
									<i class="fa fa-check-circle workers__why-ic-fa"></i>
								</div>
								<div class="workers__why-txt">
									{!! trans('cook.why_txt_2') !!}
								</div>
							</li>

							<li class="workers__why-i">
								<div class="workers__why-ic">
									<i class="fa fa-check-circle workers__why-ic-fa"></i>
								</div>
								<div class="workers__why-txt">
									{!! trans('cook.why_txt_3') !!}
								</div>
							</li>
						</ul>
					</div>

					<div class="clearfix"></div>

					<form id="order-worker" class="order-form workers__form">
						<h2>{{ trans('cook.form_ttl') }}</h2>
						<input type="hidden" name="id" value="{{ $data->id }}">
						<input type="hidden" name="type" value="2">
						<div class="row choose-block common-inputs">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.contacts') }}</div>
								<div class="inline">
									<div class="item">
										<input type="text" required name="name" value="{{ $user->name }}" placeholder="{{ trans('messages.name') }}">
									</div>
									<div class="item">
										<input type="text" required name="phone" value="{{ $user->phone }}" placeholder="{{ trans('messages.phone') }}">
									</div>
								</div>
								<p></p>

								<div class="inline">
									<div class="item">
										<input type="email" name="email" placeholder="Email" value="{{ $user->email }}">
									</div>
									@if(!Auth::user())
									<div class="auth-field item" style="display: none;">
										<input type="password" name="password" placeholder="{{ trans('messages.password') }}">
									</div>
									<div class="auth-field item" style="display: none;">
										<span class="ajax-auth btn btn-success fast-login">Войти</span>
									</div>
									@endif
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.preferred_cuisine') }}</div>
								<div class="inline">
									<div class="item">
										<select name="requirements" id="requirements" required>
											<option value="домашняя">{{ trans('messages.home_cuisine') }}</option>
											<option value="диетическое питание">{{ trans('messages.diet_food') }}</option>
											<option value="восточная">{{ trans('messages.eastern') }}</option>
											<option value="европейская">{{ trans('messages.european') }}</option>
											<option value="другое">{{ trans('messages.other') }}</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.work_schedule') }}</div>
								<div class="inline">
									<div class="item">
										<select name="schedule" id="schedule" required>
											<option value="пятидневка">{{ trans('messages.five_days') }}</option>
											<option value="3 раза в неделю">{{ trans('messages.3_days') }}</option>
											<option value="1 раз в неделю">{{ trans('messages.1_days') }}</option>
											<option value="другой график">{{ trans('messages.another_schedule') }}</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">{{ trans('messages.comments') }}</div>
								<textarea name="text"></textarea>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-5">
								<div class="title">{{ trans('messages.add_promo_code') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" name="discount">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<button type="button" class="add-btn send-order-workers">{{ trans('messages.send') }}</button>
							</div>
						</div>
					</form>

					<div class="order-result"></div>
				</div>
			</div>
		</div>

		@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
			[
				'title' => trans('messages.main_page'),
				'url' => url('/'),
			],
			[
				'title' => trans('messages.' . $data->title),
				'url' => '',
			],
		]])
	</div>

	@include('front.footer-skills')

</main>

@endsection

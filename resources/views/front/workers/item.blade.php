@extends('front.layout')

@section('content')

<section id="scroll-here" class="page-top">

	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{  trans('messages.' . $data->title) }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{  trans('messages.' . $data->title) }}</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<main class="content page order-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
				<div class="entry-typography entry-typography-top">@if (!App::isLocale('ru')) @if (!App::isLocale('ru')) {!! trans('messages.' . $data->text) !!} @else {!! $data->text !!} @endif @else {!! $data->text !!} @endif</div>

					<div class="clearfix"></div>

					<form id="order-worker" class="order-form">

						<h2>{{ trans('messages.submit_application') }}</h2>

						<input type="hidden" name="id" value="{{ $data->id }}">

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">Описание заявки</div>
								<label class="workers-labels">расскажите нам о ваших потребностях и перечислите обязанности работника</label>
								<textarea name="text"></textarea>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">Требования к кандидату</div>
								<label class="workers-labels">предпочтительные возраст, национальность, опыт работы, знание языков, семейное положение и др.</label>
								<textarea name="requirements"></textarea>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">График работы</div>
								<label class="workers-labels">укажите график работы, например: с 9:00 до 18:00, Пт-Сб, 13:00-14:00 - обеденный перерыв</label>
								<textarea name="schedule"></textarea>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-9">
								<div class="title">Предполагаемая заработная плата</div>
								<div class="inline">
									<div class="item">
										<input type="text" name="wage">
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.contacts') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" name="name" value="{{ $user->name }}" placeholder="{{ trans('messages.name') }}">
									</div>
									<div class="item">
										<input type="text" name="phone" value="{{ $user->phone }}" placeholder="{{ trans('messages.phone') }}">
									</div>
								</div>
								<p></p>

								<div class="inline">
									<div class="item">
										<input type="email" name="email" placeholder="Email" value="{{ $user->email }}">
									</div>
									@if(!Auth::user())
									<div class="auth-field item" style="display: none;">
										<input type="password" name="password" placeholder="{{ trans('messages.password') }}">
									</div>
									<div class="auth-field item" style="display: none;">
										<span class="ajax-auth btn btn-success fast-login">Войти</span>
									</div>
									@endif
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.address') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" name="city" placeholder="{{ trans('messages.almaty') }}" disabled>
									</div>
									<div class="item">
										<input type="text" name="street" value="{{ $user->street }}" placeholder="Улица">
									</div>
								</div>
								<p></p>
								<div class="inline">
									<div class="item">
										<input type="text" name="complex" value="{{ $user->complex }}" placeholder="Жилой комплекс">
									</div>
									<div class="item">
										<input type="text" name="house" value="{{ $user->house }}" placeholder="Дом">
									</div>
								</div>
								<p></p>
								<div class="inline">
									<div class="item">
										<input type="text" name="housing" value="{{ $user->housing }}" placeholder="Корпус">
									</div>
									<div class="item">
										<input type="text" name="flat" value="{{ $user->flat }}" placeholder="Квартира">
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-5">
								<div class="title">{{ trans('messages.add_promo_code') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" name="discount">
									</div>
								</div>
							</div>

							<div class="col-md-5 points" @if(!Auth::user()) style="display: none;" @endif>
								<div class="title">{{ trans('messages.apply_points') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" name="points">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<button type="button" class="add-btn send-order-workers">{{ trans('messages.send') }}</button>
							</div>
						</div>
					</form>

					<div class="order-result"></div>
				</div>
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li><a href="{{ url('workers') }}">{{ trans('messages.staff_recruitment') }}</a></li>
							@foreach($categories as $item)
								<li @if($data->id === $item->id) class="active" @endif><a href="{{ url('workers') }}/{{ $item->id }}-{{ $item->slug }}">{{ trans('messages.' . $item->title) }}</a></li>
							@endforeach
						</ul>
					</div>
					@if(isset($faq) && $faq && count($faq) && App::isLocale('ru'))
					<div class="block faq-sidebar">
						<div class="title">{{ trans('messages.q_n_a') }}</div>

						<ul>
							@foreach($faq as $item)
							<li><a href="{{ url('faq') }}/{{ $item->category }}-{{ isset($item->common) ? 'servis-v-celom' : $faq_category  }}/{{ $item->id }}-{{ $item->slug }}" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ trans('messages.' . $item->title) }}</a></li>
							@endforeach
						</ul>

						<a href="{{ url('faq') }}" class="more-faq" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ trans('messages.other_issues') }}</a>

						<div class="clearfix"></div>
					</div>
					@endif
				</aside>
			</div>
		</div>

		@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
			[
				'title' => trans('messages.main_page'),
				'url' => url('/'),
			],
			[
				'title' => trans('messages.' . $data->title),
				'url' => '',
			],
		]])
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection

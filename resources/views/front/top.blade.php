@if (App::isLocale('kz'))
<style>
.title, b, .expand-block, a, .sub-title {
	font-family: Calibri !important;
	font-family: Calibri !important;
	font-weight: 600 !important;	
}
</style>
@endif
@if($banners && isset($banners['top']['status']) && (int) $banners['top']['status'] === 1)
	<section class="banner-top"
			 @if(!empty($banners['top']['banner'])) style="background-image: url('/data/banners/{{ $banners['top']['banner'] }}');" @endif>
		@if(!empty($banners['top']['url']))
			<a href="{{ $banners['top']['url'] }}" class="overlay"></a>
		@endif
		<div class="container">
			<div class="row">
				<div class="col-md-12 short">
					@if(!empty($banners['top']['title']))
						{!! $banners['top']['title'] !!}
					@endif
				</div>
			</div>
		</div>
	</section>
@endif

<header class="header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-6">
				<a href="{{ url('/') }}" class="logo"></a>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-5 contacts">
				<div class="header-col">
					<p>{!! trans('messages.full_address') !!}</p>
					<p>{!! trans('messages.working_hours') !!}</p>
					<p><b>{{ trans('messages.email') }}</b> <a href="mailto:info@sweet-home.asia" class="header-email">info@sweet-home.asia</a></p>
				</div>
				<div class="header-col">
					<p><b>{{ trans('messages.phones') }}:</b></p>
					@if(isset($phones) && count($phones))
						@foreach($phones as $phone)
						<p><a href="tel:{{ $phone }}" class="header-phone">{{ $phone }}</a></p>
						@endforeach
					@endif
				</div>
				{{-- <a class="call-me" data-toggle="modal" data-target="#modalCallMe">Обратный звонок</a> --}}
			</div>
			<div class="col-md-4 col-sm-6 col-xs-6">
				<nav class="top-menu">
                    @if (\App\Http\Controllers\Front\CommonController::showLangPanel())
                    <ul class="language-switcher-locale-url">
                        <li class="ru first @if (App::isLocale('ru')) active @endif">
                            <a href="/setlocale/ru" class="language-link" xml:lang="ru">Рус</a>
                        </li>
                        <li class="kz @if (App::isLocale('kz')) active @endif">
                            <a href="/setlocale/kz" class="language-link" xml:lang="kz">Қаз</a>
                        </li>
                        <li class="en last @if (App::isLocale('en')) active @endif">
                            <a href="/setlocale/en" class="language-link" xml:lang="en">Eng</a>
                        </li>
                    </ul>
                    @endif
                    @if (App::isLocale('ru'))
					<ul class="side-menu">
                        <li><a href="{{ url('pages/4-o-nas') }}">О нас</a></li>
						<li><a href="{{ url('faq') }}">{{ trans('messages.q_n_a') }}</a></li>
						@if(Auth::user())
						    <li class="dropdown">
							    <a href="#"
							       class="dropdown-toggle"
							       data-toggle="dropdown"
							       role="button"
							       aria-haspopup="true"
							       aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							    <ul class="dropdown-menu">
								    <li><a href="{{ url('user') }}">Информация</a></li>
								    <li><a href="{{ url('user/orders') }}">Заказы</a></li>
								    <li><a href="{{ url('user/points') }}">Баллы</a></li>
								    <li><a href="{{ url('user/favorite') }}">Избранное</a></li>
								    <li><a href="{{ url('user/black-list') }}">Черный список</a></li>
								    <li><a href="{{ url('logout') }}">Выйти</a></li>
							    </ul>
						    </li>
						@else
						    <li><a data-toggle="modal" data-target="#modalAuth">Войти</a></li>
						@endif
					</ul>
                    @endif
				</nav>
				<div class="mobile-view">
					<div class="mobile-contacts">
					    @if(isset($phones) && count($phones))
						    <a href="tel:{{ $phones[0] }}">{{ $phones[0] }}</a>
					    @endif
					</div>
                    @if (\App\Http\Controllers\Front\CommonController::showLangPanel())
                    <ul class="language-switcher-locale-url">
                        <li class="ru first @if (App::isLocale('ru')) active @endif">
                            <a href="/setlocale/ru" class="language-link" xml:lang="ru">Рус</a>
                        </li>
                        <li class="kz @if (App::isLocale('kz')) active @endif">
                            <a href="/setlocale/kz" class="language-link" xml:lang="kz">Қаз</a>
                        </li>
                        <li class="en last @if (App::isLocale('en')) active @endif">
                            <a href="/setlocale/en" class="language-link" xml:lang="en">Eng</a>
                        </li>
                    </ul>
                    @endif
                    @if (App::isLocale('ru'))
					<div id="toggle-menu"></div>
                    @endif
				</div>
			</div>
		</div>
	</div>
	<div class="mobile-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    @if (App::isLocale('ru'))
					<ul>
						<li><a href="{{ url('pages/4-o-nas') }}">О нас</a></li>
						<li><a href="{{ url('faq') }}">{{ trans('messages.q_n_a') }}</a></li>
						@if(Auth::user())
						<li class="dropdown">
							<a href="#"
							   class="dropdown-toggle"
							   data-toggle="dropdown"
							   role="button"
							   aria-haspopup="true"
							   aria-expanded="false">Кабинет <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="{{ url('user') }}">Информация</a></li>
								<li><a href="{{ url('user/orders') }}">Заказы</a></li>
								<li><a href="{{ url('user/points') }}">Баллы</a></li>
								<li><a href="{{ url('user/favorite') }}">Избранное</a></li>
								<li><a href="{{ url('user/black-list') }}">Черный список</a></li>
								<li><a href="{{ url('logout') }}">Выйти</a></li>
							</ul>
						</li>
						@else
						<li><a data-toggle="modal" data-target="#modalAuth">Войти</a></li>
						@endif
					</ul>
                    @endif
				</div>
			</div>
		</div>
	</div>
</header>


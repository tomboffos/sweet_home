@extends('front.layout')

@section('content')

<section class="page-top">

	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.' . $data->title) }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.' . $data->title) }}</li>
				</ul>
			</div>
		</div>
	</div>

</section>

<main class="content page">
	<div class="container entry-content">
		<div class="row">
			<div class="col-md-12">
				<div class="entry-text">
					@if (!App::isLocale('ru')) {!! trans('messages.' . $data->text) !!} @else {!! $data->text !!} @endif
				</div>
			</div>
		</div>
		@if($data->title=='Бонусы и акции')
		<div class="row">
			<div class="col-lg-12 inw">
				<!-- Горизонтальная ориентация -->
                <iframe src='/inwidget/index.php?adaptive=true' data-inwidget scrolling='no' frameborder='no' style='border:none;width:100%;height:315px;overflow:hidden;'></iframe> 
			</div>
		</div>
		@endif

		@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
			[
				'title' => trans('messages.main_page'),
				'url' => url('/'),
			],
			[
				'title' => trans('messages.' . $data->title),
				'url' => '',
			],
		]])
	</div>

	@include('front.footer-skills')

</main>

@endsection
@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>FAQ: {{  trans('messages.' . $data->title) }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li><a href="{{ url('faq') }}">FAQ</a></li>
					<li><a href="{{ url('faq') }}/{{ $category->id }}-{{ $category->category }}">{{ $category->title }}</a></li>
					<li>{{  trans('messages.' . $data->title) }}</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page faq-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					<div class="question-block">
						<div class="question">
							<p>{{  trans('messages.' . $data->title) }}</p>
							<span class="open-answer">Ответ</span>
						</div>

						<div class="answer-block">
							<div class="answer">
								@if (!App::isLocale('ru')) {!! trans('messages.' . $data->text) !!} @else {!! $data->text !!} @endif
							</div>
						</div>
					</div>				
				</div>

				@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
					[
						'title' => trans('messages.main_page'),
						'url' => url('/'),
					],
					[
						'title' => 'FAQ',
						'url' => url('faq'),
					],
					[
						'title' => $category->title,
						'url' => url('faq') . '/' . $category->id . '-' . $category->category,
					],
					[
						'title' => trans('messages.' . $data->title),
						'url' => '',
					],
				]])
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
						@if(isset($categories) && !empty($categories))
							@foreach($categories as $category)
							<li @if($cat_slug == $category->slug) class="active" @endif><a href="{{ url('faq') }}/{{ $category->id }}-{{ $category->slug }}">{{ $category->title }}</a></li>
							@endforeach
						@endif
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>FAQ: {{ $category->title }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li><a href="{{ url('faq') }}">FAQ</a></li>
					<li>{{ $category->title }}</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page faq-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					@if(isset($data) && $data && count($data))
						@foreach($data as $item)
						<div class="question-block">
							<div class="question">
								<p>{{ trans($item->title) }}</p>
								<span class="open-answer">Ответ</span>
							</div>

							<div class="answer-block">
								<div class="answer">
									{!! $item->text !!}
								</div>
							</div>
						</div>
						@endforeach

						{!! $data->render() !!}

					@else

						<p>Записей нет.</p>

					@endif					
				</div>
				@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
					[
						'title' => trans('messages.main_page'),
						'url' => url('/'),
					],
					[
						'title' => 'FAQ',
						'url' => url('faq'),
					],
					[
						'title' => $category->title,
						'url' => '',
					],
				]])
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
						@if(isset($categories) && !empty($categories))
							@foreach($categories as $_category)
							<li @if($slug == $_category->slug) class="active" @endif><a href="{{ url('faq') }}/{{ $_category->id }}-{{ $_category->slug }}">{{ $_category->title }}</a></li>
							@endforeach
						@endif
						</ul>
					</div>
				</aside>
			</div>
		</div>

	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>FAQ</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>FAQ</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->
<section class="about">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">Отвечаем на Ваши вопросы</div>
				</div>
			</div>
			<div class="row">
				<div class="video-item col-md-3 col-sm-6">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/WSIv3MwQzsU?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-md-3 col-sm-6">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/RgdQTU6ALbw?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-md-3 col-sm-6">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/TbqXIiqoPnY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-md-3 col-sm-6">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/kwU9SXEu7r8?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
			</div>
			<div class="row">
				<div class="video-item col-sm-4">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/boAB3knfAN4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-sm-4">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/tRvjveLhejw?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-sm-4">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/cybSejgCF04?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
			</div>
		</div>
	</section>
<main class="content page faq-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					@if(isset($data) && !empty($data))
					@foreach($data as $item)
					<div class="question-block">
						<div class="question">
							<p>{{ $item->title }}</p>
							<span class="open-answer">Ответ</span>
						</div>

						<div class="answer-block">
							<div class="answer">
								{!! $item->text !!}
							</div>
						</div>
					</div>
					@endforeach

					{!! $data->render() !!}

					@else

					<p>Записей нет.</p>

					@endif					
				</div>
				@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
					[
						'title' => trans('messages.main_page'),
						'url' => url('/'),
					],
					[
						'title' => 'FAQ',
						'url' => '',
					],
				]])
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
						@if(isset($categories) && !empty($categories))
							@foreach($categories as $category)
							<li><a href="{{ url('faq') }}/{{ $category->id }}-{{ $category->slug }}">{{ $category->title }}</a></li>
							@endforeach
						@endif
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.cleaner') }}: {{ $data->name }}</h1>

				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li><a href="{{ url('/cleaners') }}">{{ trans('messages.cleaners') }}</a></li>
					<li>{{ trans('messages.cleaner') }}: {{ $data->name }}</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page">

	<div class="container entry-content">
		<div class="entry-text cleaner-item">
			<div class="row">
				@if(!empty($data->img))
					<div class="col-md-3">
						<div class="img">
							<img src="{{ url('/data/cleaners/'.$data->id.'/'.$data->img) }}" alt="">
						</div>
					</div>
				@endif
				<div class="@if(!empty($data->img)) col-md-6 @else col-md-9 @endif">
					<h4>{{ $data->name }}</h4>
					@if($rating)
					<div class="raiting raiting-{{ $rating }}" data-toggle="popover" title="Текущий рейтинг" data-content="Усредненный рейтинг: {{ $rating }}"></div>
					@endif
					<p>Количество выполненных заказов: <b>{{ $count }}</b></p>
				</div>
				<div class="col-md-3">
				@if($orders)
					@if(!count($in_favorite) && !count($in_black_list))
					<span 
						class="add-btn cleaner-action add-cleaner-favorite" 
						data-action="add-favorite"
						data-cleaner="{{ $data->id }}"
					>В избранное</span>
					<span 
						class="add-btn cleaner-action add-cleaner-black-list" 
						data-action="add-black-list"
						data-cleaner="{{ $data->id }}"
					>В черный список</span>
					@endif
					
					@if(count($in_black_list))
					<span 
						class="add-btn cleaner-action remove-cleaner-black-list" 
						data-action="remove-black-list"
						data-cleaner="{{ $data->id }}"
					>Убрать из черного списка</span>
					@elseif(count($in_favorite))
					<span 
						class="add-btn cleaner-action remove-cleaner-favorite" 
						data-action="remove-favorite"
						data-cleaner="{{ $data->id }}"
					>Убрать из избранного</span>
					@endif
				@endif
				</div>
			</div>
			
			@if(!empty($data->text))
			<div class="row">
				<div class="col-md-12">
					<p></p>
						<h4>О клинере:</h4>
						@if (!App::isLocale('ru')) {!! trans('messages.' . $data->text) !!} @else {!! $data->text !!} @endif
				</div>
			</div>
			@endif
			
			@if(count($testimonals))
			<div class="row testionals-page">
				<div class="col-md-12">
					<p></p>
					<h4>Отзывы:</h4>
					
					@foreach($testimonals as $testimonal)
					<div class="item">
						<span class="autor">{{ $testimonal->name }}</span>
						<div class="short">
						{!! $testimonal->text !!}
						</div>
					</div>
					@endforeach
					
					{!! $testimonals->render() !!}
				</div>
			</div>
			@endif
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
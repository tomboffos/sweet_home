@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.cleaners') }}</h1>

				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.cleaners') }}</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page">

	<div class="container entry-content">
		<div class="entry-text">
			<div class="row testionals-page cleaners-page">
				<div class="col-md-12">
					@if(count($data))
						@foreach($data as $cleaner)
							
						<div class="item">
							<span class="autor">{{ $cleaner->name }}</span>
							<div class="short">
								{!! mb_strlen($cleaner->text) > 200 ? mb_substr($cleaner->text, 0, 200) . '...' : $cleaner->text !!}
								<p><a href="{{ url('/cleaners') }}/{{ $cleaner->id }}" class="more">Подробнее</a></p>
							</div>
						</div>

						@endforeach

						{!! $data->render() !!}

					@else
					<p>Нет данных для отображения.</p>
					@endif
				</div>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
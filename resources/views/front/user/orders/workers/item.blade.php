@extends('front.layout')

@section('content')

<section class="page-top">

	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>Заказ на подбор персонала №{{ $data->id }}</h1>

				<ul>
					<li><a href="{{ url('/') }}">Главная</a></li>
					<li><a href="{{ url('user') }}">Личный кабинет</a></li>
					<li><a href="{{ url('user/orders') }}">Заказы</a></li>
					<li><a href="{{ url('user/orders/workers') }}">Заказы на подбор персонала</a></li>
					<li>Заказ №{{ $data->id }}</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page user-page order-details">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					<h5>Основная информация</h5>

					<p>Специальность: <a href="{{ url('workers') }}/{{ $profession->id }}-{{ $profession->slug }}">{{ $profession->name }}</a></p>
					<p><br></p>

					<p><b>Описание</b></p>
					<p>@if (!App::isLocale('ru')) {!! trans('messages.' . $data->text) !!} @else {!! $data->text !!} @endif</p>
					<p><br></p>

					<p><b>
						@if($data->type == 0)
						trans('messages.child_age')
						@elseif($data->type == 1)
						trans('messages.about_ward')
						@elseif($data->type == 2)
						trans('messages.preferred_cuisine')
						@elseif($data->type == 3)
						trans('messages.apartment_area')
						@else
						Требования к кандидату
						@endif
					</b></p>
					<p>{!! $data->requirements !!}</p>
					<p><br></p>

					<p><b>График работы</b></p>
					<p>{!! $data->schedule !!}</p>
					<p><br></p>

					@if(!empty($data->address))
					<p><b>Адрес</b></p>
					<p>{!! $data->address !!}</p>
					<p><br></p>
					@endif

					@if(!empty($data->wage))
					<p><b>Заработная плата</b></p>
					<p>{!! $data->wage !!}</p>
					@endif

			        <hr>

			        <h5>Информация об оплате</h5>

					<div class="table-responsive">
			            <p>Способ оплаты:
				            @if(isset($payment_methods) && count($payment_methods))
	                        @foreach($payment_methods as $type => $pay_method)
	                            @if($type === $data->payment_type) {{ $pay_method }} @endif
	                        @endforeach
	                        @endif
			            </p>

			            <p>Статус оплаты:
				            @if($data->payment_status === 1)
		                    	Оплачен
		                    @else
		                    	В ожидании
		                    @endif
			            </p>

			            <p>Использованный промокод:
			            	@if(isset($discount) && !empty($discount))
			            		{{ $discount->code }} (
									@if($discount->type === 0)
										- {{ $discount->price*100 }} %
									@else
										- {{ number_format($discount->price, 0, '', ' ') }} тг.
									@endif
			            		)
							@else
							нет
			            	@endif
			            </p>

			            <p>Использованные баллы:
			            	@if(isset($data->points) && !empty($data->points))
			            		{{ $data->points }}
							@else
							нет
			            	@endif
			            </p>
			        </div>

			        <hr>

			        <h5>Статус заказа</h5>

					<div class="table-responsive">
			            <p>Текущий статус заказа:
	                    @if($data->status === 0)
	                        {{ $order_status[$data->status] }}
	                    @elseif($data->status === 1)
	                        {{ $order_status[$data->status] }}
	                    @elseif($data->status === 2)
	                        {{ $order_status[$data->status] }}
	                    @elseif($data->status === 3)
	                        {{ $order_status[$data->status] }}
	                    @endif
	                    </p>
			        </div>

			        @if($data->status === 2)

			        	@if(!$testimonal || empty($testimonal))
					        <hr>
					        <h5>Добавить отзыв</h5>

							<form action="{{ url('user/orders/workers') }}/{{ $data->id }}" method="post" id="add-testimonal" class="common-inputs add-testimonal">

								{{ csrf_field() }}

								<textarea name="text" id="text" rows="3" placeholder="Напишите ваш отзыв">{{ old('text') }}</textarea>

								<p></p>

								<div class="row">
									<div class="col-md-12">
										<button type="button" class="add-btn add-testimonal-btn">{{ trans('messages.send') }}</button>
									</div>
								</div>

							</form>

						@else

							<hr>
							<h5>Ваш отзыв к заказу</h5>

							@if($testimonal->status === 1)

								<p>Отзыв:</p>
								<p>{!! $testimonal->text !!}</p>

							@else
								<p>Ваш отзыв на модерации.</p>
							@endif

						@endif

			        @endif

				</div>
			</div>

			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li><a href="{{ url('user') }}">Личная информация</a></li>
							<li class="active"><a href="{{ url('user/orders') }}">Заказы</a></li>
							<li><a href="{{ url('user/points') }}">Баллы</a></li>
							<li><a href="{{ url('user/favorite') }}">Избранное</a></li>
							<li><a href="{{ url('user/black-list') }}">Черный список</a></li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection

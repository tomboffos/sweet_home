@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>Заказы на уборку</h1>

				<ul>
					<li><a href="{{ url('/') }}">Главная</a></li>
					<li><a href="{{ url('user') }}">Личный кабинет</a></li>
					<li><a href="{{ url('user/orders') }}">Заказы</a></li>
					<li>Заказы на уборку</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page user-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					@if(isset($data) && $data && count($data))
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
			                    <thead>
			                        <tr>
			                            <th>Заказ</th>
			                            <th>Дата и время уборки</th>
			                            <th>Способ оплаты</th>
			                            <th>Статус оплаты</th>
			                            <th>Статус заказа</th>
			                            <th>Добавлен</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    @foreach($data as $k => $item)
			                        <tr>
			                            <td><b><a href="{{ url("user/orders/cleaning/$item->id") }}">Заказ №{{ $item->id }}</a></b></td>

			                            <td style="width: 23%;">
			                            {{ $item->cl_date->format('d F Y') }} {{ $item->cl_time->format('H:i') }}
			                            </td>

			                            <td style="width: 18%;">
			                            @if(isset($payment_methods) && count($payment_methods))
			                            @foreach($payment_methods as $type => $pay_method)
			                                @if($type === $item->payment_type) {{ $pay_method }} @endif
			                            @endforeach
			                            @endif
			                            </td>

			                            <td style="width: 13%;">
			                            @if($item->payment_status === 1)
			                                <span class="label label-success">Оплачен</span>
			                            @else
			                                <span class="label label-warning">В ожидании</span>
			                            @endif
			                            </td>

			                            <td style="width: 10%;">
			                            @if($item->status === 0)
			                                <span class="label label-warning">{{ $order_status[$item->status] }}</span>
			                            @elseif($item->status === 1)
			                                <span class="label label-info">{{ $order_status[$item->status] }}</span>
			                            @elseif($item->status === 2)
			                                <span class="label label-success">{{ $order_status[$item->status] }}</span>
			                            @elseif($item->status === 3)
			                                <span class="label label-danger">{{ $order_status[$item->status] }}</span>
			                            @endif
			                            </td>

			                            <td style="width: 18%;">{!! $item->created_at->format('d F Y') !!} г.</td>
			                        </tr>
			                    @endforeach
			                    </tbody>
			                </table>
						</div>

						{!! $data->render() !!}
					
					@else
						<p>Заказов пока не было.</p>
					@endif
				</div>
			</div>

			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li><a href="{{ url('user') }}">Личная информация</a></li>
							<li class="active"><a href="{{ url('user/orders') }}">Заказы</a></li>
							<li><a href="{{ url('user/points') }}">Баллы</a></li>
							<li><a href="{{ url('user/favorite') }}">Избранное</a></li>
							<li><a href="{{ url('user/black-list') }}">Черный список</a></li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
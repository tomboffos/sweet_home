@extends('front.layout')

@section('content')

<section class="page-top">

	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>Заказ на уборку №{{ $data->id }}</h1>

				<ul>
					<li><a href="{{ url('/') }}">Главная</a></li>
					<li><a href="{{ url('user') }}">Личный кабинет</a></li>
					<li><a href="{{ url('user/orders') }}">Заказы</a></li>
					<li><a href="{{ url('user/orders/cleaning') }}">Заказы на уборку</a></li>
					<li>Заказ №{{ $data->id }}</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page user-page order-details">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					<h5>Основная информация</h5>

					<div class="table-responsive">
			            <table class="table table-striped table-bordered table-hover text-center">
			            	<thead>
			            		<th class="text-center">{{ trans('messages.cleaning_type') }}</th>
			            		<th class="text-center">Количество комнат</th>
			            		<th class="text-center">Количество санузлов</th>
			            		<th class="text-center">Частота</th>
			            		<th class="text-center">Дата</th>
			            		<th class="text-center">Время</th>
			            	</thead>
			            	<tbody>
			            		<tr>
			            			<td>{{ $data->type->name }}</td>
				            		<td>{{ $data->number_rooms }}</td>
				            		<td>{{ $data->number_bathrooms }}</td>
				            		<td>{{ $data->cicle->name }}</td>
				            		<td>{{ $data->cl_date->format('d F Y') }}</td>
				            		<td>{{ $data->cl_time->format('H:i') }}</td>
			            		</tr>
			            	</tbody>
			            </table>
			        </div>

					@if(isset($data->services) && !empty($data->services))
						<hr>
				        <h5>{{ trans('messages.additional_services') }}</h5>

						<div class="table-responsive">
				            <table class="table table-striped table-bordered table-hover text-center">
				            	<thead>
					            	<th class="text-center" style="width: 5%;">№</th>
				            		<th class="text-center">Название</th>
				            		<th class="text-center" style="width: 20%;">Цена</th>
				            		<th class="text-center" style="width: 15%;">Тип</th>
				            		<th class="text-center" style="width: 15%;">Количество</th>
				            	</thead>
				            	<tbody>
				            		<?php $servicesSummary = 0; ?>
				            		@foreach($data->services as $k => $service)
				            		<tr>
				            			<td>{{ ++$k }}</td>
				            			<td>{{ $service->name }}</td>
					            		<td>{{ $service->price }}</td>
					            		<td>
					            			@if($service->pay === 0)
												цена
					            			@elseif($service->pay === 1)
												за ед.
					            			@elseif($service->pay === 2)
												за час
					            			@endif
					            		</td>
					            		<td>{{ $service->unit }}</td>
				            		</tr>
				            		<?php $servicesSummary += $service->unit*$service->price; ?>
				            		@endforeach
				            	</tbody>
				            </table>
				        </div>

						<p>Итого за дополнительные услуги: {{ number_format($servicesSummary, 0, '', ' ') }} тг.</p>

			        @endif

			        <p><b>Итого:</b> {{ number_format($data->total, 0, '', ' ') }} тг.</p>

			        <hr>
					<h5>Назначенный клинер</h5>

			        @if(isset($cleaner) && !empty($cleaner))

						<div class="info-cleaner">
							@if($cleaner->img && !empty($cleaner->id))
								<img src="{{ url('/data/cleaners') }}/{{ $cleaner->id }}/{{ $cleaner->img }}" alt="">
							@endif
							<div class="info-blok">
								<p>Имя: {{ $cleaner->name }}</p>
								<p>О клинере:</p>
								{!! $cleaner->text !!}
								<p></p>
								<a href="{{ url('/cleaners') }}/{{ $cleaner->id }}" title="Откроется в новом окне" target="_blank">Открыть профиль</a>
							</div>
						</div>
					@else
						<p>Клинер пока не назначен.</p>
			        @endif

			        <hr>

			        <h5>Информация об оплате</h5>

					<div class="table-responsive">
			            <p>Способ оплаты:
				            @if(isset($payment_methods) && count($payment_methods))
	                        @foreach($payment_methods as $type => $pay_method)
	                            @if($type === $data->payment_type) {{ $pay_method }} @endif
	                        @endforeach
	                        @endif
			            </p>

			            <p>Статус оплаты:
				            @if($data->payment_status === 1)
		                    	Оплачен
		                    @else
		                    	В ожидании
		                    @endif
			            </p>

			            <p>Использованный промокод:
			            	@if(isset($discount) && !empty($discount))
			            		{{ $discount->code }} (
									@if($discount->type === 0)
										- {{ $discount->price*100 }} %
									@else
										- {{ number_format($discount->price, 0, '', ' ') }} тг.
									@endif
			            		)
							@else
							нет
			            	@endif
			            </p>

			            <p>Использованные баллы:
			            	@if(isset($data->points) && !empty($data->points))
			            		{{ $data->points }}
							@else
							нет
			            	@endif
			            </p>
			        </div>

			        <hr>

			        <h5>Статус заказа</h5>

					<div class="table-responsive">
			            <p>Текущий статус заказа:
	                    @if($data->status === 0)
	                        {{ $order_status[$data->status] }}
	                    @elseif($data->status === 1)
	                        {{ $order_status[$data->status] }}
	                    @elseif($data->status === 2)
	                        {{ $order_status[$data->status] }}
	                    @elseif($data->status === 3)
	                        {{ $order_status[$data->status] }}
	                    @endif
	                    </p>
			        </div>

			        @if($data->status === 2 && isset($cleaner) && !empty($cleaner))

			        	@if(!$testimonal || empty($testimonal))
							<div id="testimonal"></div>
					        <hr>
					        <h5>Добавить отзыв</h5>

							<form action="{{ url('user/orders/cleaning') }}/{{ $data->id }}" method="post" id="add-testimonal" class="common-inputs add-testimonal">

								{{ csrf_field() }}

								<input type="hidden" name="cleaner" value="{{ $cleaner->id }}">

								<div class="radio">
									<label>
										<input type="radio" name="cleaners_action" value="1"> Добавить клинера в избранное
									</label>
								</div>

								<div class="radio">
									<label>
										<input type="radio" name="cleaners_action" value="2"> Добавить клинера в черный список
									</label>
								</div>

								<div class="radio">
									<label>
										<input type="radio" name="cleaners_action" value="0"> Не добавлять
									</label>
								</div>

								<textarea name="text" id="text" rows="3" placeholder="Напишите ваш отзыв">{{ old('text') }}</textarea>

								<p></p>
								<p>Пожалуйста, оцените уборку по параметрам:</p>

								<div class="vote-block">
									<p>Качество уборки:</p>
									<div class="stars">
										<i data-vote="1"></i>
										<i data-vote="2"></i>
										<i data-vote="3"></i>
										<i data-vote="4"></i>
										<i data-vote="5"></i>
										<input type="hidden" name="quality">
									</div>
								</div>

								<div class="vote-block">
									<p>Пунктуальность:</p>
									<div class="stars">
										<i data-vote="1"></i>
										<i data-vote="2"></i>
										<i data-vote="3"></i>
										<i data-vote="4"></i>
										<i data-vote="5"></i>
										<input type="hidden" name="punctuality">
									</div>
								</div>

								<div class="vote-block">
									<p>Приветливость исполнителя:</p>
									<div class="stars">
										<i data-vote="1"></i>
										<i data-vote="2"></i>
										<i data-vote="3"></i>
										<i data-vote="4"></i>
										<i data-vote="5"></i>
										<input type="hidden" name="affability">
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<button type="button" class="add-btn add-testimonal-btn">{{ trans('messages.send') }}</button>
									</div>
								</div>

							</form>

						@else

							<hr>
							<h5>Ваш отзыв к заказу</h5>

							@if($testimonal->status === 1)

								<p>Рейтинг: {{ $testimonal->rating }} балла из 5</p>
								<p>Отзыв:</p>
								<p>{!! $testimonal->text !!}</p>

							@else
								<p>Ваш отзыв на модерации.</p>
							@endif

						@endif

			        @endif

				</div>
			</div>

			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li><a href="{{ url('user') }}">Личная информация</a></li>
							<li class="active"><a href="{{ url('user/orders') }}">Заказы</a></li>
							<li><a href="{{ url('user/points') }}">Баллы</a></li>
							<li><a href="{{ url('user/favorite') }}">Избранное</a></li>
							<li><a href="{{ url('user/black-list') }}">Черный список</a></li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection

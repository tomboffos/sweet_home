@extends('front.layout')

@section('content')

<section class="page-top">
	@include('front.menu')
	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>Привязка карты к аккаунту</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li><a href="{{ url('user') }}">Личный кабинет</a></li>
					<li>Привязка карты к аккаунту</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>

<!-- page-top -->

<main class="content page user-page order-details">
	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					<h5>Привязка карты к аккаунту</h5>
					<p>Нажмите на кнопку и вы будете перенаправлены на сайт Казкоммерцбанка, где сможете привязать карту к сервису.</p>

					@if(isset($data) && !empty($data))
					<form action="{{ $data['action'] }}" id="kazkompay">
						<input type="hidden" name="email" value="{{ $data['email'] }}">
						<input type="hidden" name="BackLink" value="{{ $data['BackLink'] }}">
						<input type="hidden" name="PostLink" value="{{ $data['PostLink'] }}">
						<input type="hidden" name="Language" value="{{ $data['Language'] }}">
						<input type="hidden" name="FailureBackLink" value="{{ $data['FailureBackLink'] }}">
						<input type="hidden" name="Signed_Order_B64" value="{{ $data['xml'] }}">
						<input type="hidden" name="template" value="{{ $data['template'] }}">
						<button type="submit" class="btn btn-success">Привязать карту к аккаунту</button>
					</form>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li class="active"><a href="{{ url('user') }}">Личная информация</a></li>
							<li><a href="{{ url('user/orders') }}">Заказы</a></li>
							<li><a href="{{ url('user/points') }}">Баллы</a></li>
							<li><a href="{{ url('user/favorite') }}">Избранное</a></li>
							<li><a href="{{ url('user/black-list') }}">Черный список</a></li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->

@endsection
@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>Личный кабинет</h1>

				<ul>
					<li><a href="{{ url('/') }}">Главная</a></li>
					<li>Личный кабинет</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->
<script type="text/javascript" src="https://vk.com/js/api/share.js?95" charset="windows-1251"></script>
<main class="content page user-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">

				@if ($errors->any())
		        <ul class="error-list alert alert-danger">
		            @foreach ($errors->all() as $error)
		            <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		        @endif
				
				<form action="{{ url('user') }}" method="post">
				{{ csrf_field() }}
					<div class="row">
						<div class="col-md-8">
							<div class="title">{{ trans('messages.contacts') }}</div>

							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label for="name">Имя</label>
										<input type="text" class="form-control" id="name" required name="name" value="{{ $data->name }}">
									</div>
									<div class="form-group">
										<label for="email">Email</label>
										<input type="email" class="form-control" id="email" required name="email" value="{{ $data->email }}">
									</div>
									<div class="checkbox">
										<label for="subscribe">
										<input type="checkbox" id="subscribe" name="subscribe" @if(!empty($data->subscribe)) checked @endif>
										Подписаться на рассылку
										</label>
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label for="phone">Телефон</label>
										<input type="text" class="form-control" id="phone" required name="phone" value="{{ $data->phone }}">
									</div>
								</div>
							</div>

							<div class="title">Изменить пароль</div>

							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label for="password">Старый пароль</label>
										<input type="password" class="form-control" id="password" name="password">
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label for="new_password">Новый пароль</label>
										<input type="password" class="form-control" id="new_password" name="new_password">
									</div>
									<div class="form-group">
										<label for="password_confirmation">Повторите пароль</label>
										<input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation">
									</div>
								</div>
							</div>

							<div class="title">Платежные данные</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">										
										@if(!isset($cardData->approve) || is_null($cardData->approve))
											<label>Банковская карта не привязана</label>
											<a href="{{ url('user/addcard') }}" class="btn btn-success btn-sm">Привязать карту</a>
										@elseif(isset($cardData->approve) && $cardData->approve === 0)
											<label>Банковская карта успешно привязана.</label>
											<label>{{ isset($cardData->card_mask) ? $cardData->card_mask : '' }}</label>
											<a href="{{ url('user/deletecard') }}" class="btn btn-danger btn-sm">Удалить карту</a>
										@elseif($cardData->approve === 1)
											<label>Банковская карта привязана, но не подтверждена. Необходимо активировать карту заново.</label>
											<label>{{ isset($cardData->card_mask) ? $cardData->card_mask : '' }}</label>
											<a href="{{ url('user/deletecard') }}" class="btn btn-danger btn-sm">Удалить карту</a>
										@endif
									</div>
								</div>
							</div>

							<div class="title">Информация о квартире</div>

							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label for="rooms_number">Кол-во комнат</label>
										<input type="text" class="form-control" id="rooms_number" name="rooms_number" value="{{ $data->rooms_number }}">
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label for="bath_rooms_number">Кол-во санузлов</label>
										<input type="text" class="form-control" id="bath_rooms_number" name="bath_rooms_number" value="{{ $data->bath_rooms_number }}">
									</div>
								</div>
							</div>

							<div class="title">Адрес</div>

							<div class="row">
								<div class="col-md-5">
									<div class="form-group disabled">
										<label for="city">Город</label>
										<input type="text" class="form-control" id="city" name="city" value="Алматы" disabled>
									</div>
									<div class="form-group">
										<label for="complex">Жилой комплекс</label>
										<input type="text" class="form-control" id="complex" name="complex" value="{{ $data->complex }}">
									</div>
									<div class="form-group">
										<label for="housing">Корпус</label>
										<input type="text" class="form-control" id="housing" name="housing" value="{{ $data->housing }}">
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label for="street">Улица</label>
										<input type="text" class="form-control" id="street" required name="street" value="{{ $data->street }}">
									</div>
									<div class="form-group">
										<label for="house">Дом</label>
										<input type="text" class="form-control" id="house" required name="house" value="{{ $data->house }}">
									</div>
									<div class="form-group">
										<label for="flat">Квартира</label>
										<input type="text" class="form-control" id="flat" name="flat" value="{{ $data->flat }}">
									</div>
								</div>
							</div>

							<div class="title">Реферальная ссылка</div>

							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<input type="text" class="form-control" value="{{ url('/?ref=') }}{{ \Illuminate\Support\Str::upper(cyrillic_to_latin(str_replace(' ','',$data->name)))}}{{ $data->id }}">
									</div>
								</div>
								<div class="col-md-2">
									<span class="what-is-this" data-toggle="popover" title="Реферальная ссылка" data-content="Поделитесь ссылкой с друзьями и получите баллы за заказы! Баллами можно расплачиваться за услуги!">Что это?</span>
								</div>
							</div>
						</div>

						<div id="fb-root"></div>
						<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v8.0" nonce="WyMfuCy5"></script>
						<div class="col-md-4">
							<div class="title">Количество баллов</div>
							<div class="points">{{ $data->points }}</div>
							@php $promocode = \Illuminate\Support\Str::upper(cyrillic_to_latin(str_replace(' ','',$data->name))).$data->id  @endphp
							<div class="myCode">
								Ваш код <b>{{$promocode}}</b>
							</div>
							<div class="shareLinks">

								Поделитесь кодом с друзьями
								<span class="what-is-this" data-toggle="popover" title="Реферальная ссылка" data-content="Поделитесь ссылкой с друзьями и получите баллы за заказы! Баллами можно расплачиваться за услуги!">Что это?</span>
								<div class="d-flex flex-wrap" style="flex-wrap: wrap;">


									<a href="https://vk.com/share.php?url=http://{{request()->getHost()}}?ref={{$promocode}}" target="_blank">
										<img src="{{asset('assets/front/img/vk.png')}}" alt="">
									</a>


									<a href="https://api.whatsapp.com/send?text=https://{{request()->getHost()}}?ref={{$promocode}}" target="_blank" rel="noopener">
										<img src="{{asset('assets/front/img/whatsapp.png')}}" alt="">
									</a>
									<a href="https://t.me/share/url?url=https://{{request()->getHost()}}?ref={{$promocode}}" target="_blank" rel="noopener">
										<img src="{{asset('assets/front/img/telegram.png')}}" alt="">
									</a>
									<div  style='margin-top: 10px;' class="fb-share-button" data-href="http://{{request()->getHost()}}?ref={{$promocode}}" data-layout="button_count" data-size="small">
										<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%3A8000%2F%3Fref%3DSOSKA1789&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">
											Поделиться
										</a>
									</div>



								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="add-btn">Изменить</button>
						</div>
					</div>
				</form>

				</div>
			</div>
			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list dinamic-block">
						<ul>
							<li class="active"><a href="{{ url('user') }}">Личная информация</a></li>
							<li><a href="{{ url('user/orders') }}">Заказы</a></li>
							<li><a href="{{ url('user/points') }}">Баллы</a></li>
							<li><a href="{{ url('user/favorite') }}">Избранное</a></li>
							<li><a href="{{ url('user/black-list') }}">Черный список</a></li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')
	<style>
		.myCode{
			margin-top: 15px;
			font-size: 14px;
			font-weight: 700;
			color: #ccc;

		}
		.myCode b{
			color: #000;

		}
		.shareLinks{
			margin-top: 15px;
			font-size: 14px;
			font-weight: 700;
			color: #ccc;
		}
		.d-flex img{
			width: 35px;
			height: 35px;




		}
		.d-flex a{
			border-radius: 50%;
			width: 35px;
			height: 35px;
			overflow: hidden;
			margin-right: 15px;
		}

		.d-flex{
			padding-top: 15px;
			display: flex;

		}
	</style>
</main><!-- .content -->
@endsection
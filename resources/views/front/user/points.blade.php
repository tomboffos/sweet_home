@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>История начисления баллов</h1>

				<ul>
					<li><a href="{{ url('/') }}">Главная</a></li>
					<li><a href="{{ url('user') }}">Личный кабинет</a></li>
					<li>История начисления баллов</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page user-page order-details">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					<h5>История начисления баллов</h5>
					
					@if(isset($data) && $data && count($data))
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
			                    <thead>
			                        <tr>
			                            <th>Причина</th>
			                            <th class="text-center">Сумма</th>
			                            <th class="text-center">Дата</th>
			                        </tr>
			                    </thead>
			                    <tbody>

			                    @foreach($data as $k => $item)
			                        <tr>
			                            <td style="width: 55%;">{{ $item->reason }}</td>
			                            <td class="text-center" style="width: 20%;">{{ $item->amount }} баллов</td>
			                            <td class="text-center">{{ $item->created_at->format("d F Y H:i") }}</td>
			                        </tr>
			                    @endforeach
			                    </tbody>
			                </table>
						</div>
						
						{!! $data->render() !!}

					@else

						<p>Список пуст.</p>
					@endif
				</div>
			</div>

			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li><a href="{{ url('user') }}">Личная информация</a></li>
							<li><a href="{{ url('user/orders') }}">Заказы</a></li>
							<li class="active"><a href="{{ url('user/points') }}">Баллы</a></li>
							<li><a href="{{ url('user/favorite') }}">Избранное</a></li>
							<li><a href="{{ url('user/black-list') }}">Черный список</a></li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
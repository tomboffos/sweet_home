@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>Список избранных клинеров</h1>

				<ul>
					<li><a href="{{ url('/') }}">Главная</a></li>
					<li><a href="{{ url('user') }}">Личный кабинет</a></li>
					<li>Список избранных клинеров</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page user-page order-details">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-9">
				<div class="entry-text">
					<h5>Список избранных клинеров</h5>
					
					@if(isset($data) && count($data))
						
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
			                    <thead>
			                        <tr>
			                            <th class="text-center">Изображение</th>
			                            <th>Имя</th>
			                            <th class="text-center">Удалить</th>
			                        </tr>
			                    </thead>
			                    <tbody>

			                    @foreach($data as $k => $item)
			                        <tr id="cleaner-{{ $item->id }}">
			                            <td class="text-center" style="width: 5%;">

			                            	@if(isset($item->img) && !empty($item->img))
												<img class="cleaner-avatar" src="{{ url('data/cleaners') }}/{{ $item->id }}/{{ $item->img }}" alt="">
			                            	@endif

			                            </td>
			                            <td style="width: 40%;">
			                            	{{ $item->name }}
			                            	<br>
			                            	<a href="{{ url('/cleaners') }}/{{ $item->id }}" title="Откроется в новом окне" target="_blank">Открыть профиль</a>
			                            </td>
			                            <td class="text-center" style="width: 15%;"><span class="delete-cleaner" onclick="deleteCleaner({{ $item->id }}, 'favorite')">Убрать из списка</span></td>
			                        </tr>
			                    @endforeach
			                    </tbody>
			                </table>
						</div>
						
						{!! $data->render() !!}

					@else

						<p>Список пуст.</p>

					@endif
				</div>
			</div>

			<div class="col-md-3">
				<aside class="sidebar">
					<div class="menu-list">
						<ul>
							<li><a href="{{ url('user') }}">Личная информация</a></li>
							<li><a href="{{ url('user/orders') }}">Заказы</a></li>
							<li><a href="{{ url('user/points') }}">Баллы</a></li>
							<li class="active"><a href="{{ url('user/favorite') }}">Избранное</a></li>
							<li><a href="{{ url('user/black-list') }}">Черный список</a></li>
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
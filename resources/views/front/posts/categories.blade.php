@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ $meta->title }}</h1>

				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>Услуги</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-12">
				<div class="entry-text">
					@if(isset($items) && count($items))                       
                        
                        @foreach($items as $item)
                            @if(isset($item['items']))
							<h3>{{ $item['parent']['title'] }}</h3>
							<ul>
								<li><a href="{{ url("{$item['parent']['slug']}") }}">{{ $item['parent']['title'] }}</a></li>

								@if($item['parent']['slug'] === 'cleaning')
								<li><a href="{{ url("cleaning/general") }}">{{ trans('messages.spring_cleaning') }}</a></li>
								@endif

								@foreach($item['items'] as $child)
								<li><a href="{{ url("uslugi/{$child['slug']}") }}">{{ $child['title'] }}</a></li>
								@if(isset($child['items']) && count($child['items']))
									<ul class="entry-sub-ul">
										@foreach($child['items'] as $subChid)
										<li><a href="{{ url("uslugi/{$child['slug']}/{$subChid['slug']}") }}">{{ $subChid['title'] }}</a></li>
										@endforeach
									</ul>
								@endif
								@endforeach
							</ul>
							@endif

							@if(!isset($item['items']))
							<h3>{{ $item['parent']['title'] }}</h3>
							<ul>
								<li><a href="{{ url("uslugi/{$item['parent']['slug']}") }} }}">{{ $item['parent']['title'] }}</a></li>
							</ul>
							@endif

                        @endforeach
                    @endif
				</div>
			</div>
		</div>

		@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
			[
				'title' => trans('messages.main_page'),
				'url' => url('/'),
			],
			[
				'title' => trans('messages.services'),
				'url' => '',
			],
		]])

	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
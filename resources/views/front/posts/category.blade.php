@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ $post->title }}</h1>

				@if(count($breadcrumbs))
				<ul>
					@foreach($breadcrumbs as $breadcrumb)
					<li>
						@if(!empty($breadcrumb['url']))
							<a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['title'] }}</a>
						@else
							{{ $breadcrumb['title'] }}
						@endif
					</li>
					@endforeach
				</ul>
				@endif
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-12">
				<div class="entry-text entry-post">
					@if(!empty($post->text))
					<div class="entry-typography entry-typography-top js_remove">{!! $post->text !!}</div>
					@endif

					@if(!empty($form) && View::exists($form))
						@include($form)
					@endif

					@if(!empty($post->text_bottom))
					<div class="entry-typography entry-typography-bottom js_remove">
						{!! $post->text_bottom !!}
					</div>
					@endif
					
				</div>
			</div>
		</div>

		@include('front.parts._breadcrumbs_bottom')
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
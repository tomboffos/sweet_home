@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ $post->title }}</h1>

				@if(count($breadcrumbs))
				<ul>
					@foreach($breadcrumbs as $breadcrumb)
					<li>
						@if(!empty($breadcrumb['url']))
							<a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['title'] }}</a>
						@else
							{{ $breadcrumb['title'] }}
						@endif
					</li>
					@endforeach
				</ul>
				@endif
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

@include('front.cleaning._simple')

@endsection
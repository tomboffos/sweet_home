@if(isset($breadcrumbs) && count($breadcrumbs))
<div class="breadcrumbs breadcrumbs-bottom">
    <div class="row">
        <div class="col-md-12">					
            <ul>
                @foreach($breadcrumbs as $breadcrumb)
                <li>
                    @if(!empty($breadcrumb['url']))
                        <a href="{{ $breadcrumb['url'] . '#catalog' }}">{{ $breadcrumb['title'] }}</a>
                    @else
                        {{--  <a href="{{ $_SERVER['REQUEST_URI'] . '#catalog' }}" class="breadcrumbs-lnk-last">{{ $breadcrumb['title'] }}</a>  --}}
                        {{ $breadcrumb['title'] }}
                    @endif
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!-- breadcrumbs -->
@endif
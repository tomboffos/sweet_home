@if(isset( $paymentData) && count($paymentData))

<form action="{{ $paymentData['action'] }}" id="kazkompay">

	<input type="hidden" name="email" value="{{ $paymentData['email'] }}">

	<input type="hidden" name="BackLink" value="{{ $paymentData['BackLink'] }}">

	<input type="hidden" name="PostLink" value="{{ $paymentData['PostLink'] }}">

	<input type="hidden" name="Language" value="{{ $paymentData['Language'] }}">

	<input type="hidden" name="FailureBackLink" value="{{ $paymentData['FailureBackLink'] }}">

	<input type="hidden" name="Signed_Order_B64" value="{{ $paymentData['xml'] }}">

	<input type="hidden" name="template" value="{{ $paymentData['template'] }}">

	<button type="submit" class="btn btn-success">Подключить карту на сайте Казкоммерцбанка!</button>

</form>

@endif
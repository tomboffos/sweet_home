@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.successful_transaction') }}</h1>

				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.successful_transaction') }}</li>
				</ul>
			</div>
		</div>
	</div>

</section>


<main class="content page order-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-12">
				<div class="entry-text">
					<p>{{ trans('messages.successful_transaction') }}. Всю информацию всегда можно <a href="{{ url('user') }}">посмотреть</a> в личном кабиненте!</p>
				</div>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main>

@endsection
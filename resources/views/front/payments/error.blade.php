@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.card_binding_error') }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.card_binding_error') }}</li>
				</ul>
			</div>
		</div>
	</div>

</section>


<main class="content page order-page">

	<div class="container entry-content">
		<div class="row">
			<div class="col-md-12">
				<div class="entry-text">
					<p>Произошла ошибка во время привязки карты. Можно попробовать еще раз, для этого перейдите в <a href="{{ url('user') }}">личный кабинет</a> и нажмите Привязать карту. Если ошибка повторится, свяжитесь с нами!</p>
				</div>
			</div>
		</div>
	</div>

	@include('front.footer-skills')

</main>

@endsection
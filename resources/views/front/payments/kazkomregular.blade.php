@if(isset( $paymentData) && count($paymentData))
<form action="{{ $paymentData['action'] }}" id="kazkompay">
	<input type="text" name="email" value="{{ $paymentData['email'] }}">
	<input type="text" name="BackLink" value="{{ $paymentData['BackLink'] }}">
	<input type="text" name="PostLink" value="{{ $paymentData['PostLink'] }}">
	<input type="text" name="Language" value="{{ $paymentData['Language'] }}">
	<input type="text" name="FailureBackLink" value="{{ $paymentData['FailureBackLink'] }}">
	<input type="text" name="Signed_Order_B64" value="{{ $paymentData['xml'] }}">
	<button type="submit" class="btn btn-success">Оплатить на сайте КАЗКОМа!</button>
</form>
@endif
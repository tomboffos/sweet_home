<section class="skills">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title title-block">{{ trans('messages.preferences_h') }}</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 item">
				<img src="{{ asset('assets/front/img/skills/1.png') }}" alt="">
				<div class="title">{{ $skills[0]['title'] }}</div>
				<div class="short">{{ $skills[0]['short'] }}</div>
			</div>
			<div class="col-md-4 item">
				<img src="{{ asset('assets/front/img/skills/2.png') }}" alt="">
				<div class="title">{{ $skills[1]['title'] }}</div>
				<div class="short">{{ $skills[1]['short'] }}</div>
			</div>
			<div class="col-md-4 item">
				<img src="{{ asset('assets/front/img/skills/3.png') }}" alt="">
				<div class="title">{{ $skills[2]['title'] }}</div>
				<div class="short">{{ $skills[2]['short'] }}</div>
			</div>
		</div>
	</div>
</section>
<!-- skills -->

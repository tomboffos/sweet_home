@extends('front.layout')

@section('content')

<section class="page-top">
	
	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>Отзывы</h1>

				<div class="short-page-description">
					<p>Мы постоянно стремимся совершенствовать качество предоставляемых нами услуг, и ориентиром в этом стремлении для нас служит ваше мнение. Ждем ваших отзывов, пожеланий и интересных идей.</p>
				</div>

				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>Отзывы</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- breadcrumbs -->
</section>
<!-- page-top -->

<main class="content page testionals-page">
	<div class="container">
		<div class="reviews-inner">
			<div class="reviews-slider">
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev23.jpg"><img src="/assets/front/img/reviews/rev23.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev22.jpg"><img src="/assets/front/img/reviews/rev22.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev21.jpg"><img src="/assets/front/img/reviews/rev21.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev20.jpg"><img src="/assets/front/img/reviews/rev20.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev19.jpg"><img src="/assets/front/img/reviews/rev19.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev18.jpg"><img src="/assets/front/img/reviews/rev18.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev17.jpg"><img src="/assets/front/img/reviews/rev17.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev16.jpg"><img src="/assets/front/img/reviews/rev16.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev15.jpg"><img src="/assets/front/img/reviews/rev15.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev14.jpg"><img src="/assets/front/img/reviews/rev14.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev13.jpg"><img src="/assets/front/img/reviews/rev13.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev12.jpg"><img src="/assets/front/img/reviews/rev12.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev11.jpg"><img src="/assets/front/img/reviews/rev11.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev10.jpg"><img src="/assets/front/img/reviews/rev10.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev9.jpg"><img src="/assets/front/img/reviews/rev9.jpg"></a></div>
				        <div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev1.jpg"><img src="/assets/front/img/reviews/rev1.jpg"></a></div>
						<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev2.jpg"><img src="/assets/front/img/reviews/rev2.jpg"></a></div>
						<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev3.jpg"><img src="/assets/front/img/reviews/rev3.jpg"></a></div>
						<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev4.jpg"><img src="/assets/front/img/reviews/rev4.jpg"></a></div>
						<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev5.jpg"><img src="/assets/front/img/reviews/rev5.jpg"></a></div>
						<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev6.jpg"><img src="/assets/front/img/reviews/rev6.jpg"></a></div>
						<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev7.jpg"><img src="/assets/front/img/reviews/rev7.jpg"></a></div>
						<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev8.jpg"><img src="/assets/front/img/reviews/rev8.jpg"></a></div>
			</div>
		</div>
	</div>
	<div class="container entry-content">
		<div class="row">
			<div class="col-md-12">
				<div class="entry-text">

				@if(isset($data) && count($data))
					@foreach($data as $testimonal)
					@if(!empty($testimonal->text))
					<div class="item">
						<span class="autor">{{ $testimonal->name }}</span>
						<div class="short">
							{!! nl2br($testimonal->text) !!}
						</div>
					</div>
					@endif
					@endforeach

					{!! $data->render() !!}
					
				@endif
				</div>
			</div>
		</div>
		@include('front.parts._breadcrumbs_bottom', ['breadcrumbs' => [
			[
				'title' => trans('messages.main_page'),
				'url' => url('/'),
			],
			[
				'title' => trans('messages.reviews'),
				'url' => '',
			],
		]])
	</div>

	@include('front.footer-skills')

</main><!-- .content -->
@endsection
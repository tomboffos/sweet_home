@extends('front.layout')

@section('content')
<main class="content">

	@if(count($slider))
	<div id="section-slider" class="section-slider">
		@foreach($slider as $slide)
		<div class="section-slider-slide">
			@if(!empty($slide->url))
				<a href="{{ $slide->url }}" class="section-slider-overlay"></a>
			@endif
			<div class="section-slider-description">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							@if(!empty($slide->title))
								<div class="section-slider-title">{{ $slide->title }}</div>
							@endif

							@if(!empty($slide->text))
								<div class="section-slider-short">{!! $slide->text !!}</div>
							@endif
						</div>
					</div>
				</div>
			</div>

			<img src="{{ asset("data/slider/{$slide->id}/{$slide->thumbnail}") }}" alt="{{ $slide->title }}">
		</div>
		@endforeach
	</div>
	@endif

	<section class="header-btns">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center buttons">
					<a href="{{ url('cleaning') }}" class="order-cleaner">{{ trans('messages.order_eco_clean') }}</a>
					<a href="{{ url('workers') }}" class="other-services-btn scroll-to-workers">{{ trans('messages.choose_home_staff') }}</a>
				</div>
			</div>
		</div>
	</section>

	<!-- header-slide -->

	<section class="about">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					{!! trans('messages.green_cleaning_of_sweet_home') !!}
				</div>
			</div>
			<div class="about-list">
				<div class="row">
					<div class="about-item col-sm-6"><i class="fas fa-check"></i>{!! trans('messages.unique_tecnology') !!}</div>
					<div class="about-item col-sm-6"><i class="fas fa-check"></i>{!! trans('messages.detergents') !!}</div>
				</div>
				<div class="row">
					<div class="about-item col-sm-6"><i class="fas fa-check"></i>{!! trans('messages.disposable_materials') !!}</div>
					<div class="about-item col-sm-6"><i class="fas fa-check"></i>{!! trans('messages.steam_generator') !!}</div>
				</div>
				<div class="row">
					<div class="about-item col-sm-6"><i class="fas fa-check"></i>{!! trans('messages.ozonizing') !!}</div>
					<div class="about-item col-sm-6"><i class="fas fa-check"></i>{!! trans('messages.safety') !!}</div>
				</div>
			</div>
		</div>
	</section>

	@if(isset($data['banners']['inner']['status']) && (int) $data['banners']['inner']['status'] === 1)
		<section class="banner-inner">
			<div class="container"
				 @if(!empty($data['banners']['inner']['banner'])) style="background-image: url('/data/banners/{{ $data['banners']['inner']['banner'] }}');" @endif>
				@if(!empty($data['banners']['inner']['url']))
					<a href="{{ $data['banners']['inner']['url'] }}" class="overlay"></a>
				@endif
				<div class="short">
				<div class="row">
					<div class="col-md-12">
						@if(!empty($data['banners']['inner']['title']))
							{!! $data['banners']['inner']['title'] !!}
						@endif
					</div>
				</div>
				</div>
			</div>
		</section>
		<!-- banner-inner -->
	@endif

	<section class="order-clean-slide">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">{{ trans('messages.order_clean') }}</div>
					<div class="sub-title">{{ trans('messages.it_takes_1_minute') }}</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 buttons">
					<a href="{{ url('cleaning') }}" class="order-cleaner">{{ trans('messages.order_eco_clean') }}</a>
					<a href="{{ url('workers') }}" class="other-services-btn scroll-to-workers">{{ trans('messages.choose_home_staff') }}</a>
				</div>
			</div>
		</div>
	</section>
	<!-- order-clean-slide -->

	<section class="why-we">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">{{ $data['why_title'] }}</div>
				</div>
			</div>
			<?php $why_items_arr = array_chunk($data['why_items'], 2); $i = 0; ?>
			@foreach($why_items_arr as $key => $why_items)
				<div class="row">
				@foreach($why_items as $why_item)
					<div class="col-md-1 number">
						<i>{{ ++$i }}</i>
					</div>
					<div class="col-md-5 item">
						<div class="title-item">{{ $why_item['title'] }}</div>
						<div class="short">
							<div class="info-block">{!! $why_item['short'] !!}</div>
							<span class="expand-block" style="display: none;">{{ trans('messages.show_all') }}</span>
						</div>
					</div>
				@endforeach
				</div>
			@endforeach
		</div>
	</section>
	<!-- why-we -->

	<section class="other-services">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a name="personnel"></a>
					<div class="title">{{ trans('messages.choose_home_staff') }}</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="item bg-1">
						<a href="{{ url('workers/1-nyanya') }}">{{ trans('messages.babysitter') }}</a>
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="item bg-2">
						<a href="{{ url('workers/2-povar') }}">{{ trans('messages.cook') }}</a>
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="item bg-3">
						<a href="{{ url('workers/3-sidelka') }}">{{ trans('messages.nurse') }}</a>
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="item bg-4">
						<a href="{{ url('workers/4-domrabotnica') }}">{{ trans('messages.housekeeper') }}</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- other-services -->

	<section class="order-clean-slide">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">{{ trans('messages.spare_yourself_time') }}!</div>
                    <div class="sub-title">{{ trans('messages.it_takes_no_more_then_minute') }}</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 buttons">
                    <a href="{{ url('cleaning') }}" class="order-cleaner">{{ trans('messages.order_eco_clean') }}</a>
					<a href="{{ url('workers') }}" class="other-services-btn scroll-to-workers">{{ trans('messages.choose_home_staff') }}</a>
				</div>
			</div>
		</div>
	</section>
	<!-- order-clean-slide -->
	<section class="about video-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">{{ trans('messages.answer_questions') }}</div>
				</div>
			</div>
			<div class="row">
				<div class="video-item col-md-3 col-sm-6">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/WSIv3MwQzsU?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-md-3 col-sm-6">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/RgdQTU6ALbw?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-md-3 col-sm-6">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/TbqXIiqoPnY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-md-3 col-sm-6">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/kwU9SXEu7r8?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
			</div>
			<div class="row">
				<div class="video-item col-sm-4">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/boAB3knfAN4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-sm-4">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/tRvjveLhejw?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
				<div class="video-item col-sm-4">
					<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/cybSejgCF04?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
				</div>
			</div>
		</div>
	</section>
	<section class="testimonals-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">{{ trans('messages.clients_feedback') }}</div>
				</div>
			</div>
			<div class="reviews-slider">
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev23.jpg"><img src="/assets/front/img/reviews/rev23.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev22.jpg"><img src="/assets/front/img/reviews/rev22.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev21.jpg"><img src="/assets/front/img/reviews/rev21.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev20.jpg"><img src="/assets/front/img/reviews/rev20.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev19.jpg"><img src="/assets/front/img/reviews/rev19.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev18.jpg"><img src="/assets/front/img/reviews/rev18.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev17.jpg"><img src="/assets/front/img/reviews/rev17.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev16.jpg"><img src="/assets/front/img/reviews/rev16.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev15.jpg"><img src="/assets/front/img/reviews/rev15.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev14.jpg"><img src="/assets/front/img/reviews/rev14.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev13.jpg"><img src="/assets/front/img/reviews/rev13.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev12.jpg"><img src="/assets/front/img/reviews/rev12.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev11.jpg"><img src="/assets/front/img/reviews/rev11.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev10.jpg"><img src="/assets/front/img/reviews/rev10.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev9.jpg"><img src="/assets/front/img/reviews/rev9.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev1.jpg"><img src="/assets/front/img/reviews/rev1.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev2.jpg"><img src="/assets/front/img/reviews/rev2.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev3.jpg"><img src="/assets/front/img/reviews/rev3.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev4.jpg"><img src="/assets/front/img/reviews/rev4.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev5.jpg"><img src="/assets/front/img/reviews/rev5.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev6.jpg"><img src="/assets/front/img/reviews/rev6.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev7.jpg"><img src="/assets/front/img/reviews/rev7.jpg"></a></div>
				<div class="review-item"><a data-fancybox="reviews" href="/assets/front/img/reviews/rev8.jpg"><img src="/assets/front/img/reviews/rev8.jpg"></a></div>
			</div>
            @if (App::isLocale('ru'))
			<div class="row">
				<div class="col-md-12 more-testimonals">
					<a href="{{ url('testimonals') }}">Все отзывы</a>
				</div>
			</div>
            @endif
		</div>
	</section>
	<!-- testimonals-section -->
	
	<section class="skills">
		<div class="container">
			<div id="skills" class="row">
				<div class="col-md-4 item">
					<img src="{{ asset('assets/front/img/skills/1.png') }}">
					<div class="title">{{ $data['skills'][0]['title'] }}</div>
					<div class="short">{{ $data['skills'][0]['short'] }}</div>
				</div>
				<div class="col-md-4 item">
					<img src="{{ asset('assets/front/img/skills/2.png') }}">
					<div class="title">{{ $data['skills'][1]['title'] }}</div>
					<div class="short">{{ $data['skills'][1]['short'] }}</div>
				</div>
				<div class="col-md-4 item">
					<img src="{{ asset('assets/front/img/skills/3.png') }}">
					<div class="title">{{ $data['skills'][2]['title'] }}</div>
					<div class="short">{{ $data['skills'][2]['short'] }}</div>
				</div>
			</div>
		</div>
	</section>
	<!-- skills -->

</main><!-- .content -->
@endsection

<div class="primary">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="primary-menu">
					<i id="toggle-primary-menu"></i>
					<span class="title">{{ trans('messages.main_menu') }}</span>
					<ul>
						<li class="has-childs"><a>{{ trans('messages.apply') }}</a>
							<ul>
								<li><a href="{{ url('cleaning') }}">{{ trans('messages.cleaning') }}</a></li>
								<li><a href="{{ url('cleaning/general') }}">{{ trans('messages.spring_cleaning') }}</a></li>
							@if(isset($menuWorkers) && $menuWorkers && count($menuWorkers))
								@foreach($menuWorkers as $worker)
								<li><a href="{{ url('/workers') }}/{{ $worker->id }}-{{ $worker->slug }}">{{  trans('messages.' . $worker->title) }}</a></li>
								@endforeach
							@endif
							</ul>
						</li>
                        @if(App::isLocale('ru'))
						    @if(isset($menuPages) && $menuPages && count($menuPages))
						    <li class="has-childs"><a>Информация</a>
							    <ul>
								    @foreach($menuPages as $page)
								    <li><a href="{{ url('/pages') }}/{{ $page->id }}-{{ $page->slug }}">{{ $page->title }}</a></li>
								    @endforeach
							    </ul>
						    </li>
						    @endif
						    <li><a href="{{ url('testimonals') }}">Отзывы</a></li>
                        @endif
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

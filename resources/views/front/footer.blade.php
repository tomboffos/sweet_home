<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                @if($footer_menu && count($footer_menu) && App::isLocale('ru'))
                <ul class="list-links">
                    @foreach($footer_menu as $key => $menu)
                    <li>
                        <a href="{{ url('pages') }}/{{ $menu->id }}-{{ $menu->slug }}">{{ $menu->title }}</a>
                    </li>
                    @endforeach
                    <li>
                        <a href="{{ url('faq') }}">{{ trans('messages.q_n_a') }}</a>
                    </li>
                </ul>
                @endif
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <p>{{ trans('messages.follow_us') }}</p>
                <ul class="social">
                    <li class="vk">
                        <a href="{{ $social->vk }}" rel="nofollow"></a>
                    </li>
                    <li class="fa">
                        <a href="{{ $social->fa }}" rel="nofollow"></a>
                    </li>
                    <li class="in">
                        <a href="{{ $social->in }}" rel="nofollow"></a>
                    </li>
                </ul>
                <br>
                <p>{{ trans('messages.payment') }}</p>
                <img src="{{ asset('assets/front/img/pay.png') }}" alt="">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="footer-contacts">
                    <p>{!! trans('messages.full_address') !!}</p>
                    <p>{!! trans('messages.working_hours') !!}</p>
                    <p><b>{{ trans('messages.phone') }}:</b> <a href="tel:+77273270130">+7 (727) 327 01-30</a></p>
                    <p><b>WhatsApp:</b> <a href="tel:+77014127777">+7 (701) 412 77-77</a></p>
                    <p><b>{{ trans('messages.email') }}</b> <a href="mailto:info@sweet-home.asia">info@sweet-home.asia</a></p>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <ul class="list-links">
                    @if(App::isLocale('ru'))
                    <li>
                        <a href="{{ url('uslugi') }}">{{ trans('messages.services') }}</a>
                    </li>
                    @endif
                    <li>
                        <a href="{{ url('cleaning') }}">{{ trans('messages.cleaning') }}</a>
                    </li>
                    @if($workers_categories && count($workers_categories))
                        @foreach($workers_categories as $key => $worker_category)
                            <li>
                                <a href="{{ url('workers') }}/{{ $worker_category->id }}-{{ $worker_category->slug }}">{{ trans('messages.' . $worker_category->title) }}</a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- .footer -->

<div class="notificate-add-error" style="display: none;"></div>
<div class="notificate-add-success" style="display: none;"></div>
<div class="notificate-add-warning" style="display: none;"></div>
<div class="notificate-add-info" style="display: none;"></div>

@if(Auth::guest() && App::isLocale('ru'))
    <!-- auth-form -->
    <div class="modal modal-auth" id="modalAuth" tabindex="-1" role="dialog" aria-labelledby="modal-auth">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                    <div class="h4 modal-title" id="modal-auth">Вход на сайт</div>
                    <span class="question-have-account">
                        У вас нет аккаунта?
                        <a data-toggle="modal" data-target="#modalRegistration" class="close-this-modal">Зарегистрироваться</a>
                        .
                    </span>
                </div>
                <div class="modal-body">

                    <form id="modalAuthForm" role="form" method="POST" action="{{ url('/login') }}">

                        <label for="email">e-mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="pochta@ mail.ru">

                        <label for="password">пароль</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="************">

                        <div class="form-status"></div>

                        <a data-toggle="modal" data-target="#modalForgotPass" class="close-this-modal forget-pass">Забыли пароль?</a>
                        <button type="button" onclick="modalAuth()" class="add-btn">Войти</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <!-- register-form -->
    <div class="modal modal-auth" id="modalRegistration" tabindex="-1" role="dialog" aria-labelledby="modal-registration">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                    <div class="h4 modal-title" id="modal-registration">Регистрация</div>
                    <span class="question-have-account">
                        Уже есть аккаунт?
                        <a data-toggle="modal" data-target="#modalAuth" class="close-this-modal">Войдите здесь</a>
                        .
                    </span>
                </div>
                <div class="modal-body">

                    <form id="modalRegistrationForm" role="form" method="POST" action="{{ url('/register') }}">
                        <label for="email">e-mail</label>
                        <input type="email" class="form-control" id="email" name="email">

                        <label for="password">пароль</label>
                        <input type="password" class="form-control" id="password" name="password">

                        <label for="password_confirmation">повторите пароль</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">

                        <div class="form-status"></div>

                        <button type="button" onclick="modalRegister()" class="add-btn">зарегистрироваться</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <!-- forgot-pass-form -->
    <div class="modal modal-auth" id="modalForgotPass" tabindex="-1" role="dialog" aria-labelledby="modal-auth">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                    <div class="h4 modal-title" id="modal-auth">Восстановление пароля</div>
                    <span class="question-have-account">
                        <a data-toggle="modal" data-target="#modalAuth" class="close-this-modal">Вход на сайт</a>
                        |
                        <a data-toggle="modal" data-target="#modalRegistration" class="close-this-modal">Регистрация</a>
                    </span>
                </div>
                <div class="modal-body">

                    <form id="modalForgotForm" role="form" method="POST" action="{{ url('/password/email') }}">
                        <label for="email">Введите свой email, мы вышлем вам пароль.</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="">
                        <div class="form-status"></div>
                        <button type="button" onclick="modalResetPassword()" class="add-btn">отправить пароль</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endif

<!-- callme form -->
<div class="modal modal-auth" id="modalCallMe" tabindex="-1" role="dialog" aria-labelledby="modal-call-me">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="h4 modal-title" id="modal-auth">{{ trans('messages.order_call') }}</div>
            </div>
            <div class="modal-body">
                <form id="modalCallMeForm" role="form">
                    <label for="phone">{{ trans('messages.enter_phone') }}</label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="+7 (___) ___-__-__">
                    <input type="hidden" name="form_name" value="callme">
                    <button type="button" onclick="modalCallMeForm()" class="add-btn">{{ trans('messages.call_me_back') }}!</button>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="{{ asset('assets/front/fontawesome/css/all.css') }}" rel="stylesheet" type='text/css'>
<link href="{{ asset('assets/front/bxslider/jquery.bxslider.css') }}" rel="stylesheet" type='text/css'>
<link href="{{ asset('assets/front/fancybox/dist/jquery.fancybox.css') }}" rel="stylesheet" type='text/css'>
<link href="{{ asset('assets/front/css/main.css') }}" rel="stylesheet" type='text/css'>
<script src="{{ asset('assets/front/js/main.js') }}"></script>
<script src="{{ asset('assets/front/bxslider/jquery.bxslider.js') }}"></script>
<script src="{{ asset('assets/front/fancybox/dist/jquery.fancybox.js') }}"></script>
<script src="{{ asset('assets/front/js/custom.js') }}"></script>


</body>
</html>

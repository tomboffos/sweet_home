
@include('front.header', ['meta' => ($meta ? $meta : false)])

<div class="wrapper">

	@include('front.top', ['phones' => ($phones ? $phones : false), 'banners' => ($banners ? $banners : false)])

	@yield('content')

</div><!-- .wrapper -->

@include('front.footer', [
	'social' => ($social ? $social : false),
	'footer_menu' => ($footer_menu ? $footer_menu : false),
	'workers_categories' => ($workers_categories ? $workers_categories : false)
])

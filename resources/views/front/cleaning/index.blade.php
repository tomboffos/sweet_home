@extends('front.layout')

@section('content')

<section id="scroll-here" class="page-top">

	@include('front.menu')

	<div class="breadcrumbs">
		<div class="container">
			<div class="col-md-12">
				<h1>{{ trans('messages.green_cleaning') }}</h1>
				<ul>
					<li><a href="{{ url('/') }}">{{ trans('messages.main_page') }}</a></li>
					<li>{{ trans('messages.green_cleaning') }}</li>
				</ul>
			</div>
		</div>
	</div>

</section>


@include('front.cleaning._simple', ['breadcrumbs' => [
	[
		'title' => trans('messages.main_page'),
		'url' => url('/'),
	],
	[
		'title' => trans('messages.green_cleaning'),
		'url' => '',
	],
]])

@endsection

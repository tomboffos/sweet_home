<main class="content page order-page cleaning-order-page">
	<div class="container entry-content">
		<div class="row">
			<div class="col-md-8 table-cell-class-remove">
				<div class="entry-text">
                    @if(isset($post->text))
						@if(!empty($post->text))
							<div class="entry-typography entry-typography-top js_remove">
								{!! $post->text !!}
							</div>
						@endif
                    @else
                        <div class="entry-typography entry-typography-top js_remove">
                            {!! trans('messages.spring_green_cleaning_text_1') !!}
                        </div>
                    @endif
					<!-- Modal -->
                    <div class="modal fade" id="what" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&nbsp;</button>
                                    <h2>{{ trans('messages.what_included_standard') }}</h2>
                                </div>
                                <div class="modal-body">
                                    <p><strong>{{ trans('messages.clean_list_standard_title') }}</strong></p>
                                    <ul class="clean-list">
			                        {!! trans('messages.clean_list_standard') !!}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
					<form id="order-cleaning" class="order-form">
						<h2 class="entry-h2">{{ trans('messages.order_clean') }} <a data-toggle="modal" data-target="#what" class="btn add-btn">{{ trans('messages.what_included') }}</a></h2>
						<input type="hidden" name="label" value="standart">
						<div class="row choose-block">
							@if(isset($calculator['type']) && count($calculator['type']))
								<div class="col-md-5">
									<div class="title">{{ trans('messages.cleaning_type') }}</div>
									<div class="type">
									@foreach($calculator['type'] as $k => $type)
										<div class="radio @if($k === 0) js_active @endif">
											<label>
												<input type="radio" name="type" value="{{ $type['label'] }}" @if($k === 0) checked @endif >
												{{ $type['name'] }}
											</label>
										</div>
									@endforeach
									</div>
								</div>
							@endif

							@if(isset($calculator['cicle']) && count($calculator['cicle']))
								<div class="col-md-5">
									<div class="title">{{ trans('messages.periodicity') }}</div>
									<div class="cicle">
									@foreach($calculator['cicle'] as $k => $cicle)
										<div class="radio" data-label="{{ $cicle['name'] }}">
											<label>
												<input type="radio" name="cicle" value="{{ $cicle['id'] }}" @if($k === 0) checked @endif>
												{{ $cicle['name'] }}
											</label>
										</div>
									@endforeach
									<div class="radio">
										<label>
											<a href="{{ url("/workers/4-domrabotnica") }}" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ trans('messages.permanent_cleaning') }}</a>
										</label>
										</div>
									</div>
								</div>
							@endif
						</div>

						<div class="row choose-block">
							<div class="col-md-12">
								<div class="number-rooms-bath-rooms">
									<div class="title">{{ trans('messages.apartment_info') }}</div>
									<div class="text-center">
										<div class="number-rooms">
											<i class="minus">—</i>
											<input type="text" id="number_rooms" value="1 {{ trans('messages.room') }}" readonly>
											<input type="hidden" name="number_rooms" value="1">
											<i class="plus">+</i>
										</div>

										<div class="number-bath-rooms">
											<i class="minus">—</i>
											<input type="text" id="number_bath_rooms" value="1 {{ trans('messages.toilet') }}" readonly>
											<input type="hidden" name="number_bath_rooms" value="1">
											<i class="plus">+</i>
										</div>
									</div>
								</div>
								<!-- number-rooms-bath-rooms -->
							</div>
						</div>

						@if(isset($calculator['services']) && count($calculator['services']))
						<div class="row choose-block common-inputs service-items">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.additional_services') }}</div>
								<div class="inline">
								@foreach($calculator['services'] as $service)
									<div class="item">
										<div class="service-item">
											<i class="icon icon-{{ $service['icon'] }}"></i>
											<span>{{ $service['name'] }}</span>
											<div class="inputs">
												<input type="checkbox" class="hidden service-name" data-time="{{ $service['time'] }}" name="service[{{ $service['id'] }}]" value="{{ $service['id'] }}">
												<input type="checkbox" class="hidden service-value" name="service[{{ $service['id'] }}][unit]" value="1">
											</div>
										</div>

										@if($service['pay'] !== 0)
										<div class="add-service-value-modal" style="display: none;">
											<p>@if($service['pay'] === 1) {{ trans('messages.enter_quantity_unit') }}: @else {{ trans('messages.enter_quantity_hours') }}: @endif</p>
											<input type="text" class="add-service-value" value="1">
											<span class="set-service-value btn btn-success">Ок</span>
										</div>
										@endif
									</div>
								@endforeach
									<div class="item">
										<div class="service-item js_additional-cleaner">
											<i class="icon icon-cleaner"></i>
											<span>{{ trans('messages.additional_cleaner') }}</span>
											<div class="inputs">
												<input type="checkbox" class="hidden service-name" data-time="0" name="service[additional_cleaner]" value="additional_cleaner">
												<input type="checkbox" class="hidden service-value" name="service[additional_cleaner][unit]" value="1">
											</div>
										</div>
									</div>
									<div class="item">
										<div class="service-item js_additional-free-cleaner" style="display: none;">
											<div class="inputs">
												<input type="checkbox" class="hidden service-name" data-time="0" name="service[additional_free_cleaner]" value="additional_free_cleaner">
												<input type="checkbox" class="hidden service-value" name="service[additional_free_cleaner][unit]" value="1">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endif

						<div class="row choose-block common-inputs">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.date_time') }}</div>
								<div class="inline">
									<div class="date item">
										<input type="text" name="date" placeholder="{{ trans('messages.date') }}">
									</div>
									<div class="time item">
										<input type="text" name="time" placeholder="{{ trans('messages.time') }}" disabled>
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.contacts') }}</div>
								<div class="inline">
									<div class="item">
										<input type="text" name="name" placeholder="{{ trans('messages.name') }}" value="{{ $user->name }}">
									</div>
									<div class="item">
										<input type="text" name="phone" placeholder="{{ trans('messages.phone') }}" value="{{ $user->phone }}">
									</div>
								</div>
								<p></p>
								<div class="inline">
									<div class="item">
										<input type="text" name="email" placeholder="Email" value="{{ $user->email }}">
									</div>

									@if(!Auth::user())
									<div class="auth-field item" style="display: none;">
										<input type="password" name="password" placeholder="Пароль">
									</div>
									<div class="auth-field item" style="display: none;">
										<span class="ajax-auth btn btn-success fast-login">Войти</span>
									</div>
									@endif
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.address') }}</div>
								<div class="inline">
									<div class="item">
										<input type="text" name="city" placeholder="{{ trans('messages.almaty') }}" disabled>
									</div>
									<div class="item">
										<input type="text" name="street" placeholder="{{ trans('messages.street') }}"  value="{{ $user->street }}">
									</div>
								</div>
								<p></p>
								<div class="inline">
									<div class="item">
										<input type="text" name="complex" placeholder="{{ trans('messages.residential_complex') }}"  value="{{ $user->complex }}">
									</div>
									<div class="item">
										<input type="text" name="house" placeholder="{{ trans('messages.house') }}"  value="{{ $user->house }}">
									</div>
								</div>
								<p></p>
								<div class="inline">
									<div class="item">
										<input type="text" name="housing" placeholder="{{ trans('messages.building') }}"  value="{{ $user->housing }}">
									</div>
									<div class="item">
										<input type="text" name="flat" placeholder="{{ trans('messages.apartment') }}"  value="{{ $user->flat }}">
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs pay-methods">
							<div class="col-md-12">
								<div class="title">{{ trans('messages.way_of_payment') }}</div>
								<div class="inline">
									<div class="item">
										<div class="radio">
											<label>
												<input type="radio" name="pay" value="0" checked>
												{{ trans('messages.cash') }}
											</label>
										</div>
									</div>
									<div class="item">
										<div class="radio">
											<label>
												<input type="radio" name="pay" value="1">
												{{ trans('messages.card') }}
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row choose-block common-inputs">

							<div class="col-md-5">
								<div class="title">{{ trans('messages.add_promo_code') }}</div>
								<div class="inline">
									<div class="item">
											<input type="text" name="discount" id="discount" value="{{session()->get('referal')}}">
									</div>
								</div>
							</div>

							@if(Auth::user())
									<div class="col-md-5 points" >
								<div class="title">{{ trans('messages.apply_points') }}</div>

								<div class="inline">
									<div class="item">
										<input type="text" name="points" id="points">
										<div class="all_points">
											 {{trans('points.get_all')}} : {{$user->points}}
										</div>
										<a class="link_use" id="allPoint" style="margin-top: 20px;" onclick=" @if(Auth::user()) useAll({{$user->points}}) @else  @endif">@lang('points.use_all')</a>
									</div>
								</div>
							</div>
								@endif
						</div>

						<div class="row choose-block common-inputs">
							<div class="col-md-5">
								<div class="title">{{ trans('messages.order_comment') }}</div>
								<div class="inline">
									<div class="item">
										<textarea name="comment" rows="3"></textarea>
									</div>
								</div>
							</div>
						</div>

						<input type="hidden" name="elapsed" value="">

						<div class="row">
							<div class="col-md-12">
								<button type="button" class="add-btn add-order">{{ trans('messages.order_clean') }}</button>
							</div>
						</div>
					</form>
					<div class="order-result"></div>

                    @if(isset($post->text_bottom))
						@if(!empty($post->text_bottom))
							<div class="entry-typography entry-typography-bottom js_remove">
								{!! $post->text_bottom !!}
							</div>
						@endif
                    @else
                        <div class="entry-typography entry-typography-bottom js_remove">
                            <h3>{{ trans('messages.avg_cleaning_cost') }}</h3>
                            <table>
                                <thead>
                                    <tr>
                                        <th>{{ trans('messages.service') }}</th>
                                        <th>{{ trans('messages.price') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ trans('messages.cleaning') }}</td>
                                        <td>{{ trans('messages._7_') }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('messages.spring_cleaning') }}</td>
                                        <td>{{ trans('messages._17_') }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('messages.daily_cleaning') }}</td>
                                        <td>{{ trans('messages._7_') }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <h3>{{ trans('messages.preferences_h') }}</h3>
                            {!! trans('messages.preferences_text') !!}
                        </div>
                    @endif
				</div>
			</div>

			<div class="col-md-4 table-cell-class-remove">
				<aside class="sidebar">
					<div class="fixed-block">
						<div class="block dinamic-block order-total">
							<i class="fix-btn active"></i>
							<div class="title">{{ trans('messages.your_order') }}</div>
							<div class="item date" style="display: none;">
								<i class="icon icon-calendar"></i>
								<div class="panel"></div>
							</div>
							<div class="item time">
								<i class="icon icon-time"></i>
								<div class="panel"></div>
							</div>
							<div class="item time" style="display: none;">
								<i class="icon icon-time"></i>
								<div class="panel"></div>
							</div>
							<div class="item cicle">
								<i class="icon icon-cicle"></i>
								<div class="panel"></div>
							</div>

							@if(isset($calculator['services']) && count($calculator['services']))
								<span class="more-services" style="display: none;">{{ trans('messages.additional_services') }}</span>

								<div class="attachible-services">
									@foreach($calculator['services'] as $service)
									<div id="{{ $service['id'] }}" class="item js_sidebar-service-item" style="display: none;" title="{{ $service['name'] }}" data-toggle="tooltip" data-placement="top">
										<i class="icon icon-{{ $service['icon'] }}"></i>
										<div class="panel"><span class="js_service-count">1</span> x {{ number_format($service['price'], 0, '', ' ') }} {{ trans('messages.tg') }}</div>
									</div>
									@endforeach
									<div id="additional_cleaner" class="item" style="display: none;" title="{{ trans('messages.additional_cliner') }}" data-toggle="tooltip" data-placement="top">
										<i class="icon icon-cleaner"></i>
										<div class="panel">1 x {{ number_format($js['additional_cleaner'], 0, '', ' ') }} {{ trans('messages.tg') }}</div>
									</div>
									<div id="additional_free_cleaner" class="item" style="display: none;" title="{{ trans('messages.free_additional_cliner') }}" data-toggle="tooltip" data-placement="top">
										<i class="icon icon-cleaner"></i>
										<div class="panel">1 x {{ trans('messages.free') }}</div>
									</div>
								</div>
							@endif

							<input type="hidden" id="percent" value="{{100*$discount->amount}}">

								<span class="discount_info">
									@if(session()->get('referal') && $user!=preg_replace('/[^0-9]/', '', session()->get('referal'))  )
										{{trans('points.discount')}}
										@if(!isset($parent)) {{session()->get('referal')}}@else {{ \Illuminate\Support\Str::upper(cyrillic_to_latin($parent->name))}}{{ $parent->id }}
										@endif <b>{{$discount->amount}}</b>
									@endif
								</span>
							<input type="hidden" id="local" name="local" value="{{\Illuminate\Support\Facades\App::getLocale()}}">
							<span id="discountPromotion"></span>
							<span class="total-summary">{{ trans('messages.total') }}: <b></b></span>
							<span class="discount-params" style="display: block;display: flex;justify-content: flex-end;font-size: 14px;"><s id="old_price"></s></span>
							<span class="points-params" style="display: none;"></span>

							<div class="row">
								<div class="col-md-12">
									<button type="button" class="add-btn add-order">{{ trans('messages.order_clean') }}</button>
								</div>
							</div>
						</div>

						@if(isset($faq) && count($faq) && App::isLocale('ru'))
						<div class="block faq-sidebar">
							<div class="title">{{ trans('messages.q_n_a') }}</div>
							<ul>
							    @foreach($faq as $item)
							    <li><a href="{{ url('faq') }}/{{ $item->category }}-{{ $faq_category }}/{{ $item->id }}-{{ $item->slug }}" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ $item->title }}</a></li>
							    @endforeach
							</ul>

							<a href="{{ url('faq') }}" class="more-faq" title="{{ trans('messages.open_in_new_window') }}" target="_blank">{{ trans('messages.other_issues') }}</a>

							<div class="clearfix"></div>
						</div>
						@endif
					</div>
				</aside>
			</div>
		</div>
		@include('front.parts._breadcrumbs_bottom')
	</div>
	<div id="referal" style="display: none">{{session()->get('referal')}}</div>
	@include('front.footer-skills')

</main><!-- .content -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<style>
	.all_points{
		font-size: 15px;
		margin-top: 10px;
		color: #c9cccf;
	}
	.discount_info{
		font-size: 14px !important;
		text-align: center;
		margin-bottom: 10px;
	}
	.discount_info>b{
		font-size: 14px !important;
		float: none !important;
	}
	.link_use{
		font-size: 15px;
		position: relative;
        top: 10px;
		color: #008f4c;
		text-decoration: underline;
	}
	#discount_info{
		display: none;
	}
	@media (max-width: 1000px) {
		.sidebar .fix, .sidebar .fixed-block{
			position: static !important;
			width: 100% !important;
		}


	}
</style>
<script >

	function useAll(val) {

		$('#points').val(val);
		let discount = $('#points').val()
		$('#discountPromotion').html('Скидка по баллам <b>-'+discount +' тг</b>')
		$('#bonus_container').addClass('active')
		var summary = +collectFormDataWithoutTimeout(true);
		$('#old_price').html(summary + ' тг')
		summary -= val;
		$(" input[name='discount']").attr("disabled", !0).attr("style", "background: #ddd;")
		$(".points-params").attr("data-price", val);
		$(".order-total .total-summary b").text(number_format(summary, 0, '', ' ') + ' тг.');
		console.log($("#order-cleaning").serialize())


	}








	var Calculator = {
		main: {!! $js['main'] !!},
		cicle: {!! $js['cicle'] !!},
		services: {!! $js['services'] !!},
		type: {!! $js['type'] !!},
		additional_cleaner: {{ (int) $js['additional_cleaner'] }},
		additional_cleaner_border: {{ (int) $js['additional_cleaner_border'] }},
		bonus: $('#points').val(),
	};
	console.log(Calculator)
	var Schedule = {

		disabledWeekDays: [{{ count($schedule['days']) ? implode($schedule['days'], ',') : ''}}],
		minTime: '{{ $schedule['t_from'] }}',
		maxTime: '{{ $schedule['t_to'] }}',

		enableOrder: {{ date('H:i', time()+(6*60*60)) > $cleaning_time_border ? 0 : 1 }},
		timeBorder: '{{$cleaning_time_border}}',
		currentTime: '{{ date('H:i', time()+(6*60*60)) }}',
	};



</script>

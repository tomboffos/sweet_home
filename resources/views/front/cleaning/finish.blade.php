<!-- Facebook Pixel Event -->
<script>
  fbq('track', 'Purchase');
</script>
<!-- Event snippet for Заказ уборки conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-799513118/QOfgCOr08YsBEJ60nv0C',
      'transaction_id': ''
  });
</script>
<h4>Заказ успешно сформирован!</h4>

@if($new_user === true)
<p>Для вас создан аккаунт. Пароль был выслан на ваш почтовый ящик!</p>
@endif

<p>Детали заказа всегда можно <a href="{{ url('/user/orders/cleaning') }}/{{ $order->id }}">посмотреть</a> в личном кабинете.</p>

@if($order->payment_type == 1 && $user->card === 2)
	<p>Вы выбрали оплату Банковской картой. Сумма будет списана после выполнения заказа.</p>
@elseif($order->payment_type == 1 && $user->card !== 2)
	<p>Вы выбрали оплату банковской картой, но пока не привязали карту к аккаунту. Сделать это можно, нажав на кнопку ниже — вы будете перенаправлены на защищённую страницу EPay. После ввода данных карты, пожалуйста, вернитесь на наш сайт для активации данного способа оплаты. Спасибо!</p>
	{!! $payment !!}
@endif
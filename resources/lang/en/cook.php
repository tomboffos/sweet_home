<?php

// <br> - означает перенос строки

return [
    // Meta data
    'meta_title' => 'Find a home cook: Cook prices, best home cooks in Almaty',
    'meta_description' => '',
    'meta_keywords' => '',

    // Main Image
    'slide_ttl' => 'We will find the best home cook<br>for your family',
    'slide_btn' => 'Leave a request',

    // Intro
    'intro_ttl' => 'The top home cooks of Almaty are in Sweet Home’s database',
    'intro_txt' => 'Do you need someone to cook for your family? 
                    Or do you want to treat yourself and your loved ones with something tasty for a celebratory dinner? 
                    Are you waiting for guests and want to surprise them with a new dish, or you just need a kitchen assistant? 
                    Complete the form below and we will do our best to find a suitable specialist.',
    'intro_btn' => 'Leave a request',

    // Cols
    'cols_1_ttl' => 'Full-time home cook',
    'cols_1_price' => 'From 120 000 tenge per month',
    'cols_1_desc_ttl' => 'Responsibilities',
    'cols_1_desc_txt' => '<li>Plan of the full menu taking into account the employer\'s wishes</li>
                          <li>Shopping for food</li>
                          <li>Preparation of the breakfast, lunch and dinner</li>
                          <li>Baking</li>
                          <li>Maintenance of the order and cleanness of the kitchen</li>
                          <li>Care for the kitchen utensils and tableware</li>
                          <li>Table setting</li>
                          <li>Cooking for family celebrations</li>',

    'cols_2_ttl' => 'Cook for an event',
    'cols_2_price' => 'From 20 000 tenge per event',
    'cols_2_desc_ttl' => 'Responsibilities',
    'cols_2_desc_txt' => '<li>Planning of the celebration’s menu</li>
                          <li>Calculation of the number of products for the specified guest number</li>
                          <li>Shopping for products</li>
                          <li>Food preparation</li>
                          <li>Table setting</li>
                          <li>Kitchen clean-up after the event</li>',

    'cols_btn' => 'Leave a request',

    // Team
    'team_ttl' => 'Our cooks',
    'team_txt_1' => 'Have experience in professional kitchen',
    'team_txt_2' => 'Are familiar with the cuisines of different nations',
    'team_txt_3' => 'Know how to create a menu according to a special diet',
    'team_txt_4' => 'Have letters of recommendation from previous employers',
    'team_txt_5' => 'Are citizens of Kazakhstan',
    'team_txt_6' => 'Have at least three years of experience of work in a family',
    'team_txt_7' => 'Pass a multi-level verification before being included into the database',

    // Steps
    'steps_ttl' => 'How does the recruiting process go?',
    'steps_btn' => 'Leave a request',
    'steps_ttl_1' => 'Application',
    'steps_ttl_2' => 'Selection',
    'steps_ttl_3' => 'Interview',
    'steps_ttl_4' => 'Contract',
    'steps_ttl_5' => 'Support',
    'steps_txt_1' => 'You leave your order on the website or by phone. Placing an order is free!',
    'steps_txt_2' => 'The manager selects suitable candidates from the database and sends you the CVs.',
    'steps_txt_3' => 'An interview is conducted with the selected candidates.',
    'steps_txt_4' => 'After the choice is made and the contract is signed the candidate starts to work.',
    'steps_txt_5' => 'The manager keeps the contact with you and helps in resolving any issues.',

    // Why
    'why_ttl' => 'Why customers choose Sweet Home',
    'why_txt_1' => 'Extensive database of proven candidates',
    'why_txt_2' => 'Experience in the home personnel field since 2007',
    'why_txt_3' => 'Individual approach to each client',

    // Form
    'form_ttl' => 'Submit a request to find a home cook',
];

<?php
return [
    'get_all' => 'Points available',
    'discount' => 'Discount by promo code',
    'use_all' => 'Write off all points'
];
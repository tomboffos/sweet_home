<?php

// <br> - означает перенос строки

return [
    // Meta data
    'meta_title' => 'Find a housemaid: House help prices, best housemaids in Almaty',
    'meta_description' => '',
    'meta_keywords' => '',

    // Main Image
    'slide_ttl' => 'We will find the best maid<br>for your house',
    'slide_btn' => 'Leave a request',

    // Intro
    'intro_ttl' => 'The top housemaids of Almaty are in Sweet Home’s database',
    'intro_txt' => 'Are you in search for a responsible, qualified maid, a housekeeper or just want some help 
                    with your daily chores and cleaning? Look no further! 
                    Complete the application below and we will be happy to find the most suitable 
                    housemaid with respect to all your requirements.',
    'intro_btn' => 'Leave a request',

    // Cols
    'cols_1_ttl' => 'Full-time housemaid',
    'cols_1_price' => 'From 120 000 tenge per month',
    'cols_1_desc_ttl' => 'Responsibilities',
    'cols_1_desc_txt' => '<li>Daily cleaning</li>
                          <li>Order maintenance</li>
                          <li>Spring cleaning</li>
                          <li>Laundry and ironing</li>
                          <li>Caring for indoor plants</li>
                          <li>Shopping for food and cleaning products</li>',

    'cols_2_ttl' => 'Part-time housemaid',
    'cols_2_price' => 'From 7 000 tenge per day',
    'cols_2_desc_ttl' => 'Responsibilities',
    'cols_2_desc_txt' => '<li>Daily cleaning</li>
                          <li>Order maintenance</li>
                          <li>Spring cleaning</li>
                          <li>Laundry and ironing</li>
                          <li>Caring for indoor plants</li>
                          <li>Shopping for food and cleaning products</li>',

    'cols_3_ttl' => 'Housekeeper-Cook',
    'cols_3_price' => 'From 8 000 tenge per day',
    'cols_3_desc_ttl' => 'Responsibilities',
    'cols_3_desc_txt' => '<li>Daily cleaning</li>
                          <li>Order maintenance</li>
                          <li>Spring cleaning</li>
                          <li>Laundry and ironing</li>
                          <li>Caring for indoor plants</li>
                          <li>Shopping for food and cleaning products</li>
                          <li>Preparation of simple homemade meals</li>',

    'cols_btn' => 'Leave a request',

    // Team
    'team_ttl' => 'Our housemaids',
    'team_txt_1' => 'Are familiar with the latest cleaning methods',
    'team_txt_2' => 'Know how to use modern appliances',
    'team_txt_3' => 'Use green and eco-products in cleaning',
    'team_txt_4' => 'Know how to care for a VIP wardrobe',
    'team_txt_5' => 'Have letters of recommendation from previous employers',
    'team_txt_6' => 'Are citizens of Kazakhstan',
    'team_txt_7' => 'Have at least three years of experience of work in a family',
    'team_txt_8' => 'Pass a multi-level verification before being included into the database',

    // Steps
    'steps_ttl' => 'How does the recruiting process go?',
    'steps_btn' => 'Leave a request',
    'steps_ttl_1' => 'Application',
    'steps_ttl_2' => 'Selection',
    'steps_ttl_3' => 'Interview',
    'steps_ttl_4' => 'Contract',
    'steps_ttl_5' => 'Support',
    'steps_txt_1' => 'You leave your order on the website or by phone. Placing an order is free!',
    'steps_txt_2' => 'The manager selects suitable candidates from the database and sends you the CVs.',
    'steps_txt_3' => 'An interview is conducted with the selected candidates.',
    'steps_txt_4' => 'After the choice is made and the contract is signed the candidate starts to work.',
    'steps_txt_5' => 'The manager keeps the contact with you and helps in resolving any issues.',

    // Why
    'why_ttl' => 'Why customers choose Sweet Home',
    'why_txt_1' => 'Extensive database of proven candidates',
    'why_txt_2' => 'Experience in the home personnel field since 2007',
    'why_txt_3' => 'Individual approach to each client',

    // Form
    'form_ttl' => 'Submit a request to find a housemaid',
];

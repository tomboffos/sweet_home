<?php

// <br> - означает перенос строки

return [
    // Meta data
    'meta_title' => 'Find a nurse: Home nurse prices, best caregivers in Almaty',
    'meta_description' => '',
    'meta_keywords' => '',

    // Main Image
    'slide_ttl' => 'We will find the best caregiver<br>for your loved ones',
    'slide_btn' => 'Leave a request',

    // Intro
    'intro_ttl' => 'The top nurses of Almaty are in Sweet Home’s database',
    'intro_txt' => 'Do you need help in caring for loved one? Are you looking for a qualified medical professional? 
                    Or a person who can be a caregiver while helping with some chores at the same time? 
                    Just leave a request using the from below and our manager will contact you to discuss the details.',
    'intro_btn' => 'Leave a request',

    // Cols
    'cols_1_ttl' => 'Nurse with a medical degree',
    'cols_1_price' => 'From 5000 tenge per day',
    'cols_1_desc_ttl' => 'Responsibilities',
    'cols_1_desc_txt' => '<li>Caring for the ward</li>
                          <li>Compliance with the day routine</li>
                          <li>Monitoring of the physical condition of the ward</li>
                          <li>Compliance with the doctor\'s recommendations</li>
                          <li>Food preparation for the ward</li>
                          <li>Psychological support</li>
                          <li>Maintenance of the order and cleanness of the ward’s room</li>
                          <li>Hygienic procedures</li>
                          <li>Assistance during doctor visits</li>
                          <li>Special medical procedures</li>',

    'cols_2_ttl' => 'Nurse-assistant',
    'cols_2_price' => 'From 120 000 tenge per month',
    'cols_2_desc_ttl' => 'Responsibilities',
    'cols_2_desc_txt' => '<li>Caring for the ward</li>
                          <li>Compliance with the day routine</li>
                          <li>Monitoring of the physical condition of the ward</li>
                          <li>Compliance with the doctor\'s recommendations</li>
                          <li>Food preparation for the ward</li>
                          <li>Psychological support</li>
                          <li>Maintenance of the order and cleanness of the ward’s room</li>
                          <li>Hygienic procedures</li>
                          <li>Support during doctor visits</li>
                          <li>Basic medical procedures: injections, pressure measurements etc.</li>',

    'cols_3_ttl' => 'Part-time Nurse',
    'cols_3_price' => 'From 700 tenge per hour',
    'cols_3_desc_ttl' => 'Responsibilities',
    'cols_3_desc_txt' => '<li>Caring for the ward</li>
                          <li>Compliance with the day routine</li>
                          <li>Monitoring of the physical condition of the ward</li>
                          <li>Compliance with the doctor\'s recommendations</li>
                          <li>Food preparation for the ward</li>
                          <li>Psychological support</li>
                          <li>Maintenance of the order and cleanness of the ward’s room</li>
                          <li>Hygienic procedures</li>
                          <li>Support during doctor visits</li>
                          <li>Basic medical procedures: injections, pressure measurements etc.</li>',

    'cols_btn' => 'Leave a request',

    // Team
    'team_ttl' => 'Our nurses',
    'team_txt_1' => 'Are experienced in caring for people in need of help',
    'team_txt_2' => 'Have a special education',
    'team_txt_3' => 'Treat their wards with care and respect',
    'team_txt_4' => 'Are responsible and reliable',
    'team_txt_5' => 'Have letters of recommendation from previous employers',
    'team_txt_6' => 'Are citizens of Kazakhstan',
    'team_txt_7' => 'Have at least three years of experience of work in a family',
    'team_txt_8' => 'Pass a multi-level verification before being included into the database',

    // Steps
    'steps_ttl' => 'How does the recruiting process go?',
    'steps_btn' => 'Leave a request',
    'steps_ttl_1' => 'Application',
    'steps_ttl_2' => 'Selection',
    'steps_ttl_3' => 'Interview',
    'steps_ttl_4' => 'Contract',
    'steps_ttl_5' => 'Support',
    'steps_txt_1' => 'You leave your order on the website or by phone. Placing an order is free!',
    'steps_txt_2' => 'The manager selects suitable candidates from the database and sends you the CVs.',
    'steps_txt_3' => 'An interview is conducted with the selected candidates.',
    'steps_txt_4' => 'After the choice is made and the contract is signed the candidate starts to work.',
    'steps_txt_5' => 'The manager keeps the contact with you and helps in resolving any issues.',

    // Why
    'why_ttl' => 'Why customers choose Sweet Home',
    'why_txt_1' => 'Extensive database of proven candidates',
    'why_txt_2' => 'Experience in the home personnel field since 2007',
    'why_txt_3' => 'Individual approach to each client',

    // Form
    'form_ttl' => 'Submit a request to find a nurse',
];

<?php

return [
    'fills_empty' => 'Some fields are filled in incorrectly!',
    'something_went_wrong' => 'An unknown error has occurred!',
    'error_insert_bd' => 'There was a problem adding data to the database! Please inform the administration!',
    'cant_delete_last_admin' => 'If you delete this administrator, you will not be able to log in!',
    'cant_delete_last_category' => 'You can not delete all categories!',
    'calculator_total_summ_error' => 'The problem with counting the total amount!',
    'discount_code_error' => 'The problem with the preservation of the promotional code. One of the reasons is possible: 1) the promotional code does not exist 2) the promotional code is off 3) its expired 4) the promotional code is not applicable for this type of order',
    'error_while_send_form' => 'There was a problem sending data! Please contact the property!',
    'email_must_change_on_user_page' => 'To change your email account, log in to your account or register a new account!',
    'go_auth_msg' => 'You are already registered on the site, please log in!',
    'workers_order_added' => 'Added application for recruitment!',
    'discount_is_valid' => 'Promo code successfully applied!',
    'discount_is_not_valid' => 'Promo code does not exist or has expired!',
    'points_is_valid' => 'Points successfully applied!',
    'points_is_not_valid' => 'Error. Perhaps you do not have the required number of points.',
    'first_delete_or_move_all_workers' => 'Employees are attached to this category. First, remove or move employees to another category!',
    'change_email_on_user_page' => 'To change email, please go to your personal page and make changes!',
    'password_send_to_your_email' => 'The password has been sent to your email!',
    'cleaners_order_added' => 'Added cleaning order',
    'email_from_text' => 'Administration site HomeSweet',
    'subject_order_workers' => 'Added application for recruitment!',
    'subject_order_cleaning' => 'Cleaning request added!',
    'new_client_registered' => 'An account on HomeSweet has been registered for you!',
    'errors_was_founded_details_here' => 'An error has occurred. Error details:',
    'callme_subject' => 'Customer left a request for a call back',
    'email_sended' => 'The application has been sent!',
    'user_edit_success' => 'Changes saved!',
    'email_exist' => 'Email is occupied!',
    'old_password_incorrect' => 'Old password entered incorrectly!',
    'some_fields_incorrect' => 'Some fields are filled in incorrectly!',
    'discount_was_used' => 'You have already used this promotional code!',
    'order_complete_subject' => 'Order completed!',
    'choose_cleaner_before_order_complete' => 'Before completing an order, select a clinician!',

    'periodicity' => 'Type of cleaning:',
    'your_order' => 'Your order',
    'preferences_h' => 'Our advantages',
    'preferences_text' => '
    <p>Our professionals developed own technological sequence of Green cleaning:</p>
    <ul>
        <li>Only environmentally safe detergents without phosphates and active chlorine are used. Organic substances are absolutely safe for the environment.</li>
        <li>Steam equipment is used. It allows good washing and disinfection of the surface without chemical agents. Steam cleaner removes 99,9% of popular bacteria without application of cleaning substances. (*Included into spring cleaning)</li>
        <li>Only single-use wiping materials and inventory. There are no residues of chemicals and harmful microorganisms on new wiping materials.</li>
        <li>The Company introduced new service with application of air cleaning devices, namely ozonizers. (*Included into spring cleaning)</li>
        <li>Sweet Home\'s specialists passed special training on application of “green” facilities.</li>
    </ul>
    ',
    'permanent_cleaning' => 'Permanent cleaning (Housekeeper)',
    'apartment_info' => 'Apartment info',
    'additional_services' => 'Additional services:',
    'cleaning_type' => 'Type of cleaning:',
    'cleaning' => 'Cleaning',
    'green_cleaning' => 'GREEN cleaning',
    'spring_cleaning' => 'Spring cleaning',
    'comments' => 'Comments',
    'send' => 'Send',
    'apply_points' => 'Apply points',
    'q_n_a' => 'Questions and answers',
    'spring_green_cleaning' => 'Spring GREEN cleaning',
    'spring_green_cleaning_text_1' => 'Green cleaning refers to using cleaning methods with environmentally friendly ingredients and procedures. Green cleaning techniques and products avoid the use of products which contain toxic chemicals, some of which emit volatile organic compounds causing respiratory, dermatological and other conditions.

Green cleaning is safe for pregnant women, small children and highly allergic individuals.',
    'what_included_general' => 'What is included into spring-cleaning?',
    'what_included' => 'What is included?',
    'what_included_standard' => 'What is included into cleaning?',
    'clean_list_standard_title' => 'Following is included into cleaning:',
    'clean_list_standard' => '
    <li><i class="fas fa-heart"></i> dust wiping on all accessible surfaces;</li>
    <li><i class="fas fa-heart"></i>  external cleaning of household appliances;</li>
    <li><i class="fas fa-heart"></i> cleaning of all mirror and glass surfaces;</li>
    <li><i class="fas fa-heart"></i> wiping of lamps (without removal of bowls), besides cut-glass chandeliers;</li>
    <li><i class="fas fa-heart"></i> dust removal from sockets and switches;</li>
    <li><i class="fas fa-heart"></i> change of linen (if necessary) and coverage of bed;</li>
    <li><i class="fas fa-heart"></i> cleaning of floors, using vacuum cleaner (if it is available in your home);</li>
    <li><i class="fas fa-heart"></i> wiping of floor moldings and washing of floors;</li>
    <li><i class="fas fa-heart"></i> cleaning of waste basket and taking the rubbish out;</li>
    <li><i class="fas fa-heart"></i> general cleanup;</li>
    <li><i class="fas fa-heart"></i> dry cleaning of carpets and soft furniture;</li>
    <li><i class="fas fa-heart"></i> cleaning of stove, wash basin, mixing basin;</li>
    <li><i class="fas fa-heart"></i> cleaning of microwave (inside and outside);</li>
    <li><i class="fas fa-heart"></i> washing of dishes in kitchen sink (no more than 10 minutes);</li>
    <li><i class="fas fa-heart"></i> washing of bathroom fixtures;</li>
    <li><i class="fas fa-heart"></i> washing of shower cabin.</li>
    ',
    'clean_list_general_title' => 'Following is included into spring- cleaning:',
    'clean_list_general' => '
    <li><i class="fas fa-heart"></i> dust wiping on all accessible surfaces;</li>
    <li><i class="fas fa-heart"></i> external cleaning of household appliances;</li>
    <li><i class="fas fa-heart"></i> cleaning of all mirror and glass surfaces;</li>
    <li><i class="fas fa-heart"></i> wiping of lamps (without removal of bowls), besides cut-glass chandeliers;</li>
    <li><i class="fas fa-heart"></i> dust removal from sockets and switches;</li>
    <li><i class="fas fa-heart"></i> change of linen (if necessary) and coverage of bed;</li>
    <li><i class="fas fa-heart"></i> cleaning of floors, using vacuum cleaner (if it is available in your home);</li>
    <li><i class="fas fa-heart"></i> wiping of floor moldings and washing of floors;</li>
    <li><i class="fas fa-heart"></i> cleaning of waste basket and taking the rubbish out;</li>
    <li><i class="fas fa-heart"></i> general cleanup;</li>
    <li><i class="fas fa-heart"></i> dry cleaning of carpets and soft furniture;</li>
    <li><i class="fas fa-heart"></i> cleaning of stove, wash basin, mixing basin;</li>
    <li><i class="fas fa-heart"></i> cleaning of microwave (inside and outside);</li>
    <li><i class="fas fa-heart"></i> washing of dishes in kitchen sink (no more than 10 minutes);</li>
    <li><i class="fas fa-heart"></i> washing of bathroom fixtures;</li>
    <li><i class="fas fa-heart"></i> washing of shower cabin;</li>
    <li><i class="fas fa-heart"></i> cleaning of soft furniture using steam generator;</li>
    <li><i class="fas fa-heart"></i> air ozonization;</li>
    <li><i class="fas fa-heart"></i> wiping of furniture internal surface (shelves, boxes, if they are empty);</li>
    <li><i class="fas fa-heart"></i> washing of pipes and radiators;</li>
    <li><i class="fas fa-heart"></i> dust wiping from walls from floor to ceiling;</li>
    <li><i class="fas fa-heart"></i> removal of medium and acute pollutions (scale, oil, lime scale, obstinate stains and other);</li>
    <li><i class="fas fa-heart"></i> cleaning of air extraction;</li>
    <li><i class="fas fa-heart"></i> washing of tiled walls.</li>
    ',
    'green_cleaning_of_sweet_home' => '
    <div class="title">GREEN cleaning</div>
    <div class="sub-title">of Sweet Home company is</div>
    ',
    'unique_tecnology' => 'A unique<br /> technology of cleaning ',
    'detergents' => 'Detergents<br /> without chemicals',
    'disposable_materials' => 'Single-use<br /> wiping materials',
    'steam_generator' => 'Cleaning with powerful<br /> steam generator',
    'ozonizing' => 'Air ozonizing',
    'safety' => 'Safety for children,<br /> pregnant and highly<br /> allergic individuals',
    'it_takes_1_minute' => 'It takes no more than 1 minute.',
    'order_clean' => 'Order cleaning',
    'order_eco_clean' => 'Order GREEN cleaning',
    'order_spring_clean' => 'Order spring cleaning',
    'choose_home_staff' => 'Hire Home Staff',
    'babysitter' => 'Babysitter',
    'cook' => 'Cook',
    'nurse' => 'Nurse',
    'nurses' => 'Nurses',
    'housekeeper' => 'Housekeeper',
    'submit_application' => 'Submit your application',
    'number_of_rooms' => 'Number of rooms',
    'spare_yourself_time' => 'Spare yourself some time',
    'it_takes_no_more_then_minute' => 'It takes no more than 1 minute',
    'answer_questions' => 'Answer your questions',
    'clients_feedback' => 'Feedback of our clients',
    'address' => 'Address',
    'full_address' => '<b>Address:</b> Almaty, Husainov str., 225, office 322',
    'working_hours' => '<b>Opening hours:</b> on weekdays 10:00 - 18:00',
    'email' => 'E-mail:',
    'phone' => 'Telephone',
    'phones' => 'Telephones',
    'password' => 'Password',
    'child_age' => 'The age of the child (children)',
    'work_schedule' => 'Estimated work schedule',
    'five_days' => 'five days',
    '2_after_2' => '2 days after 2',
    '3_days' => '3 times a week',
    '1_days' => 'once a week',
    'night_babysitter' => 'night babysitter',
    'night_nurse' => 'night nurse',
    'permanent_housekeeper' => 'Housekeeper on a permanent basis',
    'another_schedule' => 'another schedule',
    'tg' => 'T',
    '_5_' => 'from 5000 tenge per day',
    '_6_' => 'from 6000 tenge per day',
    '_7_' => '7 000 T',
    '_17_' => 'from 17 300 T',
    '_70_' => 'from 70 000 T per month',
    '_80_' => 'from 80 000 T per month',
    'preferred_cuisine' => 'Preferred cuisine',
    'home_cuisine' => 'home cuisine',
    'diet_food' => 'diet food',
    'eastern' => 'eastern',
    'european' => 'european',
    'other' => 'other',
    'about_ward' => 'Tell about the ward (age, gender, help needed)',
    'show_all' => 'Show all',
    'follow_us' => 'Follow us',
    'payment' => 'We accept',
    'apply' => 'Apply now',
    'main_menu' => 'Main menu',
    'design' => 'Design',
    'order_call' => 'Order call',
    'enter_phone' => 'Enter your phone number',
    'call_me_back' => 'Call me back',
    'main_page' => 'Main page',
    'reviews' => 'Reviews',
    'services' => 'Services',
    'card_binding_error' => 'Card binding error',
    'successful_transaction' => 'The transaction was successful',
    'choose_your_interest' => 'Choose your field of interest',
    'free' => 'For free',
    'total' => 'Total',
    'additional_cleaner' => 'Additional cleaner',
    'other_issues' => 'Other issues',
    'open_in_new_window' => 'Will open in a new window',
    'cleaner' => 'Cleaner',
    'cleaners' => 'Cleaners',
    'staff_recruitment' => 'Staff recruitment',
    'room' => 'room',
    'toilet' => 'bathrooms',
    'window' => 'window',
    'date' => 'Date',
    'time' => 'Time',
    'date_time' => 'Date and time',
    'contacts' => 'Contact details',
    'name' => 'Name',
    'street' => 'Street',
    'residential_complex' => 'residential complex',
    'building' => 'building',
    'house' => 'house',
    'apartment' => 'apartment',
    'way_of_payment' => 'Payment method',
    'cash' => 'Cash',
    'card' => 'Bank card',
    'add_promo_code' => 'Add a promotional code',
    'order_comment' => 'Comment to the order',
    'avg_cleaning_cost' => 'Average cost of cleaning',
    'service' => 'Service',
    'price' => 'Price',
    'daily_cleaning' => 'Daily cleaning',
    'daily' => 'daily',
    'apartment_area' => 'Area of the apartment / house',
    'best_care' => '',
    'best_care_text' => '<br />
<p>Our professionals developed own technological sequence of Green cleaning:</p>
<ul>
    <li>Only environmentally safe detergents without phosphates and active chlorine are used. Organic substances are absolutely safe for the environment.</li>
    <li>Steam equipment is used. It allows good washing and disinfection of the surface without chemical agents. Steam cleaner removes 99,9% of popular bacteria without application of cleaning substances. (*Included into spring cleaning)</li>
    <li>Only single-use wiping materials and inventory. There are no residues of chemicals and harmful microorganisms on new wiping materials.</li>
    <li>The Company introduced new service with application of air cleaning devices, namely ozonizers. (*Included into spring cleaning)</li>
    <li>Sweet Home\'s specialists passed special training on application of “green” facilities.</li>
</ul>
    ',
    'additional_cliner' => 'Additional cliner',
    'free_additional_cliner' => 'Free additional cliner',
    'almaty' => 'Almaty',
    'Домработница' => 'Housekeeper',
    'Повар' => 'Cook',
    'Сиделка' => 'Nurse',
    'Няня' => 'Babysitter',
    'Что такое Избранные?' => 'What is the Favorites?',
    'Что такое Чёрный список?' => 'What is the black list?',
    'В каких городах вы работаете?' => 'What cities do you work in?',
    'У меня есть особенный запрос, к кому мне можно обратиться?' => 'I have a special request who can I contact?',
    'Время работы офиса:' => 'Office hours:',
    'enter_quantity_unit' => 'Enter the number of units',
    'enter_quantity_hours' => 'Enter the number of hours',
    'unit' => 'unit',
];

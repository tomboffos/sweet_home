<?php

// <br> - означает перенос строки

return [
    // Meta data
    'meta_title' => 'Find a nanny: Nanny prices, best nannies and babysitters in Almaty',
    'meta_description' => '',
    'meta_keywords' => '',

    // Main Image
    'slide_ttl' => 'We will find the best nanny<br>for your child',
    'slide_btn' => 'Leave a request',

    // Intro
    'intro_ttl' => 'The top nannies of Almaty are in Sweet Home’s database',
    'intro_txt' => 'Are you looking for a babysitter, a nanny or a governess? 
                    Either soft or strict, young and active or experienced and calm — we know that 
                    every child is special and therefore needs a special nanny! 
                    We will be happy to help you find the best nanny 
                    for your child and will accommodate any special requests.',
    'intro_btn' => 'Leave a request',

    // Cols
    'cols_1_ttl' => 'Nanny for infant',
    'cols_1_price' => 'From 120,000 tenge per month<br><br>',
    'cols_1_desc_ttl' => 'Responsibilities',
    'cols_1_desc_txt' => '<li>Compliance with the day routine of the child</li>
                                    <li>Following the recommendations of the pediatrician, visits to a doctor</li>
                                    <li>Hygiene procedures: bathing, swaddling, changing diapers, etc.
                                    </li>
                                    <li>Feeding of the child using the knowledge of the nutritional requirements for every age
                                    </li>
                                    <li>Care for the child’s belongings, clothes and toys</li>
                                    <li>Maintenance of the order and cleanness of the child’s room</li>
                                    <li>Educational and developmental activities in accordance with the child’s age</li>
                                    <li>Regular walks</li>',

    'cols_2_ttl' => 'Nanny',
    'cols_2_price' => 'From 100,000 tenge per month<br><br>',
    'cols_2_desc_ttl' => 'Responsibilities',
    'cols_2_desc_txt' => '<li>Complete child care</li>
                                    <li>Compliance with the day routine of the child</li>
                                    <li>Hygiene procedures</li>
                                    <li>Accompaniment to the workshops and classes</li>
                                    <li>Educational and developmental activities in accordance with the child’s age</li>
                                    <li>Preparation of meals for the child</li>
                                    <li>Care for the child’s belongings, clothes and toys</li>
                                    <li>Maintenance of the order and cleanness of the child’s room</li>',

    'cols_3_ttl' => 'Governess',
    'cols_3_price' => 'From 2 000 tenge per hour<br>or 150 000 tenge per month',
    'cols_3_desc_ttl' => 'Responsibilities',
    'cols_3_desc_txt' => '<li>Сhild care and  compliance with the day routine of the child</li>
                                    <li>Educational and developmental activities in accordance with the child’s age, among them using early development techniques
                                    </li>
                                    <li>School preparation and help with the homework
                                    </li>
                                    <li>Creative activities like music, art or foreign languages
                                    </li>
                                    <li>Recreational activities like walks, games, accompaniment to sport classes
                                    </li>
                                    <li>Cultural activities like visits to museums and galleries</li>',

    'cols_btn' => 'Leave a request',

    // Team
    'team_ttl' => 'Our nannies',
    'team_txt_1' => 'Sincerely love their job and children',
    'team_txt_2' => 'Have a specialized education',
    'team_txt_3' => 'Know the popular child development techniques',
    'team_txt_4' => 'Have letters of recommendation from previous employers',
    'team_txt_5' => 'Are citizens of Kazakhstan',
    'team_txt_6' => 'Have at least three years of experience of work in a family',
    'team_txt_7' => 'Pass a multi-level verification before being included into the database',

    // Steps
    'steps_ttl' => 'How does the recruiting process go?',
    'steps_btn' => 'Leave a request',
    'steps_ttl_1' => 'Application',
    'steps_ttl_2' => 'Selection',
    'steps_ttl_3' => 'Interview',
    'steps_ttl_4' => 'Contract',
    'steps_ttl_5' => 'Support',
    'steps_txt_1' => 'You leave your order on the website or by phone. Placing an order is free!',
    'steps_txt_2' => 'The manager selects suitable candidates from the database and sends you the CVs.',
    'steps_txt_3' => 'An interview is conducted with the selected candidates.',
    'steps_txt_4' => 'After the choice is made and the contract is signed the candidate starts to work.',
    'steps_txt_5' => 'The manager keeps the contact with you and helps in resolving any issues.',

    // Why
    'why_ttl' => 'Why customers choose Sweet Home',
    'why_txt_1' => 'Extensive database of proven candidates',
    'why_txt_2' => 'Experience in the home personnel field since 2007',
    'why_txt_3' => 'Individual approach to each client',

    // Form
    'form_ttl' => 'Submit a request to find a nanny',
];

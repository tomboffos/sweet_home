<?php

// <br> - означает перенос строки

return [
    // Meta data
    'meta_title' => '',
    'meta_description' => '',
    'meta_keywords' => '',

    // Main Image
    'slide_ttl' => 'Найдём лучшую домработницу<br>для вашего дома',
    'slide_btn' => 'Оставить заявку',

    // Intro
    'intro_ttl' => 'В базе агентства Sweet Home<br>собраны лучшие домработницы города Алматы',
    'intro_txt' => 'Агентство Sweet Home осуществляет подбор домашнего персонала в городе Алматы.
                    Если вам требуется ответственная, квалифицированная домработница, заполните заявку ниже. 
                    Мы подберём самого подходящего специалиста в соответствии с вашими пожеланиями.',
    'intro_btn' => 'Оставить заявку',

    // Cols
    'cols_1_ttl' => 'Домработница<br>на полную занятость',
    'cols_1_price' => 'От 120 000 тенге в месяц',
    'cols_1_desc_ttl' => 'Обязанности',
    'cols_1_desc_txt' => '<li>Повседневная уборка</li>
                          <li>Поддержание порядка</li>
                          <li>Генеральная уборка</li>
                          <li>Стирка и глажение белья</li>
                          <li>Уход за комнатными растениями</li>
                          <li>Покупка продуктов, чистящих средств</li>',

    'cols_2_ttl' => 'Домработница<br>на неполный день',
    'cols_2_price' => 'От 7 000 тенге в день',
    'cols_2_desc_ttl' => 'Обязанности',
    'cols_2_desc_txt' => '<li>Повседневная уборка</li>
                          <li>Поддержание порядка</li>
                          <li>Генеральная уборка</li>
                          <li>Стирка и глажение белья</li>
                          <li>Уход за комнатными растениями</li>
                          <li>Покупка продуктов, чистящих средств</li>',

    'cols_3_ttl' => 'Домработница с приготовлением пищи',
    'cols_3_price' => 'От 8 000 тенге в день',
    'cols_3_desc_ttl' => 'Обязанности',
    'cols_3_desc_txt' => '<li>Повседневная уборка</li>
                          <li>Поддержание порядка</li>
                          <li>Генеральная уборка</li>
                          <li>Стирка и глажение белья</li>
                          <li>Уход за комнатными растениями</li>
                          <li>Покупка продуктов, чистящих средств</li>
                          <li>Приготовление домашней еды</li>',

    'cols_btn' => 'Оставить заявку',

    // Team
    'team_ttl' => 'Наши домработницы',
    'team_txt_1' => 'Знакомы с последними методами уборки',
    'team_txt_2' => 'Умеют пользоваться современной бытовой техникой',
    'team_txt_3' => 'Применяют ЭКОсредства в уборке',
    'team_txt_4' => 'Умеют ухаживать за ВИП гардеробом',
    'team_txt_5' => 'Имеют рекомендации и контакты предыдущих работодателей',
    'team_txt_6' => 'Являются гражданами Казахстана',
    'team_txt_7' => 'Имеют, как минимум, трёхлетний опыт работы в семье',
    'team_txt_8' => 'Проходят многоступенчатую проверку',

    // Steps
    'steps_ttl' => 'Как проходит подбор?',
    'steps_btn' => 'Оставить заявку',
    'steps_ttl_1' => 'Заявка',
    'steps_ttl_2' => 'Подбор',
    'steps_ttl_3' => 'Интервью',
    'steps_ttl_4' => 'Договор',
    'steps_ttl_5' => 'Поддержка',
    'steps_txt_1' => 'Вы оставляете заявку на сайте или по телефону. Размещение заявки бесплатно!',
    'steps_txt_2' => 'Менеджер подбирает подходящих кандидатов и высылает вам на рассмотрение резюме.',
    'steps_txt_3' => 'С выбранными кандидатами проводится собеседование.',
    'steps_txt_4' => 'После заключения договора кандидат приступает к работе.',
    'steps_txt_5' => 'Менеджер сопровождает договор и помогает в решении вопросов.',

    // Why
    'why_ttl' => 'Почему выбирают Sweet Home',
    'why_txt_1' => 'Обширная база проверенных кандидатов',
    'why_txt_2' => 'Опыт работы с 2007 года',
    'why_txt_3' => 'Индивидуальный подход к каждому клиенту',

    // Form
    'form_ttl' => 'Оставить заявку на подбор домработницы',
];

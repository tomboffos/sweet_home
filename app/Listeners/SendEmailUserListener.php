<?php

namespace App\Listeners;

use App\Events\OrderAdded;
use App\Models\Settings;
use Mail;
use Log;

class SendEmailUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  OrderAdded  $event
     * @return void
     */
    public function handle(OrderAdded $event)
    {
        // if guest
        if(!isset($event->data->client_email) || is_null($event->data->client_email)) {
            return false;
        }

        try {
            $mail = Mail::send("email.{$event->data->view}_client_order", ['event' => $event], function ($m) use ($event) {
                $m->from(config('mail.from.address'), trans("messages.email_from_text"));
                $m->replyTo(config('mail.from.address'), trans("messages.email_from_text"));
                $m->to($event->data->client_email, $event->data->client_name)->subject(trans("messages.{$event->data->subject}"));
            });
            return true;
        } catch(\Exception $e) {
            Log::error('Email wasn\'t send. SendEmailUserListener!'.$e);
            return false;
        }
    }
}

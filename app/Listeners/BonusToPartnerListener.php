<?php

namespace App\Listeners;

use App\DiscountSecond;
use App\Events\OrderComplete;
use App\Events\OrderDiscountCompleted;
use App\Models\Discounts;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class BonusToPartnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderComplete  $event
     * @return bool
     */
    public function handle(OrderDiscountCompleted $event)
    {
        //

        $discount = Discounts::where('id',$event->order->discount_id)->first();
        $partner = User::where('id',$discount->user_id)->first();
        if (!$partner){
            return false;
        }else{
            $bonus = DiscountSecond::where('type',0)->first();
            $partner->points += $bonus->amount;
            $partner->save();

            try {

                $mail_second = Mail::send("email.order_complete_partner", ['event' => $event,'partner'=>$partner,'bonus'=>$bonus,'discount'=>$discount], function ($m) use ($partner, $event) {
                    $m->from(config('mail.from.address'), trans("messages.email_from_text"));
                    $m->replyTo(config('mail.from.address'), trans("messages.email_from_text"));
                    $m->to($partner->email, $partner->email)->subject(trans("messages.order_complete_subject"));
                });
                return true;

            } catch(\Exception $e) {
                Log::error('Email_Error !'.$e);
                return false;

            }




        }





    }
}

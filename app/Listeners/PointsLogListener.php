<?php

namespace App\Listeners;

use App\Events\PointsLogEvent;
use App\Models\PointsLog;

class PointsLogListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * add to log
     * @param [int] $user_id [user id]
     * @param [string] $reason
     * @return [boolean]
     */
    public function handle(PointsLogEvent $event)
    {
        if(empty($event->user_id) || empty($event->reason) || empty($event->amount)) {
            return false;
        }

        $data = PointsLog::create([
            'user_id' => $event->user_id,
            'reason' => $event->reason,
            'amount' => $event->amount,
        ]);

        if(!$data) {
            return false;
        }

        return true;
    }
}

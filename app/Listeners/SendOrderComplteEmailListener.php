<?php

namespace App\Listeners;

use App\Events\OrderComplete;
use App\Models\Settings;
use Mail;
use Log;

class SendOrderComplteEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *  
     * @param  OrderAdded  $event
     * @return void
     */
    public function handle(OrderComplete $event)
    {
        if(!isset($event->user->email) || empty($event->user->email)) {
            return false;
        }

        try {

             Mail::send("email.order_complete_additional", ['event' => $event], function ($m) use ($event) {
                $m->from(config('mail.from.address'), trans("messages.email_from_text"));
                $m->replyTo(config('mail.from.address'), trans("messages.email_from_text"));
                $m->to($event->user->email, $event->user->email)->subject(trans("messages.order_complete_subject"));
            });
            return true;
        } catch(\Exception $e) {
            Log::error('Email wasn\'t send. SendOrderComplteEmailListener!' .$e);
            return false;
        }
    }
}

<?php

namespace App\Listeners;

use App\Events\OrderAdded;
use App\Models\Settings;
use Mail;
use Log;

class SendEmailAdminListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  OrderAdded  $event
     * @return void
     */
    public function handle(OrderAdded $event)
    {
        // get admin emails
        $recipients = Settings::where('name', 'emails')->get();

        if(count($recipients)) {
            $recipients = explode(',', $recipients[0]->value);
        } else {
            return false;
        }

        try {
            $mail = Mail::send("email.{$event->data->view}_admin_order", ['event' => $event], function ($m) use ($event, $recipients) {
                $m->from(config('mail.from.address'), trans("messages.email_from_text"));

                if(isset($event->data->client_email) && !is_null($event->data->client_email) && isset($event->data->client_name) && !is_null($event->data->client_name)) {
                    $m->replyTo($event->data->client_email, $event->data->client_name);
                }

                // send to admin
                if(isset($recipients) && count($recipients) > 1) {
                    $m->to($recipients[0])->bcc($recipients[1])->subject(trans("messages.{$event->data->subject}"));
                } else if(isset($recipients) && count($recipients) > 0) {
                    $m->to($recipients[0])->subject(trans("messages.{$event->data->subject}"));
                }
            });
            return true;
        } catch(\Exception $e) {
            Log::error('Email wasn\'t send. SendEmailAdminListener!'.$e);
            return false;
        }
    }
}

<?php

namespace App\Listeners;

use App\Events\ClientRegistered;
use Mail;
use Log;

class SendPasswordToClientListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  ClientRegistered  $event
     * @return void
     */
    public function handle(ClientRegistered $event)
    {
        try {
            $mail = Mail::send("email.new_client", ['event' => $event], function ($m) use ($event) {
                $m->from(config('mail.from.address'), trans("messages.email_from_text"));
                $m->replyTo(config('mail.from.address'), trans("messages.email_from_text"));
                $m->to($event->data->client_email, $event->data->client_name)->subject(trans("messages.new_client_registered"));
            });
            return true;
        } catch(\Exception $e) {
            Log::error('Email wasn\'t send. SendPasswordToClientListener!');
            return false;
        }
    }
}

<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Events\PointsLogEvent;
use App\Models\Settings;
use App\User;
use Event;
use Log;

class ReferalListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * check user refer
     * @param Login $event [description]
     */
    public function handle(UserRegistered $event)
    {
        $session = session()->get('referal');

        if($session && !empty($session)) {

        	// add mark to regestered user
            $user = User::where('id', $event->user->id)->update([
                'ref' => (int) $session
            ]);

            if($user) {

            	// get reward
            	$reward = Settings::where('name', 'referal')->first();

            	// add points to user, who refered new client
            	$addPoints = User::where('id', (int) $session)->increment(
            		'points', (int) $reward->value
            	);

            	// log points adding
            	if($addPoints) {
            		Event::fire(new PointsLogEvent((int) $session, trans('points.referal'), (int) $reward->value));
            	}
            }

            // delete refereal from session
            session()->forget('referal');
        }

        return true;
    }
}
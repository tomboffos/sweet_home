<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Jenssegers\Date\Date;

class User extends Authenticatable
{
    use EntrustUserTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        // 'city',
        'street',
        'complex',
        'housing',
        'house',
        'flat',
        'rooms_number',
        'bath_rooms_number',
        'subscribe',
        'points',
        'password',
        'card',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * user can has many roles
     * @return [obj]
     */
    public function roles() {
      return $this->belongsToMany('App\Models\Role');
    }

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getDates()
    {
      return ['updated_at'];
    }

    public function getCreatedAtAttribute($date)
    {
      return new Date($date);
    }

    public function getUpdatedAtAttribute($date)
    {
      return new Date($date);
    }

    public function getPublishedAtAttribute($date)
    {
      return new Date($date);
    }

    /**
     * get cleaning orders for this user
     * @return [obj]
     */
    public function ordersCleaners()
    {
        return $this->belongsTo('App\Models\OrdersCleaners', 'id', 'client_id');
    }

    /**
     * get workers orders for this user
     * @return [obj]
     */
    public function ordersWorkers()
    {
        return $this->belongsTo('App\Models\OrdersWorkers', 'id', 'client_id');
    }

    /**
     * get testimonals for this user
     * @return [obj]
     */
    public function testimonals()
    {
        return $this->belongsTo('App\Models\Testimonals', 'id', 'client_id');
    }

    /**
     * user can contain many cleaners in black List
     * @return [obj]
     */
    public function bad_cleaners() {
      return $this->belongsToMany('App\Models\Cleaners', 'cleaners_black_list', 'client_id', 'cleaner_id');
    }

    /**
     * user can contain many cleaners in favorite List
     * @return [obj]
     */
    public function favorite_cleaners() {
      return $this->belongsToMany('App\Models\Cleaners', 'cleaners_favorite', 'client_id', 'cleaner_id');
    }
}

<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // add data to log when add points
        'App\Events\PointsLogEvent' => [
            'App\Listeners\PointsLogListener',
        ],
        'App\Events\OrderAdded' => [
            // send email to admin and client
            'App\Listeners\SendEmailUserListener',

            'App\Listeners\SendEmailAdminListener',
        ],
        'App\Events\OrderComplete' => [
            // send email to client
            'App\Listeners\SendOrderComplteEmailListener',

        ],
        'App\Events\ClientRegistered' => [
            // send automatic generated password no new client
            'App\Listeners\SendPasswordToClientListener',
        ],
        // user registered
        'App\Events\UserRegistered' => [
            // check user refer
            'App\Listeners\ReferalListener',
        ],
        'App\Events\OrderDiscountCompleted' => [
            'App\Listeners\BonusToPartnerListener'
        ],
        // user log out
        // 'Illuminate\Auth\Events\Logout' => [
            // 'App\Listeners\ReferalListener@Logout',
        // ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewFrontServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['front.layout'], 'App\Http\Controllers\Front\ComposerController');
        view()->composer(['front.menu'], 'App\Http\Controllers\Front\ComposerPartsController');
        view()->composer(['front.footer-skills'], 'App\Http\Controllers\Front\ComposerFooterSkillsController');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
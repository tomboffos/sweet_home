<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewAdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['admin.layout'], 'App\Http\Controllers\Admin\ComposerController');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

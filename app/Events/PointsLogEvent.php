<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class PointsLogEvent extends Event
{
    use SerializesModels;

    public $user_id;
    public $reason;
    public $amount;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id, $reason, $amount)
    {
    	$this->user_id = $user_id;
        $this->reason = $reason;
    	$this->amount = $amount;
    }
}
<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class OrderComplete extends Event
{
    use SerializesModels;

    public $order;
    public $user;
    public $cleaner;
    public $points_settings;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($order, $user, $cleaner, $points_settings)
    {
        $this->order = $order;
        $this->user = $user;
        $this->cleaner = $cleaner;
        $this->points_settings = $points_settings;
    }
}

<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class OrderAdded extends Event
{
    use SerializesModels;

    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
}

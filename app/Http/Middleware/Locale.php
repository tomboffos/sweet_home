<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Config;
use Session;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Если пользователь уже был на нашем сайте,
        // то в сессии будет значение выбранного им языка.
        $rawLocale = Session::get('locale');
        // Проверяем, что у пользователя в сессии установлен доступный язык 
        if (in_array($rawLocale, Config::get('app.locales'))) {
            // (а не какая-нибудь бяка)
            // Устанавливаем локаль приложения
            App::setLocale($rawLocale);
        } else {
            // В ином случае присваиваем ей язык по умолчанию
            // Устанавливаем локаль приложения
            App::setLocale(Config::get('app.locale'));
        }
        // И позволяем приложению работать дальше
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'payment/kazkom/success',
        'payment/kazkom/error',
        'payment/kazkom/service',
        'cleaning/checkpoints',
        'cleaning/order',
        'cleaning/checktime',
        'cleaning/checkemail',
        'cleaning/checkdiscount',
        'login/order',
    ];
}

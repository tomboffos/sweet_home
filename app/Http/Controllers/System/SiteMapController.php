<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Front\CommonController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Pages;
use App\Models\Faq;
use App\Models\FaqCategories;
use App\Models\Posts;
use App\Models\Testimonals;
use App\Models\Workers;
use App\Models\WorkersCategories;

class SiteMapController extends CommonController
{
    /**
     * render sitemap
     */
    public function index()
    {
        $xmlCategories = '';
        
        // Faq Categories
        $this->_faqCategories($xmlCategories);

        // Faq Items
        $this->_faqItems($xmlCategories);

        // Pages
        $this->_pagesCategories($xmlCategories);

        // Testimonals
        $this->_testimonalsCategories($xmlCategories);

        // Workers Categories
        $this->_workersCategories($xmlCategories);

        // Workers Items
        $this->_workersItems($xmlCategories);

        // Cleaning
        $this->_cleaningCategories($xmlCategories);

        // Uslugi
        $this->_uslugiCategories($xmlCategories);        

        return response($this->_xmlIndexTmpl($xmlCategories))
            ->header('Content-Type', 'application/xml');
    }

    /**
     * Faq items
     */
    public function faqItems()
    {
        $items = Faq::where('status', 1)->get();

        $xml = '';
        
        if(count($items)) {

            $date = '';

            foreach($items as $item) {

                $category = $items = FaqCategories::where('id', $item->category)->where('status', 1)->first();

                if(count($category)) {
                    $date = date('Y-m-d', strtotime($item->updated_at)) . 'T' . date('H:i:s', strtotime($item->updated_at)) . '.000' . date('P', strtotime($item->updated_at));
                    $xml .= '<url><loc>' . url("/faq/{$category->id}-{$category->slug}/{$item->id}-{$item->slug}") . '</loc><lastmod>' . $date . '</lastmod></url>';
                }
            }

            if(!empty($xml)) {
                return response($this->_xmlItemsTmpl($xml))
                    ->header('Content-Type', 'application/xml');
            }

        }

        return abort(404);
    }

    /**
     * Faq categories
     */
    public function faqCategories()
    {
        $items = FaqCategories::where('status', 1)->get();

        $xml = '';
        
        if(count($items)) {

            $date = '';

            foreach($items as $item) {

                $date = date('Y-m-d', strtotime($item->updated_at)) . 'T' . date('H:i:s', strtotime($item->updated_at)) . '.000' . date('P', strtotime($item->updated_at));
                $xml .= '<url><loc>' . url("/faq/{$item->id}-{$item->slug}") . '</loc><lastmod>' . $date . '</lastmod></url>';
            }

            return response($this->_xmlItemsTmpl($xml))
                ->header('Content-Type', 'application/xml');

        }

        return abort(404);
    }

    /**
     * Pages list
     */
    public function pagesItems()
    {
        $items = Pages::where('status', 1)->get();

        $xml = '';
        
        if(count($items)) {

            $date = '';

            foreach($items as $item) {
                $date = date('Y-m-d', strtotime($item->updated_at)) . 'T' . date('H:i:s', strtotime($item->updated_at)) . '.000' . date('P', strtotime($item->updated_at));
                $xml .= '<url><loc>' . url("/pages/{$item->id}-{$item->slug}") . '</loc><lastmod>' . $date . '</lastmod></url>';
            }

            if(!empty($xml)) {
                return response($this->_xmlItemsTmpl($xml))
                    ->header('Content-Type', 'application/xml');
            }

        }

        return abort(404);
    }

    /**
     * Get Testimonals index
     */
    public function testimonalsItems()
    {
        $items = Testimonals::where('status', 1)->where('client_id', '!=', '0')->get();

        $xml = '';
        
        if(count($items)) {

            $date = '';

            foreach($items as $item) {
                $itemDate = date('Y-m-d H:i:s', strtotime($item->updated_at));
                if(!empty($date)) {
                    $date = $date < $itemDate ? $itemDate : $date;
                } else {
                    $date = $itemDate;
                }
            }

            $date = date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date));
            $xml .= '<url><loc>' . url("/testimonals") . '</loc><lastmod>' . $date . '</lastmod></url>';

            if(!empty($xml)) {
                return response($this->_xmlItemsTmpl($xml))
                    ->header('Content-Type', 'application/xml');
            }

        }

        return abort(404);
    }

    /**
     * Get workers index
     */
    public function workersCategories()
    {
        $items = WorkersCategories::where('status', 1)->get();

        $xml = '';
        
        if(count($items)) {

            $date = '';

            foreach($items as $item) {
                $itemDate = date('Y-m-d H:i:s', strtotime($item->updated_at));
                if(!empty($date)) {
                    $date = $date < $itemDate ? $itemDate : $date;
                } else {
                    $date = $itemDate;
                }
            }

            $date = date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date));
            $xml .= '<url><loc>' . url("/workers") . '</loc><lastmod>' . $date . '</lastmod></url>';

            if(!empty($xml)) {
                return response($this->_xmlItemsTmpl($xml))
                    ->header('Content-Type', 'application/xml');
            }

        }

        return abort(404);
    }

    /**
     * Get Workers Categories pages
     */
    public function workersItems()
    {
        $items = WorkersCategories::where('status', 1)->get();

        $xml = '';
        
        if(count($items)) {

            $date = '';

            foreach($items as $item) {
                $date = date('Y-m-d', strtotime($item->updated_at)) . 'T' . date('H:i:s', strtotime($item->updated_at)) . '.000' . date('P', strtotime($item->updated_at));
                $xml .= '<url><loc>' . url("/workers/{$item->id}-{$item->slug}") . '</loc><lastmod>' . $date . '</lastmod></url>';
            }

            if(!empty($xml)) {
                return response($this->_xmlItemsTmpl($xml))
                    ->header('Content-Type', 'application/xml');
            }

        }

        return abort(404);
    }

    /**
     * Get cleaning pages
     */
    public function cleaningItems()
    {
        $xml = '<url><loc>' . url('/cleaning') . '</loc></url>';
        $xml .= '<url><loc>' . url('/cleaning/general') . '</loc></url>';

        if(!empty($xml)) {
            return response($this->_xmlItemsTmpl($xml))
                ->header('Content-Type', 'application/xml');
        }
    }

    /**
     * Get uslugi pages
     */
    public function uslugiItems()
    {
        $items = Posts::where('status', 1)->get();

        $xml = [];
        $_xml = [];
        
        if(count($items)) {

            foreach($items as $item) {
                // is child item
                if((int) $item->parent !== 0) {
                    $parent = Posts::where('id', $item->parent)->where('status', 1)->first();

                    $date = date('Y-m-d', strtotime($parent->updated_at)) . 'T' . date('H:i:s', strtotime($parent->updated_at)) . '.000' . date('P', strtotime($parent->updated_at));
                    $_xml[$parent->id]['parent'][$parent->id] = '<url><loc>' . url("/uslugi/{$parent->slug}") . '</loc><lastmod>' . $date . '</lastmod></url>';

                    $date = date('Y-m-d', strtotime($item->updated_at)) . 'T' . date('H:i:s', strtotime($item->updated_at)) . '.000' . date('P', strtotime($item->updated_at));
                    $_xml[$parent->id]['items'][] = '<url><loc>' . url("/uslugi/{$parent->slug}/{$item->slug}") . '</loc><lastmod>' . $date . '</lastmod></url>';

                } else {
                    $date = date('Y-m-d', strtotime($item->updated_at)) . 'T' . date('H:i:s', strtotime($item->updated_at)) . '.000' . date('P', strtotime($item->updated_at));
                    $_xml[$item->id]['parent'][$item->id] = '<url><loc>' . url("/uslugi/{$item->slug}") . '</loc><lastmod>' . $date . '</lastmod></url>';
                }

                unset($items, $item);
            }

            foreach($_xml as $item) {
                if(isset($item['parent']) && count($item['parent'])) {
                    foreach($item['parent'] as $parent) {
                        $xml[] = $parent;
                    }
                }

                if(isset($item['items']) && count($item['items'])) {
                    foreach($item['items'] as $item) {
                        $xml[] = $item;
                    }
                }
            }

            $xml = implode('', $xml);

            if(!empty($xml)) {
                return response($this->_xmlItemsTmpl($xml))
                    ->header('Content-Type', 'application/xml');
            }

        }

        return abort(404);
    }

    /**
     * Pages sitemap link
     */
    private function _pagesCategories(&$xmlCategories)
    {
        $posts = Posts::where('status', 1)->get();

        if(count($posts)) {

            $date = '';

            foreach($posts as $post) {
                $postDate = date('Y-m-d H:i:s', strtotime($post->updated_at));
                if(!empty($date)) {
                    $date = $date < $postDate ? $postDate : $date;
                } else {
                    $date = $postDate;
                }
            }

            $xmlCategories .= '<sitemap>
            <loc>' . url('/sitemap-pages.xml') . '</loc>
            <lastmod>' . date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date)) . '</lastmod></sitemap>';
        }
    }

    /**
     * Faq Categories sitemap link
     */
    private function _faqCategories(&$xmlCategories)
    {
        $faqCategories = FaqCategories::where('status', 1)->get();
        
        if(count($faqCategories)) {

            $date = '';

            foreach($faqCategories as $faqCategory) {
                $faqCategoryDate = date('Y-m-d H:i:s', strtotime($faqCategory->updated_at));
                if(!empty($date)) {
                    $date = $date < $faqCategoryDate ? $faqCategoryDate : $date;
                } else {
                    $date = $faqCategoryDate;
                }
            }

            $xmlCategories .= '<sitemap>
            <loc>' . url('/sitemap-faq.xml') . '</loc>
            <lastmod>' . date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date)) . '</lastmod></sitemap>';
        }
    }

    /**
     * Faq Categories sitemap link
     */
    private function _faqItems(&$xmlCategories)
    {
        $faqCategories = FaqCategories::where('status', 1)->get();
        
        if(count($faqCategories)) {

            $date = '';

            foreach($faqCategories as $faqCategory) {
                $faqCategoryDate = date('Y-m-d H:i:s', strtotime($faqCategory->updated_at));
                if(!empty($date)) {
                    $date = $date < $faqCategoryDate ? $faqCategoryDate : $date;
                } else {
                    $date = $faqCategoryDate;
                }
            }

            $xmlCategories .= '<sitemap>
            <loc>' . url('/sitemap-faq-items.xml') . '</loc>
            <lastmod>' . date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date)) . '</lastmod></sitemap>';
        }
    }

    /**
     * Get Testimonals sitemap link
     */
    private function _testimonalsCategories(&$xmlCategories)
    {
        $testimonalsCategories = Testimonals::where('status', 1)->where('client_id', '!=', '0')->get();
        
        if(count($testimonalsCategories)) {

            $date = '';

            foreach($testimonalsCategories as $testimonalCategory) {
                $testimonalCategoryDate = date('Y-m-d H:i:s', strtotime($testimonalCategory->updated_at));
                if(!empty($date)) {
                    $date = $date < $testimonalCategoryDate ? $testimonalCategoryDate : $date;
                } else {
                    $date = $testimonalCategoryDate;
                }
            }
 
            $xmlCategories .= '<sitemap>
            <loc>' . url('/sitemap-testimonals.xml') . '</loc>
            <lastmod>' . date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date)) . '</lastmod></sitemap>';
        }
    }

    /**
     * Get Workers Categories sitemap link
     */
    private function _workersCategories(&$xmlCategories)
    {
        $workersCategories = WorkersCategories::where('status', 1)->get();

        if(count($workersCategories)) {

            $date = '';

            foreach($workersCategories as $workersCategory) {
                $workersCategoryDate = date('Y-m-d H:i:s', strtotime($workersCategory->updated_at));
                if(!empty($date)) {
                    $date = $date < $workersCategoryDate ? $workersCategoryDate : $date;
                } else {
                    $date = $workersCategoryDate;
                }
            }
 
            $xmlCategories .= '<sitemap>
            <loc>' . url('/sitemap-workers.xml') . '</loc>
            <lastmod>' . date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date)) . '</lastmod></sitemap>';
        }
    }

    /**
     * Get Workers Items sitemap link
     */
    private function _workersItems(&$xmlCategories)
    {
        $workersCategories = WorkersCategories::where('status', 1)->get();

        if(count($workersCategories)) {

            $date = '';

            foreach($workersCategories as $workersCategory) {
                $workersCategoryDate = date('Y-m-d H:i:s', strtotime($workersCategory->updated_at));
                if(!empty($date)) {
                    $date = $date < $workersCategoryDate ? $workersCategoryDate : $date;
                } else {
                    $date = $workersCategoryDate;
                }
            }
 
            $xmlCategories .= '<sitemap>
            <loc>' . url('/sitemap-workers-items.xml') . '</loc>
            <lastmod>' . date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date)) . '</lastmod></sitemap>';
        }
    }

    /**
     * Get Cleaning sitemap link
     */
    private function _cleaningCategories(&$xmlCategories)
    {
        $xmlCategories .= '<sitemap><loc>' . url('/sitemap-cleaning.xml') . '</loc></sitemap>';
    }

    /**
     * Get Posts sitemap link
     */
    private function _uslugiCategories(&$xmlCategories)
    {
        $postsCategories = Posts::where('status', 1)->get();

        if(count($postsCategories)) {

            $date = '';

            foreach($postsCategories as $postsCategory) {
                $postsCategoryDate = date('Y-m-d H:i:s', strtotime($postsCategory->updated_at));
                if(!empty($date)) {
                    $date = $date < $postsCategoryDate ? $postsCategoryDate : $date;
                } else {
                    $date = $postsCategoryDate;
                }
            }
 
            $xmlCategories .= '<sitemap>
            <loc>' . url('/sitemap-uslugi.xml') . '</loc>
            <lastmod>' . date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date)) . '.000' . date('P', strtotime($date)) . '</lastmod></sitemap>';
        }
    }

    /**
     * Create xml index template
     */
    private function _xmlIndexTmpl($categories)
    {
        return '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . $categories . '</sitemapindex>';
    }

    /**
     * Create xml items template
     */
    private function _xmlItemsTmpl($items)
    {
        return '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . $items . '</urlset>';
    }
}

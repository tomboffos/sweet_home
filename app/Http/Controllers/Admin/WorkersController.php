<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Workers;
use App\Models\WorkersCategories;
use Validator;
use Storage;

class WorkersController extends CommonController
{
    /**
     * render workers list
     * @return [view]
     */
    public function index()
    {
        $data = Workers::select()->orderby('id', 'desc')->paginate($this->postsPerPage());
        $categories = WorkersCategories::select()->get();

        return view('admin.workers.index', compact('data', 'categories'));
    }

    /**
     * render new worker form and add new worker
     * @return [view]
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'image' => 'image|max:5000',
                'status' => 'required|min:0|max:1|numeric',
                'category' => 'required|min:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $create = Workers::create([
                'name' => $request->name,
                'text' => $request->text,
                'category_id' => $request->category,
                'status' => $request->status,
                'comment' => $request->comment,
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // upload image
            if ($request->file('img')) {
                if (Storage::disk('workers')->has($create->id)) {
                    Storage::disk('workers')->deleteDirectory($create->id);
                }

                $image = $request->file('img')->getClientOriginalName();
                $request->file('img')->move(public_path('data/workers').'/'.$create->id.'/', $image);

                $update = Workers::where('id', $create->id)->update([
                    'img' => $image,
                ]);
            }

            return redirect('/manager/workers/'.$create->id);
        }


        $categories = WorkersCategories::select()->get();

        return view('admin.workers.new', compact('categories'));
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'image' => 'image|max:5000',
                'status' => 'required|min:0|max:1|numeric',
                'category' => 'required|min:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // delete image
            if ($request->delete_thumb) {
                if (Storage::disk('workers')->has($id.'/'.$request->old_image)) {
                    Storage::disk('workers')->deleteDirectory($id);
                }
            }

            // upload image
            if ($request->file('img')) {
                if (Storage::disk('workers')->has($id)) {
                    Storage::disk('workers')->deleteDirectory($id);
                }

                $image = $request->file('img')->getClientOriginalName();
                $request->file('img')->move(public_path('data/workers').'/'.$id.'/', $image);
            }

            $update = Workers::where('id', $id)->update([
                'name' => $request->name,
                'comment' => $request->comment,
                'text' => $request->text,
                'category_id' => $request->category,
                'img' => isset($image) && !empty($image) ? $image : '',
                'status' => $request->status,
            ]);

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }

        $data = Workers::findorfail($id);
        $categories = WorkersCategories::select()->get();

        return view('admin.workers.item', compact('data', 'categories'));
    }

    /**
     * delete worker
     */
    public function delete($id)
    {
        // delete data
        $data = Workers::where('id', $id)->delete();

        // delete image
        if (Storage::disk('workers')->has($id)) {
            Storage::disk('workers')->deleteDirectory($id);
        }

        return redirect('/manager/workers');
    }

    /**
     * render workers profession list
     * @return [view]
     */
    public function categories()
    {
        $data = WorkersCategories::orderby('sort', 'asc')->orderby('id', 'desc')->paginate($this->postsPerPage());
        return view('admin.workers.categories.index', compact('data'));
    }

    /**
     * edit and view existing category of workers
     */
    public function category(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'title' => 'required|max:255',
                'meta_title' => 'max:255',
                'meta_description' => 'max:255',
                'slug' => 'max:255',
                'status' => 'required|min:0|max:1|numeric',
                'menu' => 'required|min:0|max:1|numeric',
                'footer' => 'required|min:0|max:1|numeric',
                'sort' => 'min:0|numeric',
                'type' => 'min:0|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            $update = WorkersCategories::where('id', $id)->update([
                'name' => $request->name,
                'title' => $request->title,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'text' => $request->text,
                'slug' => $request->slug,
                'status' => $request->status,
                'menu' => $request->menu,
                'footer' => $request->footer,
                'sort' => $request->sort,
                'type' => $request->form_type,
            ]);

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }

        $data = WorkersCategories::findorfail($id);
        $form_types = config('constants.form_types');

        return view('admin.workers.categories.item', compact('data', 'form_types'));
    }

    /**
     * render new cagtegory of workers form and add new cagtegory of workers
     * @return [view]
     */
    public function createCategory(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'title' => 'required|max:255',
                'meta_title' => 'max:255',
                'meta_description' => 'max:255',
                'slug' => 'max:255',
                'status' => 'required|min:0|max:1|numeric',
                'menu' => 'required|min:0|max:1|numeric',
                'footer' => 'required|min:0|max:1|numeric',
                'sort' => 'min:0|numeric',
                'type' => 'min:0|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            $create = WorkersCategories::create([
                'name' => $request->name,
                'title' => $request->title,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'text' => $request->text,
                'slug' => $request->slug,
                'status' => $request->status,
                'menu' => $request->menu,
                'footer' => $request->footer,
                'sort' => $request->sort,
                'type' => $request->form_type,
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect('/manager/workers/cats/'.$create->id);
        }

        $form_types = config('constants.form_types');
        return view('admin.workers.categories.new', compact('form_types'));
    }

    /**
     * delete category
     */
    public function deleteCategory($id)
    {
        $data = WorkersCategories::all();

        // check if thera are items appended to this category
        if (count(Workers::where('category_id', $id)->first())) {
            $notification['message'] = trans('messages.first_delete_or_move_all_workers');
            return redirect()->back()->with($notification);
        }

        // if there are only 1 category left, do not delete
        if (count($data) < 2) {
            $notification['message'] = trans('messages.cant_delete_last_category');
            return redirect()->back()->with($notification);
        }

        $data = WorkersCategories::where('id', $id)->delete();
        return redirect('/manager/workers/cats');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Models\Posts;
use App\Models\WorkersCategories;

class PostsController extends CommonController
{
    /**
     * render list of posts
     * @return [view]
     */
    public function index()
    {
        $data = Posts::orderby('sort', 'asc')->orderby('id', 'desc')->paginate($this->postsPerPage());
        $categories = $this->_getPostsCategories();
        
        return view('admin.posts.index', compact('data', 'categories'));
    }

    /**
     * render new page form and add new page
     * @return [view]
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'status' => 'required|min:0|max:1|numeric',
                'sort' => 'min:0|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            $create = Posts::create([
                'title' => $request->title,
                'text' => $request->text,
                'slug' => $request->slug,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'status' => $request->status,
                'parent' => $request->parent,
                'category' => $request->category,
                'text_bottom' => $request->text_bottom,                
                'sort' => $request->sort ? $request->sort : 0,
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect('/manager/posts/'.$create->id);
        }

        $parents = Posts::all();

        $categories = $this->_getPostsCategories();

        return view('admin.posts.new', compact('categories', 'parents'));
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'status' => 'required|min:0|max:1|numeric',
                'sort' => 'min:0|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            // update Page
            $update = Posts::where('id', $id)
            ->update([
                'title' => $request->title,
                'text' => $request->text,
                'slug' => $request->slug,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'status' => $request->status,
                'category' => $request->category,
                'parent' => $request->parent,
                'text_bottom' => $request->text_bottom,   
                'sort' => $request->sort ? $request->sort : 0,
            ]);

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }

        $data = Posts::findorfail($id);
        $parents = Posts::where('id', '!=', $id)->get();

        $categories = $this->_getPostsCategories();        

        return view('admin.posts.item', compact('data', 'parents', 'categories'));
    }

    /**
     * delete page
     */
    public function delete($id)
    {
        $data = Posts::where('id', $id)->delete();
        return redirect('/manager/posts');
    }

    /**
     * Get Posts Categories
     * @return array
     */
    private function _getPostsCategories()
    {
        $categories = [
            'cleaning' => 'Уборка',
            'cleaning/general' => 'Генеральная уборка',
        ];

        $workers = WorkersCategories::all();
        
        if(count($workers)) {
            foreach($workers as $worker) {
                $categories["workers/{$worker->id}-{$worker->slug}"] = $worker->title;
            }
        }

        return $categories;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Models\Role;
use Validator;

class ManagersController extends CommonController
{
    /**
     * render managers list
     * @return [view]
     */
    public function index()
    {
        // 1 - user, 2 - admin
        $data = Role::find(2)->users()->orderby('id', 'desc')->paginate($this->postsPerPage());
        return view('admin.users.managers.index', compact('data'));
    }

    /**
     * render new manager form and add new manager
     * @return [view]
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $create = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $user = User::find($create->id);
            $user->roles()->attach(2); // 1 - user, 2 - admin

            return redirect('/manager/users/managers/'.$create->id);
        }

        return view('admin.users.managers.new');
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'email|max:255',
                'password' => 'min:6|confirmed',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $sql['name'] = $request->name;

            if (isset($request->email) && !empty($request->email)) {
                $is_email_exist = User::where('email', $request->email)->get();

                if (!count($is_email_exist)) {
                    $sql['email'] = $request->email;
                }
            }

            if (isset($request->password) && !empty($request->password)) {
                $sql['password'] = bcrypt($request->password);
            }

            // update user
            $update = User::where('id', $id)->update($sql);
            
            if (!$update) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }

        $data = User::find($id);

        if (!$data) {
            abort(404);
        }
        
        if (!$data->hasRole('admin')) {
            return redirect('manager/users/clients/'.$id);
        }

        return view('admin.users.managers.item', compact('data'));
    }

    /**
     * delete manager
     */
    public function delete($id)
    {
        if (Role::find(2)->users()->count() < 2) {
            $notification['message'] = trans('messages.cant_delete_last_admin');
            return redirect()->with($notification);
        }

        $data = User::where('id', $id)->delete();
        return redirect('/manager/users/managers');
    }
}

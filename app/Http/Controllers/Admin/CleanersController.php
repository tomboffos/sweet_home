<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Cleaners;
use App\Models\CleanersSchedule;
use Validator;
use Storage;

class CleanersController extends CommonController
{
    /**
     * render cleaners list
     * @return [view]
     */
    public function index()
    {
        $data = Cleaners::select()->orderby('id', 'desc')->paginate($this->postsPerPage());
        return view('admin.cleaners.index', compact('data'));
    }

    /**
     * render new cleaner form and add new cleaner
     * @return [view]
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'image' => 'image|max:5000',
                'status' => 'required|min:0|max:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $create = Cleaners::create([
                'name' => $request->name,
                'comment' => $request->comment,
                'text' => $request->text,
                'status' => $request->status ? $request->status : 0,
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // upload image
            if ($request->file('img')) {
                $image = $request->file('img')->getClientOriginalName();
                $request->file('img')->move(public_path('data/cleaners/').'/'.$create->id.'/', $image);

                $update = Cleaners::where('id', $create->id)
                ->update([
                    'img' => $image,
                ]);

                if (!$update) {
                    $notification['message'] = trans('messages.error_insert_bd');
                    return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
                }
            }

            // update schedule
            $create_schedule = CleanersSchedule::create([
                'cleaner_id' => $create->id,
                'mon' => $request->mon ? 1 : 0,
                'tue' => $request->tue ? 1 : 0,
                'wed' => $request->wed ? 1 : 0,
                'thu' => $request->thu ? 1 : 0,
                'fri' => $request->fri ? 1 : 0,
                'sat' => $request->sat ? 1 : 0,
                'sun' => $request->sun ? 1 : 0,
                't_from' => $request->from,
                't_to' => $request->to,
                't_to_finish' => $request->to_finish,
            ]);

            if (!$create_schedule) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect('/manager/cleaners/'.$create->id);
        }

        return view('admin.cleaners.new');
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'image' => 'image|max:5000',
                'status' => 'required|min:0|max:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // delete image
            if ($request->delete_thumb) {
                if (Storage::disk('cleaners')->has($id.'/'.$request->old_image)) {
                    Storage::disk('cleaners')->deleteDirectory($id);
                }
            }

            // upload image
            if ($request->file('img')) {
                if (Storage::disk('cleaners')->has($id)) {
                    Storage::disk('cleaners')->deleteDirectory($id);
                }

                $image = $request->file('img')->getClientOriginalName();
                $request->file('img')->move(public_path('data/cleaners/').'/'.$id.'/', $image);
            }

            // update main information
            $update = Cleaners::where('id', $id)
                ->update([
                    'name' => $request->name,
                    'comment' => $request->comment,
                    'text' => $request->text,
                    'img' => (isset($image) && !empty($image) ? $image : (isset($request->old_image) && !empty($request->old_image) && !$request->delete_thumb ? $request->old_image : '')),
                    'status' => $request->status ? $request->status : 0,
                ]);

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // update schedule
            $update_schedule = CleanersSchedule::where('cleaner_id', $id)
                ->update([
                    'mon' => $request->mon ? 1 : 0,
                    'tue' => $request->tue ? 1 : 0,
                    'wed' => $request->wed ? 1 : 0,
                    'thu' => $request->thu ? 1 : 0,
                    'fri' => $request->fri ? 1 : 0,
                    'sat' => $request->sat ? 1 : 0,
                    'sun' => $request->sun ? 1 : 0,
                    't_from' => $request->from,
                    't_to' => $request->to,
                    't_to_finish' => $request->to_finish,
                ]);

            return redirect()->back();
        }
      
        $data = Cleaners::findorfail($id);
        $rating = $data->testimonals()->get()->avg('rating');
        $orders = $data->orders()->get();
        $schedule = $data->schedule()->first();

        return view('admin.cleaners.item', compact('data', 'rating', 'orders', 'schedule'));
    }

    /**
     * delete cleaner
     */
    public function delete($id)
    {
        // delete data
        $data = Cleaners::where('id', $id)->delete();

        // delete schedule
        if ($data) {
            CleanersSchedule::where('cleaner_id', $id)->delete();
        }

        // delete image
        if (Storage::disk('cleaners')->has($id)) {
            Storage::disk('cleaners')->deleteDirectory($id);
        }

        return redirect('/manager/cleaners');
    }
}

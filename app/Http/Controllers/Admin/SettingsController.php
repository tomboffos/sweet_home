<?php

namespace App\Http\Controllers\Admin;

use App\DiscountSecond;
use App\Models\Discounts;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Settings;
use Validator;

class SettingsController extends CommonController
{
    /**
     * render calculator
     * @return [view]
     */
    public function index(Request $request)
    {
        $discount_first = DiscountSecond::where('type',0)->first();
        $discount_second = DiscountSecond::where('type',1)->first();
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'posts_per_page' => 'min:1|numeric',
                'emails' => 'required|string',

                'testimonal' => 'required|numeric|min:0',
                'cleaning_time_border' => 'required',
                'phones' => 'string',
                'points_currency' => 'required',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $sql = [
                'posts_per_page' => $request->posts_per_page,
                'referal' => $request->referal,
                'testimonal' => $request->testimonal,
                'emails' => $request->emails,
                'points_currency' => $request->points_currency,
                'phones' => $request->phones,
                'cleaning_time_border' => $request->cleaning_time_border,
                'social' => json_encode($request->social),
            ];

            foreach ($request->meta as $key => $value) {
                $sql[$key] = json_encode($value);
            }

            // update settings
            foreach ($sql as $name => $value) {
                 Settings::where('name', $name)->update([
                    'value' => $value
                 ]);
            }
            if ($request['discount'] != $discount_second->amount){
                $discount_second->amount = $request['discount'];
                $discounts = Discounts::where('user_id','!=',0)->get();

                foreach ($discounts as $discount){
                    $discount['price'] = $request['discount'];
                    $discount->save();
                }
                $discount_second->save();
            }

            if ($request['bonus'] != $discount_first->amount){
                $discount_first->amount = $request['bonus'];
                $discount_first->save();
            }
            return redirect()->back();
        }

        // get all settings
        $settings = Settings::all();

        if (!$settings) {
            return abort(404);
        }

        foreach ($settings as $key => $value) {
            // social
            if (in_array($value['name'], array('social'))) {
                $data[$value['name']] = json_decode($value['value'], true);
            } // meta tags
            elseif (substr($value['name'], 0, 4) === 'meta') {
                $meta[$value['name']] = json_decode($value['value'], true);
            } // other settings
            else {
                $data[$value['name']] = $value['value'];
            }
        }


        return view('admin.settings.index', compact('data', 'meta','discount_first', 'discount_second'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class ComposerController extends Controller
{
    public function compose($view)
    {
        $view->with('sidebar', config('constants.sidebar'));
    }
}

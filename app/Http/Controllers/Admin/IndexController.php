<?php

namespace App\Http\Controllers\Admin;

use App\DiscountSecond;
use Validator;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Testimonals;
use App\Models\OrdersCleaners;
use App\Models\OrdersWorkers;
use App\Models\Settings;
use Storage;

class IndexController extends CommonController
{
    public function index()
    {
        $c_user = Role::find(1)->users()->count();
        $c_testimonals = Testimonals::all()->count();
        $c_cleaning = OrdersCleaners::all()->count();
        $c_workers= OrdersWorkers::all()->count();
        $discount_first = DiscountSecond::where('type',0)->first();
        $discount_second = DiscountSecond::where('type',1)->first();
        return view('admin.index',
            compact('c_user', 'c_testimonals', 'c_cleaning', 'c_workers','discount_first','discount_second'
        ));
    }
    public function Remote(Request $request){
        $v = Validator::make($request->all(), [
            'discount' => 'required',
            'bonus' => 'required'
        ]);
        if ($v->fails()){
            return back()->withErrors($v->errors());
        }
        $discount_first = DiscountSecond::where('type',0)->first();
        $discount_second = DiscountSecond::where('type',1)->first();


        $discount_first->amount = $request->bonus;
        $discount_second->amount = $request->discount;
        $discount_second->save();
        $discount_first->save();
        return back()->with('message','Изменено');


    }

    public function edit(Request $request)
    {
        if ($request->isMethod('post')) {
            return $this->update($request);
        }

        // get index page settings
        $settings = Settings::where('name', 'index')->first()->toArray();

        if (!$settings) {
            return abort(404);
        }

        $data = json_decode($settings['value'], true);

        return view('admin.index.edit', compact('data'));
    }

    /**
     * @param $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($request)
    {
        $post = $request->index;

        $post['banners']['top']['banner'] = $post['banners']['top']['current_banner'];
        $post['banners']['inner']['banner'] = $post['banners']['inner']['current_banner'];

        if (isset($post['banners']['top']['delete']['banner'])) {
            $this->_delete_banner('top');
            $post['banners']['top']['banner'] = '';
        }

        if (isset($post['banners']['inner']['delete']['banner'])) {
            $this->_delete_banner('inner');
            $post['banners']['inner']['banner'] = '';
        }

        if (isset($post['banners']['top']['status'])) {
            $post['banners']['top']['status'] = 1;
        } else {
            $post['banners']['top']['status'] = 0;
        }

        if (isset($post['banners']['inner']['status'])) {
            $post['banners']['inner']['status'] = 1;
        } else {
            $post['banners']['inner']['status'] = 0;
        }

        if ($request->file('index.banners.top.banner') || $request->file('index.banners.inner.banner')) {
            // top banner
            if ($request->file('index.banners.top.banner')) {
                $this->_delete_banner('top');
                $top_banner = $request->file('index.banners.top.banner')->getClientOriginalName();
                $request->file('index.banners.top.banner')->move(public_path('data/banners').'/', $top_banner);
                $post['banners']['top']['banner'] = $top_banner;
            }

            // inner banner
            if ($request->file('index.banners.inner.banner')) {
                $this->_delete_banner('inner');
                $inner_banner = $request->file('index.banners.inner.banner')->getClientOriginalName();
                $request->file('index.banners.inner.banner')->move(public_path('data/banners').'/', $inner_banner);
                $post['banners']['inner']['banner'] = $inner_banner;
            }
        }

        // update settings
        Settings::where('name', 'index')->update([
            'value' => json_encode($post)
        ]);

        return redirect()->back();
    }

    /**
     * @param string $banner
     * @return bool
     */
    private function _delete_banner($banner = '')
    {
        if (!$banner) {
            return false;
        }

        $settings = Settings::where('name', 'index')->first()->toArray();
        $settings = json_decode($settings['value'], true);

        if (!empty($settings['banners'][$banner]['banner'])) {
            Storage::disk('banners')->delete($settings['banners'][$banner]['banner']);
            $settings['banners'][$banner]['banner'] = '';
            $settings['banners'][$banner]['status'] = 0;

            // update settings
            Settings::where('name', 'index')->update([
                'value' => json_encode($settings)
            ]);

            return true;
        }

        return false;
    }
}

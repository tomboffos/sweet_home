<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Models\Discounts;

class DiscountsController extends CommonController
{
    /**
     * render list of discounts
     * @return [view]
     */
    public function index()
    {
        $data = Discounts::where('user_id', 0)->orderby('id', 'desc')->paginate($this->postsPerPage());
        $claim = false;
        return view('admin.discounts.index', compact('data','claim'));

    }

    public function refers()
    {
        $data = Discounts::where('user_id','!=', 0)->orderby('id', 'desc')->paginate($this->postsPerPage());
        $claim = true;
        return view('admin.discounts.index', compact('data','claim'));
    }
    /**
     * render new discount form and add new discount
     * @return [view]
     */
    public function create(Request $request)
    {

        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'code' => 'required|max:255|unique:discounts',
                'category' => 'required|numeric',
                'type' => 'required|numeric|min:0|max:1',
                'price' => 'required|numeric',
                'used' => 'required|min:0|max:1|numeric',
                'status' => 'required|min:0|max:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $create = Discounts::create([
                'code' => $request->code,
                'category' => $request->category,
                'type' => $request->type,
                'price' => $request->price,
                'finish' => (isset($request->finish) && !empty($request->finish) ? strtotime($request->finish) : null),
                'status' => $request->status ? $request->status : 0,
                'used' => $request->used ? $request->used : 0,
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect('/manager/discounts/'.$create->id);
        }

        // get categories
        $categories = config('constants.discounts_categories');

        // get types
        $types = config('constants.discounts_types');

        return view('admin.discounts.new', compact('categories', 'types'));
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {

        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'category' => 'required|numeric',
                'type' => 'required|numeric|min:0|max:1',
                'price' => 'required|numeric',
                'used' => 'required|min:0|max:1|numeric',
                'status' => 'required|min:0|max:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $sql = [
                'category' => $request->category,
                'type' => $request->type,
                'price' => $request->price,
                'finish' => (isset($request->finish) && !empty($request->finish) ? date("Y-m-d", strtotime($request->finish)) : null),
                'status' => $request->status ? $request->status : 0,
                'used' => $request->used ? $request->used : 0,
            ];

            // update discount if not empty. check is the discount unique
            if (isset($request->code) && !empty($request->code)) {
                $is_discount_exist = Discounts::where('code', $request->code)->get();

                if (!count($is_discount_exist)) {
                    $sql['code'] = $request->code;
                }
            }

            // update Discount
            $update = Discounts::where('id', $id)->update($sql);

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }
      
        $data = Discounts::findorfail($id);

        // get categories
        $categories = config('constants.discounts_categories');

        // get types
        $types = config('constants.discounts_types');

        // get orders where code was used
        $ordersCleaners = $data->ordersCleaners()->orderby('id', 'desc')->take(30)->get();

        // get orders where code was used
        $ordersWorkers = $data->ordersWorkers()->orderby('id', 'desc')->take(30)->get();

        return view('admin.discounts.item', compact('data', 'categories', 'types', 'ordersCleaners', 'ordersWorkers'));
    }

    /**
     * delete discount
     */
    public function delete($id)
    {
        $data = Discounts::where('id', $id)->delete();
        return redirect('/manager/discounts');
    }
}

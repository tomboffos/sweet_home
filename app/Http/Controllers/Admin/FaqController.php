<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Models\Faq;
use App\Models\FaqCategories;
use App\Models\WorkersCategories;

class FaqController extends CommonController
{
    /**
     * render list of faq items
     * @return [view]
     */
    public function index()
    {
        $data = Faq::orderby('sort', 'asc')->orderby('id', 'desc')->paginate($this->postsPerPage());
        $faq_categories = FaqCategories::where('status', 1)->get();
        return view('admin.faq.index', compact('data', 'faq_categories'));
    }

    /**
     * render new faq item form and add new faq item
     * @return [view]
     */
    public function create(Request $request)
    {

        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'status' => 'required|min:0|max:1|numeric',
                'sort' => 'min:0|numeric',
                'category' => 'required|min:0|numeric',
                'slug' => 'max:255',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            $create = Faq::create([
                'title' => $request->title,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'slug' => $request->slug,
                'text' => $request->text,
                'category' => $request->category,
                'status' => $request->status,
                'sort' => $request->sort,
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect('/manager/faq/'.$create->id);
        }

        $faq_categories = FaqCategories::where('status', 1)->get();
        return view('admin.faq.new', compact('faq_categories'));
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {

        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'status' => 'required|min:0|max:1|numeric',
                'sort' => 'min:0|numeric',
                'category' => 'required|min:0|numeric',
                'slug' => 'max:255',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            // update Faq
            $update = Faq::where('id', $id)
            ->update([
                'title' => $request->title,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'slug' => $request->slug,
                'text' => $request->text,
                'category' => $request->category,
                'status' => $request->status,
                'sort' => $request->sort,
            ]);

            if (!$update) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }

        $data = Faq::findorfail($id);
        $faq_categories = FaqCategories::where('status', 1)->get();
        return view('admin.faq.item', compact('data', 'faq_categories'));
    }

    /**
     * delete item
     */
    public function delete($id)
    {
        $data = Faq::where('id', $id)->delete();
        return redirect('/manager/faq');
    }

    /**
     * render faq profession list
     * @return [view]
     */
    public function categories()
    {
        $data = FaqCategories::orderby('id', 'desc')->paginate($this->postsPerPage());
        return view('admin.faq.categories.index', compact('data'));
    }

    /**
     * edit and view existing category of faq
     */
    public function category(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'meta_title' => 'max:255',
                'meta_description' => 'max:255',
                'category' => 'string|max:255',
                'slug' => 'max:255',
                'status' => 'required|min:0|max:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            $update = FaqCategories::where('id', $id)->update([
                'title' => $request->title,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'category' => $request->category,
                'slug' => $request->slug,
                'status' => $request->status,
            ]);

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }
      
        $data = FaqCategories::findorfail($id);
        $categories = WorkersCategories::select('slug', 'name')->where('status', 1)->get();

        return view('admin.faq.categories.item', compact('data', 'categories'));
    }

    /**
     * render new cagtegory of faq form and add new cagtegory of faq
     * @return [view]
     */
    public function createCategory(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'meta_title' => 'max:255',
                'meta_description' => 'max:255',
                'slug' => 'max:255',
                'category' => 'string|max:255',
                'status' => 'required|min:0|max:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            $create = FaqCategories::create([
                'title' => $request->title,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'slug' => $request->slug,
                'category' => $request->category,
                'status' => $request->status ? $request->status : 0,
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect('/manager/faq/cats/'.$create->id);
        }


        $categories = WorkersCategories::select('slug', 'name')->where('status', 1)->get();

        return view('admin.faq.categories.new', compact('categories'));
    }

    /**
     * delete category
     */
    public function deleteCategory($id)
    {
        $data = FaqCategories::all();

        if (count($data) < 2) {
            $notification['message'] = trans('messages.cant_delete_last_category');
            return redirect()->back()->with($notification);
        }

        $data = FaqCategories::where('id', $id)->delete();
        return redirect('/manager/faq/cats');
    }
}

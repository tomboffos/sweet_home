<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\QueryException;
use App\Models\Settings;
use Validator;
use Log;

class CalculatorController extends CommonController
{
    /**
     * render calculator
     * @return [view]
     */
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'base_price' => 'min:0|numeric',
                'one_room_price' => 'min:0|numeric',
                'one_bathroom_price' => 'min:0|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // main settings
            $calc_main = [
                'base_price' => (float)$request->base_price,
                'one_room_price' => (float)$request->one_room_price,
                'one_room_elevated_price' => (float)$request->one_room_elevated_price,
                'one_bathroom_price' => (float)$request->one_bathroom_price,
                'number_room_elevated_price' => (float)$request->number_room_elevated_price,
                'base_time' => $request->base_time,
                'room_time' => $request->room_time,
                'bathroom_time' => $request->bathroom_time,
            ];

            // type
            if (isset($request->clean_type_name) && count($request->clean_type_name)) {
                $count_type = count($request->clean_type_name);
                for ($i=0; $i < $count_type; $i++) {
                    $calc_type[$i] = [
                        'id' => md5($request->clean_type_name[$i].$request->clean_type_k[$i]),
                        'label' => $request->clean_type_label[$i],
                        'name' => $request->clean_type_name[$i],
                        'ratio' => (float)$request->clean_type_k[$i],
                    ];
                }
            }

            // services
            $count_services = count($request->clean_services_name);
            for ($i=0; $i < $count_services; $i++) {
                $calc_services[$i]['id'] = md5($request->clean_services_name[$i].$request->clean_services_price[$i].$request->clean_services_pay[$i]);
                $calc_services[$i]['name'] = $request->clean_services_name[$i];
                $calc_services[$i]['price'] = (float)$request->clean_services_price[$i];
                $calc_services[$i]['pay'] = (float)$request->clean_services_pay[$i];
                $calc_services[$i]['icon'] = $request->clean_services_icon[$i];
                $calc_services[$i]['time'] = $request->clean_services_time[$i];
            }

            // cicle
            $count_cicle = count($request->clean_cicle_name);
            for ($i=0; $i < $count_cicle; $i++) {
                $calc_cicle[$i]['id'] = md5($request->clean_cicle_name[$i].$request->clean_cicle_price[$i]);
                $calc_cicle[$i]['name'] = $request->clean_cicle_name[$i];
                $calc_cicle[$i]['ratio'] = (float)$request->clean_cicle_price[$i];
            }

            $sql = [
                'calc_main' => json_encode($calc_main),
                'calc_services' => json_encode($calc_services),
                'calc_cicle' => json_encode($calc_cicle),
                'calc_type' => (isset($calc_type) ? json_encode($calc_type) : ''),
                'calc_additional_cleaner' => $request->calc_additional_cleaner,
                'calc_additional_cleaner_border' => $request->calc_additional_cleaner_border,
            ];

            // update main settings
            foreach ($sql as $name => $value) {
                try {
                    Settings::where('name', $name)->update([
                        'value' => $value
                    ]);
                } catch (QueryException $e) {
                    Log::error("Adding Calculator settings. Error: ".$e);
                    $notification['message'] = trans('messages.error_insert_bd').'Error: '.$e;
                    return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
                }
            }

            return redirect()->back();
        }

        // get all calculator settings
        $query = Settings::where('name', 'like', 'calc_%')->orderby('id', 'ASC')->get();

        if (!$query) {
            die('Nothing found');
        }

        // prepare data
        foreach ($query as $key => $value) {
            $data[$value['name']] = json_decode($value['value'], true);
        }

        // get service pay types
        // if you go to edit this array, don't forget edit some_assets/admin/../main.js!
        $services_pay = config('constants.services_pay');

        // get service icons
        // if you go to edit this array, don't forget edit some_assets/admin/../main.js!
        $services_icons = config('constants.services_icons');

        return view('admin.calculator.index', compact('data', 'services_pay', 'services_icons'));
    }

    /**
     * render general calculator
     * @return [view]
     */
    public function general(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'base_price' => 'min:0|numeric',
                'one_room_price' => 'min:0|numeric',
                'one_bathroom_price' => 'min:0|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // main settings
            $calc_general_main = [
                'base_price' => (float)$request->base_price,
                'one_room_price' => (float)$request->one_room_price,
                'one_room_elevated_price' => (float)$request->one_room_elevated_price,
                'one_bathroom_price' => (float)$request->one_bathroom_price,
                'number_room_elevated_price' => (float)$request->number_room_elevated_price,
                'one_window_price' => (float)$request->one_window_price,
                'base_time' => $request->base_time,
                'room_time' => $request->room_time,
                'bathroom_time' => $request->bathroom_time,
                'window_time' => $request->window_time,
            ];

            // type
            if (isset($request->clean_general_type_name) && count($request->clean_general_type_name)) {
                $count_general_type = count($request->clean_general_type_name);
                for ($i=0; $i < $count_general_type; $i++) {
                    $calc_general_type[$i] = [
                        'id' => md5($request->clean_general_type_name[$i].$request->clean_general_type_k[$i]),
                        'label' => $request->clean_general_type_label[$i],
                        'name' => $request->clean_general_type_name[$i],
                        'ratio' => (float)$request->clean_general_type_k[$i],
                    ];
                }
            }

            // services
            $count_general_services = count($request->clean_general_services_name);
            for ($i=0; $i < $count_general_services; $i++) {
                $calc_general_services[$i]['id'] = md5($request->clean_general_services_name[$i].$request->clean_general_services_price[$i].$request->clean_general_services_pay[$i]);
                $calc_general_services[$i]['name'] = $request->clean_general_services_name[$i];
                $calc_general_services[$i]['price'] = (float)$request->clean_general_services_price[$i];
                $calc_general_services[$i]['pay'] = (float)$request->clean_general_services_pay[$i];
                $calc_general_services[$i]['icon'] = $request->clean_general_services_icon[$i];
                $calc_general_services[$i]['time'] = $request->clean_general_services_time[$i];
            }

            // cicle
            $count_general_cicle = count($request->clean_general_cicle_name);
            for ($i=0; $i < $count_general_cicle; $i++) {
                $calc_general_cicle[$i]['id'] = md5($request->clean_general_cicle_name[$i].$request->clean_general_cicle_price[$i]);
                $calc_general_cicle[$i]['name'] = $request->clean_general_cicle_name[$i];
                $calc_general_cicle[$i]['ratio'] = (float)$request->clean_general_cicle_price[$i];
            }

            $sql = [
                'calc_general_main' => json_encode($calc_general_main),
                'calc_general_services' => json_encode($calc_general_services),
                'calc_general_cicle' => json_encode($calc_general_cicle),
                'calc_general_type' => (isset($calc_general_type) ? json_encode($calc_general_type) : ''),
                'calc_general_additional_cleaner' => $request->calc_general_additional_cleaner,
                'calc_general_additional_cleaner_border' => $request->calc_general_additional_cleaner_border,
            ];

            // update main settings
            foreach ($sql as $name => $value) {
                try {
                    Settings::where('name', $name)->update([
                        'value' => $value
                    ]);
                } catch (QueryException $e) {
                    Log::error("Adding Calculator settings. Error: {$e}");
                    $notification['message'] = trans('messages.error_insert_bd') . "Error: {$e}";
                    return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
                }
            }

            return redirect()->back();
        }

        // get all calculator settings
        $query = Settings::where('name', 'like', 'calc_general_%')->orderby('id', 'ASC')->get();

        if (!$query) {
            die('Nothing found');
        }

        // prepare data
        foreach ($query as $key => $value) {
            $data[$value['name']] = json_decode($value['value'], true);
        }

        // get service pay types
        // if you go to edit this array, don't forget edit some_assets/admin/../main.js!
        $services_pay = config('constants.services_pay');

        // get service icons
        // if you go to edit this array, don't forget edit some_assets/admin/../main.js!
        $services_icons = config('constants.services_icons');

        return view('admin.calculator.general', compact('data', 'services_pay', 'services_icons'));
    }
}

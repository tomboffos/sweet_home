<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Transactions;

class TransactionsController extends CommonController
{
    /**
     * render list of pages
     * @return [view]
     */
    public function index()
    {
        $data = Transactions::orderby('id', 'desc')->paginate($this->postsPerPage());
        return view('admin.transactions.index', compact('data'));
    }
}

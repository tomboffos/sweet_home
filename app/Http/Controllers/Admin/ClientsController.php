<?php

namespace App\Http\Controllers\Admin;

use App\Models\Discounts;
use App\Models\UsedDiscounts;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Models\Role;
use App\Models\PointsLog;
use App\Models\UsersCards;
use Validator;
use Excel;
use Event;
use App\Events\PointsLogEvent;

// TODO; добавить возможность удалять клинеров из черного списка и из избранного
// TODO; возможно добавить возможность добавлять в избранное и в черный список клинеров

class ClientsController extends CommonController
{
    /**
     * render clients list
     * @return [view]
     */
    public function index()
    {
        // 1 - user, 2 - admin
        $data = Role::find(1)->users()->orderby('id', 'desc')->paginate($this->postsPerPage());
        return view('admin.users.clients.index', compact('data'));
    }

    /**
     * render new client form and add new client
     * @return [view]
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'min:6|confirmed',
                'points' => 'numeric',
                'rooms_number' => 'min:0|numeric',
                'bath_rooms_number' => 'min:0|numeric',
                'street' => 'required|max:255|string',
                'complex' => 'max:255|string',
                'house' => 'required|max:255|string',
                'housing' => 'max:255|string',
                'flat' => 'max:255|string',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $create = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'phone' => $request->phone,
                'points' => $request->points,
                'city' => $request->city,
                'rooms_number' => $request->rooms_number ? $request->rooms_number : null,
                'bath_rooms_number' => $request->bath_rooms_number ? $request->bath_rooms_number : null,
                'complex' => $request->complex,
                'housing' => $request->housing,
                'street' => $request->street,
                'house' => $request->house,
                'flat' => $request->flat,
                'subscribe' => (isset($request->subscribe) && !empty($request->subscribe) ? 1 : 0),
            ]);

            if (!$create) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }
            
            $user = User::find($create->id);
            $user->roles()->attach(1); // 1 - user, 2 - admin

            // add points to log
            if (isset($request->points) && !empty($request->points)) {
                Event::fire(new PointsLogEvent($create->id, trans('points.administration'), $request->points));
            }

            return redirect('/manager/users/clients/'.$create->id);
        }

        return view('admin.users.clients.new');
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'email|max:255',
                'password' => 'min:6|confirmed',
                'points' => 'numeric',
                'rooms_number' => 'min:0|numeric',
                'bath_rooms_number' => 'min:0|numeric',
                'street' => 'required|max:255|string',
                'complex' => 'max:255|string',
                'house' => 'required|max:255|string',
                'housing' => 'max:255|string',
                'flat' => 'max:255|string',
            ]);

            if ($v->fails()) {
                $notification['message'] = 'Некоторые поля заполнены не верно!';
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }
            
            // update email if not empty. check is the email unique
            if (isset($request->email) && !empty($request->email)) {
                $is_email_exist = User::where('email', $request->email)->get();

                if (!count($is_email_exist)) {
                    $sql['email'] = $request->email;
                }
            }

            // update pass if not empty
            if (isset($request->password) && !empty($request->password)) {
                $sql['password'] = bcrypt($request->password);
            }

            // get client info before update
            $client_info = User::where('id', $id)->first();

            $sql = [
                'name' => $request->name,
                'phone' => $request->phone,
                'points' => $request->points,
                'city' => $request->city,
                'rooms_number' => $request->rooms_number ? $request->rooms_number : null,
                'bath_rooms_number' => $request->bath_rooms_number ? $request->bath_rooms_number : null,
                'complex' => $request->complex,
                'housing' => $request->housing,
                'street' => $request->street,
                'house' => $request->house,
                'flat' => $request->flat,
                'subscribe' => (isset($request->subscribe) && !empty($request->subscribe) ? 1 : 0),
            ];
            $discount =Discounts::where('user_id',$client_info->id)->first();



            // update user
            $update = User::where('id', $id)->update($sql);
            if($discount){
                $request->name = str_replace(' ','',$request->name);
                $request->name = \Illuminate\Support\Str::upper(cyrillic_to_latin($request->name)).$client_info->id;

                $usedDiscounts = UsedDiscounts::where('discount',$discount->code)->get();
                foreach ($usedDiscounts as $usedDiscount) {
                    $usedDiscount['discount'] = $request->name;
                    $usedDiscount->save();
                }
                $discount['code'] =  $request->name;
                $discount['user_id'] = $client_info->id;
                $discount->save();
            }
            if (!$update) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // add points to log
            if (isset($request->points) && !empty($request->points) && $client_info->points != $request->points) {
                Event::fire(new PointsLogEvent($id, trans('points.administration'), $request->points));
            }

            return redirect()->back();
        }
        
        // get User data
        $data = User::findorfail($id);

        if ($data->hasRole('admin')) {
            return redirect('manager/users/managers/'.$id);
        }

        // get user's orders
        $ordersCleaners = $data->ordersCleaners()->orderby('id', 'desc')->take(30)->get();

        // get user's orders
        $ordersWorkers = $data->ordersWorkers()->orderby('id', 'desc')->take(30)->get();

        // get user's testimonals
        $testimonals = $data->testimonals()->orderby('id', 'desc')->take(30)->get();

        // get user's black list of cleaners
        $bad_cleaners = $data->bad_cleaners()->orderby('id', 'desc')->take(30)->get();

        // get user's favorite cleaners
        $favorite_cleaners = $data->favorite_cleaners()->orderby('id', 'desc')->take(30)->get();

        // get client card
        $clientCard = UsersCards::where('user_id', $data->id)->first();

        return view('admin.users.clients.item', compact('data', 'ordersCleaners', 'ordersWorkers', 'testimonals', 'bad_cleaners', 'favorite_cleaners', 'clientCard'));
    }

    /**
     * export clients in Excel file
     * @param  Request $request [description]
     * @param  [string] $type [subscribes or all]
     * @return [file] [xls excel file]
     */
    public function export(Request $request, $type)
    {
        if (!in_array($type, array('subscribes', 'all'))) {
            return abort(404);
        }

        Excel::create('Список клиентов', function ($excel) use ($type) {
            $excel->sheet('Список', function ($sheet) use ($type) {

                if ($type === 'subscribes') {
                    $sheet->fromModel(User::select('id', 'name as Имя', 'email', 'phone as Телефон', 'city as Город', 'street as Улица', 'complex as Район', 'housing as Жилой_комплекс', 'house as Дом', 'flat as Квартира', 'points as Баллы')->where('subscribe', 1)->get());
                } else {
                    $sheet->fromModel(User::select('id', 'name as Имя', 'email', 'phone as Телефон', 'city as Город', 'street as Улица', 'complex as Район', 'housing as Жилой_комплекс', 'house as Дом', 'flat as Квартира', 'points as Баллы', 'subscribe as Подписан на рассылку')->get());
                }
            });
        })->download('xls');
    }

    /**
     * manually approve client card
     * @param  [int] $id [user id]
     * @return [response]
     */
    public function approveClientCard($id)
    {

        // get user data
        $user = User::findorfail($id);

        // get user card
        $userCard = UsersCards::where('user_id', $user->id)->where('approve', 1)->first();

        if (!$userCard || !count($userCard)) {
            return redirect()->back();
        }

        // approve user card
        $kazkom = new \App\Http\Controllers\Front\KazkomController();
        $approve = $kazkom->approve_client_card($user, $userCard);

        // if card was approved
        if (count($approve) && isset($approve['approve']) && $approve['approve'] === 0) {
            $updateClientCard = UsersCards::where('user_id', $user->id)->update('approve', 0);

            if ($updateClientCard) {
                return redirect()->back();
            }
        }

        $notification['message'] = trans('messages.something_went_wrong');
        return redirect()->back()->with($notification);
    }

    /**
     * delete client
     */
    public function delete($id)
    {
        $data = User::where('id', $id)->delete();

        if ($data) {
            PointsLog::where('user_id', $id)->delete();
        }

        return redirect('/manager/users/clients');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Slider;
use Validator;
use Storage;

class SliderController extends CommonController
{
    /**
     * render list of slider
     * @return [view]
     */
    public function index()
    {
        $data = Slider::orderby('id', 'desc')->paginate($this->postsPerPage());
        return view('admin.slider.index', compact('data'));
    }

    /**
     * render new slider form and add new slider
     * @return [view]
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'max:255',
                'thumbnail' => 'required|image',
                'sort' => 'min:0|numeric',
                'status' => 'required|min:0|max:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            $create = Slider::create([
                'title' => $request->title,
                'text' => $request->text,
                'url' => $request->url,
                'sort' => $request->sort,
                'status' => $request->status,
                'thumbnail' => null,
            ]);

            if ($request->file('thumbnail')) {
                $thumbnail = $request->file('thumbnail')->getClientOriginalName();
                $request->file('thumbnail')->move(public_path('data/slider') . '/' . $create->id . '/', $thumbnail);

                Slider::where('id', $create->id)->update([
                    'thumbnail' => $thumbnail,
                ]);
            }

            if (!$create) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect('/manager/slider/'.$create->id);
        }

        return view('admin.slider.new');
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'max:255',
                'thumbnail' => 'image',
                'sort' => 'min:0|numeric',
                'status' => 'required|min:0|max:1|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            // update Slider
            $update = Slider::where('id', $id)
            ->update([
                'title' => $request->title,
                'text' => $request->text,
                'url' => $request->url,
                'sort' => $request->sort,
                'status' => $request->status,
            ]);

            if (!empty($request->thumbnail_delete)) {
                Storage::disk('slider')->delete("{$id}/{$request->thumbnail_delete}");

                Slider::where('id', $id)->update([
                    'thumbnail' => null,
                ]);
            }

            if ($request->file('thumbnail')) {
                $current = Slider::findorfail($id);

                if (!empty($current->thumbnail)) {
                    Storage::disk('slider')->delete("{$id}/{$current->thumbnail}");
                }

                $thumbnail = $request->file('thumbnail')->getClientOriginalName();
                $request->file('thumbnail')->move(public_path('data/slider') . '/' . $id . '/', $thumbnail);

                Slider::where('id', $id)->update([
                    'thumbnail' => $thumbnail,
                ]);
            }

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }

        $data = Slider::findorfail($id);
        return view('admin.slider.item', compact('data'));
    }

    /**
     * delete Slider
     */
    public function delete($id)
    {
        $data = Slider::where('id', $id)->delete();
        Storage::disk('slider')->deleteDirectory($id);
        return redirect('/manager/slider');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\DiscountSecond;
use App\Events\OrderDiscountCompleted;
use App\Models\UsedDiscounts;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Models\OrdersCleaners;
use App\Models\Settings;
use App\Models\Cleaners;
use App\Models\Discounts;
use App\Models\UsersCards;
use App\Models\CardsPayments;
use App\Models\Testimonals;
use App\User;
use Jenssegers\Date\Date;
use Event;
use App\Events\PointsLogEvent;
use App\Events\OrderComplete;
use DateTime;

class OrdersCleanersController extends CommonController
{
    /**
     * render orders list
     * @return [view]
     */
    public function index()
    {
        $data = OrdersCleaners::orderby('id', 'desc')->paginate($this->postsPerPage());

        // get payment methods
        $payment_methods = config('constants.payment_methods');

        // get payment statuses
        $payment_status = config('constants.payment_status');

        // get order statuses
        $order_status = config('constants.order_status');

        return view('admin.orders.cleaners', compact('data', 'payment_methods', 'payment_status', 'order_status'));
    }

    /**
     * edit and view Cleaner order
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            return $this->update($request, $id);
        }

        // set date locale
        Date::setLocale('en');

        // get order
        $data = OrdersCleaners::findorfail($id);

        // convert json strings to obj
        $data->type = json_decode($data->type);
        $data->cicle = json_decode($data->cicle);
        $data->services = json_decode($data->services);

        // get discount code for this order
        if (!is_null($data->discount_id) || !empty($data->discount_id)) {
            $discount = $data->discounts()->where('category', 0)->where('status', 1)->first();

        }

        // get client info
        $user = User::findorfail($data->client_id);

        // get cleaners from favorite list of this user
        $favorite_cleaners = $user->favorite_cleaners()->get()->toArray();

        // get cleaners from black list of this user
        $bad_cleaners = $user->bad_cleaners()->get()->toArray();

        // get all cleaners for this date
        $cleaners = Cleaners::leftJoin('cleaners_schedule', function ($join) {
            $join->on('cleaners.id', '=', 'cleaners_schedule.cleaner_id');
        })->where('cleaners.status', 1)
        ->where('cleaners_schedule.'.Date::parse($data->cl_date)->format('D'), 1)
        ->where('cleaners_schedule.t_from', '<=', (string)Date::parse($data->cl_time)->format('H:i:s'))
        ->where('cleaners_schedule.t_to', '>=', (string)Date::parse($data->cl_time)->format('H:i:s'))
        ->get()->toArray();

        // prepare cleaners list
        if ($cleaners && count($cleaners)) {
            // add favorite cleaner in common list
            if (count($favorite_cleaners)) {
                foreach ($cleaners as $key => $cleaner) {
                    foreach ($favorite_cleaners as $k => $favorite) {
                        if ($cleaner['cleaner_id'] === $favorite['id']) {
                            $cleaners[$key]['favorite'] = true;
                        }
                    }
                }
            }

            // add bad cleaner in common list
            if (count($bad_cleaners)) {
                foreach ($cleaners as $key => $cleaner) {
                    foreach ($bad_cleaners as $k => $bad) {
                        if ($cleaner['cleaner_id'] === $bad['id']) {
                            $cleaners[$key]['bad'] = true;
                        }
                    }
                }
            }
        }

        // get all calculator settings
        $field = 'calc_%';
        $substr = 5;

        if ($data->order_label === 'general') {
            $field = 'calc_general_%';
            $substr = 13;
        }

        $calculator = $this->calcSettings($field, $substr);

        if (!$calculator) {
            die('Calculator settings not found!');
        }

        // get service pay types
        // if you go to edit this array, don't forget edit /assets/admin/js/main.js!
        $services_pay = config('constants.services_pay');

        // get payment methods
        $payment_methods = config('constants.payment_methods');

        // get payment statuses
        $payment_status = config('constants.payment_status');

        // get order statuses
        $order_status = config('constants.order_status');

        // get client card
        $clientCard = UsersCards::where('user_id', $user->id)->first();
        $discount = UsedDiscounts::where('used_discounts.user_id',$user['id'])->join('discounts','discounts.code','=','used_discounts.discount')->select('discounts.*','discounts.user_id')->first();
        return view('admin.orders.cleaner', compact('data', 'services_pay', 'cleaners', 'calculator', 'discount', 'user', 'payment_methods', 'payment_status', 'order_status', 'clientCard'));
    }

    /**
        * Update order
        **/
    public function update($request, $id)
    {
        $v = Validator::make($request->all(), [
            'clean_number_rooms' => 'numeric',
            'clean_number_bathrooms' => 'numeric',
            'clean_number_window' => 'numeric',
            'clean_cicle' => 'string|max:255',
            'clean_type' => 'string|max:255',
            'clean_date' => 'required|string|max:255',
            'clean_time' => 'required|string|max:255',
            'elapsed' => 'required|string|max:255',
            'cleaner' => 'numeric',
            'payment_type' => 'required|numeric',
            'payment_status' => 'required|numeric',
            'order_status' => 'required|numeric',
            'points' => 'numeric',
            'total' => 'required',
            'discount_id' => 'numeric',
            'discount_code' => 'string',
            'client_id' => 'string',
        ]);

        if ($v->fails()) {
            $notification['message'] = trans('messages.fills_empty');
            return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
        }

        // calculate total price
        // add clean type, clean cicle, clean services
        if (!($arr = $this->postOrderCalculator($request))) {
            $notification['message'] = trans('messages.calculator_total_summ_error');
            return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
        }

        // get discount code if exist
        if (isset($request->discount_code) && !empty($request->discount_code)) {
            $discount = Discounts::where('status', 1)
                ->where('category', 0)
                ->where('code', $request->discount_code)
                ->first();

            if (!$discount) {
                $notification['message'] = trans('messages.discount_code_error');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }
        }

        // current order data
        $order = OrdersCleaners::where('id', $id)->first();

        // get client info
        $user = User::findorfail($request->client_id);

        // add/remove additional cleaner
        $additional_cleaners = !empty($order->additional) ? json_decode($order->additional, true) : null;

        if (isset($additional_cleaners['paid_cleaner']) && isset($additional_cleaners['paid_cleaner']['price'])) {
            $compare_sum = $arr['total'] - (int) $additional_cleaners['paid_cleaner']['price'];
        } else {
            $compare_sum = $arr['total'];
        }

        // get all calculator settings
        $field = 'calc_%';
        $substr = 5;

        if ($order->order_label === 'general') {
            $field = 'calc_general_%';
            $substr = 13;
        }

        $calculator = $this->calcSettings($field, $substr);

        if ($compare_sum - $calculator['additional_cleaner'] < $calculator['additional_cleaner_border']) {
            if (isset($additional_cleaners['free_cleaner'])) {
                unset($additional_cleaners['free_cleaner']);
            }
        } else {
            if (!isset($additional_cleaners['free_cleaner'])) {
                $additional_cleaners['free_cleaner'] = [
                    'count' => 1,
                    'price' => 0
                ];
            }
        }

        $order->update(['additional' => !empty($additional_cleaners) ? json_encode($additional_cleaners) : null]);

        // prepare data to update order
        $sql = [
            'discount_id' => isset($discount->id) ? $discount->id : null,
            'number_rooms' => $request->clean_number_rooms,
            'number_bathrooms' => $request->clean_number_bathrooms,
            'number_window' => isset($request->clean_number_window) ? $request->clean_number_window : null,
            'cl_date' => date("Y-m-d", strtotime($request->clean_date)),
            'cl_time' => $request->clean_time,
            'elapsed' => $this->calculateElapsedTime($request, $calculator),
            'cleaner_id' => (int)$request->cleaner,
            'payment_type' => (int)$request->payment_type,
            'payment_status' => (int)$request->payment_status,
            'status' => (int)$request->order_status,
            'points' => (int)$request->points,
            'comment' => $request->comment,
            'total' => $arr['total'],
            'type' => (isset($arr['type']) ? $arr['type'] : ''),
            'services' => (isset($arr['services']) ? $arr['services'] : ''),
            'cicle' => (isset($arr['cicle']) ? $arr['cicle'] : ''),
        ];

        // delete array whith helper data
        unset($arr);

        $update = OrdersCleaners::where('id', $id)->update($sql);

        if (!$update) {
            $notification['message'] = trans('messages.error_insert_bd');
            return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
        }

        // get points code if exist
        if (isset($request->points) && $request->points != $order->points) {
            // get currency of points
            $currency = Settings::where('name', 'points_currency')->first();

            // if new points value > current order points then check if user has so much points
            if ($request->points > $order->points) {
                if ($user->points >= (int) $request->points - $order->points) {
                    User::where('id', $user->id)->update([
                        'points' => $user->points - ( (int) $request->points - $order->points )
                    ]);
                    Event::fire(new PointsLogEvent($user->id, trans('points.administration'), (int) $request->points - $order->points));
                }
            } // if new points value < current order points
            elseif ($request->points < $order->points) {
                User::where('id', $user->id)->update([
                    'points' => $user->points + ( (int) $order->points - $request->points )
                ]);
                Event::fire(new PointsLogEvent($user->id, trans('points.administration'), (int) $order->points - $request->points));
            }
        }

        // send email to client if order complete
        if ((int) $request->order_status === 2 && isset($request->send_order_complete) && !empty($request->send_order_complete)) {
            if (empty($request->cleaner)) {
                $notification['message'] = trans('messages.choose_cleaner_before_order_complete');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }



            if($request->discount_code){
                $discount = Discounts::where('id',$order->discount_id)->first();
                $partner = User::where('id',$discount->user_id)->first();
                Event::fire(new OrderDiscountCompleted($order,$user));

            }
            $order = OrdersCleaners::where('id', $id)->first();
            $testimonal = Testimonals::where('order_id', $order->id)->get();

            if (!count($testimonal)) {
                $points_settings = Settings::where('name', 'testimonal')->first();
                $cleaner = Cleaners::where('id', $order->cleaner_id)->first();

                Event::fire(new OrderComplete($order, $user, $cleaner, $points_settings));
            }


        }

        return redirect()->back();
    }

    /**
     * get money form without client
     * @param  int $id
     * @return [type]
     */
    public function getMonteyFromClient($id)
    {
        if (empty($id)) {
            return redirect()->back();
        }

        $order = OrdersCleaners::findorfail($id);

        // if pay method is credit card
        if ($order->payment_type === 1) {
            // get client card
            $clientCard = UsersCards::where('user_id', $order->client_id)->where('approve', 0)->first();

            // if client has approved card
            if ($clientCard && !empty($clientCard->card_id)) {
                // get form to get money without client
                $kazkom = new \App\Http\Controllers\Front\KazkomController();
                $doPayment = $kazkom->pay_without_client($order, $clientCard);

                // if errors return back with errors
                if (isset($doPayment['errors']) && count($doPayment['errors'])) {
                    $error['message'] = trans('messages.errors_was_founded_details_here').' ';

                    foreach ($doPayment['errors'] as $key => $value) {
                        $error['message'] .= $value.' ';
                    }

                    return redirect()->back()->with($error);
                }

                // if succes mark order as Was Purshased
                if (isset($doPayment['success']) && count($doPayment['success'])) {
                    $updateOrder = OrdersCleaners::where('id', $id)->update([
                        'payment_status' => 1
                    ]);

                    // add data of payment to history (CardsPayments table)
                    if ($updateOrder) {
                        foreach ($doPayment['success'] as $key => $value) {
                            $sql[$key] = $value;
                        }

                        $addCardPayment = CardsPayments::create($sql);

                        if ($addCardPayment) {
                            return redirect()->back();
                        }
                    }
                }
            }
        }

        $notification['message'] = trans('messages.something_went_wrong');
        return redirect()->back()->with($notification);
    }

    /**
     * get total price when edit order
     * get clean type, clean services, clean cicle
     * @param  [array of obj] $request [_POST data]
     * @return [int]
     */
    public function postOrderCalculator($request)
    {
        $field = 'calc_%';
        $substr = 5;

        if ($request->order_label === 'general') {
            $field = 'calc_general_%';
            $substr = 13;
        }

        // get calculator settings
        if (!($calc_settings = $this->calcSettings($field, $substr))) {
            return false;
        }

        // get total price
        $total = $this->calculateTotal($request, $calc_settings);

        if ($total === false) {
            return false;
        }

        $data['total'] = $total;

        // add clean Type
        foreach ($calc_settings['type'] as $key => $value) {
            if ($request->order_label === $value['label']) {
                $data['type'] = json_encode($calc_settings['type'][$key]);
            }
        }

        // add clean Services
        if (isset($request->clean_services_name) && count($request->clean_services_name)) {
            // prepare post services
            $count_services = count($request->clean_services_name);
            for ($i = 0; $i < $count_services; $i++) {
                $calc_services[$i]['name'] = $request->clean_services_name[$i];
                $calc_services[$i]['price'] = (float)$request->clean_services_price[$i];
                $calc_services[$i]['pay'] = (float)$request->clean_services_pay[$i];
                $calc_services[$i]['time'] = $request->clean_services_time[$i];
                $calc_services[$i]['unit'] = (float)$request->clean_services_parts[$i];
            }

            $data['services'] = json_encode($calc_services);
        }

        // add clean Cicle
        if (isset($request->clean_cicle)) {
            foreach ($calc_settings['cicle'] as $key => $value) {
                if ($request->clean_cicle === $value['name']) {
                    $data['cicle'] = json_encode($calc_settings['cicle'][$key]);
                }
            }
        }

        return $data;
    }

    /**
     * calculate order total when edit order
     * @param  Request $request [POST data]
     * @return [response, array] [contents total amount]
     */
    public function calculateAjaxOrderTotal(Request $request)
    {
        if ($request->isMethod('post') && $request->ajax()) {
            $field = 'calc_%';
            $substr = 5;

            if ($request->order_label === 'general') {
                $field = 'calc_general_%';
                $substr = 13;
            }

            // get calculator settings
            if (!($calc_settings = $this->calcSettings($field, $substr))) {
                return response(['status' => 400, 'data' => 'calculator settings not found']);
            }

            // get total price
            $total = $this->calculateTotal($request, $calc_settings);

            if ($total === false) {
                return response(['status' => 400, 'data' => 'Ошибка при подсчете суммы!']);
            }

            return response(['status' => 200, 'data' => $total]);
        }
    }

    /**
     * calculate total price
     * @param  [array of obj] $request [_POST data]
     * @param  [array] $calc_settings [calculator settings]
     * @return [int]
     */
    public function calculateTotal($request, $calc_settings)
    {
        $total = 0;

        $order = OrdersCleaners::find($request->order_id);

        // add basic price
        $total = $calc_settings['main']['base_price'];

        // Стоимость уборки =
        // (базовая стоимость + комнаты*шт + санузлы*шт + доп.услуги*шт или часы)*коэффициент типа *коэффициент частоты

        $request->clean_number_rooms = $request->clean_number_rooms ? $request->clean_number_rooms : 1;
        $request->clean_number_bathrooms = $request->clean_number_bathrooms ? $request->clean_number_bathrooms : 1;

        // rooms
        if ($request->clean_number_rooms > $calc_settings['main']['number_room_elevated_price']) {
            $total += $calc_settings['main']['one_room_price'] * $calc_settings['main']['number_room_elevated_price'] + ($request->clean_number_rooms - $calc_settings['main']['number_room_elevated_price']) * $calc_settings['main']['one_room_elevated_price'];
        } else {
            $total += $calc_settings['main']['one_room_price'] * $request->clean_number_rooms;
        }

        // BathRooms Count
        $total += $request->clean_number_bathrooms * $calc_settings['main']['one_bathroom_price'];

        // windows (general cleaning)
        if (isset($request->order_label)
            && $request->order_label === 'general'
            && isset($request->clean_number_window)
            && !empty($request->clean_number_window)) {
            $total += $calc_settings['main']['one_window_price'] * (int) $request->clean_number_window;
        }

        // Services
        if (isset($request->clean_services_name) && count($request->clean_services_name)) {
            // prepare post services
            $count_services = count($request->clean_services_name);
            for ($i=0; $i < $count_services; $i++) {
                $calc_services[$i]['name'] = $request->clean_services_name[$i];
                $calc_services[$i]['price'] = (float)$request->clean_services_price[$i];
                $calc_services[$i]['pay'] = (float)$request->clean_services_pay[$i];
                $calc_services[$i]['unit'] = (float)$request->clean_services_parts[$i];
            }

            foreach ($calc_services as $key => $value) {
                // price
                if ((int)$value['pay'] === 0) {
                    $total += $value['price']*$value['unit'];
                }

                // price for 1 unit
                if ((int)$value['pay'] === 1) {
                    $total += $value['price']*$value['unit'];
                }

                // price for 1 hour
                if ((int)$value['pay'] === 2) {
                    $total += $value['price']*$value['unit'];
                }
            }
        }

        // Type
        if (isset($request->clean_type)) {
            foreach ($calc_settings['type'] as $key => $value) {
                if ($request->clean_type === $value['name']) {
                    $total = $calc_settings['type'][$key]['ratio']*$total;
                }
            }
        }

        // Cicle
        if (isset($request->clean_cicle)) {
            foreach ($calc_settings['cicle'] as $key => $value) {
                if ($request->clean_cicle === $value['name']) {
                    $total = $calc_settings['cicle'][$key]['ratio']*$total;
                }
            }
        }

        // If additional cleaner exist
        if (!empty($order->additional)) {
            $order->additional = json_decode($order->additional, true);
            if (isset($order->additional['paid_cleaner']) && isset($order->additional['paid_cleaner']['price'])) {
                $total += $order->additional['paid_cleaner']['price'];
            }
        }

        // Apply Discount code
        if (isset($request->discount_code) && !empty($request->discount_code)) {
            $discount = Discounts::where('status', 1)
                ->where('category', 0)
                ->where('code', $request->discount_code)
                ->first();

            if ($discount) {
                // if current discount === posted do nothing
                if ($request->discount_id !== $discount->id) {
                    // percentage
                    if ($discount->type === 0) {
                        $total = $total-($discount->price*$total);
                    }

                    // amount
                    if ($discount->type === 1) {
                        $total = $total-$discount->price;
                    }
                }
            } else {
                return false;
            }
        } // Apply points
        elseif (isset($request->points) && !empty($request->points) && $request->points >= 0) {
            $user = User::findorfail($request->client_id);

            // get current points of this order
            $currentOrderPointsValue = OrdersCleaners::where('id', $request->order_id)->first();

            // if points changed
            if ($currentOrderPointsValue->points !== $request->points) {
                // get currency of points
                $currency = Settings::where('name', 'points_currency')->first();

                // if new points value > current order points then check if user has so much points
                if ($request->points > $currentOrderPointsValue->points) {
                    if ($user->points > 0 && $user->points >= (int) $request->points - $currentOrderPointsValue->points) {
                        $total = $total-($currency->value*$request->points);
                    } else {
                        return false;
                    }
                } // if new points value < current order points
                elseif ($request->points < $currentOrderPointsValue->points) {
                    $total = $total-($currency->value*$request->points);
                }
            }
        }

        return $total;
    }

    /**
     * @param $request
     * @return string
     */
    public function calculateElapsedTime($request, $calculator)
    {
        $order = OrdersCleaners::find($request->order_id);
        $elapsed = new DateTime();

        $elapsed->setDate(0, 0, 0);
        $elapsed->setTime(0, 0, 0);

        // Set base time
        $base_time = explode(':', $calculator['main']['base_time']);
        $this->_summTime($elapsed, $base_time[0], $base_time[1]);
        
        // Set room time
        $request->clean_number_rooms = $request->clean_number_rooms ? (int) $request->clean_number_rooms : 1;
        $room_time = explode(':', $calculator['main']['room_time']);

        for ($i = 0; $i < $request->clean_number_rooms; $i++) {
            $this->_summTime($elapsed, $room_time[0], $room_time[1]);
        }

        // Set bathrooms time
        $request->clean_number_bathrooms = $request->clean_number_bathrooms ? (int) $request->clean_number_bathrooms : 1;
        $bathroom_time = explode(':', $calculator['main']['bathroom_time']);

        for ($i = 0; $i < $request->clean_number_bathrooms; $i++) {
            $this->_summTime($elapsed, $bathroom_time[0], $bathroom_time[1]);
        }

        // Services
        $count_services = isset($request->clean_services_name) ? count($request->clean_services_name) : false;

        if ($count_services) {
            for ($i = 0; $i < $count_services; $i++) {
                for ($k = 0; $k < (int) $request->clean_services_parts[$i]; $k++) {
                    $service_time = explode(':', $request->clean_services_time[$i]);
                    if (count($service_time) > 1) {
                        $this->_summTime($elapsed, $service_time[0], $service_time[1]);
                    }
                }
            }
        }

        $cleaners_num = 1;

        // If additional cleaners exist
        if (!empty($order->additional)) {
            $order->additional = json_decode($order->additional, true);
            
            if (isset($order->additional['paid_cleaner'])) {
                $cleaners_num += 1;
            }
            
            if (isset($order->additional['free_cleaner'])) {
                $cleaners_num += 1;
            }

            if ($cleaners_num > 1) {
                $minutes = (int) $elapsed->format('i') + ((int) $elapsed->format('H') * 60);
        
                if ($minutes > 0) {
                    $minutes = floor($minutes / $cleaners_num);
                }

                $elapsed->setTime(floor($minutes / 60), floor($minutes % 60));
            }
        }

        return $elapsed->format('H:i');
    }

    /**
     * get calculator settings
     * @return [array]
     */
    public function calcSettings($field = 'calc_%', $substr = 5)
    {
        // get all calclulator settings
        $calculator_query = Settings::where('name', 'like', $field)->orderby('id', 'ASC')->get();

        if (!$calculator_query) {
            return false;
        }

        // prepare data
        foreach ($calculator_query as $key => $value) {
            $calculator[substr($value['name'], $substr)] = json_decode($value['value'], true);
        }

        return $calculator;
    }

    /**
     * copy current order
     * @param  Request $request
     * @return [response]
     */
    public function copyOrder(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax()) {
            return response(['status' => 400]);
        }

        // prepare services, type, cicle
        $data = $this->postOrderCalculator($request);

        $sql = [
            'client_id' => $request->client_id,
            'services' => (isset($data['services']) ? $data['services'] : null),
            'additional' => (isset($data['additional']) ? $data['additional'] : null),
            'type' => (isset($data['type']) ? $data['type'] : null),
            'number_rooms' => $request->clean_number_rooms,
            'number_bathrooms' => $request->clean_number_bathrooms,
            'number_window' => isset($request->clean_number_window) ? $request->clean_number_window : null,
            'cicle' => (isset($data['cicle']) ? $data['cicle'] : ''),
            'cl_date' => date("Y-m-d", strtotime($request->clean_date)),
            'cl_time' => $request->clean_time,
            'total' => $request->total,
            'payment_type' => $request->payment_type,
            'elapsed' => $request->elapsed,
            'comment' => $request->comment,
            'payment_status' => 0,
            'status' => 0,
            'label' => $request->order_label,
        ];

        // add discount if not empty or points. not both!
        if (!empty($request->discount_code)) {
            $discount = Discounts::where('code', $request->discount_code)
                ->where('category', 0)
                ->where('status', 1)
                ->where('finish', '>=', (string)date("Y-m-d H:i:s"))
                ->first();

            $sql['discount_id'] = (count($discount) ? $discount->id : null);
        } elseif (!empty($request->points)) {
            $sql['points'] = (!empty($request->points) ? $request->points : null);
        }

        // create new order
        $createOrder = OrdersCleaners::create($sql);

        return response(['status' => 200, 'id' => $createOrder->id]);
    }

    /**
     * delete order
     */
    public function delete($id)
    {
        OrdersCleaners::where('id', $id)->delete();
        return redirect('/manager/orders/cleaners');
    }

    /**
     * Summ Time
     * @param object $elapsed
     * @param string $h
     * @param string $m
     */
    private function _summTime(&$elapsed, $h, $m)
    {
        $elapsed->setTime((int) $elapsed->format('H') + (int) $h, (int) $elapsed->format('i') + (int) $m);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Testimonals;
use Validator;

class TestimonalsController extends CommonController
{
    /**
     * render testimonals list
     * @return [view]
     */
    public function index()
    {
        $data = Testimonals::select()
            ->orderby('sort', 'asc')
            ->orderby('id', 'desc')
            ->paginate($this->postsPerPage());

        return view('admin.testimonals.index', compact('data'));
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'rating' => 'required|numeric|min:0|max:5',
                'sort' => 'required|numeric',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // update Testimonals
            $update = Testimonals::where('id', $id)->update([
                'rating' => $request->rating,
                'text' => $request->text,
                'status' => $request->status ? $request->status : 0,
                'sort' => $request->sort ? $request->sort : 0,
            ]);
            
            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }
      
        $data = Testimonals::findorfail($id);
        $user = $data->user()->first();

        if ($data->type === 0) {
            $order = $data->orderCleaners()->first();
            $cleaner = $data->cleaner()->first();
        } else {
            $order = $data->orderWorkers()->first();
            $cleaner = false;
        }

        if (!$user || !$order) {
            die('User or Order or Cleaner not found!');
        }

        return view('admin.testimonals.item', compact('data', 'user', 'order', 'cleaner'));
    }

    /**
     * delete Testimonals
     */
    public function delete($id)
    {
        $data = Testimonals::where('id', $id)->delete();
        return redirect('/manager/testimonals');
    }
}

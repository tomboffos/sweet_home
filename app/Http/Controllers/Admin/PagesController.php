<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Models\Pages;

class PagesController extends CommonController
{
    /**
     * render list of pages
     * @return [view]
     */
    public function index()
    {
        $data = Pages::orderby('sort', 'asc')->orderby('id', 'desc')->paginate($this->postsPerPage());
        return view('admin.pages.index', compact('data'));
    }

    /**
     * render new page form and add new page
     * @return [view]
     */
    public function create(Request $request)
    {

        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'status' => 'required|min:0|max:1|numeric',
                'sort' => 'min:0|numeric',
                'menu' => 'required|min:0|max:1|numeric',               'footer' => 'required|min:0|max:1|numeric',

            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            $create = Pages::create([
                'title' => $request->title,
                'text' => $request->text,
                'slug' => $request->slug,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'status' => $request->status,
                'sort' => $request->sort ? $request->sort : 0,
                'menu' => $request->menu,               'footer' => $request->footer,

            ]);

            if (!$create) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect('/manager/pages/'.$create->id);
        }

        return view('admin.pages.new');
    }

    /**
     * edit and view existing item
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'status' => 'required|min:0|max:1|numeric',
                'sort' => 'min:0|numeric',
                'menu' => 'required|min:0|max:1|numeric',               'footer' => 'required|min:0|max:1|numeric',

            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // translit title for pretty url
            if (empty($request->slug)) {
                $request->slug = $this->str2url($request->title);
            }

            // update Page
            $update = Pages::where('id', $id)
            ->update([
                'title' => $request->title,
                'text' => $request->text,
                'slug' => $request->slug,
                'meta_title' => $request->meta_title,
                'meta_description' => $request->meta_description,
                'status' => $request->status,
                'sort' => $request->sort ? $request->sort : 0,
                'menu' => $request->menu,               'footer' => $request->footer,
            ]);

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }

        $data = Pages::findorfail($id);
        return view('admin.pages.item', compact('data'));
    }

    /**
     * delete page
     */
    public function delete($id)
    {
        $data = Pages::where('id', $id)->delete();
        return redirect('/manager/pages');
    }
}

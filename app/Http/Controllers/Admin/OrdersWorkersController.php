<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Models\OrdersWorkers;
use App\Models\WorkersCategories;
use App\Models\Settings;
use App\Models\Cleaners;
use App\Models\Discounts;
use App\User;
use Jenssegers\Date\Date;

class OrdersWorkersController extends CommonController
{
    /**
     * render orders list
     * @return [view]
     */
    public function index()
    {
        $data = OrdersWorkers::orderby('id', 'desc')->paginate($this->postsPerPage());

        // get payment methods
        $payment_methods = config('constants.payment_methods');

        // get payment statuses
        $payment_status = config('constants.payment_status');

        // get order statuses
        $order_status = config('constants.order_status');

        return view('admin.orders.workers', compact('data', 'payment_methods', 'payment_status', 'order_status'));
    }

    /**
     * edit and view Worker order
     */
    public function item(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $v = Validator::make($request->all(), [
                'payment_type' => 'required|numeric',
                'payment_status' => 'required|numeric',
                'order_status' => 'required|numeric',
                'points' => 'numeric',
                'total' => 'required',
                'discount_id' => 'numeric',
                'discount_code' => 'string',
            ]);

            if ($v->fails()) {
                $notification['message'] = trans('messages.fills_empty');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            // get discount code if exist
            if (isset($request->discount_code) && !empty($request->discount_code)) {
                $discount = Discounts::where('status', 1)->where('category', 1)->where('code', $request->discount_code)->where('finish', '>=', (string)date("Y-m-d H:i:s"))->first();

                if (!$discount) {
                    $notification['message'] = trans('messages.discount_code_error');
                    return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
                }
            }

            // prepare data to update order
            $sql = [
                'discount_id' => isset($discount->id) ? $discount->id : null,
                'worker_id' => (int) $request->worker_id,
                'payment_type' => (int) $request->payment_type,
                'text' => $request->text,
                'requirements' => $request->requirements,
                'schedule' => $request->schedule,
                // 'wage' => $request->wage,
                'address' => $request->address,
                'payment_status' => (int) $request->payment_status,
                'status' => (int) $request->order_status,
                'points' => (int) $request->points,
                'total' => $request->total,
                'type' => $request->form_type,
            ];

            $update = OrdersWorkers::where('id', $id)->update($sql);

            if (!$update) {
                $notification['message'] = trans('messages.error_insert_bd');
                return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
            }

            return redirect()->back();
        }
        
        // get order
        $data = OrdersWorkers::findorfail($id);

        // get discount code for this order
        if (!is_null($data->discount_id) || !empty($data->discount_id)) {
            $discount = $data->discounts()->where('category', 1)->where('status', 1)->first();
        }else{
            $discount = null;

        }

        // get client info
        if (!is_null($data->client_id)) {
            $user = User::findorfail($data->client_id);
        }

        // get client info
        if (!is_null($data->guest)) {
            $user = json_decode($data->guest);
        }

        // get Worker Professions
        $professions = WorkersCategories::where('status', 1)->get();

        // get current worker profession
        $profession = $data->professions()->first();

        // get service pay types
        // if you go to edit this array, don't forget edit /assets/admin/js/main.js!
        $services_pay = config('constants.services_pay');

        // get payment methods
        $payment_methods = config('constants.payment_methods');

        // get payment statuses
        $payment_status = config('constants.payment_status');

        // get order statuses
        $order_status = config('constants.order_status');

        // form types
        $form_types = config('constants.form_types');

        return view('admin.orders.worker', compact(
            'data',
            'services_pay',
            'discount',
            'user',
            'payment_methods',
            'payment_status',
            'order_status',
            'professions',
            'profession',
            'form_types'
        ));
    }

    /**
     * delete order
     */
    public function delete($id)
    {
        OrdersWorkers::where('id', $id)->delete();
        return redirect('/manager/orders/workers');
    }
}

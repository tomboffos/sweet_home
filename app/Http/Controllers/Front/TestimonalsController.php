<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Testimonals;
use App\User;

class TestimonalsController extends CommonController
{
    /**
     * render testimonals index page
     * @return [view]
     */
    public function index(Request $request)
    {
        $data = Testimonals::select('users.name', 'testimonals.text')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'testimonals.client_id');
            })->where('testimonals.status', 1)
                ->orderby('testimonals.id', 'desc')
                ->paginate($this->postsPerPage());

        $meta = $this->meta(null);

        if(isset($request->page)) {
            $meta->meta_title = 'Страница ' . (int) $request->page . ' – ' . $meta->meta_title;
        }

        return view('front.testimonals.index', compact('data', 'meta'));
    }
}

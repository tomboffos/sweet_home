<?php
namespace App\Http\Controllers\Front;

use App\Models\Settings;
use App\Models\Pages;
use App\Models\WorkersCategories;

class ComposerController extends CommonController
{
    public function banners()
    {
        $data = Settings::where('name', 'index')->first();

        if (!$data) {
            return false;
        }

        $data = json_decode($data->value, true);

        return $data['banners'];
    }

    /**
     * get social links
     * @return bool|mixed
     */
    public function social()
    {
        $social = Settings::where('name', 'social')->first();
        $socials[] = $social;

        if (count($socials)) {
            return json_decode($social->value);
        }

        return false;
    }

    /**
     * get contacts
     * @return array|bool
     */
    public function phones()
    {
        $phones = Settings::where('name', 'phones')->first();
        $telephones[] = $phones;
        if (count($telephones)) {
            return explode(',', $phones->value);
        }
        return false;
    }

    /**
     * get pages
     * @return [obj]
     */
    public function pages()
    {
        $pages = Pages::where('status', 1)
                      ->where('footer', 1)
                      ->orderby('sort', 'asc')
                      ->get();

        if (count($pages)) {
            return $pages;
        }

        return false;
    }

    /**
     * get workers categories
     * @return bool
     */
    public function workers_categories()
    {
        $workers_categories = WorkersCategories::where('status', 1)
            ->where('footer', 1)
            ->orderby('sort', 'asc')
            ->get();

        if (count($workers_categories)) {
            return $workers_categories;
        }

        return false;
    }

    /**
     * get all vars and include in views
     * @param $view
     */
    public function compose($view)
    {
        $view
            ->with('social', $this->social())
            ->with('phones', $this->phones())
            ->with('footer_menu', $this->pages())
            ->with('banners', $this->banners())
            ->with('workers_categories', $this->workers_categories());
    }
}

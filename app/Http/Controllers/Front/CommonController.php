<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Settings;

class CommonController extends Controller
{
    /**
     * get number of items per page
     * @return [int]
     */
    public function postsPerPage()
    {
        $n = Settings::where('name', 'posts_per_page')->first();
        return (int)$n->value;
    }

    /**
     * get meta titles
     * @return [obj]
     */
    public function meta($data = null)
    {
        if (!is_null($data) || !empty($data)) {
            return $data;
        }

        $route = \Request::route()->getName();

        $name = "meta_$route";
        $meta = Settings::where('name', $name)->first();
        $metas[] = $meta;

        if (count($metas)) {
            return json_decode($meta->value);
        }

        return false;
    }

    /**
     * 
     * @return string
     */
    public static function showLangPanel()
    {
        $route = \Request::route()->getName();
        return in_array($route, ['index', 'cleaning', 'cleaning_general', 'workers']);
    }
}

<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OrdersCleaners;
use App\Models\UsersCards;
use App\User;
use Log;
use Curl;
use SimpleXMLElement;

class KazkomController extends Controller
{
    /**
     * return view data (html)
     * @param [_POST] $request [from form]
     * @param [obj] $data [order data]
     * @return [view] [html]
     */
    public function view($order, $user)
    {
        return view()->make('front.payments.kazkom', [
            'paymentData' => $this->get_form_to_add_card($order, $user)
        ]);
    }

    /**
     * Prepare data to send in KazkomPayService
     * @param  [numeric] $order_id   [current order ID]
     * @param  [decimal] $total_summ [current order total summ]
     * @return [json]
     */
    public function get_form_to_add_card($order, $user)
    {
        // Invert key
        $this->invert();

        // get config
        $config = $this->config();

        // Open public key
        $this->load_private_key(storage_path("kazkom/cert.prv"), $config['kazkom_pay_password']);

        // xml order template
        $merchant = '<merchant cert_id="%certificate%" name="%merchant_name%"><order order_id="%order_id%" amount="%amount%" currency="%currency%"><department merchant_id="%merchant_id%" abonent_id="%abonent_id%" recepient="%abonent_id%" sessionid="" approve="0" service_id="" abonent_iin=""/></order></merchant>';

        // Prepare order data, fill xml order template
        $merchant = preg_replace('/\%certificate\%/', $config['kazkom_pay_merchant_cert_id'], $merchant);
        $merchant = preg_replace('/\%merchant_name\%/', $config['kazkom_pay_merchant_name'], $merchant);
        $merchant = preg_replace('/\%order_id\%/', sprintf("%09d", $order->id), $merchant);
        $merchant = preg_replace('/\%currency\%/', $config['kazkom_pay_currency'], $merchant);
        $merchant = preg_replace('/\%merchant_id\%/', $config['kazkom_pay_merchant_id'], $merchant);
        $merchant = preg_replace('/\%amount\%/', 5, $merchant); // 5 - fixed price
        $merchant = preg_replace('/\%abonent_id\%/', $user->id, $merchant);

        // Prepare sign
        $merchant_sign = '<merchant_sign type="RSA">'.$this->sign64($merchant).'</merchant_sign>';

        // Make document
        $xml = "<document>".$merchant.$merchant_sign."</document>";
        
        // Prepare data and send to view via ajax
        $data['email'] = $user->email;
        $data['BackLink'] = $config['BackLink'];
        $data['PostLink'] = $config['PostLink'];
        $data['Language'] = $config['Language'];
        $data['FailureBackLink'] = $config['FailureBackLink'];
        $data['xml'] = base64_encode($xml);
        $data['action'] = $config['action_add_card'];
        $data['template'] = 'default_card2.xsl';

        return $data;
    }

    /**
     * get list of client cards
     * @param integer $user_id
     * @return array Contents card id and approve code - 0 or 1
     */
    public function get_client_cards_list($user_id = 1)
    {
        // Invert key
        $this->invert();

        // get config
        $config = $this->config();

        // Open public key
        $this->load_private_key(storage_path("kazkom/cert.prv"), $config['kazkom_pay_password']);

        // xml order template
        $merchant = '<merchant id="%merchant_id%"><client abonent_id="%abonent_id%" action="list"/></merchant>';

        // Prepare template to request
        $merchant = preg_replace('/\%merchant_id\%/', $config['kazkom_pay_merchant_id'], $merchant);
        $merchant = preg_replace('/\%abonent_id\%/', $user_id, $merchant);

        // Prepare sign
        $merchant_sign = '<merchant_sign type="RSA">'.$this->sign64($merchant).'</merchant_sign>';

        // Make document
        $xml = "<document>".$merchant.$merchant_sign."</document>";

        // get response from Kazkom server. Response is xml document.
        $response = Curl::to($config['action_control']."?".urlencode($xml))->get();

        if (!$response || empty($response)) {
            return false;
        }

        // parse xml and return array. Contents card id and approve code - 0 or 1.
        return $this->cardsListXmlParser($response);
    }

    /**
     * handle xml when client card is deleted
     * @param  [int] $user_id
     * @param  [string] $card_id
     * @return [array] contains card id and client id
     */
    public function get_client_card_delete_data($user_id, $card_id)
    {
        // Invert key
        $this->invert();

        // get config
        $config = $this->config();

        // Open public key
        $this->load_private_key(storage_path("kazkom/cert.prv"), $config['kazkom_pay_password']);

        // xml order template
        $merchant = '<merchant id="%merchant_id%"><client abonent_id="%abonent_id%" action="delete"  card_id="%card_id%"/></merchant>';

        // Prepare template to request
        $merchant = preg_replace('/\%merchant_id\%/', $config['kazkom_pay_merchant_id'], $merchant);
        $merchant = preg_replace('/\%abonent_id\%/', $user_id, $merchant);
        $merchant = preg_replace('/\%card_id\%/', $card_id, $merchant);

        // Prepare sign
        $merchant_sign = '<merchant_sign type="RSA">'.$this->sign64($merchant).'</merchant_sign>';

        // Make document
        $xml = "<document>".$merchant.$merchant_sign."</document>";

        // get response from Kazkom server. Response is xml document.
        $response = Curl::to($config['action_control']."?".urlencode($xml))->get();

        if (!$response || empty($response)) {
            return false;
        }

        // parse xml and return array. Contents card id and client id.
        return $this->deleteCardXmlParser($response);
    }

    /**
     * manually aprove client card
     * @param  integer $user_id
     * @param  integer $card_id
     * @param  integer $order_id
     * @return [type]
     */
    public function approve_client_card($user, $card)
    {
        // Invert key
        $this->invert();

        // get config
        $config = $this->config();

        // Open public key
        $this->load_private_key(storage_path("kazkom/cert.prv"), $config['kazkom_pay_password']);

        // xml order template
        $merchant = '<merchant id="%merchant_id%"><client abonent_id="%abonent_id%" action="approve" card_id="%card_id%" order_id="%order_id%"/></merchant>';

        // Prepare template to request
        $merchant = preg_replace('/\%merchant_id\%/', $config['kazkom_pay_merchant_id'], $merchant);
        $merchant = preg_replace('/\%abonent_id\%/', $user->id, $merchant);
        $merchant = preg_replace('/\%card_id\%/', $card->card_id, $merchant);
        $merchant = preg_replace('/\%order_id\%/', $card->id, $merchant);

        // Prepare sign
        $merchant_sign = '<merchant_sign type="RSA">'.$this->sign64($merchant).'</merchant_sign>';

        // Make document
        $xml = "<document>".$merchant.$merchant_sign."</document>";

        // get response from Kazkom server. Response is xml document.
        $response = Curl::to($config['action_control']."?".urlencode($xml))->get();

        if (!$response || empty($response)) {
            return false;
        }

        // parse xml and return array. Contents card id and approve code - 0 or 1.
        return $this->cardsListApproveXmlParser($response, $card->card_id);
    }

    /**
     * Pay without client from admin area
     * @param  [numeric] $order_id   [current order ID]
     * @param  [decimal] $total_summ [current order total summ]
     * @return [string] form
     */
    public function pay_without_client($order, $card)
    {
        // Invert key
        $this->invert();

        // get config
        $config = $this->config();

        // Open public key
        $this->load_private_key(storage_path("kazkom/cert.prv"), $config['kazkom_pay_password']);

        // xml order template
        $merchant = '<merchant cert_id="%certificate%" name="%merchant_name%"><order order_id="%order_id%" amount="%amount%" currency="%currency%" type="0"><department main="%merchant_id%" benef="%benef%" amount="%amount%" abonent_id="%abonent_id%" recepient="%recepient%" card_id="%card_id%"/></order></merchant>';

        // Prepare order data, fill xml order template
        $merchant = preg_replace('/\%certificate\%/', $config['kazkom_pay_merchant_cert_id'], $merchant);
        $merchant = preg_replace('/\%merchant_name\%/', $config['kazkom_pay_merchant_name'], $merchant);
        $merchant = preg_replace('/\%order_id\%/', sprintf("%09d", $order->id), $merchant);
        $merchant = preg_replace('/\%currency\%/', $config['kazkom_pay_currency'], $merchant);
        $merchant = preg_replace('/\%merchant_id\%/', $config['kazkom_pay_merchant_id'], $merchant);
        $merchant = preg_replace('/\%benef\%/', $config['kazkom_pay_merchant_id_debit'], $merchant);
        $merchant = preg_replace('/\%amount\%/', $order->total, $merchant);
        $merchant = preg_replace('/\%abonent_id\%/', $order->client_id, $merchant);
        $merchant = preg_replace('/\%recepient\%/', $order->client_id, $merchant);
        $merchant = preg_replace('/\%card_id\%/', $card->card_id, $merchant);

        // Prepare sign
        $merchant_sign = '<merchant_sign type="RSA">'.$this->sign64($merchant).'</merchant_sign>';

        // Make document
        $xml = "<document>".$merchant.$merchant_sign."</document>";

        $response = Curl::to($config['action_trans'])
            ->withData(array('Signed_Order_B64' => base64_encode($xml)))
            ->post();

        if (!$response || empty($response)) {
            return false;
        }

        // parse xml and return array. Contents payment of order data
        return $this->getMoneyXmlParser($response, $order, $card);
    }

    /**
     * Send 0 if all POSTed data from KazkomPayService is valid
     * @param $[request] [_POST data]
     * @return [numeric]
     */
    public function service($request)
    {
        if (!isset($request->response) || empty($request->response)) {
            return false;
        }

        $result = $this->postResponseXmlParser(stripslashes($request->response));

        if ($result['status']) {
            echo 0;
            die();
        }

        // log if fail
        Log::error('Ошибка при авторизации KazkomPayService! Описание ошибки: '.$result['code']);
        die();
    }

    /**
     * parse xml list of users cards
     * @param [string] $response
     * @return [array]
     */
    public function cardsListXmlParser($response)
    {
        // get config
        $config = $this->config();
        
        $array = new SimpleXMLElement($response);

        if (!$array || !count($array)) {
            return false;
        }

        // get data from request tag
        $arr_request = $array->request->attributes();

        if (!count($arr_request)) {
            return false;
        }

        foreach ($arr_request as $key => $value) {
            $request[$key] = (string)$value;
        }

        // check company merchant_id
        if ($request['id'] !== $config['kazkom_pay_merchant_id']) {
            return false;
        };

        // get client data from Users Cards table
        $user = User::where('id', $request['abonent_id'])->first();

        if (!$user || !count($user)) {
            return false;
        }

        // get data from order tag
        $arr_cards = $array->cards;

        if (!count($arr_cards) || !count((array) $arr_cards)) {
            return false;
        }

        // get card data from list of users cards
        // we need only one
        foreach ($arr_cards as $key => $value) {
            $card = $value->item->attributes();
            if ((int) $card->HBID === (int) $user->id) {
                $cardData['cardID'] = (string) $card->CardID;
                $cardData['CardMask'] = (string) $card->CardMask;
                $cardData['approve'] = (int) $card->approve;
                break;
            }
        }

        if (isset($cardData) && count($cardData)) {
            return $cardData;
        }

        return false;
    }

    /**
     * parse xml response when manually card approved
     * @param [string] $response
     * @return [array]
     */
    public function cardsListApproveXmlParser($response, $cardID)
    {
        // get config
        $config = $this->config();
        
        $array = new SimpleXMLElement($response);

        if (!$array || !count($array)) {
            return false;
        }

        // get data from request tag
        $arr_request = $array->request->attributes();

        if (!count($arr_request)) {
            return false;
        }

        foreach ($arr_request as $key => $value) {
            $request[$key] = (string)$value;
        }

        // check company merchant_id
        if ($request['id'] !== $config['kazkom_pay_merchant_id']) {
            return false;
        };

        // get client data from Users Cards table
        $user = User::where('id', $request['abonent_id'])->first();

        if (!$user || !count($user)) {
            return false;
        }

        // get data from order tag
        $arr_cards = $array->cards;

        if (!count($arr_cards)) {
            return false;
        }

        // get card data from list of users cards
        // we need only one
        foreach ($arr_cards as $key => $value) {
            $card = $value->item->attributes();
            if ((int) $card->HBID === (int) $user->id && (string) $card->CardID === $cardID) {
                $cardData['cardID'] = (string) $card->CardID;
                $cardData['CardMask'] = (string) $card->CardMask;
                $cardData['approve'] = (int) $card->approve;
                break;
            }
        }

        if (isset($cardData) && count($cardData)) {
            return $cardData;
        }

        return false;
    }

    /**
     * parse xml response when geted money from client
     * @param  [XML] $response
     * @param  [object] $order [contents order data]
     * @param  [object] $card [contents client card data]
     * @return [array]
     */
    public function getMoneyXmlParser($response, $order, $card)
    {
        // get config
        $config = $this->config();
        
        $array = new SimpleXMLElement($response);

        if (!$array || !count($array)) {
            $errors['errors'] = [];
            $errors['errors']['response'] = 'Response empty!';
        }

        // get data from error tag
        $arr_error = $array->error->attributes();

        if (count($arr_error)) {
            foreach ($arr_error as $key => $value) {
                if (in_array($key, array('input', 'payment', 'system')) && !empty((string) $value)) {
                    $errors['errors'][$key] = (string) $value;
                }
            }
        }

        // there are some errors
        if (isset($errors) && count($errors)) {
            return $errors;
        }

        // get data from payment tag
        $arr_payment = $array->payment->attributes();

        if (!count($arr_payment)) {
            $errors['errors'] = [];
            $errors['errors']['payment'] = 'Payment data doesn\'t exist!';
            return $errors;
        }

        foreach ($arr_payment as $key => $value) {
            $payment[$key] = (string) $value;
        }

        // check company merchant_id
        if ($payment['MerchantID'] !== $config['kazkom_pay_merchant_id_debit']) {
            $errors['errors'] = [];
            $errors['errors']['MerchantID'] = 'MerchantID don\'t right!';
        };

        // get order data
        if ((string) $payment['card_id'] === $card->card_id && (int) $payment['OrderId'] === $order->id && (string) $payment['Result'] === '00') {
            $paymentResponse['success'] = [];

            foreach ($payment as $key => $value) {
                $paymentResponse['success'][$key] = (string) $value;
            }
        } else {
            $paymentResponse['errors'] = [];
            $paymentResponse['errors']['card'] = 'Card or Order data doesn\'t exist!';
        }

        return $paymentResponse;
    }

    /**
     * parse xml list of users cards
     * @param [string] $response
     * @return [array]
     */
    public function deleteCardXmlParser($response)
    {
        // get config
        $config = $this->config();
        
        $array = new SimpleXMLElement($response);

        if (!$array || !count($array)) {
            return false;
        }

        // get data from request tag
        $arr_request = $array->request->attributes();

        if (!count($arr_request)) {
            return false;
        }

        foreach ($arr_request as $key => $value) {
            $request[$key] = (string)$value;
        }

        // check company merchant_id
        if ($request['id'] !== $config['kazkom_pay_merchant_id']) {
            return false;
        };

        // get client data from Users Cards table
        $user = UsersCards::where('user_id', $request['abonent_id'])->first();

        if (!$user || !count($user)) {
            return false;
        }

        // get data from order tag
        $arr_cards = $array->cards;

        if (!count($arr_cards)) {
            return false;
        }

        // get card data from list of users cards
        // we need only one
        foreach ($arr_cards as $key => $value) {
            $card = $value->item->attributes();
            if ((int) $card->abonent_id === (int) $request['abonent_id'] && (string) $card->status === 'OK') {
                $cardData['cardID'] = (string) $card->CardID;
                $cardData['abonent_id'] = (int) $card->abonent_id;
                break;
            }
        }

        if (isset($cardData) && count($cardData)) {
            return $cardData;
        }

        return false;
    }

    /**
     * parse incoming XML document
     * @return [type] [description]
     */
    public function postResponseXmlParser($response)
    {
        $array = new SimpleXMLElement($response);

        Log::info("Response from KAZKOM added.");

        if (!$array || !count($array)) {
            Log::error("no response from KazkomPayService.");
            return ['status' => false, 'code' => 'no response from KazkomPayService.'];
        }

        // get data from results tag
        $arr_payment = $array->bank->results->payment->attributes();
        foreach ($arr_payment as $key => $value) {
            $payment[$key] = (string)$value;
        }

        // check response_code and
        // company merchant_id
        if ($payment['response_code'] != '00' || $payment['merchant_id'] !== config('payments.kazkom.kazkom_pay_merchant_id')) {
            Log::error("Check response_code or merchant_id failed.");
            return ['status' => false, 'code' => 'Check response_code or merchant_id failed.'];
        };

        // check cert_id
        $cert_id = $array->bank->customer->merchant->attributes();
        if ((string)$cert_id['cert_id'] !== config('payments.kazkom.kazkom_pay_merchant_cert_id')) {
            Log::error("Check cert_id failed.");
            return ['status' => false, 'code' => 'Check cert_id failed.'];
        };

        // get data from order tag
        $arr_order = $array->bank->customer->merchant->order->attributes();
        foreach ($arr_order as $key => $value) {
            $order[$key] = (string)$value;
            // Log::info("# ".$key." : ".(string)$value);
        }

        // get current order data
        $order_data = UsersCards::where('id', (int)$order['order_id'])->first();

        // check order id && order total
        if (!$order_data || !count($order_data)) {
            Log::error("Check order or total amount failed.");
            return ['status' => false, 'code' => 'Check order or total amount failed.'];
        }

        UsersCards::where('id', (int)$order['order_id'])->update([
            'approve' => 1 // 0 - approved, 1 - not approved
        ]);

        // Log::error("Card data added.");

        return ['status' => true];
    }

    /**
     * get payment config data
     * @return [array]
     */
    public function config()
    {
        return config('payments.kazkom');
    }

    /**
     * Load private key
     * @param  [string] $filename
     * @param  [string] $password
     * @return [string]
     */
    public function load_private_key($filename, $password = null)
    {
        if (!is_file($filename)) {
            return false;
            die("Key not found");
        }

        $c = file_get_contents($filename);

        if ($password) {
            $prvkey = openssl_get_privatekey($c, $password) or die(openssl_error_string());
        } else {
            $prvkey = openssl_get_privatekey($c) or die(openssl_error_string());
        }

        if (is_resource($prvkey)) {
            $this->private_key = $prvkey;
            return $c;
        }

        return false;
    }

    /**
     * Inverse string
     */
    public function invert()
    {
        $this->invert = 1;
    }

    /**
     * Reverse string
     * @param  [string] $str
     * @return [string]
     */
    public function reverse($str)
    {
        return strrev($str);
    }
    
    /**
     * Sign the string with private key
     * @param  [string] $str
     * @return [string]
     */
    public function sign($str)
    {
        if ($this->private_key) {
            openssl_sign($str, $out, $this->private_key);

            if ($this->invert == 1) {
                $out = $this->reverse($out);
            }

            return $out;
        }
    }

    /**
     * Sign the string with private key and encode with base64
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function sign64($str)
    {
        return base64_encode($this->sign($str));
    }

    /**
     * Check string with public key if it's signed with private key
     * @param  [string] $data
     * @param  [string] $str
     * @param  [string] $filename
     * @return [numeric] 1, 0 or -1
     */
    public function check_sign($data, $str, $filename)
    {
        if ($this->invert == 1) {
            $str = $this->reverse($str);
        }

        if (!is_file($filename)) {
            return false;
        }

        $pubkey = file_get_contents($filename);

        return openssl_verify($data, $str, $pubkey);
    }

    /**
     * Check string with public key $file if string $str in Base64 is signed with private key $data
     * @param  [string] $data
     * @param  [string] $str
     * @param  [string] $filename
     * @return [numeric] 1, 0 or -1
     */
    public function check_sign64($data, $str, $filename)
    {
        return $this->check_sign($data, base64_decode($str), $filename);
    }
}

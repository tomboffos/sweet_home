<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Workers;
use App\Models\WorkersCategories;
use App\User;
use App\Models\OrdersWorkers;
use App\Models\Settings;
use App\Models\Discounts;
use App\Models\FaqCategories;
use App\Models\UsedDiscounts;
use App;
use Log;
use Validator;
use Auth;
use Mail;
use Event;
use App\Events\PointsLogEvent;
use App\Events\OrderAdded;
use App\Events\ClientRegistered;

class WorkersController extends CommonController
{
    /**
     * render workers index page
     * @return [view]
     */
    public function index()
    {
        $data = WorkersCategories::where('status', 1)->get();

        return view('front.workers.index', [
            'meta' => $this->meta(null),
            'data' => $data
        ]);
    }

    /**
     * render item page
     * @return [view]
     */
    public function item(Request $request, $id, $slug)
    {
        $query = WorkersCategories::where('status', 1);

        $textColumn = 'text_' . App::getLocale();
        // get all categories
        $categories = $query->select('id', 'slug', 'title', 'meta_title', 'meta_description', $textColumn, 'type')->get();

        // get current category
        $data = $query->where('id', $id)
            ->where('slug', $slug)
            ->firstOrFail();

        // get user data
        if (!$user = Auth::user()) {
            $user = (object) [
                'name' => '',
                'phone' => '',
                'email' => '',
                'street' => '',
                'complex' => '',
                'housing' => '',
                'house' => '',
                'flat' => '',
            ];
        }

        $faq = $this->faq($slug);

        return view('front.workers.item_' . (int)$data->type, [
            'data' => $data,
            'user' => $user,
            'categories' => $categories,
            'faq' => $faq['faq'],
            'faq_category' => $faq['slug'],
            'meta' => $this->meta($data),
        ]);
    }

    /**
     * order on workers order page
     * @param Request $request [form _POST data]
     * @return [obj]
     */
    public function order(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax()) {
            return response(['status' => 400, 'msg' => trans('messages.error_while_send_form')]);
        }

        $v = Validator::make($request->all(), [
            'id' => 'required|min:1|numeric',
            'type' => 'required|min:0|numeric',
            'text' => 'max:5000',
            'requirements' => 'required|max:2000',
            'schedule' => 'required|max:255',
            // 'wage' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'email|max:255',
            'phone' => 'required|max:18',
        ]);

        if ($v->fails()) {
            $error = trans('messages.fills_empty');

            foreach ($v->errors()->toArray() as $key => $value) {
                $error .= $value[0]."<br>";
            }

            return response(['status' => 400, 'msg' => $error]);
        }

        // check email of authenticated user.
        // if this is new email then user must change it in user page
        if (Auth::user() && Auth::user()->email !== $request->email) {
            return response(['status' => 400, 'msg' => trans('messages.email_must_change_on_user_page')]);
        }

        // update client data if need
        if ($user = Auth::user()) {
            // upade user data
            User::where('id', $user->id)->update([
                'name' => $request->name,
                'phone' => $request->phone,
                // 'street' => $request->street,
                // 'complex' => $request->complex,
                // 'housing' => $request->housing,
                // 'house' => $request->house,
                // 'flat' => $request->flat,
            ]);
        }

        // check email of guest user.
        // if email is not free to use, lets return message to auth on service
        // if email is free to use, lets register new user and new add order
        if (!Auth::user() && !empty($request->email)) {
            $user = User::where('email', $request->email)->count();

            // if email exist
            if ($user) {
                return response(['status' => 400, 'msg' => trans('messages.go_auth_msg')]);
            }

            // if email free to use register new user
            $login = new LoginController();
            $password = $login->registerUser($request->toArray());

            if (!$password || empty($password)) {
                return response(['status' => 400, 'msg' => trans('messages.error_insert_bd')]);
            }
        }

        // add new order
        if ($data = $this->addNewOrder($request)) {
            $event = [
                'order_id' => $data->order->id,
                'order_total' => $data->order->total,
                'client_name' => (isset($data->user->name) ? $data->user->name : null),
                'client_email' => (isset($data->user->email) ? $data->user->email : null),
                'view' => 'workers',
                'subject' => 'subject_order_workers',
            ];

            // send email to admin and client
            Event::fire(new OrderAdded((object) $event));

            // send password to new registered client
            if (isset($password)) {
                $event['password'] = $password;
                Event::fire(new ClientRegistered((object) $event));
            }

            return response([
                'data' => view()->make('front.workers.finish', [
                    'order' => $data->order,
                    'new_user' => (isset($password) ? true : false),
                    'msg' => (isset($password) ? trans('messages.password_send_to_your_email') : false),
                ])->render()
            ]);
        }

        return response(['status' => 400, 'msg' => trans('error_insert_bd')]);
    }

    /**
     * add new order
     * @param [obj] $request
     * @return [boolean]
     */
    public function addNewOrder($request)
    {
        $user = (!Auth::guest() ? Auth::user() : null);

        // prepare order data
        $sql = [
            'client_id' => (isset($user->id) ? $user->id : null),
            'guest' => (!isset($user->id) ? json_encode(['name' => $request->name, 'phone' => $request->phone]) : null),
            'worker_id' => $request->id,
            'profession_id' => $request->id,
            'requirements' => $request->requirements,
            'text' => $request->text,
            'schedule' => $request->schedule,
            'type' => $request->type,
            'discount_id' => null,
            'points' => null,
        ];

        // only one sale type can use
        if (isset($request->discount) && !empty($request->discount)) {
            // get discount
            $discount = Discounts::select('id', 'used')
                ->where('code', $request->discount)
                ->where('status', 1)
                ->where('category', 1)
                ->where('finish', '>=', date("Y-m-d H:i:s"))
                ->first();

            if ($discount) {
                // if used === 1, discount can use only one time
                if ((int) $discount->used === 1) {
                    // check if user was uses this discount
                    $is_discount_used = UsedDiscounts::where('user_id', $user->id)
                        ->where('discount', $request->discount)
                        ->where('type', 1)
                        ->first();

                    if (!$is_discount_used) {
                        $sql['discount_id'] = $discount->id;
                    }
                } else {
                    $sql['discount_id'] = $discount->id;
                }
            }
        } elseif (!is_null($user)
            && isset($request->points)
            && !empty($request->points)
            && $user->points >= $request->points) {
                $sql['points'] = $request->points;
        }

        $order = OrdersWorkers::create($sql);

        if ($order) {
            // add discound code to used_discounts table
            if (!is_null($sql['discount_id'])) {
                UsedDiscounts::create([
                    'order_id' => $order->id,
                    'user_id' => $user->id,
                    'discount' => $request->discount,
                    'type' => 1,
                ]);
            }

            // add points to log
            if (!is_null($user) && isset($sql['points']) && !is_null($sql['points'])) {
                User::where('id', $user->id)->update([
                    'points' => $user->points - $sql['points']
                ]);
                Event::fire(new PointsLogEvent($user->id, trans('points.sale_workers_order').$order->id, $request->points));
            }

            return (object) ['order' => $order, 'user' => $user];
        }

        return false;
    }

    /**
     * check email in order page
     * @param Request $request
     * @return [response]
     */
    public function checkEmail(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax()) {
            return response(['status' => 400, 'msg' => trans('messages.something_went_wrong')]);
        }

        $v = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($v->fails()) {
            return response(['status' => 400, 'msg' => trans('messages.fills_empty')]);
        }

        // if email === authenticated users email do nothing
        if (Auth::user() && Auth::user()->email === $request->email) {
            return response(['status' => 200]);
        }

        // if email !== authenticated users email then user must change email on user page
        if (Auth::user() && Auth::user()->email !== $request->email) {
            return response(['status' => 400, 'msg' => trans('messages.change_email_on_user_page')]);
        }

        $email = User::where('email', $request->email)->first();

        // view auth form
        if (count($email)) {
            return response(['status' => 300, 'msg' => trans('messages.go_auth_msg')]);
        }

        return response(['status' => 200]);
    }

    /**
     * check if Discount code exist and if's valid
     * @param  Request $request [_POST data]
     * @return [response]
     */
    public function checkDiscount(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax()) {
            return response(['status' => 400, 'msg' => trans('messages.error_while_send_form')]);
        }

        $v = Validator::make($request->all(), [
            'discount' => 'min:1|string',
        ]);

        if ($v->fails()) {
            $error = trans('messages.fills_empty');

            foreach ($v->errors()->toArray() as $key => $value) {
                $error .= $value[0]."<br>";
            }

            return response(['status' => 400, 'msg' => $error]);
        }

        // get discount
        $discount = Discounts::select('id', 'used')
            ->where('code', $request->discount)
            ->where('status', 1)
            ->where('category', 1)
            ->where('finish', '>=', date("Y-m-d H:i:s"))
            ->first();

        if (!$discount) {
            return response(['status' => 400, 'msg' => trans('messages.discount_is_not_valid')]);
        }

        // check if user was used this discount
        if (!Auth::guest()) {
            // if used === 1, discount can use only one time
            if ((int) $discount->used === 1) {
                $is_discount_used = UsedDiscounts::where('user_id', Auth::user()->id)
                    ->where('discount', $request->discount)
                    ->where('type', 1)
                    ->first();

                if ($is_discount_used) {
                    return response(['status' => 400, 'msg' => trans('messages.discount_was_used')]);
                }
            }
        }

        return response(['status' => 200, 'msg' => trans('messages.discount_is_valid')]);
    }

    /**
     * check if Points > 0
     * @param Request $request [_POST data]
     * @return [response]
     */
    public function checkPoints(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax() || !Auth::user()) {
            return response(['status' => 400, 'msg' => trans('messages.error_while_send_form')]);
        }

        if ($request->points <= Auth::user()->points && $request->points > 0) {
            return response(['status' => 200, 'msg' => trans('messages.points_is_valid')]);
        } else {
            return response(['status' => 400, 'msg' => trans('messages.points_is_not_valid')]);
        }
    }

    /**
     * get faq items
     * @return [obj]
     */
    public function faq($slug)
    {
        $category = FaqCategories::where('category', $slug)->where('status', 1)->first();
        if ($category && count($category)) {
            $faq = $category->faq()
                ->select('id', 'category', 'title', 'slug')
                ->where('status', 1)
                ->orderby('sort', 'asc')->take(5)
                ->get();
        }

        if (!isset($faq) || !$faq || !count($faq)) {
            $category = FaqCategories::where('category', 'servis-v-celom')->where('status', 1)->first();
            if ($category ) {
                $faq = $category->faq()
                    ->select('id', 'category', 'title', 'slug')
                    ->where('status', 1)
                    ->orderby('sort', 'asc')->take(5)
                    ->get()->toArray();
            }

            if (!isset($faq) || !$faq || !count($faq)) {
                $faq = false;
            } else {
                foreach ($faq as $k => $item) {
                    $faq[$k]['common'] = (object) true;
                    $faq[$k] = (object) $faq[$k];
                }

                $faq = collect($faq);
            }
        }

        return ['faq' => $faq, 'slug' => $slug];
    }
}

<?php

namespace App\Http\Controllers\Front;

use App\DiscountSecond;
use App\Models\Discounts;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\User;
use Password;
use Event;
use App\Events\UserRegistered;

/*
* Auth
*/

class LoginController extends CommonController
{

    use ResetsPasswords;

    /**
     * auth page
     * @return [view]
     */
    public function login()
    {
        return view('auth.login', [
            'meta' => $this->meta(null),
        ]);
    }

    /**
     * register page
     * @return [view]
     */
    public function register()
    {
        return view('auth.register', [
            'meta' => $this->meta(null),
        ]);
    }

    // reset password
    public function reset(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }

        $meta = $this->meta(null);
        
        if (!$meta) {
            $meta = json_decode(json_encode([
                "label" => "Авторизация",
                "title" => "Авторизация",
                "meta_title" => "",
                "meta_description" => "",
                "meta_keywords" => ""
            ]));
        }

        $email = $request->input('email');

        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'email', 'meta'));
        }

        if (view()->exists('auth.passwords.reset')) {
            return view('auth.passwords.reset')->with(compact('token', 'email', 'meta'));
        }

        return view('auth.reset')->with(compact('token', 'email', 'meta'));
    }

    public function getEmail()
    {
        $meta = $this->meta(null);
        
        if (!$meta) {
            $meta = json_decode(json_encode([
                "label" => "Авторизация",
                "title" => "Авторизация",
                "meta_title" => "",
                "meta_description" => "",
                "meta_keywords" => ""
            ]));
        }

        if (property_exists($this, 'linkRequestView')) {
            return view($this->linkRequestView)->with(compact('meta'));
        }

        if (view()->exists('auth.passwords.email')) {
            return view('auth.passwords.email')->with(compact('meta'));
        }

        return view('auth.password')->with(compact('meta'));
    }

    // ajax auth
    public function loginAjax(Request $request)
    {
        if (!$request->ajax()) {
            return response()->json(['status' => 400, 'success' => false]);
        }

        $v = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);

        if ($v->fails()) {
            return response()->json(['status' => 400, 'success' => false, 'errors' => $v->errors()]);
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], true)) {
            return response()->json(['status' => 200, 'success' => true]);
        }
    }

    // ajax auth in order page
    public function loginOrderAjax(Request $request)
    {
        if (!$request->ajax()) {
            return response()->json(['status' => 400, 'success' => false]);
        }

        $v = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);

        if ($v->fails()) {
            return response()->json(['status' => 400, 'success' => false, 'errors' => $v->errors()]);
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], true)) {
            $user = Auth::user();

            $data = (object) [
                'name' => $user->name,
                'phone' => $user->phone,
                'street' => $user->street,
                'complex' => $user->complex,
                'housing' => $user->housing,
                'house' => $user->house,
                'flat' => $user->flat,
            ];

            return response()->json(['status' => 200, 'success' => true, 'data' => $data]);
        }
    }

    // ajax register
    public function registerAjax(Request $request)
    {
        if (!$request->ajax()) {
            return response()->json(['status' => 400, 'success' => false]);
        }

        $v = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($v->fails()) {
            return response()->json(['status' => 400, 'success' => false, 'errors' => $v->errors()]);
        }

        // register new user and authenticate it
        if ($this->registerUser(['email' => $request->email, 'password' => $request->password])) {
            return response()->json(['status' => 200, 'success' => true]);
        }

        return response()->json(['status' => 400, 'success' => false]);
    }

    // ajax reset password
    public function resetAjax(Request $request)
    {
        if (!$request->ajax()) {
            return response()->json(['status' => 400, 'success' => false]);
        }

        $v = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);

        if ($v->fails()) {
            return response()->json(['status' => 400, 'success' => false, 'errors' => $v->errors()]);
        }

        $response = Password::sendResetLink($request->only('email'), function ($m) {
            $m->subject('Напоминание пароля');
        });

        if ($response === 'passwords.sent') {
            return response()->json([
                'status' => 200,
                'success'=> true,
                'msg' => 'На вашу почту было отправлено письмо с инструкциями.'
            ]);
        }

        return response()->json([
            'status' => 400,
            'success'=> 'none',
            'msg' => "Ошибка при отправке. Пожалуйста, свяжитесь с администрацией."
        ]);
    }

    /**
     * register new user and authenticate it
     * @param  [obj] $request
     * @return [obj]
     */
    public function registerUser($data)
    {
        // generate random password if not exist
        $password = isset($data['password']) & !empty($data['password']) ? $data['password'] : str_random(6);
        $data['password'] = bcrypt($password);

        $create = User::create($data);

        // add user role
        if ($create) {
            $user = User::find($create->id);
            $user->roles()->attach(1); // 1 - user, 2 - admin
            if (!$user['ref']){
                $referal = session()->get('referal');

                $user['ref'] = preg_replace("/[^0-9]/", "", $referal );
                $user->save();
            }
            Auth::attempt(['email' => $data['email'], 'password' => $password]);

            // check for referal



            $discountSetting = DiscountSecond::where('type',1)->first();
            $discount  = new Discounts();
            $discount['code'] = $user['id'];
            if ($user->name){
                $nameWithOutSpace = str_replace(' ', '', $user->name);
                $discount['code'] = \Illuminate\Support\Str::upper(cyrillic_to_latin($nameWithOutSpace)) . $user->id;

            }
            $discount['type'] = 1;
            $discount['user_id'] = $user['id'];
            $discount['category'] = 0;
            $discount['price'] =$discountSetting->amount;
            $discount['status'] = 1;
            $discount['finish'] = '2100-01-01';
            $discount['used'] = 0;
            $discount->save();


            return $password;
//            Event::fire(new UserRegistered($user));
        }

        return false;
    }
}

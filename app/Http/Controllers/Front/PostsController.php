<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Posts;
use App\Models\WorkersCategories;
use Auth;
use App\Http\Controllers\Front\CleaningController;

class PostsController extends CommonController
{
    /**
     * handle requests
     * @param object $request
     * @return mix
     */
    public function handle(Request $request)
    {
        // List af all parent posts
        if (empty($request->category)) {
            return $this->categories($request);
        }

        // get first category
        if (empty($request->subCategory)) {
            return $this->category($request->category);
        }

        // get second category
        if (empty($request->sub2Category)) {
            return $this->category($request->subCategory);
        }

        // get third category
        if (empty($request->sub3Category)) {
            return $this->category($request->sub2Category);
        }

        return abort(404);
    }

    /**
     * render category list
     * @param object $request
     * @return mix
     */
    public function categories($request)
    {
        $data = Posts::where('status', 1)->orderby('id', 'asc')->get();

        if(!count($data)) {
            return abort(404);
        }

        $meta = $this->meta(null);

        if(isset($request->page)) {
            $meta->meta_title = 'Страница ' . (int) $request->page . ' – ' . $meta->meta_title;
        }

        $categories = $this->_getPostsCategories();
        $items = [];

        foreach($data as $item) {
            if(!empty($item->category) && isset($categories[$item->category])) {
                $items[$item->category]['parent'] = [
                    'title' => $categories[$item->category],
                    'slug' => $item->category,
                    'parent' => $item->parent,
                ];
                $items[$item->category]['items'][$item->id] = $item->toArray();
            }
        }

        if(count($items)) {
            foreach($items as $n => $item) {
                if(isset($item['items']) && count($item['items'])) {
                    foreach($item['items'] as $k => $child) {
                        if((int) $child['parent'] !== 0 && isset($item['items'][$child['parent']])) {
                            $items[$n]['items'][$child['parent']]['items'][] = $child;
                            unset($items[$n]['items'][$k]);
                        }
                    }
                }
            }
        }

        return view('front.posts.categories', compact('items', 'meta'));
    }

    /**
     * render category
     * @param string $category
     * @return mix
     */
    public function category($url)
    {
        if (empty($url)) {
            return abort(404);
        }

        $post = Posts::where('slug', $url)
            ->where('status', 1)
            ->firstOrFail();

        $meta = $this->meta($post);
        $breadcrumbs = $this->_breadcrumbs($post);

        // get order form
        $workers = WorkersCategories::all();
        $form = $this->_getPostsForm($post, $workers);

        // get current post type (cleaning or workers)
        $itemType = !empty($post->category) ? explode('/', $post->category)[0] : '';

        // get workres form data
        if($itemType === 'workers' && !empty($form) && count($workers)) {
            foreach($workers as $worker) {
                if("workers/{$worker->id}-{$worker->slug}" === $post->category) {
                    $workerForm = $this->_getWorkerForm($worker->id, $worker->slug);
                    $user = $workerForm['user'];
                    $data = $workerForm['data'];
                }
            }

            return view('front.posts.category', compact('post', 'data', 'meta', 'breadcrumbs', 'form', 'user'));
        }

        // get cleaning form data
        if($itemType === 'cleaning') {
            $cleaningForm = $this->_getCleaningForm();
            $user = $cleaningForm['user'];
            $calculator = $cleaningForm['calculator'];
            $schedule = $cleaningForm['schedule'];
            $cleaning_time_border = $cleaningForm['cleaning_time_border']->value;
            $js = $cleaningForm['settings'];
            $faq = $cleaningForm['faq'];
            $faq_category = $cleaningForm['faq_category'];

            return view('front.posts.cleaning', compact('post', 'meta', 'breadcrumbs', 'form', 'user', 'calculator', 'schedule', 'cleaning_time_border', 'js'));
        }

        return view('front.posts.category', compact('post', 'meta', 'breadcrumbs'));
    }

    /**
     * prepare breadcrumbs
     * @param  object $category
     * @return array
     */
    private function _breadcrumbs($category)
    {
        $breadcrumbs = [
            [
                'title' => 'Главная',
                'url' => url('/'),
            ],
            [
                'title' => 'Услуги',
                'url' => url('uslugi'),
            ],
        ];

        if((int) $category->parent === 0) {

            $postsCategories = $this->_getPostsCategories();

            if(isset($postsCategories[$category->category])) {
                $breadcrumbs[] = [
                    'title' => $postsCategories[$category->category],
                    'url' => url("{$category->category}"),
                ];
            }

            $breadcrumbs[] = [
                'title' => $category->title,
                'url' => '',
            ];
            return $breadcrumbs;
        }

        $postsCategories = $this->_getPostsCategories();

        if(isset($postsCategories[$category->category])) {
            $breadcrumbs[] = [
                'title' => $postsCategories[$category->category],
                'url' => url("{$category->category}"),
            ];
        }

        $categories = [];
        $categories = $this->_categoriesTree($category, $categories);
        $categories = array_reverse($categories);

        foreach($categories as $item) {
            $breadcrumbs[] = [
                'title' => $item->title,
                'url' => url("/uslugi/{$item->slug}"),
            ];
        }

        $breadcrumbs[] = [
            'title' => $category->title,
            'url' => '',
        ];

        return $breadcrumbs;
    }

    /**
     * get categories tree
     * @param  object $category
     * @param  array $categories
     * @return array
     */
    private function _categoriesTree($category, &$categories)
    {
        while((int) $category->parent !== 0) {
            $category = Posts::where('id', $category->parent)
                ->where('status', 1)
                ->first();

            if(count($category)) {
                $categories[] = $category;
                return $this->_categoriesTree($category, $categories);
            }
        }

        return $categories;
    }

    /**
     * Get Posts Categories
     * @return array
     */
    private function _getPostsCategories()
    {
        $categories = [
            'cleaning' => 'Уборка',
            'cleaning/general' => 'Генеральная уборка',
        ];

        $workers = WorkersCategories::all();
        
        if(count($workers)) {
            foreach($workers as $worker) {
                $categories["workers/{$worker->id}-{$worker->slug}"] = $worker->title;
            }
        }

        return $categories;
    }

    /**
     * Get Posts Form
     * @return array
     */
    private function _getPostsForm($data, $workers)
    {
        $categories = [
            'cleaning' => 'front.cleaning._cleaning',
            'cleaning/general' => 'front.cleaning._cleaning_general',
        ];

        $workers = WorkersCategories::all();
        
        if(count($workers)) {
            foreach($workers as $worker) {
                $categories["workers/{$worker->id}-{$worker->slug}"] = "front.workers._{$worker->slug}";
            }
        }

        return isset($categories[$data->category]) ? $categories[$data->category] : '';
    }

    /**
     * Get workers form data
     */
    private function _getWorkerForm($id, $slug)
    {
        $data = WorkersCategories::where('status', 1)
            ->where('id', $id)
            ->where('slug', $slug)
            ->first();

        if (!count($data)) {
            return [];
        }

        // get user data
        if (!$user = Auth::user()) {
            $user = (object) [
                'name' => '',
                'phone' => '',
                'email' => '',
                'street' => '',
                'complex' => '',
                'housing' => '',
                'house' => '',
                'flat' => '',
            ];
        }

        return [
            'data' => $data,
            'user' => $user,
        ];
    }

    /**
     * Get Cleaming form data
     */
    private function _getCleaningForm()
    {
        $cleaning = new CleaningController();
        return $cleaning->getSimpleCalculatorFormData();
    }
}

<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\Models\Settings;
use App\Models\FaqCategories;
use App\Models\Discounts;
use App\Models\OrdersCleaners;
use App\Models\UsersCards;
use App\Models\Cleaners;
use App\Models\UsedDiscounts;
use App\User;
use App\Events\PointsLogEvent;
use App\Events\OrderAdded;
use App\Events\ClientRegistered;
use Event;
use Auth;
use Validator;

class CleaningController extends CommonController
{
    /**
     * render cleaning index page
     * @return [view]
     */
    public function index()
    {
        $formData = $this->getSimpleCalculatorFormData();
        $discount = App\DiscountSecond::where('type',1)->first();
        $parentName = 0;
        if(Auth::user())
            $parentName = User::where('id',$formData['user']['ref'])->select('name')->first();

        return view('front.cleaning.index', [
            'meta' => $this->meta(null),
            'faq' => $formData['faq'],
            'faq_category' => $formData['faq_category'],
            'user' => $formData['user'],
            'calculator' => $formData['calculator'],
            'schedule' => $formData['schedule'],
            'cleaning_time_border' => $formData['cleaning_time_border']->value,
            'js' => $formData['settings'],
            'discount' => $discount,
            'parentName' => $parentName
        ]);
    }

    /**
     * do order
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function order(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax()) {
            return response(['status' => 400, 'msg' => trans('messages.something_went_wrong')]);
        }

        $v = Validator::make($request->all(), [
            'name' => 'required|string|min:1|max:255',
            'email' => 'required|email',
            'phone' => 'required|string|min:1|max:255',
            'pay' => 'required|numeric|min:0|max:1',
            'type' => 'required',
            'cicle' => 'required',
            'date' => 'required',
            'time' => 'required',
        ]);

        if ($v->fails()) {
            $error = trans('messages.fills_empty');

            foreach ($v->errors()->toArray() as $key => $value) {
                $error .= $value[0]."<br>";
            }

            return response(['status' => 400, 'msg' => $error]);
        }

        // check date
        if (strtotime(date('d-m-Y', strtotime($request->date))) < strtotime(date('d-m-Y', strtotime('+2 days')))) {
            return response(['status' => 400, 'msg' => 'Возможно вы не указали дату или на эту дату все клинеры заняты, пожалуйста, укажите другую дату!']);
        }

        // check email of authenticated user.
        // if this is new email then user must change it in user page
        if (Auth::user() && Auth::user()->email !== $request->email) {
            return response(['status' => 400, 'msg' => trans('messages.email_must_change_on_user_page')]);
        }

        // update client data if need
        if ($user = Auth::user()) {
            // upade user data
            User::where('id', $user->id)->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'street' => $request->street,
                'complex' => $request->complex,
                'housing' => $request->housing,
                'house' => $request->house,
                'flat' => $request->flat,
            ]);
            if ($request->discount && $user->ref == null){

                $discount = Discounts::where('code',$request->discount)->first();
                $user->ref = $discount['user_id'];
                $user->save();
            }

            $discount = Discounts::where('user_id',$user['id'])->first();
            if ($discount) {
                    $nameWithOutSpace = str_replace(' ', '', $request->name);
                $discount['code'] = \Illuminate\Support\Str::upper(cyrillic_to_latin($nameWithOutSpace)) . $user->id;
                $discount->save();
            }

        }

        // register user
        if (!Auth::user()) {
            $user = User::where('email', $request->email)->count();

            if ($user) {
                return response(['status' => 400, 'msg' => trans('messages.go_auth_msg')]);
            }



            // register user
            $login = new LoginController();
            if (session()->has('referal')){
                $referal = session()->get('referal');

                $request['ref'] = preg_replace("/[^0-9]/", "", $referal );

            }
         
            $password = $login->registerUser($request->toArray());

            if (!$password || empty($password)) {
                return response(['status' => 400, 'msg' => trans('messages.error_insert_bd')]);
            }
        }

        // get all calculator settings
        $field = 'calc_%';
        $substr = 5;

        if (isset($request->label) && $request->label === 'general') {
            $field = 'calc_general_%';
            $substr = 13;
        }

        // get calculator settings
        $settings = $this->calculatorSettings($field, $substr);

        // get total summary
        $summary = $this->getSummary($request, $settings);
        $old_price = $summary;
        if (!$summary) {
            return response(['status' => 400, 'msg' => trans('messages.calculator_total_summ_error')]);
        }

        // init discount or points
        $summary = $this->getSummaryWithDiscountOrPoints($request, $summary);


        // add new order

        if ($data = $this->addNewOrder($request, $summary, $settings)) {
            $user = Auth::user();

            $event = [
                'order_id' => $data->id,
                'order_total' => $data->total,
                'client_name' => $user->name,
                'old_price' => $old_price,
                'client_email' => $user->email,
                'user_id' => $user->id,
                'view' => 'cleaning',
                'subject' => 'subject_order_cleaning',
                'discount_code' => $request->has('discount') ? $request->discount : null,

            ];

            // send email to admin and client
            Event::fire(new OrderAdded((object) $event));

            // send password to user if it's just registered
            if (isset($password)) {
                $event['password'] = $password;
                Event::fire(new ClientRegistered((object) $event));
            }

            // payment services init;
            // $request->pay: 0 - cash;
            // if card doesn't added, render button to add card
            if ($request->pay > 0) {
                $UsersCards = UsersCards::where('user_id', $user->id)->first();

                if (!$UsersCards || !count($UsersCards) || empty($UsersCards->card_id) || empty($UsersCards->card_mask)) {
                    $payment = new PaymentController();

                    // create node in User Cards table
                    $userCardsOrder = UsersCards::create([
                        'user_id' => $user->id,
                        'approve' => 1
                    ]);

                    // create form to add new card
                    if ($userCardsOrder) {
                        $paymentData = $payment->make($userCardsOrder, $user, $request);

                        $payment = null;
                        unset($payment);
                    }
                }
            }
            session()->forget('referal');
            session()->save();
            return response([
                'data' => view()->make('front.cleaning.finish', [
                    'order' => $data,
                    'user' => $user,
                    'new_user' => (isset($password) ? true : false),
                    'payment' => (isset($paymentData) ? $paymentData : false),
                    'msg' => (isset($password) ? trans('messages.password_send_to_your_email') : false),
                ])->render()
            ]);
        }

        return response(['status' => 400, 'msg' => trans('error_insert_bd')]);
    }


    /**
     * add new order
     * @param [type] $request [description]
     */
    public function addNewOrder($request, $summary, $settings)
    {
        if (!($user = Auth::user())) {
            return false;
        }

        $elapsed = explode(':', $request->elapsed);

        if (count($elapsed) < 2) {
            return false;
        }
        $discount = null;
        if ($request->has('discount'))
            $discount = Discounts::where('code',$request->discount)->first();
        $sql = [
            'client_id' => $user->id,
            'number_rooms' => $request->number_rooms,
            'number_bathrooms' => $request->number_bath_rooms,
            'number_window' => isset($request->number_window) ? $request->number_window : null,
            'cl_date' => date("Y-m-d", strtotime($request->date)),
            'cl_time' => $request->time,
            'elapsed' => implode(':', $elapsed),
            'payment_type' => $request->pay,
            'discount_id' => $discount != null ? $discount->id : null,
            'points' => null,
            'comment' => isset($request->comment) && !empty($request->comment) ? $request->comment : null,
            'total' => round($summary['summary']),
            'additional' => null,
            'services' => null,
            'label' => isset($request->label) && $request->label === 'general' ? 'general' : 'standart',
        ];

        // prepare calculator settings to add in view
        $calculator = $this->prepareCalculatorSettingsForView($settings);

        // add free cleaner if need
        if (round($summary['summary']) - (int) $calculator['additional_cleaner'] > (int) $calculator['additional_cleaner_border']) {
            $sql['additional'] = json_encode(['free_cleaner' => ['count' => 1, 'price' => 0]]);
        }

        // services
        if (isset($request->service) && count($request->service)) {
            foreach ($request->service as $key => $service) {
                foreach ($calculator['services'] as $k => $value) {
                    if ($key === $value['id']) {
                        $sql['services'][] = [
                            'id' => $value['id'],
                            'name' => $value['name'],
                            'price' => $value['price'],
                            'pay' => $value['pay'],
                            'time' => $value['time'],
                            'unit' => $service['unit'],
                        ];
                    }
                }
            }

            // additional cleaner
            if (isset($request->service['additional_cleaner'])) {
                $additonal = !empty($sql['additional']) ? json_decode($sql['additional'], true) : [];
                $additonal['paid_cleaner'] = [
                    'count' => 1,
                    'price' => 1000,
                ];

                $sql['additional'] = json_encode($additonal);
            }

            if ($sql['services']){
                if (count($sql['services'])) {
                    $sql['services'] = json_encode($sql['services']);
                }
            }
        }

        // coefficient of type; rotate bellow all available types
        if ($request->type && !empty($request->type)) {
            foreach ($calculator['type'] as $k => $type) {
                if ($type['label'] === $request->type) {
                    $sql['type'] = json_encode([
                        'id' => $type['id'],
                        'name' => $type['name'],
                        'label' => $type['label'],
                        'ratio' => $type['ratio'],
                    ]);
                }
            }
        }

        // coefficient of cicle; rotate bellow all available cicles
        if ($request->cicle && !empty($request->cicle)) {
            foreach ($calculator['cicle'] as $k => $cicle) {
                if ($cicle['id'] === $request->cicle) {
                     $sql['cicle'] = json_encode([
                        'id' => $cicle['id'],
                        'name' => $cicle['name'],
                        'ratio' => $cicle['ratio'],
                     ]);
                }
            }
        }

        // only one sale type can use discount or points
        if (isset($request->discount) && !empty($request->discount)) {
            $sql['discount_id'] = (isset($summary['sale']['type']) && $summary['sale']['type'] === 'discount' ? $summary['sale']['value'] : null);
        } elseif (isset($request->points) && !empty($request->points) && $user->points >= $request->points) {
            $sql['points'] = (isset($summary['sale']['type']) && $summary['sale']['type'] === 'points' ? $summary['sale']['value'] : null);
        }
        if ($request->discount) {
            $sql['discount_id'] = Discounts::where('code', $request->discount)->first();

            $usedDiscount = UsedDiscounts::where('user_id',$user->id)->where('discount',$request->discount)->first();
            if (!$usedDiscount){
                $sql['discount_id'] = $sql['discount_id']['id'];
            }else{
                $sql['discount_id'] = null;
            }


        }
        // create new order
        $order = OrdersCleaners::create($sql);
        $order = OrdersCleaners::find($order->id);


        if (!is_null($order)) {

            // add discound code to used_discounts table
            if (!is_null($sql['discount_id'])) {
                $disc = UsedDiscounts::create([
                    'order_id' => $order->id,
                    'user_id' => $user->id,
                    'discount' => $request->discount,
                    'type' => 0,
                ]);

            }

            // add points to log
            if (isset($sql['points']) && !is_null($sql['points'])) {
                User::where('id', $user->id)->update([
                    'points' => $user->points-$sql['points']
                ]);
                Event::fire(new PointsLogEvent($user->id, trans('points.sale_cleaning_order').$order->id, $request->points));
            }

            return $order;
        }

        return false;
    }

    /**
     * pay order from user account
     * @return [view]
     */
    public function payFromUserAccount($id)
    {
        // current client info
        $user = Auth::user();

        // get order
        $data = OrdersCleaners::where('id', $id)
            ->where('client_id', $user->id)
            ->where('payment_type', '!=', 0)
            ->where('payment_status', 0)
            ->first();

        if (!$data || !count($data)) {
            return redirect()->back();
        }

        $request = (object) [
            'email' => $user->email,
            'pay' => $data->payment_type,
        ];

        $payment = new PaymentController();
        $paymentData = $payment->make($request, $data);
        unset($payment);

        return view('front.user.orders.cleaning.pay', [
            'data' => view()->make('front.cleaning.finish', [
                'order' => $data,
                'payment' => (isset($paymentData) ? $paymentData : false),
            ])->render(),
            'meta' => $this->meta(null),
            'order' => $data,
        ]);
    }

    /**
     * calculate summary
     * @param  [obj] $request [_POST data form order page]
     * @return [int]
     */
    public function getSummary($request, $settings)
    {
        // Стоимость уборки = (базовая стоимость + комнаты*шт + санузлы*шт + доп.услуги*шт или часы)*коэффициент типа*коэффициент частоты

        // prepare calculator settings to add in view
        $calculator = $this->prepareCalculatorSettingsForView($settings);

        $summary = 0;

        // base price
        $summary += $calculator['main']['base_price'];

        // rooms
        if ($request->number_rooms > $calculator['main']['number_room_elevated_price']) {
            $summary += $calculator['main']['one_room_price'] * $calculator['main']['number_room_elevated_price'] + ($request->number_rooms - $calculator['main']['number_room_elevated_price']) * $calculator['main']['one_room_elevated_price'];
        } else {
            $summary += $calculator['main']['one_room_price'] * ($request->number_rooms ? $request->number_rooms : 1);
        }

        // bathrooms
        $summary += $calculator['main']['one_bathroom_price'] * ($request->number_bath_rooms ? $request->number_bath_rooms : 0);

        // windows (general cleaning)
        if (isset($request->number_window) && !empty($request->number_window)) {
            $summary += $calculator['main']['one_window_price'] * (int) $request->number_window;
        }

        // services
        if ($request->service && count($request->service)) {
            foreach ($request->service as $key => $service) {
                foreach ($calculator['services'] as $k => $value) {
                    if ($key === $value['id']) {
                        // common service price
                        if ($value['pay'] === 0) {
                            $summary += $value['price'];
                        } // price per unit
                        elseif ($value['pay'] === 1) {
                            $summary += number_format($value['price']*$service['unit'], 0, '', '');
                        } // pirce per hour
                        elseif ($value['pay'] === 2) {
                            $summary += number_format($value['price']*$service['unit'], 0, '', '');
                        }
                    }
                }
            }

            // additional cleaner
            if (isset($request->service['additional_cleaner'])) {
                $summary = number_format($summary + $calculator['additional_cleaner'], 0, '', '');
            }
        }

        // coefficient of type; rotate bellow all available types
        if ($request->type && !empty($request->type)) {
            foreach ($calculator['type'] as $k => $type) {
                if ($type['label'] === $request->type) {
                    $summary = number_format($summary*$type['ratio'], 0, '', '');
                }
            }
        }

        // coefficient of cicle; rotate bellow all available cicles
        if ($request->cicle && !empty($request->cicle)) {
            foreach ($calculator['cicle'] as $k => $cicle) {
                if ($cicle['id'] === $request->cicle) {
                    $summary = number_format($summary*$cicle['ratio'], 0, '', '');
                }
            }
        }

        return (int) $summary;
    }

    /**
     * add discount or points to summary
     * @param [obj] $request [_POST data]
     * @param [int] $summary
     * @return [int]
     */
    public function getSummaryWithDiscountOrPoints($request, $summary)
    {
        $sale = ['value' => null, 'type' => null];
        $discountMain = App\DiscountSecond::where('type',1)->first();
        $currency = Settings::where('name', 'points_currency')->first();
        if ($request->discount && !empty($request->discount)) {
            // get discount
            $discount = Discounts::select('id', 'price', 'type', 'used')
                ->where('code', $request->discount)
                ->where('status', 1)
                ->where('category', 0)
                ->where('finish', '>=', date("Y-m-d H:i:s"))
                ->first();

            if ($discount) {
                // if used === 1, discount can use only one time


                    // check if user was uses this discount
                    $is_discount_used = UsedDiscounts::where('user_id', Auth::user()->id)
                        ->where('discount', $request->discount)
                        ->where('type', 0)
                        ->first();


                    if (!$is_discount_used) {
                        // percent
                        if ($discount['type'] == 1){
                            $summary -= ($currency->value*$discount->price);

                        }else{
                            $summary -= ($summary*$discount->price);
                        }


                        $sale = ['value' => $discount->id, 'type' => 'discount'];


                    }
                } else {
//                    $summary -= ($currency*$discountMain->amount);
//
//
//                    $sale = ['value' => $discount->id, 'type' => 'discount'];
                }
        } elseif (Auth::user() && $request->points <= Auth::user()->points && $request->points > 0) {


            if (!$currency) {
                $currency = 1;
            } else {
                $currency = $currency->value;
            }

            $summary -= ($currency*$request->points);
            $user = User::find(Auth::user()->id);
            $user['points'] -= $request->points;
            $user->save();
            $sale = ['value' => $request->points, 'type' => 'points'];
        }

        return ['summary' => (int) number_format($summary, 0, '', ''), 'sale' => $sale];
    }


    /**
     * prepare calculator settings to append to js
     * @param  [array] $calculator
     * @return [array]
     */
    public function prepareCalculatorSettingsForJs($calculator)
    {
        foreach ($calculator as $key => $value) {
            $data[$key] = json_encode($value);
        }

        return $data;
    }

    /**
     * prepare calculator settings to add in view
     * @param  [array] $settings
     * @return [array]
     */
    public function prepareCalculatorSettingsForView($settings)
    {
        foreach ($settings as $key => $value) {
            $data[$key] = json_decode($value, true);
        };

        return $data;
    }

    /**
     * calculator settings
     * @param string $field
     * @param int $substr
     * @return array
     */
    public function calculatorSettings($field = 'calc_%', $substr = 5)
    {
        $calculator = Settings::where('name', 'like', $field)
                              ->where('lang', App::getLocale())
                              ->get();

        foreach ($calculator as $key => $value) {
            $data[substr($value['name'], $substr)] = $value['value'];
        }

        return $data;
    }

    /**
     * check email in order page
     * @param Request $request
     * @return [response]
     */
    public function checkEmail(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax()) {
            return response(['status' => 400, 'msg' => trans('messages.something_went_wrong')]);
        }

        $v = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($v->fails()) {
            return response(['status' => 400, 'msg' => trans('messages.fills_empty')]);
        }

        // if email === authenticated users email do nothing
        if (Auth::user() && Auth::user()->email === $request->email) {
            return response(['status' => 200]);
        }

        // if email !== authenticated users email then user must change email on user page
        if (Auth::user() && Auth::user()->email !== $request->email) {
            return response(['status' => 400, 'msg' => trans('messages.change_email_on_user_page')]);
        }

        $email = User::where('email', $request->email)->first();

        // view auth form
        if (count($email)) {
            return response(['status' => 300, 'msg' => trans('messages.go_auth_msg')]);
        }

        return response(['status' => 200]);
    }

    /**
     * check if Discount code exist and if's valid
     * @param  Request $request [_POST data]
     * @return [response]
     */
    public function checkDiscount(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax()) {
            return response(['status' => 400, 'msg' => trans('messages.error_while_send_form')]);
        }

        $v = Validator::make($request->all(), [
            'discount' => 'min:1|string',
        ]);

        if ($v->fails()) {
            $error = trans('messages.fills_empty');

            foreach ($v->errors()->toArray() as $key => $value) {
                $error .= $value[0]."<br>";
            }

            return response(['status' => 400, 'msg' => $error]);
        }

        // get discount
        $discount = App\DiscountSecond::where('type',1)->first();



        $discountCheck = Discounts::where('code',$request['discount'])
            ->where('category',0)
            ->where('finish', '>=', date("Y-m-d H:i:s"))
            ->first();
        $user = Auth::user();
        $usedDiscount = null;
        $exception = 1;

        if ($user){
            $usedDiscount = UsedDiscounts::where('user_id',$user->id)->first();
            $usedDiscounts = UsedDiscounts::where('user_id',$user->id)->get();
            foreach ($usedDiscounts as $used){
                $discountOne = Discounts::where('code',$used['discount'])->first();
                if ($discountCheck['user_id'] != 0){
                    if ($discountOne['user_id'] != 0 || $discountOne['user_id'] != null){
                        $exception = 0;

                        break;
                    }
                }
            }
        }


        if ($exception == 0){
            return  response(['status' => 400,'msg'=>'Реферальный промокод уже использован вами']);
        }




        if(!$discountCheck){
            return response(['status' => 400, 'msg' => 'Такого кода нет']);
        }

        if ($user && $discountCheck['user_id'] == $user['id'])
            return response(['status' => 400 , 'msg'=>'Это ваш промокод']);

        // check if user was uses this discount
        if (!Auth::guest()) {
            // if used === 1, discount can use only one time
            if ((int) $discount->used === 1) {
                $is_discount_used = UsedDiscounts::where('user_id', Auth::user()->id)
                    ->where('discount', $request->discount)
                    ->where('type', 0)
                    ->first();

                if ($is_discount_used) {
                    return response(['status' => 400, 'msg' => trans('messages.discount_was_used')]);
                }
            }
        }

        return response(['status' => 200, 'msg' => trans('messages.discount_is_valid'), 'data' => $discountCheck]);
    }

    /**
     * check if Points > 0
     * @param Request $request [_POST data]
     * @return [response]
     */
    public function checkPoints(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax() || !Auth::user()) {
            return response(['status' => 400, 'msg' => trans('messages.error_while_send_form')]);
        }

        if ($request->points <= Auth::user()->points && $request->points >= 0) {
            $currency = Settings::where('name', 'points_currency')->first();

            if (!$currency) {
                $currency = 1;
            } else {
                $currency = $currency->value;
            }

            return response(['status' => 200, 'msg' => trans('messages.points_is_valid'), 'data' => number_format($currency*$request->points, 0, '', '')]);
        } else {
            return response(['status' => 400, 'msg' => trans('messages.points_is_not_valid')]);
        }
    }

    /**
     * check free time to cleaning in schedules and orders
     * @param Request $request [_POST data]
     * @return mixed
     */
    public function checkTime(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax() || empty($request->date) || empty($request->elapsed)) {
            return response(['status' => 400, 'msg' => trans('messages.something_went_wrong')]);
        }

        if (strtotime(date('d-m-Y', strtotime($request->date))) < strtotime(date('d-m-Y', strtotime('+2 days')))) {
            return response(['status' => 400, 'msg' => 'Возможно вы не указали дату или на эту дату все клинеры заняты, пожалуйста, укажите другую дату!']);
        }

        // get name of week day from request date
        $week_day = strtolower(date("D", strtotime($request->date)));

        // get free cleaners
        $schedules = Cleaners::select('cleaners_schedule.cleaner_id',
            'cleaners_schedule.mon',
            'cleaners_schedule.tue',
            'cleaners_schedule.wed',
            'cleaners_schedule.thu',
            'cleaners_schedule.fri',
            'cleaners_schedule.sat',
            'cleaners_schedule.sun',
            'cleaners_schedule.t_from',
            'cleaners_schedule.t_to',
            'cleaners_schedule.t_to_finish')
            ->leftJoin('cleaners_schedule', function ($join) {
                $join->on('cleaners_schedule.cleaner_id', '=', 'cleaners.id');
            })
            ->where('cleaners.status', 1)
            ->where('cleaners_schedule.' . $week_day, 1)
            ->get()
            ->toArray();

        if (!count($schedules)) {
            return response(['status' => 400, 'msg' => 'К сожалению, на данное число нет свободных клинеров.']);
        }

        // prepare date (reverse)
        $formated_date = array_reverse(explode('-', $request->date));
        $formated_date = implode('-', $formated_date);

        // get all orders at this date
        $orders = OrdersCleaners::where('status', '!=', 2)
            ->where('status', '!=', 3)
            ->where('cl_date', $formated_date)
            ->whereNotNull('cleaner_id')
            ->get()
            ->toArray();

        $request->elapsed = date('H:i', strtotime($request->elapsed));

        // if orders not found
        if (!count($orders)) {

            // get time wich will be used for future order
            $expendedTime = (strtotime($request->elapsed) - strtotime("00:00:00"));
            $schedules_array_from = [];
            $schedules_array_to = [];
            
            foreach ($schedules as $key => $schedule) {

                $cleaning_finish_time = date("H:i:s", strtotime($schedule['t_from']) + $expendedTime);
        
                if ($cleaning_finish_time <= $schedule['t_to_finish']) {
                    $new_t_to = (strtotime($request->elapsed) - strtotime("00:00:00"));

                    $schedules_array_from[] = $schedule['t_from'];                    
                    $schedules_array_to[] = date("H:i:s", strtotime($schedule['t_to_finish']) - $new_t_to);
                }
            }
        
            if (count($schedules_array_from) && count($schedules_array_to)) {
                $schedule = [
                    't_from' => min(array_values(array_unique($schedules_array_from))),
                    't_to' => max(array_values(array_unique($schedules_array_to))),
                ];
        
                return response(['status' => 200, 'data' => $schedule]);
            }
        
            return response(['status' => 400, 'msg' => 'К сожалению, на данное число нет свободных клинеров.']);
        }

        // If orders found

        $order_cleaners_array = [];
        
        // count cleaner used in orders
        if(count($orders)) {
            foreach ($orders as $key => $value) {
                if (!in_array($value['cleaner_id'], $order_cleaners_array) && !is_null($value['cleaner_id'])) {
                    if (isset($order_cleaners_array[$value['cleaner_id']])) {
                        $order_cleaners_array[$value['cleaner_id']] += 1;
                    } else {
                        $order_cleaners_array[$value['cleaner_id']] = 1;
                    }
                }
            }
        }

        $future_seconds = strtotime($request->elapsed) - strtotime("00:00:00");
        
        // if search in cleaners schedules and in orders
        foreach ($schedules as $k_schedule => $value_schedule) {
            foreach ($orders as $key_order => $value_order) {
                // find current cleaner in order
                if (!is_null($value_order['cleaner_id']) && $value_order['cleaner_id'] === $value_schedule['cleaner_id']) {
                    if (isset($order_cleaners_array[$value_order['cleaner_id']]) && $order_cleaners_array[$value_order['cleaner_id']] < 2) {
                        // how much time will be used if request order is after existing order
                        $elapsed_after = (strtotime($value_order['elapsed']) - strtotime("00:00:00"))+(strtotime("01:30:00")-strtotime("00:00:00"));
                        $request_order_begin_time_after = date("H:i:s", strtotime($value_order['cl_time']) + $elapsed_after);
        
                        // how much time will be used if request order is before existing order
                        $elapsed_before = $future_seconds + (strtotime("01:30:00")-strtotime("00:00:00"));
                        $request_order_finish_time_before = date("H:i:s", strtotime($value_schedule['t_from']) + $elapsed_before);
        
                        // get elapsed time for future order
                        
                        $future_order_finish_time = date("H:i:s", strtotime($request_order_begin_time_after) + $future_seconds);
        
                        // if existing order time after requesting
                        // do not delete this cleaner from candidates list, otherwise - delete it
                        if (strtotime($future_order_finish_time) >= strtotime($request_order_begin_time_after)
                            && strtotime($future_order_finish_time) <= strtotime($value_schedule['t_to_finish'])) {
                            $schedules[$k_schedule]['t_from'] = $request_order_begin_time_after;
                            $schedules[$k_schedule]['t_to'] = date("H:i:s", strtotime($value_schedule['t_to_finish']) - $future_seconds);
                        } // if existing order time before requesting
                        elseif (strtotime($request_order_finish_time_before) <= strtotime($value_order['cl_time'])) {
                            $t_from_range = (strtotime($value_order['cl_time']) - strtotime("00:00:00"))-(strtotime($request_order_finish_time_before) - strtotime("00:00:00"));
                            $schedules[$k_schedule]['t_to'] = date("H:i:s", strtotime($value_schedule['t_from']) + $t_from_range);
                        } else {
                            unset($schedules[$k_schedule]);
                        }
                    } else {
                        unset($schedules[$k_schedule]);
                    }
                }
                // if cleaner not added yet
                else {

                    // elapsed time to request order
                    $cleaning_finish_time = date("H:i:s", strtotime($value_schedule['t_from']) + $future_seconds);
        
                    if ($cleaning_finish_time <= $value_schedule['t_to_finish']) {
                        $schedules[$k_schedule]['t_to'] = date("H:i:s", strtotime($value_schedule['t_to_finish']) - $future_seconds);
                    } else {
                        unset($schedules[$k_schedule]);
                    }
                }
            }
        }

        $schedules_array_from = [];
        $schedules_array_to = [];
        
        // update time range
        foreach ($schedules as $key => $value) {
            if(isset($value['t_from']) && isset($value['t_to'])) {
                $schedules_array_from[] = $value['t_from'];
                $schedules_array_to[] = $value['t_to'];                
            }
        }
        
        if (count($schedules_array_from) && count($schedules_array_to)) {
            $schedule = [
                't_from' => min(array_values(array_unique($schedules_array_from))),
                't_to' => max(array_values(array_unique($schedules_array_to))),
            ];
        
            return response(['status' => 200, 'data' => $schedule]);
        }

        return response(['status' => 400, 'msg' => 'К сожалению, на данное число нет свободных клинеров.']);
    }

    /**
     * get faq items
     * @return [obj]
     */
    public function faq($slug = 'uborka')
    {
        $category = FaqCategories::where('category', $slug)
                                 ->where('status', 1)
                                 ->first();
        if (
            !$faq = $category
                ->faq()
                ->select('id', 'category', 'title', 'slug')
                ->where('status', 1)
                ->orderby('sort', 'asc')
                ->take(3)
                ->get()
        ) {
            $faq = $this->faq('servis-v-celom');
        }

        return compact('faq', 'slug');
    }

    /**
     * render general cleaning index page
     * @return [view]
     */
    public function general()
    {
        // get calculator settings
        $settings = $this->calculatorSettings('calc_general_%', 13);

        // prepare calculator settings to add in view
        $calculator = $this->prepareCalculatorSettingsForView($settings);

        // prepare calculator settings to add in js
        $settings = $this->prepareCalculatorSettingsForJs($calculator);

        // get user data
        if (!$user = Auth::user()) {
            $user = (object) [
                'name' => '',
                'phone' => '',
                'email' => '',
                'street' => '',
                'complex' => '',
                'housing' => '',
                'house' => '',
                'flat' => '',
            ];
        }

        $faq = $this->faq();

        // get Cleaners Schedule
        $schedule_arr = Cleaners::select('cleaners_schedule.mon', 'cleaners_schedule.tue', 'cleaners_schedule.wed', 'cleaners_schedule.thu', 'cleaners_schedule.fri', 'cleaners_schedule.sat', 'cleaners_schedule.sun', 'cleaners_schedule.t_from', 'cleaners_schedule.t_to')
            ->leftJoin('cleaners_schedule', function ($join) {
                $join->on('cleaners_schedule.cleaner_id', '=', 'cleaners.id');
            })
            ->where('cleaners.status', 1)
            ->get()
            ->toArray();

        $schedule = [
            'days' => [
                0, 1, 2, 3, 4, 5, 6
            ],
            't_from' => '08:00',
            't_to' => '20:00',
        ];

        // prepare schedule week days array for date time picker
        if ($schedule_arr && count($schedule_arr)) {
            foreach ($schedule_arr as $key => $value) {
                // days
                if ($value['sun'] == 1 && isset($schedule['days'][0])) {
                    unset($schedule['days'][0]);
                }
                if ($value['mon'] == 1 && isset($schedule['days'][1])) {
                    unset($schedule['days'][1]);
                }
                if ($value['tue'] == 1 && isset($schedule['days'][2])) {
                    unset($schedule['days'][2]);
                }
                if ($value['wed'] == 1 && isset($schedule['days'][3])) {
                    unset($schedule['days'][3]);
                }
                if ($value['thu'] == 1 && isset($schedule['days'][4])) {
                    unset($schedule['days'][4]);
                }
                if ($value['fri'] == 1 && isset($schedule['days'][5])) {
                    unset($schedule['days'][5]);
                }
                if ($value['sat'] == 1 && isset($schedule['days'][6])) {
                    unset($schedule['days'][6]);
                }

                // time
                if ($value['t_from'] < $schedule['t_from']) {
                    $schedule['t_from'] = $value['t_from'];
                }
                if ($value['t_to'] > $schedule['t_to']) {
                    $schedule['t_to'] = $value['t_to'];
                }
            }
        }
        $discount = null;
        if (session()->has('referal')){
            $discount = Discounts::where('code',session()->get('referal'))->first();

        }
            // get cleaning time border
        $cleaning_time_border = Settings::select('value')->where('name', 'cleaning_time_border')->first();

        return view('front.cleaning.general', [
            'meta' => $this->meta(null),
            'faq' => $faq['faq'],
            'faq_category' => $faq['slug'],
            'user' => $user,
            'calculator' => $calculator,
            'schedule' => $schedule,
            'discount' => $discount,
            'cleaning_time_border' => $cleaning_time_border->value,
            'js' => $settings,
        ]);
    }

    /**
     * get form data for simple Calculator
     */
    public function getSimpleCalculatorFormData()
    {
        // get calculator settings
        $settings = $this->calculatorSettings();

        // prepare calculator settings to add in view
        $calculator = $this->prepareCalculatorSettingsForView($settings);

        // prepare calculator settings to add in js
        $settings = $this->prepareCalculatorSettingsForJs($calculator);

        // get user data
        if (!$user = Auth::user()) {
            $user = (object) [
                'name' => '',
                'phone' => '',
                'email' => '',
                'street' => '',
                'complex' => '',
                'housing' => '',
                'house' => '',
                'flat' => '',
            ];
        }

        // get Cleaners Schedule
        $schedule_arr = Cleaners::select('cleaners_schedule.mon', 'cleaners_schedule.tue', 'cleaners_schedule.wed', 'cleaners_schedule.thu', 'cleaners_schedule.fri', 'cleaners_schedule.sat', 'cleaners_schedule.sun', 'cleaners_schedule.t_from', 'cleaners_schedule.t_to')
            ->leftJoin('cleaners_schedule', function ($join) {
                $join->on('cleaners_schedule.cleaner_id', '=', 'cleaners.id');
            })
            ->where('cleaners.status', 1)
            ->get()
            ->toArray();

        $schedule = [
            'days' => [
                0, 1, 2, 3, 4, 5, 6
            ],
            't_from' => '08:00',
            't_to' => '20:00',
        ];

        // prepare schedule week days array for date time picker
        if ($schedule_arr && count($schedule_arr)) {
            foreach ($schedule_arr as $key => $value) {
                // days
                if ($value['sun'] == 1 && isset($schedule['days'][0])) {
                    unset($schedule['days'][0]);
                }
                if ($value['mon'] == 1 && isset($schedule['days'][1])) {
                    unset($schedule['days'][1]);
                }
                if ($value['tue'] == 1 && isset($schedule['days'][2])) {
                    unset($schedule['days'][2]);
                }
                if ($value['wed'] == 1 && isset($schedule['days'][3])) {
                    unset($schedule['days'][3]);
                }
                if ($value['thu'] == 1 && isset($schedule['days'][4])) {
                    unset($schedule['days'][4]);
                }
                if ($value['fri'] == 1 && isset($schedule['days'][5])) {
                    unset($schedule['days'][5]);
                }
                if ($value['sat'] == 1 && isset($schedule['days'][6])) {
                    unset($schedule['days'][6]);
                }

                // time
                if ($value['t_from'] < $schedule['t_from']) {
                    $schedule['t_from'] = $value['t_from'];
                }
                if ($value['t_to'] > $schedule['t_to']) {
                    $schedule['t_to'] = $value['t_to'];
                }
            }
        }

        // get cleaning time border
        $cleaning_time_border = Settings::select('value')->where('name', 'cleaning_time_border')->first();

        // get faq data
        $faq = $this->faq();

        return [
            'faq' => $faq['faq'],
            'faq_category' => $faq['slug'],
            'settings' => $settings,
            'user' => $user,
            'calculator' => $calculator,
            'schedule' => $schedule,
            'cleaning_time_border' => $cleaning_time_border,
            'js' => $settings,
        ];
    }
}

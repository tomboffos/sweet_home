<?php

namespace App\Http\Controllers\Front;

use App;
use App\Models\Settings;

class ComposerFooterSkillsController extends CommonController
{
    /**
     * get skills
     * @return [obj]
     */
    public function skills()
    {
        $skills = Settings::where('name', 'index')->where('lang', App::getLocale())->first();

        if ($skills) {
            $skills = json_decode($skills->value, true);
            return $skills['skills'];
        }

        return false;
    }

    /**
     * get all vars and include in views
     * @param  [string] $view
     */
    public function compose($view)
    {
        $view->with('skills', $this->skills());
    }
}

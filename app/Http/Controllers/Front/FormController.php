<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use Log;
use App\Models\Settings;

class FormController extends CommonController
{
   
    /**
     * form handle
     * @param  Request $request [_POST data]
     * @return [void]
     */
    public function handle(Request $request)
    {
        if (!$request->isMethod('post') || !$request->ajax() || !isset($request->form_name) || empty($request->form_name)) {
            return response(['status' => 400, 'msg' => trans('messages.something_went_wrong')]);
        }

        switch ($request->form_name) {
            case 'callme':
                $data = [
                    'view' => 'callme',
                    'subject' => 'callme_subject',
                    'phone' => $request->phone
                ];
                break;
        }

        if ($this->sendEmail((object) $data)) {
            return response(['status' => 200, 'msg' => trans('messages.email_sended')]);
        }

        return response(['status' => 400, 'msg' => trans('messages.something_went_wrong')]);
    }

    /**
     * send email
     * @param  [object] $data
     * @return [boolean]
     */
    public function sendEmail($data)
    {
        // get admin emails
        $recipients = Settings::where('name', 'emails')->first();

        if (count($recipients)) {
            $recipients = explode(',', $recipients->value);
        } else {
            return false;
        }

        try {
            $mail = Mail::send("email.{$data->view}", ['data' => $data], function ($m) use ($data, $recipients) {
                $m->from(config('mail.from.address'), trans("messages.email_from_text"));
                $m->replyTo(config('mail.from.address'), trans("messages.email_from_text"));
    
                // send to admin
                if (isset($recipients) && count($recipients) > 1) {
                    $m->to($recipients[0])->bcc($recipients[1])->subject(trans("messages.{$data->subject}"));
                } elseif (isset($recipients) && count($recipients) > 0) {
                    $m->to($recipients[0])->subject(trans("messages.{$data->subject}"));
                }
            });
            return true;
        } catch (\Exception $e) {
            Log::error("Can't send email! " . $e->getMessage());
            return false;
        }
    }
}

<?php

namespace App\Http\Controllers\Front;

use App\Models\Discounts;
use App\Models\UsedDiscounts;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Event;
use App\Events\PointsLogEvent;
use App\Models\Settings;
use App\Models\Cleaners;
use App\Models\Testimonals;
use App\Models\OrdersCleaners;
use App\Models\OrdersWorkers;
use App\Models\WorkersCategories;
use App\Models\PointsLog;
use App\Models\UsersCards;
use Auth;
use Validator;
use Hash;

class UserController extends CommonController
{
    /**
     * render user index page
     * @return [view]
     */
    public function index(Request $request)
    {
        // edit user data
        if ($request->isMethod('post')) {
            return $this->save($request);
        }

        $data = Auth::user();

        // get meta tags
        $meta = (object) [
            'title' => $data->name ? $data->name : 'Личный кабинет',
            'meta_title' => $data->name,
            'meta_description' => $data->name,
            'meta_keywords' => $data->name,
        ];

        $cardData = UsersCards::where('user_id', $data->id)->first();

        return view('front.user.index', [
            'data' => $data,
            'meta' => $this->meta($meta),
            'cardData' => $cardData,
        ]);
    }

    /**
     * render form
     * @param Request $request
     * @return [html]
     */
    public function addCard(Request $request)
    {
        $user = Auth::user();

        // delete user cards
        UsersCards::where('user_id', $user->id)->delete();

        // create node of user cards
        $addCardOrder = UsersCards::create([
            'user_id' => $user->id
        ]);

        if (!$addCardOrder) {
            return redirect()->back();
        }

        // get form
        $kazkom = new KazkomController();
        $data = $kazkom->get_form_to_add_card($addCardOrder, $user);

        return view('front.user.addcard', [
            'data' => $data,
            'meta' => $this->meta(null)
        ]);
    }

    /**
     * delete user card action
     * @param [object] $user
     * @return [string]
     */
    public function deleteCard()
    {
        $user = Auth::user();

        $user_card = UsersCards::where('user_id', $user->id)->first();

        if (!$user_card || !count($user_card)) {
            $notification['message'] = trans('messages.something_went_wrong');
            return redirect()->back()->with($notification);
        }

        if (isset($user_card->id) && (is_null($user_card->card_id) || empty($user_card->card_id))) {
            $deleteCard = UsersCards::where('user_id', $user->id)->delete();
            return redirect()->back();
        }

        $kazkom = new KazkomController();
        $data = $kazkom->get_client_card_delete_data($user_card->user_id, $user_card->card_id);

        if ($data && count($data)) {
            // delete client card
            $deleteCard = UsersCards::where('card_id', $data['cardID'])
                ->where('user_id', $data['abonent_id'])
                ->delete();

            if ($deleteCard) {
                return redirect()->back();
            }
        }

        $notification['message'] = trans('messages.something_went_wrong');
        return redirect()->back()->with($notification);
    }

    /**
     * get user card id and approve status
     * @param  [object] $user
     * @return [array] Contents card id and approve code - 0 or 1.
     */
    public function getUserCardIdAndApproveStatus($user)
    {
        $kazkom = new KazkomController();
        return $kazkom->get_client_cards_list($user->id);
    }

    /**
     * add card id to client
     * @param [object] $data [user data]
     */
    public function addCardIdAndApproveStatus($data)
    {
        $cardData = $this->getUserCardIdAndApproveStatus($data);

        if ($cardData && count($cardData)) {
            // save card id
            $saveUserCard = UsersCards::where('user_id', $data->id)->update([
                'card_id' => $cardData['cardID'],
                'approve' => $cardData['approve'],
                'card_mask' => $cardData['CardMask'],
            ]);

            return true;
        }

        return false;
    }

    /**
     * edit user
     * @param [obj] $request [_POST data]
     * @return [view]
     */
    public function save($request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|min:1|max:255|string',
            'phone' => 'required|min:18|max:18|string',
            'rooms_number' => 'min:0|numeric',
            'bath_rooms_number' => 'min:0|numeric',
            'street' => 'required|max:255|string',
            'complex' => 'max:255|string',
            'house' => 'required|max:255|string',
            'housing' => 'max:255|string',
            'flat' => 'max:255|string',
            'new_password' => 'min:6|confirmed',
        ]);

        if ($v->fails()) {
            $notification['message'] = trans('messages.some_fields_incorrect');
            return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
        }

        $sql = [
            'name' => $request->name,
            'phone' => $request->phone,
            'rooms_number' => $request->rooms_number ? $request->rooms_number : null,
            'bath_rooms_number' => $request->bath_rooms_number ? $request->bath_rooms_number : null,
            'complex' => $request->complex,
            'housing' => $request->housing,
            'street' => $request->street,
            'house' => $request->house,
            'flat' => $request->flat,
            'subscribe' => (isset($request->subscribe) && !empty($request->subscribe) ? 1 : 0),
        ];
        
        // save new password
        if (isset($request->new_password) && !empty($request->new_password)) {
            if (!Hash::check($request->password, Auth::user()->password)) {
                $v->errors()->add('password', trans('messages.old_password_incorrect'));
                return redirect()->back()->withErrors($v->errors())->withInput();
            }

            $sql['password'] = bcrypt($request->new_password);
        }

        // update email if not empty. check is the email unique
        if (isset($request->email) && !empty($request->email) && Auth::user()->email !== $request->email) {
            $is_email_exist = User::where('email', $request->email)->get();

            if (!count($is_email_exist)) {
                $sql['email'] = $request->email;
            } else {
                $v->errors()->add('email', trans('messages.email_exist'));
                return redirect()->back()->withErrors($v->errors())->withInput();
            }
        }

        $update = User::where('id', Auth::user()->id)->update($sql);

        if ($update) {
            $v->errors()->add('message', trans('messages.user_edit_success'));
            $discount =Discounts::where('user_id',Auth::user()->id)->first();


            if($discount){
                $request->name = str_replace(' ','',$request->name);
                $request->name = \Illuminate\Support\Str::upper(cyrillic_to_latin($request->name)).Auth::user()->id;

                $usedDiscounts = UsedDiscounts::where('discount',$discount->code)->get();
                foreach ($usedDiscounts as $usedDiscount) {
                    $usedDiscount['discount'] = $request->name;
                    $usedDiscount->save();
                }
                $discount['code'] = $request->name;
                $discount['user_id'] = Auth()->user()->id;
                $discount->save();
            }

            return redirect()->back()->withErrors($v->errors());

        }


        $notification['message'] = trans('messages.error_insert_bd');
        return redirect()->back()->with($notification);
    }

    /**
     * choose cleaning or workers orders
     * @return [view]
     */
    public function orders()
    {
        return view('front.user.orders.index', [
            'meta' => $this->meta()
        ]);
    }

    /**
     * list of user cleaning orders
     * @return [view]
     */
    public function ordersCleaninig()
    {
        $data = OrdersCleaners::where('client_id', Auth::user()->id)
            ->orderby('id', 'desc')
            ->paginate($this->postsPerPage());

        // get payment methods
        $payment_methods = config('constants.payment_methods');

        // get payment statuses
        $payment_status = config('constants.payment_status');

        // get order statuses
        $order_status = config('constants.order_status');

        // get meta tags
        $meta = $this->meta();

        return view('front.user.orders.cleaning.index', compact('data', 'meta', 'payment_methods', 'payment_status', 'order_status'));
    }

    /**
     * list of user workers orders
     * @return [view]
     */
    public function ordersWorkers()
    {
        $data = OrdersWorkers::where('client_id', Auth::user()->id)
            ->orderby('id', 'desc')
            ->paginate($this->postsPerPage());

        // get payment methods
        $payment_methods = config('constants.payment_methods');

        // get payment statuses
        $payment_status = config('constants.payment_status');

        // get order statuses
        $order_status = config('constants.order_status');

        // get meta tags
        $meta = $this->meta();

        return view('front.user.orders.workers.index', compact('data', 'meta', 'payment_methods', 'payment_status', 'order_status'));
    }

    /**
     * render cleaning order
     * @return [view]
     */
    public function orderCleaninig(Request $request, $id)
    {
        // add testimonal
        if ($request->isMethod('post')) {
            return $this->addCleaningTestimonal($request, $id);
        }

        $data = OrdersCleaners::where('client_id', Auth::user()->id)
            ->where('id', $id)
            ->first();

        if (!$data) {
            return abort(404);
        }

        // convert json strings to obj
        $data->type = json_decode($data->type);
        $data->cicle = json_decode($data->cicle);
        $data->services = json_decode($data->services);

        // get discount code for this order
        if (!is_null($data->discount_id) || !empty($data->discount_id)) {
            $discount = $data->discounts()->where('category', 0)->where('status', 1)->first();
        }

        // get cleaner for this order
        if ($data->cleaner_id && !is_null($data->cleaner_id)) {
            $cleaner = Cleaners::findorfail($data->cleaner_id);
        }

        // get testimonal
        $testimonal = Testimonals::where('order_id', $data->id)->first();

        // get payment methods
        $payment_methods = config('constants.payment_methods');

        // get payment statuses
        $payment_status = config('constants.payment_status');

        // get order statuses
        $order_status = config('constants.order_status');

        // get meta tags
        $meta = (object) [
            'title' => "Заказ на уборку №{$data->id}",
            'meta_title' => "",
            'meta_description' => "",
            'meta_keywords' => "",
        ];

        $discount = Discounts::find($data->discount_id);

        $cleaner = null;
        if ($data->cleaner_id)
            $cleaner = Cleaners::find($data->cleaner_id);

        return view('front.user.orders.cleaning.item', compact('data', 'meta', 'payment_methods', 'payment_status', 'order_status', 'discount', 'cleaner', 'testimonal'));
    }

    /**
     * render workers order
     * @return [view]
     */
    public function orderWorkers(Request $request, $id)
    {
        // add testimonal
        if ($request->isMethod('post')) {
            return $this->addWorkersTestimonal($request, $id);
        }

        $data = OrdersWorkers::where('client_id', Auth::user()->id)
            ->where('id', $id)
            ->first();

        if (!$data) {
            return abort(404);
        }

        // get discount code for this order
        if (!is_null($data->discount_id) || !empty($data->discount_id)) {
            $discount = $data->discounts()->where('category', 1)->where('status', 1)->first();
        }

        // get testimonal
        $testimonal = Testimonals::where('order_id', $data->id)->first();

        // get Worker Professions
        $professions = WorkersCategories::where('status', 1)->get();

        // get current worker profession
        $profession = $data->professions()->first();

        // get payment methods
        $payment_methods = config('constants.payment_methods');

        // get payment statuses
        $payment_status = config('constants.payment_status');

        // get order statuses
        $order_status = config('constants.order_status');

        // get meta tags
        $meta = (object) [
            'title' => "Заказ на подбор персонала №{$data->id}",
            'meta_title' => "",
            'meta_description' => "",
            'meta_keywords' => "",
        ];

        return view('front.user.orders.workers.item', compact('data', 'meta', 'payment_methods', 'payment_status', 'order_status', 'discount', 'profession', 'testimonal'));
    }

    /**
     * render favorite cleaners list
     * @return [view]
     */
    public function favoriteCleaners(Request $request)
    {
        // delete cleaner from favorite list
        if ($request->isMethod('post') && $request->ajax()) {
            return $this->deleteCleaner($request, 'favorite');
        }

        $data = Auth::user()
            ->favorite_cleaners()
            ->orderby('id', 'desc')
            ->paginate($this->postsPerPage());

        return view('front.user.favorite', [
            'data' => $data,
            'meta' => $this->meta(null)
        ]);
    }

    /**
     * render black list of cleaners
     * @return [view]
     */
    public function blackListCleaners(Request $request)
    {
        // delete cleaner from favorite list
        if ($request->isMethod('post') && $request->ajax()) {
            return $this->deleteCleaner($request, 'black-list');
        }

        $data = Auth::user()
            ->bad_cleaners()
            ->orderby('id', 'desc')
            ->paginate($this->postsPerPage());


        return view('front.user.black-list', [
            'data' => $data,
            'meta' => $this->meta(null)
        ]);
    }

    /**
     * render page with history of points
     * @return [view]
     */
    public function points()
    {

        $data = PointsLog::where('user_id', Auth::user()->id)
            ->orderby('id', 'desc')
            ->paginate($this->postsPerPage());

        return view('front.user.points', [
            'data' => $data,
            'meta' => $this->meta(null)
        ]);
    }

    /**
     * delete cleaner from favorite or black list
     * @param [obj] $request
     * @param [string] $type
     * @return [response]
     */
    public function deleteCleaner($request, $type)
    {
        $v = Validator::make($request->all(), [
            'cleaner' => 'required|numeric',
        ]);

        if ($v->fails()) {
            $notification['message'] = trans('messages.fills_empty');
            return response(['status' => 400, 'msg' => $notification]);
        }

        if ($type === 'favorite') {
            Auth::user()->favorite_cleaners()->detach($request->cleaner);
            return response(['status' => 200, 'msg' => 'Клинер удален из списка!']);
        }

        if ($type === 'black-list') {
            Auth::user()->bad_cleaners()->detach($request->cleaner);
            return response(['status' => 200, 'msg' => 'Клинер удален из списка!']);
        }

        $notification['message'] = trans('messages.error_insert_bd');
        return response(['status' => 400, 'msg' => $notification]);
    }

    /**
     * add testimonal to cleaners order
     * @param [obj] $request [_POST data]
     * @param [int] $id [order id]
     */
    public function addCleaningTestimonal($request, $id)
    {
        $v = Validator::make($request->all(), [
            'text' => 'max:5000|string',
            'cleaner' => 'required|numeric',
            'quality' => 'required|min:1|max:5|numeric',
            'punctuality' => 'required|min:1|max:5|numeric',
            'affability' => 'required|min:1|max:5|numeric',
        ]);

        if ($v->fails()) {
            $notification['message'] = trans('messages.fills_empty');
            return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
        }

        $testimonal = Testimonals::create([
            'client_id' => Auth::user()->id,
            'cleaner_id' => $request->cleaner,
            'order_id' => $id,
            'rating' => collect([$request->quality, $request->punctuality, $request->affability])->avg(),
            'text' => $request->text,
            'type' => 0, // cleaning
            'status' => 0,
        ]);

        if ($testimonal) {
            if (isset($request->cleaners_action) && (int) $request->cleaners_action > 0) {
                Auth::user()->favorite_cleaners()->detach($request->cleaner);
                Auth::user()->bad_cleaners()->detach($request->cleaner);

                // add to favorite
                if ((int) $request->cleaners_action === 1) {
                    Auth::user()->favorite_cleaners()->attach($request->cleaner);
                } // add to black list
                elseif ((int) $request->cleaners_action === 2) {
                    Auth::user()->bad_cleaners()->attach($request->cleaner);
                }
            }

            $reward = Settings::where('name', 'testimonal')->first();

            // add points for testimonal
            if (isset($request->text) && !empty($request->text)) {
                User::where('id', Auth::user()->id)->update([
                        'points' => (Auth::user()->points+$reward->value)
                    ]);

                // TODO; send email to admin
                Event::fire(new PointsLogEvent(Auth::user()->id, trans("points.testimonal").$id, $reward->value));
            }

            return redirect()->back();
        }

        $notification['message'] = trans('messages.error_insert_bd');
        return redirect()->back()->withInput()->with($notification);
    }

    /**
     * add testimonal to workers order
     * @param [obj] $request [_POST data]
     * @param [int] $id [order id]
     */
    public function addWorkersTestimonal($request, $id)
    {
        $v = Validator::make($request->all(), [
            'text' => 'required|max:5000|string',
        ]);

        if ($v->fails()) {
            $notification['message'] = trans('messages.fills_empty');
            return redirect()->back()->withErrors($v->errors())->withInput()->with($notification);
        }

        $testimonal = Testimonals::create([
            'client_id' => Auth::user()->id,
            'order_id' => $id,
            'text' => $request->text,
            'type' => 1, // workers
            'status' => 0,
        ]);

        if ($testimonal) {
            $reward = Settings::where('name', 'testimonal')->first();

            // add points for testimonal
            User::where('id', Auth::user()->id)->update([
                    'points' => (Auth::user()->points+$reward->value)
                ]);

            // TODO; send email to admin
            Event::fire(new PointsLogEvent(Auth::user()->id, trans("points.testimonal").$id, $reward->value));

            return redirect()->back();
        }

        $notification['message'] = trans('messages.error_insert_bd');
        return redirect()->back()->withInput()->with($notification);
    }
}

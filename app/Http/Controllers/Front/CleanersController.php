<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Models\Settings;
use App\Models\Cleaners;
use App\Models\Testimonals;
use App\Models\OrdersCleaners;
use Auth;
use Validator;

class CleanersController extends CommonController
{
    /**
     * render list of cleaners
     * @return mix
     */
    public function index(Request $request)
    {
        $data = Cleaners::where('status', 1)
            ->orderby('id', 'asc')
            ->paginate($this->postsPerPage());

        // get meta tags
        $meta = (object) [
            'title' => 'Клинеры',
            'meta_title' => 'Клинеры',
            'meta_description' => 'Клинеры',
            'meta_keywords' => 'Клинеры',
        ];

        if(isset($request->page)) {
            $meta->meta_title = 'Страница ' . (int) $request->page . ' – ' . $meta->meta_title;
        }

        return view('front.cleaners.index', compact('data', 'meta'));
    }

    /**
     * render cleaner profile page
     * @return mix
     */
    public function item($id)
    {
        $data = Cleaners::where('id', $id)->where('status', 1)->first();

        if (!count($data)) {
            return abort(404);
        }

        $count = OrdersCleaners::where('cleaner_id', $id)->get()->count();
        $rating = Testimonals::where('cleaner_id', $id)->where('status', 1)->get()->avg('rating');

        $testimonals = Testimonals::select('users.name', 'testimonals.text')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'testimonals.client_id');
            })
            ->where('testimonals.status', 1)
            ->where('testimonals.cleaner_id', $id)
            ->orderby('testimonals.sort', 'asc')
            ->paginate($this->postsPerPage());

        $user = Auth::user();

        // if not guest
        if ($user) {
            $orders = OrdersCleaners::where('cleaner_id', $id)
                ->where('client_id', $user->id)
                ->where('status', 2)
                ->get()
                ->count();

            $in_favorite = $user->favorite_cleaners()->where('cleaner_id', $id)->get();
            $in_black_list = $user->bad_cleaners()->where('cleaner_id', $id)->get();
        }

        // get meta tags
        $meta = (object) [
            'title' => $data->name ? $data->name : 'Клинер',
            'meta_title' => $data->name,
            'meta_description' => $data->name,
            'meta_keywords' => $data->name,
        ];

        $meta = $this->meta($meta);

        return view('front.cleaners.item', [
            'data' => $data,
            'count' => $count,
            'rating' => $rating,
            'meta' => $meta,
            'user' => $user,
            'testimonals' => $testimonals,
            'orders' => (isset($orders) ? $orders : false),
            'in_favorite' => (isset($in_favorite) ? $in_favorite : false),
            'in_black_list' => (isset($in_black_list) ? $in_black_list : false),
        ]);
    }

    /**
     * add in favorite or black list
     * @param Request $request
     * @return mix
     */
    public function action(Request $request)
    {
        if (!$request->isMethod('post')
            || !$request->ajax()
            || empty($request->action)
            || empty($request->cleaner)) {
            return $this->error();
        }

        $user = Auth::user();

        switch ($request->action) {
            case 'add-favorite':
                $user->favorite_cleaners()->attach([(int) $request->cleaner]);
                return $this->success();
                break;

            case 'add-black-list':
                $user->bad_cleaners()->attach(['cleaner_id' => (int) $request->cleaner]);
                return $this->success();
                break;

            case 'remove-favorite':
                $user->favorite_cleaners()->detach(['cleaner_id' => (int) $request->cleaner]);
                return $this->success();
                break;

            case 'remove-black-list':
                $user->bad_cleaners()->detach(['cleaner_id' => (int) $request->cleaner]);
                return $this->success();
                break;
            
            default:
                return $this->error();
                break;
        }

        return $this->error();
    }

    /**
     * @return mix
     */
    public function success()
    {
        return response(['status' => 200, 'success' => true]);
    }

    /**
     * @return mix
     */
    public function error()
    {
        return response(['status' => 400, 'success' => false]);
    }
}

<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Models\UsersCards;
use App\User;

class PaymentController extends CommonController
{
    /**
     * return payment view data (html)
     * @return [void]
     */
    public function make($order, $user, $request)
    {
        $payMethods = config('payments.paymethods') ? config('payments.paymethods') : [];
        $paymentService = config('payments.'.$payMethods[(int) $request->pay]);

        $controller = 'App\Http\Controllers\Front\\'.$paymentService['controller'];
        $data = new $controller();
        return $data->view($order, $user);
    }

    /**
     * if transaction success
     * @return [type] [description]
     */
    public function success(Request $request, $service)
    {
        $data = Auth::user();

        $UsersCards = UsersCards::where('user_id', $data->id)->first();

        if (isset($UsersCards->approve) && (int) $UsersCards->approve === 1 && (is_null($UsersCards->card_id) || is_null($UsersCards->card_mask))) {
            $user = new UserController();
            $user->addCardIdAndApproveStatus($data);
        }
        
        return view('front.payments.success', [
            'meta' => $this->meta()
        ]);
    }

    /**
     * if transaction error
     * @return [type] [description]
     */
    public function error(Request $request, $service)
    {
        // delete user card info
        UsersCards::where('user_id', Auth::user()->id)->delete();

        return view('front.payments.error', [
            'meta' => $this->meta()
        ]);
    }

    /**
     * other data from payment service
     * @return [type] [description]
     */
    public function service(Request $request, $service)
    {
        $paymentService = config('payments.'.$service);

        $controller = 'App\Http\Controllers\Front\\'.$paymentService['controller'];
        $data = new $controller();
        return $data->service($request);
    }
}

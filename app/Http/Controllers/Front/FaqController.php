<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Faq;
use App\Models\FaqCategories;

class FaqController extends CommonController
{
    /**
     * render faq index page
     * @return [view]
     */
    public function index(Request $request)
    {
        $data = Faq::where('status', 1)
            ->orderby('sort', 'asc')
            ->paginate($this->postsPerPage());

        $categories = FaqCategories::where('status', 1)->get();
        $meta = $this->meta(null);

        if(isset($request->page)) {
            $meta->meta_title = 'Страница ' . (int) $request->page . ' – ' . $meta->meta_title;
        }

        return view('front.faq.index', compact('data', 'categories', 'meta'));
    }

    /**
     * render faq category page
     * @return [view]
     */
    public function category($id, $slug)
    {
        $categories = FaqCategories::where('status', 1)->get();
        $category = FaqCategories::where('id', $id)
            ->where('slug', $slug)
            ->where('status', 1)
            ->firstOrFail();

        $data = $category->faq()
            ->where('status', 1)
            ->orderby('sort', 'asc')
            ->paginate($this->postsPerPage());

        $meta = $this->meta($category);

        return view('front.faq.category', compact('data', 'meta', 'categories', 'slug', 'category'));
    }

    /**
     * render faq item page
     * @return [view]
     */
    public function item($cat_id, $cat_slug, $item, $slug)
    {
        $categories = FaqCategories::where('status', 1)->get();

        $category = FaqCategories::where('id', (int) $cat_id)
            ->where('slug', $cat_slug)
            ->where('status', 1)
            ->firstOrFail();

        $data = $category->faq()
            ->where('status', 1)
            ->where('id', $item)
            ->where('slug', $slug)
            ->firstOrFail();

        $meta = $this->meta($data);

        return view('front.faq.item', compact('data', 'meta', 'categories', 'cat_slug', 'category'));
    }
}

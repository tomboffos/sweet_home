<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Pages;

class ArticlesController extends CommonController
{
    /**
     * render article page
     * @return [view]
     */
    public function item($id, $slug)
    {
        $data = Pages::where('id', $id)
            ->where('slug', $slug)
            ->where('status', 1)
            ->first();

        if (!$data) {
            return abort(404);
        }

        return view('front.articles.item', [
            'data' => $data,
            'meta' => $this->meta($data)
        ]);
    }
}

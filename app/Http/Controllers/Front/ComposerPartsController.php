<?php

namespace App\Http\Controllers\Front;

use App\Models\Settings;
use App\Models\Pages;
use App\Models\WorkersCategories;

class ComposerPartsController extends CommonController
{

    /**
     * get pages for top menu
     * @return [obj]
     */
    public function pages()
    {
        $pages = Pages::where('status', 1)
            ->where('menu', 1)
            ->orderby('sort', 'asc')
            ->get();

        if (count($pages)) {
            return $pages;
        }

        return false;
    }

    /**
     * get workers categories for top menu
     * @return [obj]
     */
    public function workers()
    {
        $workers = WorkersCategories::where('status', 1)
            ->where('menu', 1)
            ->orderby('sort', 'asc')
            ->get();

        if (count($workers)) {
            return $workers;
        }

        return false;
    }

    /**
     * get all vars and include in views
     * @param  [string] $view
     */
    public function compose($view)
    {
        $view->with('menuPages', $this->pages())
            ->with('menuWorkers', $this->workers());
    }
}

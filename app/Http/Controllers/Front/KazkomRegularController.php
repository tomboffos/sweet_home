<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\OrdersCleaners;
use Log;
use SimpleXMLElement;

class KazkomRegularController extends CommonController
{
    /**
     * first request. add card. WORKS
     * @return [type] [description]
     */
    public function first()
    {
        // Invert key
        $this->invert();

        // get config
        $config = $this->config();

        // Open public key
        $this->load_private_key(storage_path("kazkom/kkbca.pem"), $config['kazkom_pay_password']);

        // xml order template
        $merchant = '<merchant cert_id="%certificate%" name="%merchant_name%"><order order_id="%order_id%" amount="%amount%" currency="%currency%"><department merchant_id="%merchant_id%" amount="%amount%" abonent_id="%abonent_id%" recepient="%abonent_id%" sessionid="%sessionid%" approve="0" service_id="%service_id%" recur_freq="%recur_freq%" recur_exp="%recur_exp%" person_id="%person_id%"/></order></merchant>';

        // Prepare order data, fill xml order template
        $merchant = preg_replace('/\%certificate\%/', $config['kazkom_pay_merchant_cert_id'], $merchant);
        $merchant = preg_replace('/\%merchant_name\%/', $config['kazkom_pay_merchant_name'], $merchant);
        $merchant = preg_replace('/\%order_id\%/', sprintf("%09d", 233), $merchant);
        $merchant = preg_replace('/\%currency\%/', $config['kazkom_pay_currency'], $merchant);
        $merchant = preg_replace('/\%merchant_id\%/', $config['kazkom_pay_merchant_id'], $merchant);
        $merchant = preg_replace('/\%amount\%/', 5, $merchant); // 5 - fixed price
        $merchant = preg_replace('/\%abonent_id\%/', 143, $merchant);
        $merchant = preg_replace('/\%sessionid\%/', '', $merchant);
        $merchant = preg_replace('/\%service_id\%/', '', $merchant);
        $merchant = preg_replace('/\%recur_freq\%/', 10, $merchant);
        $merchant = preg_replace('/\%recur_exp\%/', 20173112, $merchant);
        $merchant = preg_replace('/\%person_id\%/', 143, $merchant);

        // Prepare sign
        $merchant_sign = '<merchant_sign type="RSA">'.$this->sign64($merchant).'</merchant_sign>';

        // Make document
        $xml = "<document>".$merchant.$merchant_sign."</document>";
        
        // Prepare data and send to view via ajax
        $data['email'] = 'ismail.albakov@gmail.com';
        $data['BackLink'] = $config['BackLink'];
        $data['PostLink'] = $config['PostLink'];
        $data['Language'] = $config['Language'];
        $data['FailureBackLink'] = $config['FailureBackLink'];
        $data['xml'] = base64_encode($xml);
        $data['action'] = 'https://epay.kkb.kz/jsp/process/logon.jsp';
        echo '<form action="'. $data['action'] .'">
		<input type="text" name="email" value="'. $data['email'] .'">
		<input type="text" name="BackLink" value="'. $data['BackLink'] .'">
		<input type="text" name="PostLink" value="http://homesweet.kz/kazkom/service">
		<input type="text" name="Language" value="'. $data['Language'] .'">
		<input type="text" name="FailureBackLink" value="'. $data['FailureBackLink'] .'">
		<input type="text" name="Signed_Order_B64" value="'. $data['xml'] .'">
		<button type="submit" class="btn btn-success">Подключить карту!</button>
		</form>';
        die();
    }

    /**
     * get money from client without him DOESN'T WORK
     * @return [type] [description]
     */
    public function second()
    {
        // Invert key
        $this->invert();

        // get config
        $config = $this->config();

        // Open public key
        $this->load_private_key(storage_path("kazkom/kkbca.pem"), $config['kazkom_pay_password']);

        // xml order template
        $merchant = '<merchant cert_id="%certificate%" name="%merchant_name%"><order order_id="%order_id%" amount="%amount%" currency="%currency%"><department Reference="%Reference%" merchant_id="%merchant_id%" payerMail="%payerMail%"/></order></merchant>';

        // Prepare order data, fill xml order template
        $merchant = preg_replace('/\%certificate\%/', $config['kazkom_pay_merchant_cert_id'], $merchant);
        $merchant = preg_replace('/\%merchant_name\%/', $config['kazkom_pay_merchant_name'], $merchant);
        $merchant = preg_replace('/\%order_id\%/', sprintf("%09d", 234), $merchant);
        $merchant = preg_replace('/\%currency\%/', $config['kazkom_pay_currency'], $merchant);
        $merchant = preg_replace('/\%merchant_id\%/', $config['kazkom_pay_merchant_id'], $merchant);
        $merchant = preg_replace('/\%amount\%/', 8000, $merchant);
        $merchant = preg_replace('/\%Reference\%/', 160816133008, $merchant);
        $merchant = preg_replace('/\%payerMail\%/', 'ismail.albakov@gmail.com', $merchant);

        // Prepare sign
        $merchant_sign = '<merchant_sign type="RSA">'.$this->sign64($merchant).'</merchant_sign>';

        // Make document
        $xml = "<document>".$merchant.$merchant_sign."</document>";

        // Prepare data and send to view via ajax
        $data['xml'] = base64_encode($xml);
        $data['action'] = 'https://epay.kkb.kz/jsp/hbpay/rec.jsp';
        echo '<form action="'. $data['action'] .'">
		<input type="text" name="Signed_Order_B64" value="'. $data['xml'] .'">
		<button type="submit" class="btn btn-success">Оплатить без клиента!</button>
		</form>';
        die();
    }

    /**
     * handle response after first request
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function service(Request $request)
    {
        $response = $request->response;
        return $this->postResponseXmlParser($response);
    }

    /**
     * parse incoming XML document
     * @return [type] [description]
     */
    public function postResponseXmlParser($response)
    {
        $array = new SimpleXMLElement($response);

        Log::info("Response from KAZKOM added.");

        if (!$array || !count($array)) {
            Log::error("no response from KazkomPayService.");
            return ['status' => false, 'code' => 'no response from KazkomPayService.'];
        }

        // get data from results tag
        $arr_payment = $array->bank->results->payment->attributes();

        foreach ($arr_payment as $key => $value) {
            $payment[$key] = (string)$value;
        }

        // check response_code and
        // company merchant_id
        if ($payment['response_code'] != '00' || $payment['merchant_id'] !== config('payments.kazkom.kazkom_pay_merchant_id')) {
            Log::error("Check response_code or merchant_id failed.");
            return ['status' => false, 'code' => 'Check response_code or merchant_id failed.'];
        };

        // check cert_id
        $cert_id = $array->bank->customer->merchant->attributes();

        if ((string)$cert_id['cert_id'] !== config('payments.kazkom.kazkom_pay_merchant_cert_id')) {
            Log::error("Check cert_id failed.");
            return ['status' => false, 'code' => 'Check cert_id failed.'];
        };

        foreach ($payment as $key => $value) {
            Log::info($key." : ". $value);
        }

        return true;
    }

    /**
     * get payment config data
     * @return [array]
     */
    public function config()
    {
        return config('payments.kazkom');
    }

    /**
     * Load private key
     * @param  [string] $filename
     * @param  [string] $password
     * @return [string]
     */
    public function load_private_key($filename, $password = null)
    {

        if (!is_file($filename)) {
            return false;
            die("Key not found");
        }

        $c = file_get_contents($filename);

        if ($password) {
            $prvkey = openssl_get_privatekey($c, $password) or die(openssl_error_string());
        } else {
            $prvkey = openssl_get_privatekey($c) or die(openssl_error_string());
        }

        if (is_resource($prvkey)) {
            $this->private_key = $prvkey;
            return $c;
        }

        return false;
    }

    /**
     * Inverse string
     */

    public function invert()
    {
        $this->invert = 1;
    }

    /**
     * Reverse string
     * @param  [string] $str
     * @return [string]
     */
    public function reverse($str)
    {
        return strrev($str);
    }


    /**
     * Sign the string with private key
     * @param  [string] $str
     * @return [string]
     */
    public function sign($str)
    {
        if ($this->private_key) {
            openssl_sign($str, $out, $this->private_key);

            if ($this->invert == 1) {
                $out = $this->reverse($out);
            }

            return $out;
        }
    }

    /**
     * Sign the string with private key and encode with base64
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function sign64($str)
    {
        return base64_encode($this->sign($str));
    }

    /**
     * Check string with public key if it's signed with private key
     * @param  [string] $data
     * @param  [string] $str
     * @param  [string] $filename
     * @return [numeric] 1, 0 or -1
     */
    public function check_sign($data, $str, $filename)
    {
        if ($this->invert == 1) {
            $str = $this->reverse($str);
        }

        if (!is_file($filename)) {
            return false;
        }

        $pubkey = file_get_contents($filename);

        return openssl_verify($data, $str, $pubkey);
    }

    /**
     * Check string with public key $file if string $str in Base64 is signed with private key $data
     * @param  [string] $data
     * @param  [string] $str
     * @param  [string] $filename
     * @return [numeric] 1, 0 or -1
     */
    public function check_sign64($data, $str, $filename)
    {
        return $this->check_sign($data, base64_decode($str), $filename);
    }
}

<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Http\Requests;
use App\Models\Testimonals;
use App\Models\Settings;
use App\Models\Slider;

use App;
use Auth;
use Session;

class IndexController extends CommonController
{
    /**
     * render index page
     * @return [view]
     */
    public function index(Request $request)
    {
        // if referal
//        $request->ref = intval(preg_replace("/[^0-9]/", "", $request->ref ));


        if (isset($request->ref) && !empty($request->ref) && !Auth::user()) {
            session()->put('referal',$request->ref);
        }

        $testimonals = Testimonals::select('users.name', 'testimonals.text')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'testimonals.client_id');
            })
            ->where('testimonals.status', 1)
            ->orderby('testimonals.id', 'desc')
            ->take(3)
            ->get();

        $locale = App::getLocale();
        $slider = Slider::where('status', 1)
            ->where('lang', $locale)
            ->orderby('sort', 'asc')
            ->orderby('id', 'desc')->get();

        // get main informaion for index page
        
        $data = Settings::where('name', 'index')
                        ->where('lang', $locale)
                        ->first();
        if (!$data) {
            return abort(404);
        }

        $data = json_decode($data->value, true);
        $meta = $this->meta(null);

        return view('front.index', compact('meta', 'testimonals', 'data', 'slider'));
    }
}

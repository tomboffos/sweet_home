<?php
Route::get('setlocale/{locale}', function ($locale) {
    // Проверяем, что у пользователя выбран доступный язык 
    if (in_array($locale, \Config::get('app.locales'))) {
        // И устанавливаем его в сессии под именем locale
        Session::put('locale', $locale);
    }
    // Редиректим его <s>взад</s> на ту же страницу
    return redirect()->back(); 
});

Route::group(['middleware' => ['guest']], function() {
	// Password reset page
	Route::get('password/reset', ['as' => 'auth', 'uses' => 'Front\LoginController@reset']);
});

/**
 * Admin area
 */
Route::group(['middleware' => ['web', 'auth', 'role:admin'], 'prefix' => '/manager'], function() {

	Route::auth();

	// Index page
	Route::get('/', 'Admin\IndexController@index');
	Route::match(['get', 'post'], 'edit', 'Admin\IndexController@edit');
    Route::post('remote/','Admin\IndexController@Remote')->name('Remote');
	/**
	 * Slider
	 */
	Route::group(['prefix' => '/slider'], function() {
		Route::get('/', 'Admin\SliderController@index');
		Route::match(['get', 'post'], 'new', 'Admin\SliderController@create');
		Route::match(['get', 'post'], '{id}', 'Admin\SliderController@item')->where(['id' => '[0-9]+']);
		Route::get('delete/{id}', 'Admin\SliderController@delete')->where(['id' => '[0-9]+']);
	});

	/**
	 * Pages
	 */
	Route::get('pages', 'Admin\PagesController@index');
	Route::match(['get', 'post'], 'pages/new', 'Admin\PagesController@create');
	Route::match(['get', 'post'], 'pages/{id}', 'Admin\PagesController@item')->where(['id' => '[0-9]+']);
	Route::get('pages/delete/{id}', 'Admin\PagesController@delete')->where(['id' => '[0-9]+']);

	/**
	 * Posts
	 */
	Route::get('posts', 'Admin\PostsController@index');
	Route::match(['get', 'post'], 'posts/new', 'Admin\PostsController@create');
	Route::match(['get', 'post'], 'posts/{id}', 'Admin\PostsController@item')->where(['id' => '[0-9]+']);
	Route::get('posts/delete/{id}', 'Admin\PostsController@delete')->where(['id' => '[0-9]+']);

	/**
	 * Users Group
	 */
	Route::group(['prefix' => '/users'], function() {

		// Managers
		Route::get('managers', 'Admin\ManagersController@index');
		Route::match(['get', 'post'], 'managers/new', 'Admin\ManagersController@create');
		Route::match(['get', 'post'], 'managers/{id}', 'Admin\ManagersController@item')->where(['id' => '[0-9]+']);
		Route::get('managers/delete/{id}', 'Admin\ManagersController@delete')->where(['id' => '[0-9]+']);

		// Clients
		Route::get('clients', 'Admin\ClientsController@index');
		Route::match(['get', 'post'], 'clients/new', 'Admin\ClientsController@create');
		Route::match(['get', 'post'], 'clients/{id}', 'Admin\ClientsController@item')->where(['id' => '[0-9]+']);
		Route::get('clients/delete/{id}', 'Admin\ClientsController@delete')->where(['id' => '[0-9]+']);
		Route::get('clients/export/{type}', 'Admin\ClientsController@export')->where(array('type' => '[subscribes|all]+'));

		// approve client card
		Route::get('clients/approvecard/{id}', 'Admin\ClientsController@approveClientCard')
			->where(['id' => '[0-9]+']);
	});

	/**
	 * Testimonals
	 */
	Route::group(['prefix' => '/testimonals'], function() {
		Route::get('/', 'Admin\TestimonalsController@index');
		Route::match(['get', 'post'], '{id}', 'Admin\TestimonalsController@item')->where(['id' => '[0-9]+']);
		Route::get('delete/{id}', 'Admin\TestimonalsController@delete')->where(['id' => '[0-9]+']);
	});

	/**
	 * Cleaners
	 */
	Route::group(['prefix' => '/cleaners'], function() {
		Route::get('/', 'Admin\CleanersController@index');
		Route::match(['get', 'post'], 'new', 'Admin\CleanersController@create');
		Route::match(['get', 'post'], '{id}', 'Admin\CleanersController@item')->where(['id' => '[0-9]+']);
		Route::get('delete/{id}', 'Admin\CleanersController@delete')->where(['id' => '[0-9]+']);
	});

	/**
	 * Workers
	 */
	Route::group(['prefix' => '/workers'], function() {

		// professions
		Route::get('/cats', 'Admin\WorkersController@categories');
		Route::match(['get', 'post'], 'cats/new', 'Admin\WorkersController@createCategory');
		Route::match(['get', 'post'], 'cats/{id}', 'Admin\WorkersController@category')->where(['id' => '[0-9]+']);
		Route::get('cats/delete/{id}', 'Admin\WorkersController@deleteCategory')->where(['id' => '[0-9]+']);

		// workers
		Route::get('/', 'Admin\WorkersController@index');
		Route::match(['get', 'post'], 'new', 'Admin\WorkersController@create');
		Route::match(['get', 'post'], '{id}', 'Admin\WorkersController@item')->where(['id' => '[0-9]+']);
		Route::get('delete/{id}', 'Admin\WorkersController@delete')->where(['id' => '[0-9]+']);
	});

	/**
	 * Orders
	 */
	Route::group(['prefix' => '/orders'], function() {

		// cleaners
		Route::get('/cleaners', 'Admin\OrdersCleanersController@index');
		Route::match(['get', 'post'], 'cleaners/{id}', 'Admin\OrdersCleanersController@item')->where(['id' => '[0-9]+']);
		Route::get('cleaners/delete/{id}', 'Admin\OrdersCleanersController@delete')->where(['id' => '[0-9]+']);
		Route::post('cleaners/calculate', 'Admin\OrdersCleanersController@calculateAjaxOrderTotal');

		Route::get('cleaners/kazkom/pay/{id}', 'Admin\OrdersCleanersController@getMonteyFromClient')
			->where(['id' => '[0-9]+']);
		Route::post('cleaners/copyorder', 'Admin\OrdersCleanersController@copyOrder');

		// workers
		Route::get('workers/', 'Admin\OrdersWorkersController@index');
		Route::match(['get', 'post'], 'workers/{id}', 'Admin\OrdersWorkersController@item')->where(['id' => '[0-9]+']);
		Route::get('workers/delete/{id}', 'Admin\OrdersWorkersController@delete')->where(['id' => '[0-9]+']);
	});

	/**
	 * Faq
	 */
	Route::group(['prefix' => '/faq'], function() {

		// cats
		Route::get('/cats', 'Admin\FaqController@categories');
		Route::match(['get', 'post'], 'cats/new', 'Admin\FaqController@createCategory');
		Route::match(['get', 'post'], 'cats/{id}', 'Admin\FaqController@category')->where(['id' => '[0-9]+']);
		Route::get('cats/delete/{id}', 'Admin\FaqController@deleteCategory')->where(['id' => '[0-9]+']);

		// faq
		Route::get('/', 'Admin\FaqController@index');
		Route::match(['get', 'post'], 'new', 'Admin\FaqController@create');
		Route::match(['get', 'post'], '{id}', 'Admin\FaqController@item')->where(['id' => '[0-9]+']);
		Route::get('delete/{id}', 'Admin\FaqController@delete')->where(['id' => '[0-9]+']);
	});

	/**
	 * Discounts
	 */
	Route::group(['prefix' => '/discounts'], function() {
		Route::get('/', 'Admin\DiscountsController@index')->name('promocode');
	    Route::get('/discount/codes','Admin\DiscountsController@refers')->name('discounts');
		Route::match(['get', 'post'], 'new', 'Admin\DiscountsController@create');
		Route::match(['get', 'post'], '{id}', 'Admin\DiscountsController@item')->where(['id' => '[0-9]+']);
		Route::get('delete/{id}', 'Admin\DiscountsController@delete')->where(['id' => '[0-9]+']);
	});

	/**
	 * Calculator
	 */
	Route::group(['prefix' => '/calc'], function() {
		Route::match(['get', 'post'], '/', 'Admin\CalculatorController@index');
		Route::match(['get', 'post'], '/general', 'Admin\CalculatorController@general');
	});

	/**
	 * Settings
	 */
	Route::group(['prefix' => '/settings'], function() {
		Route::match(['get', 'post'], '/', 'Admin\SettingsController@index');
	});

	/**
	 * Transactions log
	 */
	Route::get('/transactions', 'Admin\TransactionsController@index');

	/**
	 * Editor Files Upload
	 */
	Route::group(['prefix' => '/laravel-filemanager'], function() {

		// Show LFM
		Route::get('/', '\Tsawler\Laravelfilemanager\controllers\LfmController@show');

	   	// upload
		Route::any('upload', '\Tsawler\Laravelfilemanager\controllers\UploadController@upload');

	   	// list images & files
		Route::get('jsonimages', '\Tsawler\Laravelfilemanager\controllers\ItemsController@getImages');
		Route::get('jsonfiles', '\Tsawler\Laravelfilemanager\controllers\ItemsController@getFiles');

	   	// folders
		Route::get('newfolder', '\Tsawler\Laravelfilemanager\controllers\FolderController@getAddfolder');
		Route::get('deletefolder', '\Tsawler\Laravelfilemanager\controllers\FolderController@getDeletefolder');
		Route::get('folders', '\Tsawler\Laravelfilemanager\controllers\FolderController@getFolders');

	   	// crop
		Route::get('crop', '\Tsawler\Laravelfilemanager\controllers\CropController@getCrop');
		Route::get('cropimage', '\Tsawler\Laravelfilemanager\controllers\CropController@getCropimage');

	   	// rename
		Route::get('rename', '\Tsawler\Laravelfilemanager\controllers\RenameController@getRename');

	   	// scale/resize
		Route::get('resize', '\Tsawler\Laravelfilemanager\controllers\ResizeController@getResize');
		Route::get('doresize', '\Tsawler\Laravelfilemanager\controllers\ResizeController@performResize');

	   	// download
		Route::get('download', '\Tsawler\Laravelfilemanager\controllers\DownloadController@getDownload');

	   	// delete
		Route::get('delete', '\Tsawler\Laravelfilemanager\controllers\DeleteController@getDelete');
	});
});

/**
 * Front area
 */
Route::group(['middleware' => ['web']], function() {

	Route::auth();

	Route::get('/', ['as' => 'index', 'uses' => 'Front\IndexController@index']);

	Route::group(['middleware' => ['guest']], function() {
		/*
			Auth & Reg
		*/
	   	Route::get('login', ['as' => 'auth', 'uses' => 'Front\LoginController@login']);
	   	Route::get('register', ['as' => 'auth', 'uses' => 'Front\LoginController@register']);

		/*
			Ajax Auth & Reg
		*/
		Route::post('login/ajax', 'Front\LoginController@loginAjax');
		Route::post('register/ajax', 'Front\LoginController@registerAjax');
		Route::post('password/reset/ajax', 'Front\LoginController@resetAjax');
		Route::post('login/order', 'Front\LoginController@loginOrderAjax');
	});

	/* Order Cleaning */
	Route::get('cleaning', ['as' => 'cleaning', 'uses' => 'Front\CleaningController@index']);
	Route::get('cleaning/general', ['as' => 'cleaning_general', 'uses' => 'Front\CleaningController@general']);
	Route::post('cleaning/checkemail', 'Front\CleaningController@checkEmail');
	Route::post('cleaning/checkdiscount', 'Front\CleaningController@checkDiscount');
	Route::post('cleaning/checkpoints', 'Front\CleaningController@checkPoints');
	Route::post('cleaning/order', 'Front\CleaningController@order');
	Route::post('cleaning/checktime', 'Front\CleaningController@checkTime');

	/* Order Workers */
	Route::get('workers', ['as' => 'workers', 'uses' => 'Front\WorkersController@index']);
	Route::get('workers/{id}-{slug}', ['as' => 'workers', 'uses' => 'Front\WorkersController@item'])
		->where(['id' => '[0-9]+']);
	Route::post('workers/order', 'Front\WorkersController@order');
	Route::post('workers/checkemail', 'Front\WorkersController@checkEmail');
	Route::post('workers/checkdiscount', 'Front\WorkersController@checkDiscount');
	Route::post('workers/checkpoints', 'Front\WorkersController@checkPoints');

	/* Testimonals */
	Route::get('testimonals', ['as' => 'testimonals', 'uses' => 'Front\TestimonalsController@index']);

	/* Pages */
	Route::get('pages/{id}-{slug}', ['as' => 'pages', 'uses' => 'Front\ArticlesController@item'])
		->where(['id' => '[0-9]+']);

	/* Posts */
	Route::get('uslugi/{category?}/{subCategory?}/{sub2Category?}', ['as' => 'posts', 'uses' => 'Front\PostsController@handle']);

	/* FAQ */
	Route::get('faq', ['as' => 'faq', 'uses' => 'Front\FaqController@index']);
	Route::get('faq/{id}-{slug}', 'Front\FaqController@category')
		->where(['id' => '[0-9]+']);
	Route::get('faq/{cat_id}-{cat_slug}/{item}-{slug}', 'Front\FaqController@item')
		->where(['cat_id' => '[0-9]+'])->where(['item' => '[0-9]+']);

	/* User */
	Route::group(['middleware' => ['auth'], 'prefix' => 'user'], function() {

		Route::match(['get', 'post'], '/', ['as' => 'user', 'uses' => 'Front\UserController@index']);
		Route::get('orders', ['as' => 'user_orders', 'uses' => 'Front\UserController@orders']);
		Route::get('points', ['as' => 'user_points', 'uses' => 'Front\UserController@points']);

		Route::get('orders/cleaning', ['as' => 'user_orders', 'uses' => 'Front\UserController@ordersCleaninig']);
		Route::match(['get', 'post'], 'orders/cleaning/{id}', 'Front\UserController@orderCleaninig')
			->where(['id' => '[0-9]+']);

		Route::get('orders/workers', ['as' => 'user_orders', 'uses' => 'Front\UserController@ordersWorkers']);
		Route::match(['get', 'post'], 'orders/workers/{id}', 'Front\UserController@orderWorkers')
			->where(['id' => '[0-9]+']);

		Route::match(['get', 'post'], 'favorite',
			['as' => 'user_favourite', 'uses' => 'Front\UserController@favoriteCleaners']);

		Route::match(['get', 'post'], 'black-list',
			['as' => 'user_black_list', 'uses' => 'Front\UserController@blackListCleaners']);

		Route::get('orders/cleaning/{id}/pay',
			['as' => 'user_orders', 'uses' => 'Front\CleaningController@payFromUserAccount'])
				->where(['id' => '[0-9+]']);

		// Route::get('addcard', ['as' => 'user_add_card', 'uses' => 'Front\UserController@addCardIdAndApproveStatus']);
		Route::get('addcard', ['as' => 'user_add_card', 'uses' => 'Front\UserController@addCard']);
		Route::get('deletecard', ['as' => 'user', 'uses' => 'Front\UserController@deleteCard']);
	});

	/* Cleaners */
	Route::group(['prefix' => 'cleaners'], function() {
		Route::get('/', ['as' => 'cleaners', 'uses' => 'Front\CleanersController@index']);
		Route::get('{id}', ['as' => 'cleaners', 'uses' => 'Front\CleanersController@item'])
		->where(['id' => '[0-9]+']);
		Route::post('action', ['as' => 'cleaners', 'uses' => 'Front\CleanersController@action']);
	});

	/* Payment */
	Route::match(['get', 'post'], 'payment/{service}/success',
		['as' => 'pay_success', 'uses' => 'Front\PaymentController@success']);

	Route::match(['get', 'post'], 'payment/{service}/error',
		['as' => 'pay_error', 'uses' => 'Front\PaymentController@error']);

	Route::match(['get', 'post'], 'payment/{service}/service',
		['as' => 'pay_service', 'uses' => 'Front\PaymentController@service']);

	/*
	Call me form
	 */
	Route::post('form/handle', 'Front\FormController@handle');

	// Route::get('kazkom/list', ['as' => 'user', 'uses' => 'Front\KazkomController@get_client_cards_list']);
	// Route::get('kazkom/delete', ['as' => 'user', 'uses' => 'Front\UserController@deleteCard']);
	// Route::get('kazkom/pay', ['as' => 'user', 'uses' => 'Front\KazkomController@pay_without_client']);
	// Route::get('kazkom/first', ['as' => 'user', 'uses' => 'Front\KazkomRegularController@first']);
	// Route::get('kazkom/second', ['as' => 'user', 'uses' => 'Front\KazkomRegularController@second']);
	// Route::match(['get', 'post'], 'kazkom/service', ['as' => 'user', 'uses' => 'Front\KazkomRegularController@service']);
});

/**
 * System
 */
Route::group(['middleware' => ['web']], function() {

	// List of Sitemaps
	Route::get('/sitemap.xml', ['as' => 'sitemap', 'uses' => 'System\SiteMapController@index']);

	// Faq Sitemap
	Route::get('/sitemap-faq.xml', ['as' => 'sitemap-faq', 'uses' => 'System\SiteMapController@faqCategories']);
	Route::get('/sitemap-faq-items.xml', ['as' => 'sitemap-faq', 'uses' => 'System\SiteMapController@faqItems']);

	// Pages
	Route::get('/sitemap-pages.xml', ['as' => 'sitemap-faq', 'uses' => 'System\SiteMapController@pagesItems']);

	// Testimonals
	Route::get('/sitemap-testimonals.xml', ['as' => 'sitemap-testimonals', 'uses' => 'System\SiteMapController@testimonalsItems']);

	// Workers
	Route::get('/sitemap-workers.xml', ['as' => 'sitemap-workers', 'uses' => 'System\SiteMapController@workersCategories']);
	Route::get('/sitemap-workers-items.xml', ['as' => 'sitemap-workers', 'uses' => 'System\SiteMapController@workersItems']);

	// Cleaning
	Route::get('/sitemap-cleaning.xml', ['as' => 'sitemap-cleaning', 'uses' => 'System\SiteMapController@cleaningItems']);

	// Uslugi
	Route::get('/sitemap-uslugi.xml', ['as' => 'sitemap-uslugi', 'uses' => 'System\SiteMapController@uslugiItems']);
});

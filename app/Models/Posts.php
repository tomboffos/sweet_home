<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Posts extends Model
{
    protected $table = 'posts';

    protected $fillable = [
    	'title',
    	'text',
    	'slug',
    	'meta_title',
    	'meta_description',
    	'sort',
    	'status',
    	'parent',
			'category',
			'text_bottom',
   	];

   	protected $dates = ['created_at', 'updated_at'];

   	public function getDates()
   	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}
}
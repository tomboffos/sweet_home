<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class UsedDiscounts extends Model
{
    protected $table = 'used_discounts';

	protected $fillable = [
		'user_id',
		'order_id',
		'discount',
		'type',
	];

	protected $dates = ['created_at', 'updated_at'];

	public function getDates()
	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}
}

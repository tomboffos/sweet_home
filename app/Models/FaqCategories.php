<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class FaqCategories extends Model
{
    protected $table = 'faq_categories';

    protected $fillable = [
    	'title',
    	'meta_title',
    	'meta_description',
    	'slug',
    	'category',
    	'status',
   	];

   	protected $dates = ['created_at', 'updated_at'];

   	public function getDates()
   	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function faq()
	{
		return $this->belongsTo('App\Models\Faq', 'id', 'category');
	}
}

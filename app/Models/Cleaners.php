<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Cleaners extends Model
{

    protected $table = 'cleaners';

    protected $fillable = [
    	'name',
    	'img',
    	'text',
    	'comment',
    	'status',
   	];

   	protected $dates = ['created_at', 'updated_at'];

   	public function getDates()
   	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}

	/**
	 * get testimonals appended to this cleaner
	 * @return [obj]
	 */
	public function testimonals()
	{
		return $this->hasMany('App\Models\Testimonals', 'cleaner_id');
	}

	/**
	 * get orders appended to this cleaner
	 * @return [obj]
	 */
	public function orders()
	{
		return $this->hasMany('App\Models\OrdersCleaners', 'cleaner_id');
	}

	/**
	 * get schedule appended to this cleaner
	 * @return [obj]
	 */
	public function schedule()
	{
		return $this->belongsTo('App\Models\CleanersSchedule', 'id', 'cleaner_id');
	}
}

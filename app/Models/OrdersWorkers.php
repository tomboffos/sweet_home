<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class OrdersWorkers extends Model
{
    protected $table = 'orders_workers';

	protected $fillable = [
		'discount_id',
		'points',
		'worker_id',
		'client_id',
		'guest',
		'profession_id',
		'text',
		'requirements',
		'schedule',
		'wage',
		'address',
		'total',
		'payment_type',
		'payment_status',
		'status',
		'type',
	];

	protected $dates = ['created_at', 'updated_at', 'cl_date', 'cl_time'];

	public function getDates()
	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}

    /**
     * get testimonals for this order
     * @return [obj]
     */
    public function discounts()
    {
        return $this->belongsTo('App\Models\Discounts', 'discount_id');
    }

    /**
     * get testimonals for this order
     * @return [obj]
     */
    public function professions()
    {
        return $this->belongsTo('App\Models\WorkersCategories', 'profession_id');
    }
}

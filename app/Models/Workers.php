<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Workers extends Model
{
    protected $table = 'workers';

    protected $fillable = [
    	'name',
    	'category_id',
    	'img',
    	'text',
    	'comment',
    	'status',
   	];

   	protected $dates = ['created_at', 'updated_at'];

   	public function getDates()
   	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}
}

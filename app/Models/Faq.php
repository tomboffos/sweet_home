<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Faq extends Model
{
    protected $table = 'faq';

	protected $fillable = [
		'title',
		'text',
		'category',
		'sort',
		'status',
		'slug',
	];

	protected $dates = ['created_at', 'updated_at'];

	public function getDates()
	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}
}

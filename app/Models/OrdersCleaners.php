<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class OrdersCleaners extends Model
{
    protected $table = 'orders_cleaners';

	protected $fillable = [
		'discount_id',
		'points',
		'cleaner_id',
		'client_id',
		'additional',
		'services',
		'type',
		'number_rooms',
		'number_bathrooms',
		'cicle',
		'cl_date',
		'cl_time',
		'elapsed',
		'comment',
		'total',
		'payment_type',
		'payment_status',
		'status',
		'number_window',
		'label',
	];

	protected $dates = ['created_at', 'updated_at', 'cl_date', 'cl_time', 'elapsed'];

	public function getDates()
	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getClDateAttribute($date)
	{
	  return new Date($date);
	}

	public function getClTimeAttribute($date)
	{
	  return new Date($date);
	}

	public function getElapsedAttribute($date)
	{
	  return new Date($date);
	}

	/**
     * get testimonals for this order
     * @return [obj]
     */
    public function testimonals()
    {
        return $this->belongsTo('App\Models\Testimonals', 'order_id');
    }

    /**
     * get testimonals for this order
     * @return [obj]
     */
    public function discounts()
    {
        return $this->belongsTo('App\Models\Discounts', 'discount_id');
    }
}

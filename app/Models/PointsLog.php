<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class PointsLog extends Model
{
    protected $table = 'points_log';

	protected $fillable = [
		'user_id',
		'reason',
		'amount',
	];

	protected $dates = ['created_at', 'updated_at'];

	public function getDates()
	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class UsersCards extends Model
{
	protected $table = 'users_cards';

	protected $fillable = [
        'user_id',
        'card_id',
        'approve',
        'card_mask',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getDates()
    {
      return ['updated_at'];
    }

    public function getCreatedAtAttribute($date)
    {
      return new Date($date);
    }

    public function getUpdatedAtAttribute($date)
    {
      return new Date($date);
    }

    public function getPublishedAtAttribute($date)
    {
      return new Date($date);
    }
}

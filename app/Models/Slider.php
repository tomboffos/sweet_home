<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Slider extends Model
{
	protected $table = 'slider';

	protected $fillable = [
		'title',
		'text',
		'url',
		'thumbnail',
		'status',
		'sort',
	];

	protected $dates = ['created_at', 'updated_at'];
}

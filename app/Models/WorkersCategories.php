<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class WorkersCategories extends Model
{
    protected $table = 'workers_categories';

    protected $fillable = [
    	'name',
    	'title',
    	'meta_title',
    	'meta_description',
    	'slug',
    	'text',
    	'status',
    	'menu',
      'footer',
      'sort',
    	'type',
   	];

   	protected $dates = ['created_at', 'updated_at'];

   	public function getDates()
   	{
	    return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	    return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	    return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	    return new Date($date);
	}

    public function getText()
    {
        $textColumn = 'text_' . App::getLocale();
        return $this->$textColumn;
    }
}
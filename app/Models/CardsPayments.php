<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class CardsPayments extends Model
{
    protected $table = 'cards_payments';

    protected $fillable = [
    	'abonent_id',
    	'card_id',
    	'OrderId',
    	'amount',
    	'ActionTime',
    	'message',
    	'Reference',
    	'int_reference',
    	'approval_code',
    	'bank_ses',
    	'Result',
   	];

   	protected $dates = ['created_at', 'updated_at'];

   	public function getDates()
   	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}
}
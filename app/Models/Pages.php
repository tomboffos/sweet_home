<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Pages extends Model
{
    protected $table = 'pages';

    protected $fillable = [
    	'title',
    	'text',
    	'slug',
    	'meta_title',
    	'meta_description',
    	'sort',
    	'status',
    	'menu',        'footer',
   	];

   	protected $dates = ['created_at', 'updated_at'];

   	public function getDates()
   	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}
}
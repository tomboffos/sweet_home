<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Testimonals extends Model
{
	protected $table = 'testimonals';

	protected $fillable = [
		'client_id',
		'cleaner_id',
		'order_id',
		'rating',
		'text',
		'sort',
		'status',
		'type',
	];

	protected $dates = ['created_at', 'updated_at'];

	public function getDates()
	{
	  return ['updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}

	/**
	 * get user appended to this testiomonal
	 * @return [obj]
	 */
	public function user()
	{
		return $this->belongsTo('App\User', 'client_id');
	}

	/**
	 * get order from Cleaners table appended to this testimonal
	 * @return [obj]
	 */
	public function orderCleaners()
	{
		return $this->belongsTo('App\Models\OrdersCleaners', 'order_id');
	}

	/**
	 * get order from Workers table appended to this testimonal
	 * @return [obj]
	 */
	public function orderWorkers()
	{
		return $this->belongsTo('App\Models\OrdersWorkers', 'order_id');
	}

	/**
	 * get cleaner appended to this testiomonal
	 * @return [obj]
	 */
	public function cleaner()
	{
		return $this->belongsTo('App\Models\Cleaners', 'cleaner_id');
	}
}

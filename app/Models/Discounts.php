<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Discounts extends Model
{
    protected $table = 'discounts';

    protected $fillable = [
    	'code',
    	'category',
    	'type',
    	'price',
    	'status',
    	'finish',
    	'used',
   	];

   	protected $dates = ['finish', 'created_at', 'updated_at'];

   	public function getDates()
   	{
	  return ['finish', 'updated_at'];
	}

	public function getCreatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getUpdatedAtAttribute($date)
	{
	  return new Date($date);
	}

	public function getPublishedAtAttribute($date)
	{
	  return new Date($date);
	}

	/**
	 * get cleaners orders appended to this discount code
	 * @return [obj]
	 */
	public function ordersCleaners()
	{
		return $this->hasMany('App\Models\OrdersCleaners', 'discount_id');
	}

    /**
     * get workers orders appended to this discount code
     * @return [obj]
     */
    public function ordersWorkers()
    {
        return $this->hasMany('App\Models\OrdersWorkers', 'discount_id');
    }
}

<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;



class CleanersSchedule extends Model

{

    protected $table = 'cleaners_schedule';



    protected $fillable = [

    	'cleaner_id',

    	'mon',

    	'tue',

    	'wed',

    	'thu',

    	'fri',

    	'sat',

    	'sun',

    	't_from',

        't_to',

    	't_to_finish',

   	];



    public $timestamps = false;

}

